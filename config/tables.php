<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 11/19/18
 * Time: 5:49 PM
 */

return [
    /**
     * User
     */
    'user'         => [
        // Only view name in Column
        'columns'  => [
            'Name',
            'Email',
            'Avatar',
            'Banned At',
        ],
        // Data Key Index
        'viewable' => [
            'name',
            'email',
            'avatar',
            'banned_at',
        ],
        // Actions for row
        'actions'  => [
            'info'   => [
                'route' => 'backend.user.show',
            ],
            'edit'   => [
                'route' => 'backend.user.edit',
            ],
            'delete' => [
                'route' => 'backend.user.destroy',
            ],
        ],
    ],

    /**
     * Category
     */
    'category'     => [
        // Only view name in Column
        'columns'  => [
            'Title',
            'Slug',
        ],
        // Data Key Index
        'viewable' => [
            'title',
            'slug',
        ],
        // Actions for row
        'actions'  => [
            'edit'   => [
                'route' => 'backend.category.edit',
            ],
            'delete' => [
                'route' => 'backend.category.destroy',
            ],
            'custom' => [
                'route'     => 'backend.sub-category.create',
                'parameter' => 'id',
                'target'    => '_self',
                'tooltip'   => 'Create Sub-Category',
                'lable'     => 'Add Sub-Category',
                'class'     => 'm-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--pill',
            ],
        ],
    ],

    /**
     * Country
     */
    'country'      => [
        // Only view name in Column
        'columns'  => [
            'Name',
            'Slug',
        ],
        // Data Key Index
        'viewable' => [
            'name',
            'slug',
        ],
        // Actions for row
        'actions'  => [
            'edit'   => [
                'route' => 'backend.country.edit',
            ],
            'delete' => [
                'route' => 'backend.country.destroy',
            ],
        ],
    ],

    /**
     * Director
     */
    'director'     => [
        // Only view name in Column
        'columns'  => [
            'Name',
            'Slug',
            'WebUrl',
            'Gender',
            'Image',
        ],
        // Data Key Index
        'viewable' => [
            'name',
            'slug',
            'web_url',
            'gender',
            'thumbnail',
        ],
        // Actions for row
        'actions'  => [
            'info'   => [
                'route' => 'backend.director.show',
            ],
            'edit'   => [
                'route' => 'backend.director.edit',
            ],
            'delete' => [
                'route' => 'backend.director.destroy',
            ],
        ],
    ],

    /**
     * Cast
     */
    'cast'         => [
        // Only view name in Column
        'columns'  => [
            'Name',
            'Slug',
            'WebUrl',
            'Gender',
            'Image',
        ],
        // Data Key Index
        'viewable' => [
            'name',
            'slug',
            'web_url',
            'gender',
            'thumbnail',
        ],
        // Actions for row
        'actions'  => [
            'info'   => [
                'route' => 'backend.cast.show',
            ],
            'edit'   => [
                'route' => 'backend.cast.edit',
            ],
            'delete' => [
                'route' => 'backend.cast.destroy',
            ],
        ],
    ],

    /**
     * Company
     */
    'company'      => [
        // Only view name in Column
        'columns'  => [
            'Name',
            'Slug',
            'WebUrl',
        ],
        // Data Key Index
        'viewable' => [
            'name',
            'slug',
            'web_url',
        ],
        // Actions for row
        'actions'  => [
            'info'   => [
                'route' => 'backend.company.show',
            ],
            'edit'   => [
                'route' => 'backend.company.edit',
            ],
            'delete' => [
                'route' => 'backend.company.destroy',
            ],
        ],
    ],

    /**
     * Banner
     */
    'banner'       => [
        // Only view name in Column
        'columns'  => [
            'Title',
            'Url',
            'Start Date',
            'End Date',
            'Published Date',
        ],
        // Data Key Index
        'viewable' => [
            'title',
            'url',
            'start_date',
            'end_date',
            'published_at',
        ],
        // Actions for row
        'actions'  => [
            'info'   => [
                'route' => 'backend.banner.show',
            ],
            'edit'   => [
                'route' => 'backend.banner.edit',
            ],
            'delete' => [
                'route' => 'backend.banner.destroy',
            ],
        ],
    ],

    /**
     * Rtmp Stream
     */
    'rtmp_stream'  => [
        // Only view name in Column
        'columns'  => [
            'Name',
            'Url',
            'Streamer',
        ],
        // Data Key Index
        'viewable' => [
            'name',
            'url',
            'streamer',
        ],
        // Actions for row
        'actions'  => [
            'edit'   => [
                'route' => 'backend.rtmp-stream.edit',
            ],
            'delete' => [
                'route' => 'backend.rtmp-stream.destroy',
            ],
        ],
    ],

    /**
     * Info
     */
    'info'         => [
        // Only view name in Column
        'columns'  => [
            'Livestream',
            'Image',
            'Privacy Policy',
            'Image',
            'T & C',
            'Image',
        ],
        // Data Key Index
        'viewable' => [
            'preview_livestream',
            'image_livestream_thumb',
            'preview_privacy_policy',
            'image_privacy_policy_thumb',
            'preview_terms_and_condition',
            'image_terms_and_condition_thumb',
        ],
        // Actions for row
        'actions'  => [
            'edit'   => [
                'route' => 'backend.info.edit',
            ],
            'delete' => [
                'route' => 'backend.info.destroy',
            ],
        ],
    ],

    /**
     * Notification
     */
    'notification' => [
        // Only view name in Column
        'columns'  => [
            'Title',
            'Recipient Type',
            'Published Date',
        ],
        // Data Key Index
        'viewable' => [
            'title',
            'recipient_type',
            'published_at',
        ],
        // Actions for row
        'actions'  => [
            'custom' => [
                'icon'      => 'la la-send la-2x',
                'route'     => 'backend.notification.published',
                'parameter' => 'id',
                'target'    => '_self',
                'tooltip'   => 'Published',
                'class'     => 'm-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill',
            ],
            'edit'   => [
                'route' => 'backend.notification.edit',
            ],
            'delete' => [
                'route' => 'backend.notification.destroy',
            ],
        ],
    ],
];
