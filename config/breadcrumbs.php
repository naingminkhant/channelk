<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 11/19/18
 * Time: 12:56 PM
 */

return [
    'home'                     => [
        'label'  => 'Home',
        'parent' => null,
        'route'  => 'dashboard',
    ],
    'user'                     => [
        'label'  => 'User',
        'parent' => 'home',
        'route'  => 'backend.user.index',
    ],
    'user:create'              => [
        'label'  => 'Create',
        'parent' => 'user',
        'route'  => 'backend.user.create',
    ],
    'user:edit'                => [
        'label'  => 'Edit',
        'parent' => 'user',
        'route'  => 'backend.user.edit',
    ],
    'user:info'                => [
        'label'  => 'Info',
        'parent' => 'user',
        'route'  => 'backend.user.show',
    ],
    'category'                 => [
        'label'  => 'Category',
        'parent' => 'home',
        'route'  => 'backend.category.index',
    ],
    'category:create'          => [
        'label'  => 'Create',
        'parent' => 'category',
        'route'  => 'backend.category.create',
    ],
    'category:edit'            => [
        'label'  => 'Edit',
        'parent' => 'category',
        'route'  => 'backend.category.edit',
    ],
    'country'                  => [
        'label'  => 'Country',
        'parent' => 'home',
        'route'  => 'backend.country.index',
    ],
    'country:create'           => [
        'label'  => 'Edit',
        'parent' => 'country',
        'route'  => 'backend.country.create',
    ],
    'country:edit'             => [
        'label'  => 'Edit',
        'parent' => 'country',
        'route'  => 'backend.country.edit',
    ],
    'director'                 => [
        'label'  => 'Director',
        'parent' => 'home',
        'route'  => 'backend.director.index',
    ],
    'director:create'          => [
        'label'  => 'Create',
        'parent' => 'director',
        'route'  => 'backend.director.create',
    ],
    'director:edit'            => [
        'label'  => 'Edit',
        'parent' => 'director',
        'route'  => 'backend.director.edit',
    ],
    'director:info'            => [
        'label'  => 'Info',
        'parent' => 'director',
        'route'  => 'backend.director.show',
    ],
    'cast'                     => [
        'label'  => 'Cast',
        'parent' => 'home',
        'route'  => 'backend.cast.index',
    ],
    'cast:create'              => [
        'label'  => 'Create',
        'parent' => 'cast',
        'route'  => 'backend.cast.create',
    ],
    'cast:edit'                => [
        'label'  => 'Edit',
        'parent' => 'cast',
        'route'  => 'backend.cast.edit',
    ],
    'cast:info'                => [
        'label'  => 'Info',
        'parent' => 'cast',
        'route'  => 'backend.cast.show',
    ],
    'company'                  => [
        'label'  => 'Company',
        'parent' => 'home',
        'route'  => 'backend.company.index',
    ],
    'company:create'           => [
        'label'  => 'Create',
        'parent' => 'company',
        'route'  => 'backend.company.create',
    ],
    'company:edit'             => [
        'label'  => 'Edit',
        'parent' => 'company',
        'route'  => 'backend.company.edit',
    ],
    'company:info'             => [
        'label'  => 'Info',
        'parent' => 'company',
        'route'  => 'backend.company.show',
    ],
    'video'                    => [
        'label'  => 'Video',
        'parent' => 'home',
        'route'  => 'backend.video.index',
    ],
    'video:create'             => [
        'label'  => 'Create',
        'parent' => 'video',
        'route'  => 'backend.video.create',
    ],
    'video:edit'               => [
        'label'  => 'Edit',
        'parent' => 'video',
        'route'  => 'backend.video.edit',
    ],
    'video:info'               => [
        'label'  => 'Info',
        'parent' => 'video',
        'route'  => 'backend.video.show',
    ],
    'series'                   => [
        'label'  => 'Playlist',
        'parent' => 'home',
        'route'  => 'backend.series.index',
    ],
    'series:create'            => [
        'label'  => 'Create',
        'parent' => 'series',
        'route'  => 'backend.series.create',
    ],
    'series:edit'              => [
        'label'  => 'Edit',
        'parent' => 'series',
        'route'  => 'backend.series.edit',
    ],
    'series:episode:create'    => [
        'label'  => 'Episode Create',
        'parent' => 'series',
        'route'  => 'backend.episode.create',
    ],
    'series:episode:edit'      => [
        'label'  => 'Episode Edit',
        'parent' => 'series',
        'route'  => 'backend.episode.edit',
    ],
    'series:episode:info'      => [
        'label'  => 'Episode Info',
        'parent' => 'series',
        'route'  => 'backend.episode.show',
    ],
    'banner'                   => [
        'label'  => 'Banner',
        'parent' => 'home',
        'route'  => 'backend.banner.index',
    ],
    'banner:create'            => [
        'label'  => 'Create',
        'parent' => 'banner',
        'route'  => 'backend.banner.create',
    ],
    'banner:edit'              => [
        'label'  => 'Edit',
        'parent' => 'banner',
        'route'  => 'backend.banner.edit',
    ],
    'banner:info'              => [
        'label'  => 'Info',
        'parent' => 'banner',
        'route'  => 'backend.banner.show',
    ],
    'channel-agenda'           => [
        'label'  => 'Channel Agenda',
        'parent' => 'home',
        'route'  => 'backend.channel-agenda.index',
    ],
    'channel-agenda:create'    => [
        'label'  => 'Create',
        'parent' => 'channel-agenda',
        'route'  => 'backend.channel-agenda.create',
    ],
    'channel-agenda:edit'      => [
        'label'  => 'Edit',
        'parent' => 'channel-agenda',
        'route'  => 'backend.channel-agenda.edit',
    ],
    'notification'             => [
        'label'  => 'Notification',
        'parent' => 'home',
        'route'  => 'backend.notification.index',
    ],
    'notification:create'      => [
        'label'  => 'Create',
        'parent' => 'notification',
        'route'  => 'backend.notification.create',
    ],
    'notification:edit'        => [
        'label'  => 'Edit',
        'parent' => 'notification',
        'route'  => 'backend.notification.edit',
    ],
    'role'                     => [
        'label'  => 'Role',
        'parent' => 'home',
        'route'  => 'backend.role.index',
    ],
    'role:create'              => [
        'label'  => 'Create',
        'parent' => 'role',
        'route'  => 'backend.role.create',
    ],
    'role:edit'                => [
        'label'  => 'Edit',
        'parent' => 'role',
        'route'  => 'backend.role.edit',
    ],
    'permission'               => [
        'label'  => 'Permission',
        'parent' => 'role',
        'route'  => 'backend.permission.index',
    ],
    'rtmp-stream'              => [
        'label'  => 'Rtmp Stream',
        'parent' => 'home',
        'route'  => 'backend.rtmp-stream.index',
    ],
    'rtmp-stream:create'       => [
        'label'  => 'Create',
        'parent' => 'rtmp-stream',
        'route'  => 'backend.rtmp-stream.create',
    ],
    'rtmp-stream:edit'         => [
        'label'  => 'Edit',
        'parent' => 'rtmp-stream',
        'route'  => 'backend.rtmp-stream.edit',
    ],
    'info'                     => [
        'label'  => 'Info',
        'parent' => 'home',
        'route'  => 'backend.info.index',
    ],
    'info:create'              => [
        'label'  => 'Create',
        'parent' => 'info',
        'route'  => 'backend.info.create',
    ],
    'info:edit'                => [
        'label'  => 'Edit',
        'parent' => 'info',
        'route'  => 'backend.info.edit',
    ],
    'cms:video-recommended'    => [
        'label'  => 'Individual Recommended CMS',
        'parent' => 'home',
        'route'  => 'backend.cms.video-recommended',
    ],
    'cms:playlist-recommended' => [
        'label'  => 'Playlist Recommended CMS',
        'parent' => 'home',
        'route'  => 'backend.cms.playlist-recommended',
    ],
    'cms:new-released-video'   => [
        'label'  => 'New Released Video CMS',
        'parent' => 'home',
        'route'  => 'backend.cms.new-released.video',
    ],
    'cms:playlist'             => [
        'label'  => 'Playlist CMS',
        'parent' => 'home',
        'route'  => 'backend.cms.playlist',
    ],
    'cms:cast'                 => [
        'label'  => 'Cast CMS',
        'parent' => 'home',
        'route'  => 'backend.cms.cast',
    ],
];
