<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 7/11/18
 * Time: 2:31 PM
 */

return [
    'home' => [
        'label'      => 'Dashboard',
        'icon'       => 'flaticon-line-graph',
        'route'      => 'dashboard',
        'visibility' => true,
    ],

    'user' => [
        'label'      => 'User Management',
        'icon'       => 'flaticon-users',
        'url'        => '#',
        'visibility' => true,
        'permission' => 'user-manage',
        'children'   => [
            [
                'label'      => 'Users',
                'route'      => 'backend.user.index',
                'visibility' => true,
                'permission' => 'user-manage',
            ],
        ],
    ],

    'backend:staff' => [
        'label'      => 'Backend Staff',
        'icon'       => 'flaticon-profile',
        'url'        => '#',
        'visibility' => true,
        'permission' => 'roles-management',
        'children'   => [
            [
                'label'      => 'Staff',
                'route'      => 'backend.staff.index',
                'visibility' => true,
                'permission' => 'roles-management',
            ],
            [
                'label'      => 'Role',
                'route'      => 'backend.role.index',
                'visibility' => true,
                'permission' => 'roles-management',
            ],
        ],
    ],

    'components' => [
        'label'      => 'Components',
        'icon'       => 'flaticon-squares',
        'url'        => '#',
        'visibility' => true,
        'permission' => 'component-show-menu',
        'children'   => [
            [
                'label'      => 'Category',
                'route'      => 'backend.category.index',
                'visibility' => true,
                'permission' => 'category-index',
            ],
            [
                'label'      => 'Country',
                'route'      => 'backend.country.index',
                'visibility' => true,
                'permission' => 'country-index',
            ],
            [
                'label'      => 'Director',
                'route'      => 'backend.director.index',
                'visibility' => true,
                'permission' => 'director-index',
            ],
            [
                'label'      => 'Cast',
                'route'      => 'backend.cast.index',
                'visibility' => true,
                'permission' => 'cast-index',
            ],
            [
                'label'      => 'Production Company',
                'route'      => 'backend.company.index',
                'visibility' => true,
                'permission' => 'production-company-index',
            ],
        ],
    ],

    'video' => [
        'label'      => 'Video Management',
        'icon'       => 'flaticon-folder-1',
        'url'        => '#',
        'visibility' => true,
        'permission' => 'video-manage-show-menu',
        'children'   => [
            [
                'label'      => 'Individual Video',
                'route'      => 'backend.video.index',
                'visibility' => true,
                'permission' => 'video-index',
            ],
            [
                'label'      => 'Playlist',
                'route'      => 'backend.series.index',
                'visibility' => true,
                'permission' => 'series-index',
            ],
        ],
    ],

    'cms' => [
        'label'      => 'CMS Management',
        'icon'       => 'flaticon-folder-1',
        'url'        => '#',
        'visibility' => true,
        'permission' => 'video-manage-show-menu',
        'children'   => [
            [
                'label'      => 'Individual Recommended',
                'route'      => 'backend.cms.video-recommended',
                'visibility' => true,
                'permission' => 'video-index',
            ],
            [
                'label'      => 'Playlist Recommended',
                'route'      => 'backend.cms.playlist-recommended',
                'visibility' => true,
                'permission' => 'series-index',
            ],
            [
                'label'      => 'New Released Video',
                'route'      => 'backend.cms.new-released.video',
                'visibility' => true,
                'permission' => 'video-index',
            ],
            [
                'label'      => 'Playlist',
                'route'      => 'backend.cms.playlist',
                'visibility' => true,
                'permission' => 'series-index',
            ],
            [
                'label'      => 'Cast',
                'route'      => 'backend.cms.cast',
                'visibility' => true,
                'permission' => 'cast-index',
            ],
        ],
    ],

    'banner' => [
        'label'      => 'Banner',
        'icon'       => 'flaticon-web',
        'route'      => 'backend.banner.index',
        'visibility' => true,
        'permission' => 'banner-index',
    ],

    'channel-agenda' => [
        'label'      => 'Channel Agenda',
        'icon'       => 'flaticon-clipboard',
        'route'      => 'backend.channel-agenda.index',
        'visibility' => true,
        'permission' => 'channel-agenda-index',
    ],

    'rtmp-stream' => [
        'label'      => 'Rtmp Stream',
        'icon'       => 'flaticon-imac',
        'route'      => 'backend.rtmp-stream.index',
        'visibility' => true,
        'permission' => 'rtmp-stream-index',
    ],

    'info' => [
        'label'      => 'Info',
        'icon'       => 'flaticon-information',
        'route'      => 'backend.info.index',
        'visibility' => true,
        'permission' => 'info-manage',
    ],

    'notification' => [
        'label'      => 'Notification',
        'icon'       => 'flaticon-bell',
        'route'      => 'backend.notification.index',
        'visibility' => true,
        'permission' => 'notification-index',
    ],

    'about-us' => [
        'label'      => 'About Us',
        'icon'       => 'flaticon-information',
        'route'      => 'backend.about-us.create',
        'visibility' => true,
        'permission' => 'info-manage',
    ],

    'contact-us' => [
        'label'      => 'Contact Us',
        'icon'       => 'flaticon-information',
        'route'      => 'backend.contact-us.create',
        'visibility' => true,
        'permission' => 'info-manage',
    ],

    'faq' => [
        'label'      => 'FAQ',
        'icon'       => 'flaticon-information',
        'route'      => 'backend.faq.create',
        'visibility' => true,
        'permission' => 'info-manage',
    ],

    'support' => [
        'label'      => 'Support',
        'icon'       => 'flaticon-information',
        'route'      => 'backend.support.create',
        'visibility' => true,
        'permission' => 'info-manage',
    ],
];
