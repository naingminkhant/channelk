<?php
/**
 * Created by channelk.
 * Author: naingminkhant
 * Date: 2019-01-09
 * Time: 00:20
 */

return [
    'user'               => [
        'manage' => 'Management',
    ],
    'backend-staff'      => [
        'show-menu' => 'Show Menu',
    ],
    'role'               => [
        'manage' => 'Management',
    ],
    'staff'              => [
        'index'  => 'Index',
        'create' => 'create',
        'edit'   => 'Edit',
        'delete' => 'Delete',
    ],
    'component'          => [
        'show-menu' => 'Show Menu',
    ],
    'category'           => [
        'index'  => 'Index',
        'create' => 'create',
        'edit'   => 'Edit',
        'delete' => 'Delete',
    ],
    'country'            => [
        'index'  => 'Index',
        'create' => 'create',
        'edit'   => 'Edit',
        'delete' => 'Delete',
    ],
    'director'           => [
        'index'  => 'Index',
        'create' => 'create',
        'edit'   => 'Edit',
        'delete' => 'Delete',
    ],
    'cast'               => [
        'index'  => 'Index',
        'create' => 'create',
        'edit'   => 'Edit',
        'delete' => 'Delete',
    ],
    'production-company' => [
        'index'  => 'Index',
        'create' => 'create',
        'edit'   => 'Edit',
        'delete' => 'Delete',
    ],
    'video-manage'       => [
        'show-menu' => 'Show Menu',
    ],
    'video'              => [
        'index'  => 'Index',
        'create' => 'create',
        'edit'   => 'Edit',
        'delete' => 'Delete',
        'show'   => 'Show',
    ],
    'series'             => [
        'index'  => 'Index',
        'create' => 'create',
        'edit'   => 'Edit',
        'delete' => 'Delete',
    ],
    'banner'             => [
        'index'  => 'Index',
        'create' => 'create',
        'edit'   => 'Edit',
        'delete' => 'Delete',
        'show'   => 'Show',
    ],
    'channel-agenda'     => [
        'index'  => 'Index',
        'create' => 'create',
        'edit'   => 'Edit',
        'delete' => 'Delete',
    ],
    'rtmp-stream'        => [
        'index'  => 'Index',
        'create' => 'create',
        'edit'   => 'Edit',
        'delete' => 'Delete',
    ],
    'info'               => [
        'manage' => 'Management',
    ],
    'notification'       => [
        'index'   => 'Index',
        'create'  => 'create',
        'edit'    => 'Edit',
        'delete'  => 'Delete',
        'publish' => 'Publish',
    ],
];
