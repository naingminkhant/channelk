<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 7/9/18
 * Time: 4:09 PM
 */

namespace Arga\PhoneBook;

use Arga\Storage\Database\Contracts\SerializableModel;
use Arga\Storage\Database\BaseModel;

/**
 * Class PhoneBook
 *
 * @package Arga\PhoneBook
 * @property \Doctrine\DBAL\Schema\Column phone_no
 * @property \Doctrine\DBAL\Schema\Column operator
 */
class PhoneBook extends BaseModel implements SerializableModel
{
    protected $table = 'phone_books';

    protected $fillable = [
        'phone_no',
        'operator',
        'model_type',
        'model_id',
    ];

    const MPT = 'mpt';
    const OOREDOO = 'ooredoo';
    const TELENOR = 'telenor';
    const MYTEL = 'mytel';
    const UNKNOWN = 'unknown';

    public function related_model()
    {
        return $this->morphTo();
    }

    public function getPhoneNo(): ?string
    {
        return $this->phone_no;
    }

    public function getOperator(): ?string
    {
        return $this->operator;
    }

    public function toOriginal(): array
    {
        return [
            'id'       => $this->getId(),
            'phone_no' => $this->getPhoneNo(),
            'operator' => $this->getOperator(),
        ];
    }

    public function toAll(): array
    {
        return [
            'id'       => $this->getId(),
            'phone_no' => $this->getPhoneNo(),
            'operator' => $this->getOperator(),
        ];
    }
}
