<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 7/9/18
 * Time: 4:09 PM
 */

namespace Arga\PhoneBook;

interface PhoneableModel
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function phones();

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function phone();
}