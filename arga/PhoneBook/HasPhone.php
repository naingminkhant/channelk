<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 7/9/18
 * Time: 4:09 PM
 */

namespace Arga\PhoneBook;

/**
 * Trait HasPhone
 *
 * @package Arga\PhoneBook
 * @property \Arga\PhoneBook\PhoneBook phone
 * @property \Arga\PhoneBook\PhoneBook[] phones
 */
trait HasPhone
{
    public function phone()
    {
        return $this->morphOne(PhoneBook::class, 'model');
    }

    public function phones()
    {
        return $this->morphMany(PhoneBook::class, 'model');
    }

    public function getPhone(): ?PhoneBook
    {
        return $this->phone;
    }

    public function getPhoneNumbers(): ?array
    {
        return $this->phones->pluck('phone_no')->toArray();
    }

    public function getPhonesDetail(): ?array
    {
        $phones = null;
        /**
         * @var \Arga\PhoneBook\PhoneBook $phone
         */
        foreach ($this->phones as $phone) {
            $phones [] = $phone->serialize();
        }

        return $phones;
    }
}