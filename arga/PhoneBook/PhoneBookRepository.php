<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 7/9/18
 * Time: 4:09 PM
 */

namespace Arga\PhoneBook;

use Arga\Storage\Database\BaseRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use League\Fractal\TransformerAbstract;

class PhoneBookRepository extends BaseRepository
{
    protected function model(): Builder
    {
        return PhoneBook::query();
    }

    /**
     * @param array $data
     * @param null $id
     * @return array|null
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateData(array $data, $id = null): ?array
    {
        /**
         * format for validate
         */
        $data = [
            'phones' => $data,
        ];

        $this->validate($data, [
            'phones'   => 'required',
            'phones.*' => 'numeric',
        ]);

        return $data['phones'];
    }

    public function __construct(TransformerAbstract $abstract = null)
    {
        $this->transformer = $abstract ?? new PhoneBookTransformer();
    }

    /**
     * @param \Arga\PhoneBook\PhoneableModel $model
     * @param array $data
     * @return array|\Traversable
     * @throws \Illuminate\Validation\ValidationException
     */
    public function attach(PhoneableModel $model, array $data)
    {
        $formatData = PhoneHelper::formatData($data);

        $data = $this->validateData($formatData);

        foreach ($data as $key => $item) {

            $checkOperator = PhoneHelper::checkOperator($item);

            if ($checkOperator === PhoneBook::UNKNOWN) {
                DB::rollBack();

                return msg_invalid_phone();
            }

            $saveData[$key] = new PhoneBook([
                'phone_no' => $item,
                'operator' => $checkOperator,
            ]);
        }

        $model->phones()->delete();

        /** @var array $saveData */
        return $model->phones()->saveMany($saveData);
    }

}
