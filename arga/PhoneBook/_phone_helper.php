<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 7/10/18
 * Time: 2:36 PM
 */

namespace Arga\PhoneBook {

    class PhoneHelper
    {
        public static function checkOperator($value = null): string
        {
            $char = substr($value, 1, 2);

            switch ($char) {
                case 12:
                case 13:
                case 14:
                case 18:
                case 19:
                case 22:
                case 24:
                case 42:
                case 43:
                case 46:
                case 52:
                case 25:
                case 26:
                case 40:
                case 44:
                case 45:
                    $operator = PhoneBook::MPT;
                    break;
                case 97:
                case 96:
                    $operator = PhoneBook::OOREDOO;
                    break;
                case 77:
                case 78:
                case 79:
                    $operator = PhoneBook::TELENOR;
                    break;
                case 69:
                    $operator = PhoneBook::MYTEL;
            }

            return $operator ?? PhoneBook::UNKNOWN;
        }

        public static function formatData($data): ?array
        {
            if (is_array($data)) {
                foreach ($data as $item) {
                    $output = str_replace(['0', '-'], '', $item);
                }
            } else {
                $output[] = str_replace(['0', '-'], '', $data);
            }

            /** @var array $output */
            return $output ?? [];
        }
    }
}