<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 11/16/18
 * Time: 1:08 PM
 */

namespace Arga;

use Arga\ApiKeyManager\Providers\ApiManagerServiceProvider;
use Arga\Auth\Commands\MakeAdmin;
use Arga\Auth\Commands\UpdateAdmin;
use Arga\Storage\Cloudinary\CloudinaryServiceProvider;
use Illuminate\Support\ServiceProvider;

class ArgaServiceProvider extends ServiceProvider
{
    protected $commands = [
        MakeAdmin::class,
        UpdateAdmin::class,
    ];

    public function boot()
    {
        $this->loadHelper();
    }

    public function register()
    {
        $this->commands($this->commands);

        $this->app->register(CloudinaryServiceProvider::class);
        $this->app->register(ApiManagerServiceProvider::class);
    }

    private function loadHelper()
    {
        require_once(__DIR__."/../arga/Helpers/helper.php");
    }
}
