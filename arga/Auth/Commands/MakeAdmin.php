<?php

namespace Arga\Auth\Commands;

use Arga\Auth\Admin;
use App\Models\Role;
use Illuminate\Console\Command;

class MakeAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:admin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create system Admin';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        $name = $this->ask('Who are you?');
        $email = $this->ask($name.'.., What is your email?');
        $password = $this->secret('SET your secret password');
        $confirm = $this->secret('Confirm your password');

        if ($password !== $confirm) {
            throw new \Exception("Password confirmation is invalid");
        }

        $admin = new Admin();
        $admin->name = $name;
        $admin->email = $email;
        $admin->password = bcrypt($password);
        $admin->save();

        $role = [
            'name' => 'Super Admin',
            'slug' => 'super-admin',
        ];
        $role = Role::firstOrCreate($role);
        $admin->roles()->sync($role->id);
        $this->line('Successfully! Created admin account.');
    }
}
