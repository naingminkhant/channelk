<?php

namespace Arga\Auth\Commands;

use Arga\Auth\Admin;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class UpdateAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:admin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update system admin';

    protected const CHANGE_EMAIL = 'Change Email';
    protected const CHANGE_PASSWORD = 'Change Password';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $choose = $this->choice('Choose ', [
            static::CHANGE_EMAIL,
            static::CHANGE_PASSWORD,
        ], null);

        switch ($choose) {
            case static::CHANGE_EMAIL:
                $this->email();
                break;
            case static::CHANGE_PASSWORD:
                $this->password();
                break;
            default:
                $this->line('Something is wrong!');
                break;
        }
    }

    protected function email()
    {
        $current = $this->getCurrentEmail();
        $new = $this->ask('Input New email');

        $this->update($current, [
            'email' => $new,
        ]);
    }

    protected function password()
    {
        $email = $this->getCurrentEmail();
        $new = $this->secret('Input new password');

        $this->update($email, [
            'password' => bcrypt($new),
        ]);
    }

    private function update($email, array $data)
    {
        if (!$this->checkCurrentEmail($email)) {
            $this->error('Current email is invalid!');

            return;
        }

        if (!$this->checkCurrentPassword($email)) {
            $this->error('Current password is invalid!');

            return;
        }

        $status = Admin::where('email', $email)->update($data);

        if ($status) {
            $this->info('Successfully Update');
        }
    }

    protected function getCurrentEmail(): string
    {
        $email = $this->ask('Input Current Email');

        return $email;
    }

    protected function checkCurrentEmail($email): bool
    {
        $admin = Admin::where('email', $email)->first();

        return (bool) $admin;
    }

    protected function checkCurrentPassword($email): bool
    {
        $admin = Admin::where('email', $email)->first();

        $password = $this->secret('Input Current Password');

        return Hash::check($password, $admin->getAuthPassword());
    }
}
