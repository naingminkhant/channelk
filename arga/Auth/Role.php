<?php

namespace Arga\Auth;

use Arga\Storage\Database\BaseModel;

/**
 * @property \Arga\Auth\User users
 * @property \Doctrine\DBAL\Schema\Column slug
 * @property \Doctrine\DBAL\Schema\Column display_name
 * @property \Doctrine\DBAL\Schema\Column description
 */
class Role extends BaseModel
{
    const ENTERPRISE = 'enterprise';
    const BASIC = 'basic';

    protected $table = 'roles';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany(User::class);
    }

    /**
     * @return \Arga\Auth\Role|null
     */
    public static function getBasic(): ?self
    {
        return (new Role)->where('slug', self::BASIC)->first();
    }

    /**
     * @return \Arga\Auth\Role|null
     */
    public static function getEnterprise(): ?self
    {
        return (new Role)->where('slug', self::ENTERPRISE)->first();
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getDisplayName(): ?string
    {
        return $this->display_name;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function serialize()
    {
        return [
            'id'          => $this->getId(),
            'slug'        => $this->getSlug(),
            'name'        => $this->getDisplayName(),
            'description' => $this->getDescription(),
        ];
    }
}
