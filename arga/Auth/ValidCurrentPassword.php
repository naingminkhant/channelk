<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 11/6/18
 * Time: 12:55 PM
 */

namespace Arga\Auth;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Hash;

class ValidCurrentPassword implements Rule
{
    public function __construct()
    {
    }

    public function passes($attribute, $value)
    {
        $user = auth()->user();
        if (Hash::check($value, $user->getAuthPassword())) {
            return true;
        }

        return false;
    }

    public function message()
    {
        return 'Current Password is Invalid';
    }
}
