<?php
/**
 * Created by channelk.
 * Author: naingminkhant
 * Date: 2019-01-09
 * Time: 15:44
 */

namespace Arga\Auth;

trait PermissionMiddlewareTrait
{
    protected $_middleware = [];

    protected function actionMiddelware(array $middlewares)
    {
        $this->_middleware = $middlewares;

        $route = request()->route();

        if($route === null) {
            return;
        }

        $method = $route->getActionMethod();

        $middleware = array_get($this->_middleware, $method);

        $this->middleware('permission:'.$middleware);
    }
}
