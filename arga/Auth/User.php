<?php

namespace Arga\Auth;

use App\Models\SearchKeyword;
use App\Models\Video;
use App\Service\Traits\HasComment;
use Arga\AddressBook\AddressableModel;
use Arga\AddressBook\HasAddress;
use Arga\Notification\Notification;
use Arga\Storage\Database\SubUUIDModel;
use Arga\PhoneBook\HasPhone;
use Arga\PhoneBook\PhoneableModel;
use Arga\Storage\Cloudinary\FileableModel;
use Arga\Storage\Cloudinary\HasFile;
use Arga\Storage\Database\Contracts\SerializableModel;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

/**
 * Class User
 *
 * @property \Arga\Auth\UserVerification verification
 * @property \Doctrine\DBAL\Schema\Column id
 * @property \Doctrine\DBAL\Schema\Column uuid
 * @property \Doctrine\DBAL\Schema\Column name
 * @property \Doctrine\DBAL\Schema\Column email
 * @property \Doctrine\DBAL\Schema\Column provider
 * @property \Doctrine\DBAL\Schema\Column provider_id
 * @property \Doctrine\DBAL\Schema\Column password
 * @property \Doctrine\DBAL\Schema\Column email_verified_at
 * @property \Doctrine\DBAL\Schema\Column banned_at
 * @property \Doctrine\DBAL\Schema\Column activation_code
 * @property \Illuminate\Database\Eloquent\Relations\HasMany search_keywords
 */
class User extends Authenticatable implements
    MustVerifyEmail,
    SerializableModel,
    AddressableModel,
    PhoneableModel,
    FileableModel
{
    use Notifiable;
    use HasApiTokens;
    use HasAddress;
    use HasPhone;
    use SubUUIDModel;
    use SoftDeletes;
    use HasFile;
    use HasComment;

    const PASSPORT_ACCESS_TOKEN_NAME = 'ChannelK';

    protected $table = 'users';

    protected $fillable = [
        'uuid',
        'name',
        'email',
        'provider',
        'provider_id',
        'password',
        'email_verified_at',
        'banned_at',
        'activation_code',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id',
        'password',
        'remember_token',
    ];

    public function related_model()
    {
        return $this->morphMany(Notification::class, 'model');
    }

    public function favorite_videos()
    {
        return $this->belongsToMany(Video::class, 'favorites', 'user_id', 'related_model_id')
            ->where('related_model_type', 'video')
            ->orderByDesc('favorites.created_at');
    }

    public function recent_videos()
    {
        return $this->belongsToMany(Video::class, 'played_recents', 'user_id', 'related_model_id')
            ->where('related_model_type', 'video')
            ->orderByDesc('played_recents.created_at');
    }

    public function search_keywords()
    {
        return $this->hasMany(SearchKeyword::class);
    }

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getUuid()
    {
        return $this->uuid;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getProvider()
    {
        return $this->provider;
    }

    public function getProviderId()
    {
        return $this->provider_id;
    }

    public function getEmailVerifiedAt()
    {
        return $this->email_verified_at;
    }

    public function getBannedAt()
    {
        return $this->banned_at;
    }

    /**
     * @param array|null $options
     * @return string
     * todo fix (cloudinary)
     */
    public function getAvatar(array $options = [])
    {
        $options = $options ?? [
                'width'  => 100,
                'height' => 100,
            ];

        $avatar = $this->getThumbnailUrl($options);

        return $avatar ?? url('img/avatar/no_available_image.png');
    }

    public function isBanned(): bool
    {
        return $this->banned_at ? true : false;
    }

    public function toOriginal(): array
    {
        return [
            'id'          => $this->getUuid(),
            'name'        => $this->getName(),
            'email'       => $this->getEmail(),
            'provider'    => $this->getProvider(),
            'provider_id' => $this->getProviderId(),
            'avatar'      => $this->getAvatar(),
            'banned_at'   => $this->getBannedAt(),
        ];
    }

    public function toAll(): array
    {
        return [
            'id'                => $this->getUuid(),
            'name'              => $this->getName(),
            'email'             => $this->getEmail(),
            'provider'          => $this->getProvider(),
            'provider_id'       => $this->getProviderId(),
            'email_verified_at' => $this->getEmailVerifiedAt(),
            'avatar'            => $this->getAvatar(),
            'banned_at'         => $this->getBannedAt(),
        ];
    }
}
