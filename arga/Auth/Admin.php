<?php

namespace Arga\Auth;

use App\Service\Traits\HasPermission;
use App\Service\Traits\HasRole;
use Arga\Storage\Database\UUIDModel;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class Admin
 *
 * @property \Doctrine\DBAL\Schema\Column id
 * @property \Doctrine\DBAL\Schema\Column name
 * @property \Doctrine\DBAL\Schema\Column email
 * @property \Doctrine\DBAL\Schema\Column password
 */
class Admin extends Authenticatable
{
    use Notifiable, UUIDModel, HasRole, HasPermission;

    public $incrementing = false;

    protected $guard = 'admin';

    protected $table = 'admins';

    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    protected $hidden = ['password', 'remember_token'];

    /**
     * @return bool
     * @todo fix
     */
    public function isAdmin()
    {
        return true;
    }
}
