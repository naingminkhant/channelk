<?php

namespace Arga\Auth;

use Arga\Storage\Database\BaseModel;

/**
 * @property \Doctrine\DBAL\Schema\Column activate
 * @property \Doctrine\DBAL\Schema\Column user_id
 * @property \Doctrine\DBAL\Schema\Column token
 * @property \Doctrine\DBAL\Schema\Column verification_code
 */
class UserVerification extends BaseModel
{
    protected $table = 'user_verifications';

    protected $fillable = [
        'user_id',
        'token',
        'verification_code',
        'activate',
        'verified_at',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * @return bool
     */
    public function getActivate(): bool
    {
        return (bool) $this->activate;
    }

    public function getUserId()
    {
        return $this->user_id;
    }

    public function getToken()
    {
        return $this->token;
    }

    public function getVerificationCode()
    {
        return $this->verification_code;
    }
}
