<?php

namespace Arga\Helpers;

use Arga\Storage\Cloudinary\CloudinaryClient;
use Arga\Storage\Cloudinary\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

trait CloudinaryHelper
{
    /**
     * @param \Symfony\Component\HttpFoundation\File\UploadedFile $file
     * @return \Arga\Storage\Cloudinary\File
     * @throws \Exception
     */
    public function uploadFile(UploadedFile $file): File
    {
        /** @var CloudinaryClient $fileUploader */
        $fileUploader = app(CloudinaryClient::class);
        $info = $fileUploader->upload($file);

        return $info;
    }

    public function destroyFile($id)
    {
        $client = app(CloudinaryClient::class);
        return $client->destroy($id);
    }
}
