<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 12/13/18
 * Time: 4:58 PM
 */

namespace Arga\Notification;

trait HasNotification
{
    public function notifications()
    {
        return $this->morphMany(Notification::class, 'model');
    }
}
