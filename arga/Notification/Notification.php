<?php

namespace Arga\Notification;

use App\Models\ChannelNotification;
use Arga\Storage\Database\BaseModel;
use Arga\Storage\Database\Contracts\SerializableModel;

/**
 * Class Notification
 *
 * @property \Doctrine\DBAL\Schema\Column model_type
 * @property \Doctrine\DBAL\Schema\Column model_id
 * @property \Doctrine\DBAL\Schema\Column channel_notification_id
 * @property \Doctrine\DBAL\Schema\Column read_at
 */
class Notification extends BaseModel implements SerializableModel
{
    protected $table = 'notifications';

    protected $fillable = [
        'model_type',
        'model_id',
        'channel_notification_id',
        'read_at',
    ];

    public function related_model()
    {
        return $this->morphTo();
    }

    public function channelNotification() {
        return $this->belongsTo(ChannelNotification::class);
    }

    public function toOriginal(): array
    {
        return [
            'id'                      => $this->getId(),
            'model_type'              => $this->model_type,
            'model_id'                => $this->model_id,
            'channel_notification_id' => $this->channel_notification_id,
            'read_at'                 => $this->read_at,
        ];
    }

    public function toAll(): array
    {
        return [
            'id'                      => $this->getId(),
            'model_type'              => $this->model_type,
            'model_id'                => $this->model_id,
            'channel_notification_id' => $this->channel_notification_id,
            'read_at'                 => $this->read_at,
        ];
    }
}
