<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 7/11/18
 * Time: 2:44 PM
 */

namespace Arga\Sidebar;

use Illuminate\Support\Facades\Route;

class Menu
{
    protected $children;

    protected $parent = null;

    protected $url;

    protected $label;

    protected $icon;

    protected $route;

    protected $visibility = false;

    public static $currentUrl;

    protected $active = false;

    protected $config;

    protected $hasPermission = true;

    public function __construct(array $config, Menu $parent = null)
    {
        if (self::$currentUrl === null) {
            throw new \Exception('Sidebar data must have config.');
        }

        $this->label = array_get($config, 'label');
        $this->icon = array_get($config, 'icon');
        $this->visibility = array_get($config, 'visibility');
        $this->route = array_get($config, 'route');
        $this->parent = $parent;

        if (isset($config['url'])) {
            $this->url = $config['url'];
        } else {
            if ($this->visibility) {
                $this->url = route($config['route']);
            } else {
                $this->route = $config['route'];
            }
        }

        $this->children = collect();

        $this->config = $config;

        if (isset($config['children'])) {
            $this->addChildren($config['children']);
        }

        $this->checkActive();
        $this->checkPermissions();
    }

    public function addChildren(array $children)
    {
        foreach ($children as $child) {
            $this->addChild(new static($child, $this));
        }
    }

    public function addChild(Menu $menu)
    {
        $this->children->push($menu);
    }

    protected function checkActive()
    {
        if ($this->url === '#') {
            return;
        }

        if ($this->hasChildren()) {
            return;
        }

        $active = Active::check($this->url, self::$currentUrl);
        $this->active = $active;

        if ($active && $this->hasParent()) {
            $this->parent->active = true;
        }
    }

    public function hasChildren(): bool
    {
        return $this->children->count() > 0;
    }

    protected function hasParent(): bool
    {
        return $this->parent != null;
    }

    public function isActive($active = 'm-menu__item--active')
    {
        if ($active && $this->active) {
            return $active;
        }

        return $this->active;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function getIcon()
    {
        return $this->icon;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function getVisibility(): bool
    {
        return $this->visibility ?? false;
    }

    public function getChildren()
    {
        return $this->children ?? [];
    }

    public function hasPermission(): bool
    {
        return $this->hasPermission;
    }

    public function getPermission()
    {
        return array_get($this->config, 'permission');
    }

    protected function checkPermissions()
    {
        $permission = $this->getPermission();
        if (!$permission) {
            return;
        }
        if (!auth('admin')->user()->hasPermissionTo($permission)) {
            $this->hasPermission = false;

            return;
        }
    }
}
