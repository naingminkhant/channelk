<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 7/12/18
 * Time: 3:29 PM
 */

namespace Arga\Sidebar;

use Spatie\Url\Url as UrlParser;

class Active
{
    public static function check($url, $currentUrl = null): bool
    {
        $currentUrl = $currentUrl ?? request()->fullUrl();
        $currentUrl = trim($currentUrl, '/#?');
        $url = trim($url, '/#?');

        // for create route
        $search = [
            '/create',
            '/upload',
        ];

        // for edit route
        if (strrpos($currentUrl, '/edit') !== false) {
            $removeStr = explode('/', $currentUrl);
            $secondStr = $removeStr[count($removeStr) - 2];
            $lastStr = $removeStr[count($removeStr) - 1];
            $removeStr = '/'.$secondStr.'/'.$lastStr;
            array_push($search, $removeStr);
        }

        // for info route
        $route = request()->route();
        $parameterName = implode('', $route->parameterNames());
        $parameter = $route->parameter($parameterName);
        if ($parameter) {
            $removeStr = '/'.$parameter;
            array_push($search, $removeStr);
        }

        $preCurrentUrl = str_replace($search, "", $currentUrl);
        $preUrl = str_replace($search, "", $url);

        if ($preCurrentUrl === $preUrl) {
            return true;
        }

        $currentUrl = UrlParser::fromString($currentUrl);
        $url = UrlParser::fromString($url);

        if ($currentUrl->getPath() !== $url->getPath() || $currentUrl->getHost() !== $url->getHost()) {
            return false;
        }

        if ($currentUrl->getQuery() === $url->getQuery()) {
            return true;
        }

        $urlQueries = $url->getAllQueryParameters();

        foreach ($urlQueries as $key => $value) {
            if ($value !== $currentUrl->getQueryParameter($key)) {
                return false;
            }
        }

        return true;
    }
}
