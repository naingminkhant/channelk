<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 7/5/18
 * Time: 12:45 PM
 */

namespace Arga\Transformer;

use League\Fractal\TransformerAbstract;

class BaseTransformer extends TransformerAbstract
{
    private $filters;

    public function __construct($filters = null)
    {
        $this->filters = $filters;
    }

    protected function filterField(array $data): array
    {
        if (!empty($this->filters)) {
            $fields = explode(',', $this->filters);
            foreach ($fields as $field) {
                $output[$field] = array_key_exists($field, $data) ? $data[$field] : 'No Exist Column.';
            }
            /** @var $output */
            $data = $output;
        }

        return $data;
    }
}