<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 12/3/18
 * Time: 12:45 PM
 */

namespace Arga\ApiKeyManager\Providers;

use Arga\ApiKeyManager\Console\Command\ActivateApiKey;
use Arga\ApiKeyManager\Console\Command\DeactivateApiKey;
use Arga\ApiKeyManager\Console\Command\GetAllApiKey;
use Arga\ApiKeyManager\Console\Command\MakeApiKey;
use Arga\ApiKeyManager\Http\Middleware\AuthenticateApiKey;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class ApiManagerServiceProvider extends ServiceProvider
{
    /**
     * @param \Illuminate\Routing\Router $router
     * @return void
     */
    public function boot(Router $router)
    {
        $this->registerMiddleware($router);
        $this->registerMigration(__DIR__.'/../Database/migrations');
    }

    /**
     * @param \Illuminate\Routing\Router $router
     */
    protected function registerMiddleware(Router $router)
    {
        $router->aliasMiddleware('apikey', AuthenticateApiKey::class);
    }

    protected function registerMigration($directory)
    {
        $this->publishes([
            $directory => database_path('migrations'),
        ], 'migrations');
    }

    public function register()
    {
        $this->commands([
            MakeApiKey::class,
            ActivateApiKey::class,
            DeactivateApiKey::class,
            GetAllApiKey::class,
        ]);
    }
}
