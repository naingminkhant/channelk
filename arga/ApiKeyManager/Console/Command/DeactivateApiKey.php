<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 12/3/18
 * Time: 1:41 PM
 */

namespace Arga\ApiKeyManager\Console\Command;

use Arga\ApiKeyManager\Http\Models\ApiKey;
use Illuminate\Console\Command;

class DeactivateApiKey extends Command
{
    /**
     * Error messages
     */
    const MESSAGE_ERROR_INVALID_NAME = 'Invalid name.';
    const MESSAGE_ERROR_NAME_DOES_NOT_EXIST = 'Name does not exist.';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'apikey:deactivate {key_name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deactivate api key by Key Name';

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        $name = $this->argument('key_name');
        if ($error = $this->validateName($name)) {
            $this->error($error);

            return;
        }
        $model = ApiKey::where('name', $name)->first();

        if ($model instanceof ApiKey && !$model->active) {
            $this->info($name." already Deactivate");

            return;
        }
        $model->active = false;
        $model->save();

        $this->info("Deactivate Key : ".$name);
    }

    protected function validateName($name)
    {
        if (!ApiKey::isValidName($name)) {
            return self::MESSAGE_ERROR_INVALID_NAME;
        }
        if (!ApiKey::nameExists($name)) {
            return self::MESSAGE_ERROR_NAME_DOES_NOT_EXIST;
        }

        return null;
    }

}
