<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 12/3/18
 * Time: 2:15 PM
 */

namespace Arga\ApiKeyManager\Console\Command;

use Arga\ApiKeyManager\Http\Models\ApiKey;
use Illuminate\Console\Command;

class GetAllApiKey extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'apikey:get';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate a new API key';

    public function handle()
    {
        $columns = [
            'name',
            'key',
            'active',
            'created_at',
            'deleted_at',
        ];

        $model = ApiKey::withTrashed()->get($columns);

        $this->table($columns, $model);
    }
}
