<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 12/3/18
 * Time: 1:22 PM
 */

namespace Arga\ApiKeyManager\Console\Command;

use Arga\ApiKeyManager\Http\Models\ApiKey;
use Illuminate\Console\Command;

class MakeApiKey extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'apikey:make {key_name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate a new API key';

    /**
     * Error messages
     */
    const MESSAGE_ERROR_INVALID_NAME_FORMAT = 'Invalid name.  Must be a lowercase alphabetic characters and hyphens less than 255 characters long.';
    const MESSAGE_ERROR_NAME_ALREADY_USED = 'Name is unavailable.';

    public function handle()
    {
        $name = $this->argument('key_name');

        if ($error = $this->validateName($name)) {
            $this->error($error);

            return;
        }

        $model = new ApiKey();
        $model->name = $name;
        $model->key = ApiKey::generate();
        $model->save();

        $this->info('API key created');
        $this->info('Name: '.$model->name);
        $this->info('Key: '.$model->key);
    }

    protected function validateName($name)
    {
        if (!ApiKey::isValidName($name)) {
            return self::MESSAGE_ERROR_INVALID_NAME_FORMAT;
        }
        if (ApiKey::nameExists($name)) {
            return self::MESSAGE_ERROR_NAME_ALREADY_USED;
        }

        return null;
    }
}
