<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 12/3/18
 * Time: 1:00 PM
 */

namespace Arga\ApiKeyManager\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ApiKey
 *
 * @property \Doctrine\DBAL\Schema\Column name
 * @property \Doctrine\DBAL\Schema\Column key
 * @property \Doctrine\DBAL\Schema\Column active
 */
class ApiKey extends Model
{
    use SoftDeletes;

    protected $table = 'api_keys';

    protected $fillable = [
        'name',
        'key',
        'active',
    ];

    protected static $nameRegex = '/^[a-z-]{1,255}$/';

    public static function generate()
    {
        do {
            $key = str_random(64);
        } while (self::keyExists($key));

        return $key;
    }

    public static function keyExists($key)
    {
        return self::where('key', $key)->withTrashed()->first();
    }

    public static function getByActiveKey($key)
    {
        return self::where([
            'key'    => $key,
            'active' => true,
        ])->first();
    }

    public static function isValidName($name): bool
    {
        return (bool) preg_match(self::$nameRegex, $name);
    }

    public static function nameExists($name)
    {
        return self::where('name', $name)->first();
    }
}
