<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 12/3/18
 * Time: 12:55 PM
 */

namespace Arga\ApiKeyManager\Http\Middleware;

use Arga\ApiKeyManager\Http\Models\ApiKey;
use Closure;
use Illuminate\Http\Request;

class AuthenticateApiKey
{
    const AUTH_HEADER = 'api-key';

    public function handle(Request $request, Closure $next)
    {
        $header = $request->header(self::AUTH_HEADER);
        $apikey = ApiKey::getByActiveKey($header);

        if ($apikey instanceof ApiKey) {
            return $next($request);
        }

        if (env('API_ENV') === 'test' && config('api.'.$header)) {
            return $next($request);
        }

        return response([
            'error' => 'Unauthorized',
        ], 401);
    }
}
