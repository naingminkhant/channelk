<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 7/9/18
 * Time: 1:19 PM
 */

namespace Arga\AddressBook;

use Arga\Transformer\BaseTransformer;

class AddressBookTransformer extends BaseTransformer
{
    private $detail;

    public function __construct($filters = null, $detail = false)
    {
        parent::__construct($filters);
        $this->detail = $detail;
    }

    public function transform(AddressBook $book)
    {
        if ($this->detail) {
            $data = $book->toAll();
        } else {
            $data = $book->toOriginal();
        }

        return $this->filterField($data);
    }

}
