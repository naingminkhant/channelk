<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 7/5/18
 * Time: 4:57 PM
 */

namespace Arga\AddressBook;

use Arga\Storage\Database\Contracts\SerializableModel;
use Arga\Storage\Database\BaseModel;

/**
 * @property \Doctrine\DBAL\Schema\Column quarter
 * @property \Doctrine\DBAL\Schema\Column street
 * @property \Doctrine\DBAL\Schema\Column house_no
 * @property \Doctrine\DBAL\Schema\Column lane_mark
 * @property \Doctrine\DBAL\Schema\Column latitude
 * @property \Doctrine\DBAL\Schema\Column longitude
 */
class AddressBook extends BaseModel implements SerializableModel
{
    protected $table = 'address_books';

    protected $fillable = [
        'quarter',
        'street',
        'house_no',
        'lane_mark',
        'latitude',
        'longitude',
        'model_type',
        'model_id',
    ];

    public function related_model()
    {
        return $this->morphTo();
    }

    public function getRelatedModel(): ?AddressableModel
    {
        return $this->getRelationValue('model');
    }

    public function getQuarter()
    {
        return $this->quarter;
    }

    public function getStreet()
    {
        return $this->street;
    }

    public function getHouseNo()
    {
        return $this->house_no;
    }

    public function getLaneMark()
    {
        return $this->lane_mark;
    }

    public function getLatitude(): ?float
    {
        return (float) $this->latitude;
    }

    public function getLongitude(): ?float
    {
        return (float) $this->longitude;
    }

    public function toOriginal(): array
    {
        return [
            'id'        => $this->getId(),
            'quarter'   => $this->getQuarter(),
            'street'    => $this->getStreet(),
            'house_no'  => $this->getHouseNo(),
            'lane_mark' => $this->getLaneMark(),
            'latitude'  => $this->getLatitude(),
            'longitude' => $this->getLongitude(),
        ];
    }

    public function toAll(): array
    {
        $output = [
            'id'        => $this->getId(),
            'quarter'   => $this->getQuarter(),
            'street'    => $this->getStreet(),
            'house_no'  => $this->getHouseNo(),
            'lane_mark' => $this->getLaneMark(),
            'latitude'  => $this->getLatitude(),
            'longitude' => $this->getLongitude(),
        ];

        return $output;
    }
}
