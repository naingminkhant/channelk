<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 7/5/18
 * Time: 5:28 PM
 */

namespace Arga\AddressBook;

interface AddressableModel
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function address();
}