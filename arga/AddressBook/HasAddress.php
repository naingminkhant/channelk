<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 7/5/18
 * Time: 5:03 PM
 */

namespace Arga\AddressBook;

/**
 * @property \Arga\AddressBook\AddressBook address
 */
trait HasAddress
{
    public function address()
    {
        return $this->morphOne(AddressBook::class, 'model');
    }

    public function getAddress(): array
    {
        $address = [];
        if ($this->address) {
            $address = $this->address->toAll();
        }

        return $address;
    }
}
