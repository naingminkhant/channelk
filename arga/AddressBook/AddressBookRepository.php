<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 7/5/18
 * Time: 5:19 PM
 */

namespace Arga\AddressBook;

use Arga\Storage\Database\BaseRepository;
use Illuminate\Database\Eloquent\Builder;
use League\Fractal\TransformerAbstract;

class AddressBookRepository extends BaseRepository
{
    public function __construct(TransformerAbstract $abstract = null)
    {
        $this->transformer = $abstract ?? new AddressBookTransformer();
    }

    /**
     * @param array $data
     * @param null $id
     * @return array|null
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateData(array $data, $id = null): ?array
    {
        $this->validate($data, [
            'quarter'   => 'required|string|max:255',
            'street'    => 'required|string|max:255',
            'house_no'  => 'required|string|max:255',
            'lane_mark' => 'required|string|max:255',
            'latitude'  => 'nullable|numeric',
            'longitude' => 'nullable|numeric',
        ]);

        return $data;
    }

    protected function attach(AddressableModel $model, array $data)
    {
        $output = $model->address()->save(new AddressBook($data));

        return $output;
    }

    public function createOrUpdate(AddressableModel $model, array $data)
    {
        $data = AddressHelper::formatData($data);

        $data = $this->validateData($data);

        $output = $model->address()->first();

        if ($output) {
            $output->update($data);
        } else {
            $output = $this->attach($model, $data);
        }

        return $this->item($output);
    }

    /**
     * @return Builder
     */
    protected function model(): Builder
    {
        return AddressBook::query();
    }
}
