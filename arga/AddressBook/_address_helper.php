<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 7/10/18
 * Time: 2:52 PM
 */

namespace Arga\AddressBook {

    class AddressHelper
    {
        public static function formatData(array $data): array
        {
            $output = [
                'quarter'   => array_get($data, 'quarter'),
                'street'    => array_get($data, 'street'),
                'house_no'  => array_get($data, 'house_no'),
                'lane_mark' => array_get($data, 'lane_mark'),
                'latitude'  => array_get($data, 'latitude'),
                'longitude' => array_get($data, 'longitude'),
            ];

            return $output;
        }
    }
}
