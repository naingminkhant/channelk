<?php

namespace Arga\Storage\Database;

use Arga\Auth\Admin;
use Illuminate\Database\Eloquent\Model;
use League\Fractal\TransformerAbstract;

abstract class BaseModel extends Model
{
    public $incrementing = false;

    use UUIDModel;

    public function getId()
    {
        return $this->getOriginal('id');
    }

    public function transform($data, TransformerAbstract $transformer)
    {
        return fractal($data, $transformer)->toArray();
    }

    public function collection($data, TransformerAbstract $transformer)
    {
        $data = $this->transform($data, $transformer);

        $paginate = array_get($data, 'meta.pagination');

        if ($paginate) {
            $data['meta']['pagination']['start_index'] =
                (($paginate['current_page'] - 1) * $paginate['per_page']) + 1;

            return $data;
        }

        return $data['data'];
    }

    public function isAdmin(): bool
    {
        if (auth('admin')->user() instanceof Admin) {
            return true;
        }

        return false;
    }
}
