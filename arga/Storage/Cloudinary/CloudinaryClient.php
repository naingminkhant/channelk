<?php

namespace Arga\Storage\Cloudinary;

use Cloudinary\Uploader;
use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @method configure(array $array)
 */
class CloudinaryClient
{
    protected $cloud_name;

    protected $api_key;

    protected $api_secret;

    protected $path;

    const CLOUDINARY = 'cloudinary';

    public function __construct($config)
    {
        $this->cloud_name = $config['cloud_name'];
        $this->api_key = $config['api_key'];
        $this->api_secret = $config['api_secret'];
        $this->path = env('IMAGE_PATH');

        $this->configuration($config);
    }

    /**
     * @param string $preset
     */
    public function setUploadPreset($preset)
    {
        $this->configure(['upload_preset' => $preset]);
    }

    protected function configuration($config)
    {
        \Cloudinary::config($config);
    }

    public function uploadAndAttachFromUrl(Model $model, $fileUrl)
    {
        $saveFile = $this->uploadFromUrl($fileUrl);
        $saveFile->related_model()->associate($model);
        $saveFile->save();

        return $saveFile->fresh();
    }

    public function uploadFromUrl($fileUrl, $folder = 'images/', $options = [])
    {
        if (!$fileUrl) {
            return null;
        }

        $cloudInfo = $this->saveToCloudFromUrl($fileUrl, $folder);

        $model = File::create([
            'name'          => str_random(10),
            'cloud_service' => self::CLOUDINARY,
            'path'          => $cloudInfo['public_id'],
        ]);

        return $model;
    }

    /**
     * @param \Illuminate\Database\Eloquent\Model $model
     * @param \Symfony\Component\HttpFoundation\File\UploadedFile $file
     * @return \Arga\Storage\Cloudinary\File|null
     * @throws \Exception
     */
    public function uploadAndAttach(Model $model, UploadedFile $file)
    {
        $saveFile = $this->upload($file);
        $saveFile->related_model()->associate($model);
        $saveFile->save();

        return $saveFile->fresh();
    }

    /**
     * @param \Symfony\Component\HttpFoundation\File\UploadedFile $file
     * @param string $folder
     * @param array $options
     * @return \Arga\Storage\Cloudinary\File
     * @throws \Exception
     */
    public function upload(UploadedFile $file, $folder = 'images/', $options = [])
    {
        $extension = $file->guessClientExtension();
        if (!$extension) {
            throw new \Exception("Unsupported File.");
        }

        $cloudInfo = $this->saveToCloud($file, $folder);

        $model = File::create([
            'name'          => $file->getClientOriginalName(),
            'cloud_service' => self::CLOUDINARY,
            'path'          => $cloudInfo['public_id'],
        ]);

        return $model;
    }

    public function saveToCloud(UploadedFile $file, $folder)
    {
        return Uploader::upload($file->getRealPath(), [
            'folder' => $folder,
        ]);
    }

    public function saveToCloudFromUrl($file, $folder)
    {
        return Uploader::upload($file, [
            'folder' => $folder,
        ]);
    }

    /**
     * @param $id (id accept array or string)
     * @param \Illuminate\Database\Eloquent\Model $model
     */
    public function update($id, Model $model)
    {
        if (is_array($id)) {
            foreach ($id as $value) {
                $file = File::find($value);
                if ($file instanceof File) {
                    $file->related_model()->associate($model);
                    $file->update();
                }
            }

            return;
        }

        $file = File::find($id);
        if ($file instanceof File) {
            $file->related_model()->associate($model);
            $file->update();
        }
    }

    public function updateCoverImage($id, Model $model)
    {
        if (is_array($id)) {
            foreach ($id as $value) {
                $file = File::find($value);
                if ($file instanceof File) {
                    $file->related_model()->associate($model);
                    $file->update([
                        'is_cover' => true,
                    ]);
                }
            }

            return;
        }
    }

    /**
     * @param $id
     * @return boolean
     */
    public function destroy($id)
    {
        $file = File::find($id)->delete();

        return $file;
    }
}
