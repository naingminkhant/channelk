<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 7/5/18
 * Time: 3:11 PM
 */

namespace Arga\Storage\Cloudinary;

/**
 * Interface FileableModel
 *
 * @package Arga\Storage\Cloudinary
 * @property \Illuminate\Database\Eloquent\Relations\MorphOne file
 */
interface FileableModel
{
    public function file();
}
