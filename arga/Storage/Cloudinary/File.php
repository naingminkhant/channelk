<?php

namespace Arga\Storage\Cloudinary;

use Arga\Storage\Database\Contracts\SerializableModel;
use Arga\Storage\Database\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Intervention\Image\Facades\Image;

/**
 * @property \Doctrine\DBAL\Schema\Column path
 * @property \Doctrine\DBAL\Schema\Column cloud_service
 * @property \Doctrine\DBAL\Schema\Column is_cover
 */
class File extends BaseModel implements FileDisplayable, SerializableModel
{
    use SoftDeletes;

    protected $table = 'images';

    protected $fillable = [
        'name',
        'cloud_service',
        'path',
        'is_cover',
    ];

    const LOCAL = 'local';
    const SOCIAL = 'social';
    const CLOUDINARY = 'cloudinary';

    public function related_model()
    {
        return $this->morphTo();
    }

    public function getRelatedModel(): ?FileableModel
    {
        return $this->getRelationValue('model');
    }

    protected function getUrl($options = [])
    {
        if ($this->cloud_service === self::LOCAL) {
            return url('storage/'.(string)$this->path);
        }

        if($this->cloud_service === 'social')
        {
            return $this->path;
        }

        if ($this->cloud_service === self::CLOUDINARY) {
            return cloudinary_url($this->path, $options);
        }

        return null;
    }

    /**
     * @param array $options
     * @return mixed|null|string|string[]
     */
    protected function getThumbnail($options = [])
    {
        if ($options) {
            return $this->getUrl($options);
        }

        return $this->getUrl([
            'height'  => 300,
            'quality' => 50,
            'crop'    => 'thumb',
        ]);
    }

    public function getCloudService()
    {
        return $this->cloud_service;
    }

    public function getFileUrl(): string
    {
        return $this->getUrl();
    }

    public function getThumbnailUrl($options = []): string
    {
        return $this->getThumbnail($options);
    }

    public function toOriginal(): array
    {
        return [
            'id'            => $this->getId(),
            'cloud_service' => $this->getCloudService(),
            'path'          => $this->getFileUrl(),
            'thumbnail'     => $this->getThumbnailUrl(),
        ];
    }

    public function toAll(): array
    {
        return [
            'id'            => $this->getId(),
            'cloud_service' => $this->getCloudService(),
            'path'          => $this->getFileUrl(),
            'thumbnail'     => $this->getThumbnailUrl(),
        ];
    }
}
