<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 7/5/18
 * Time: 3:14 PM
 */

namespace Arga\Storage\Cloudinary;

/**
 * @property \Arga\Storage\Cloudinary\File latest_file
 * @property \Arga\Storage\Cloudinary\File file
 * @property \Arga\Storage\Cloudinary\File cover_image
 * @property \Illuminate\Database\Eloquent\Relations\MorphMany files
 */
trait HasFile
{
    /**
     * @return mixed
     */
    public function latest_file()
    {
        return $this->morphOne(File::class, 'related_model')
            ->where('is_cover', false)
            ->latest();
    }

    /**
     * @return mixed
     */
    public function file()
    {
        return $this->morphOne(File::class, 'related_model')
            ->where('is_cover', false);
    }

    public function cover_image()
    {
        return $this->morphOne(File::class, 'related_model')
            ->where('is_cover', true);
    }

    /**
     * @return mixed
     */
    public function files()
    {
        return $this->morphMany(File::class, 'related_model')
            ->where('is_cover', false);
    }

    /**
     * @param array $options
     * @return string|null
     */
    public function getThumbnailUrl($options = []): ?string
    {
        if ($this->file) {
            return $this->file->getThumbnailUrl($options);
        }

        return null;
    }

    public function getCoverThumbnailUrl(): ?string
    {
        $options = [
            'height'  => 100,
            'quality' => 50,
            'crop'    => 'thumb',
        ];

        if ($this->cover_image) {
            return $this->cover_image->getThumbnailUrl($options);
        }

        return null;
    }

    /**
     * @param array $options
     * @return array
     */
    public function getThumbnailUrls($options = []): array
    {
        $output = [];
        foreach ($this->files as $file) {
            if ($file instanceof File) {
                array_push($output, $file->getThumbnailUrl($options));
            }
        }

        return $output;
    }

    /**
     * @return string|null
     */
    public function getFileUrl(): ?string
    {
        if ($this->file) {
            return $this->file->getFileUrl();
        }

        return null;
    }

    public function getCoverImageUrl(): ?string
    {
        if ($this->cover_image) {
            return $this->cover_image->getFileUrl();
        }

        return null;
    }

    /**
     * @return array|null
     */
    public function getFile(): ?array
    {
        if ($this->file) {
            return $this->file->toAll();
        }

        return null;
    }

    /**
     * @return array
     */
    public function getFiles(): array
    {
        $output = [];
        foreach ($this->files as $file) {
            if ($file instanceof File) {
                array_push($output, $file->toAll());
            }
        }

        return $output;
    }

    /**
     * @return array|null
     */
    public function getCoverImage(): ?array
    {
        if ($this->cover_image) {
            return $this->cover_image->toAll();
        }

        return null;
    }
}
