<?php
/**
 * Created by channelk.
 * Author: naingminkhant
 * Date: 2019-06-04
 * Time: 12:46
 */

namespace Arga\Storage\Cloudinary;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;

class LocalFileStorage
{
    protected $_path = 'upload/images/';

    /**
     * @param \Illuminate\Http\UploadedFile $file
     * @return \Arga\Storage\Cloudinary\File|null
     */
    public function upload(UploadedFile $file): ?File
    {
        $filename = $this->createFilename($file);
        $file->move(storage_path('app/public/'.$this->_path), $filename);

        $model = File::create([
            'name'          => $file->getClientOriginalName(),
            'cloud_service' => File::LOCAL,
            'path'          => $this->_path.$filename,
        ]);

        return $model;
    }

    /**
     * @param \Arga\Storage\Cloudinary\FileableModel $model
     * @param $fileUrl
     * @param bool $detach
     * @return \Arga\Storage\Cloudinary\File|null
     */
    public function uploadAndAttachFromUrl(FileableModel $model, $fileUrl, $detach = false)
    {
        /** @var \Arga\Storage\Cloudinary\File $saveFile */
        $saveFile = File::create([
            'name'          => $model->getName(),
            'cloud_service' => File::SOCIAL,
            'path'          => $fileUrl,
        ]);
        if ($detach && $model->file) {
            $model->file->delete();
        }
        $saveFile->related_model()->associate($model);
        $saveFile->save();

        return $saveFile->fresh();
    }

    /**
     * @param \Illuminate\Http\UploadedFile $file
     * @return mixed|string
     */
    protected function createFilename(UploadedFile $file)
    {
        $extension = $file->getClientOriginalExtension();
        $filename = str_replace(".".$extension, "", $file->getClientOriginalName()); // Filename without extension

        /**
         * Add timestamp hash to name of the file
         */
        $filename .= "_".md5(time()).".".$extension;

        return $filename;
    }

    public function uploadAndAttach(FileableModel $model, UploadedFile $file, $detach = false)
    {
        $saveFile = $this->upload($file);
        if ($detach && $model->file) {
            $model->file->delete();
        }
        $saveFile->related_model()->associate($model);
        $saveFile->save();

        return $saveFile->fresh();
    }
}
