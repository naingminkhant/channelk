<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 7/3/18
 * Time: 12:05 PM
 */

namespace Arga\Storage\Cloudinary;

interface FileDisplayable
{
    public function getFileUrl(): string;

    public function getThumbnailUrl(): string;
}