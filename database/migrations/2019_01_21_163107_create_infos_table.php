<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('infos', function (Blueprint $table) {
            $table->uuid('id');
            $table->text('info_one')->nullable();
            $table->uuid('image_one')->nullable();
            $table->foreign('image_one')->references('id')->on('images');
            $table->text('info_two')->nullable();
            $table->uuid('image_two')->nullable();
            $table->foreign('image_two')->references('id')->on('images');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('infos');
    }
}
