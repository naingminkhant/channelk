<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideoLanguage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('video_language', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');

            $table->uuid('language_id')->index();
            $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade');

            $table->uuid('model_type')->index();
            $table->uuid('model_id')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('video_language');
    }
}
