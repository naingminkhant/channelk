<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('series', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->string('title');
            $table->string('slug')->unique();

            $table->uuid('country_id')->index();
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');

            $table->string('trailer_url')->nullable();
            //$table->string('full_url');
            $table->text('story_line')->nullable();
            $table->date('released_date');
            $table->decimal('budget', 12,0)->nullable();

            $table->uuid('director_id')->index();
            $table->foreign('director_id')->references('id')->on('directors')->onDelete('cascade');

            $table->uuid('production_company_id')->index();
            $table->foreign('production_company_id')->references('id')->on('production_companies')->onDelete('cascade');

            $table->boolean('active')->default(false);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('series');
    }
}
