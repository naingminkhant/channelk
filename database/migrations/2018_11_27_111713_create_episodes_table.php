<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEpisodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('episodes', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->string('title');
            $table->string('slug')->unique();
            $table->time('duration');

            $table->string('trailer_url')->nullable();
            $table->string('full_url')->nullable();
            $table->text('story_line')->nullable();
            $table->date('released_date')->nullable();

            $table->uuid('series_id')->index();
            $table->foreign('series_id')->references('id')->on('series')->onDelete('cascade');

            $table->tinyInteger('sort');
            $table->boolean('active')->default(false);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('episodes');
    }
}
