<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AdminsTableSeeder::class);
        $this->call(CastsTableSeeder::class);
        $this->call(CompaniesTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(CountriesTableSeeder::class);
        $this->call(DirectorsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(VidoesTableSeeder::class);
    }
}
