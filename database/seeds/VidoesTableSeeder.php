<?php

use Illuminate\Database\Seeder;

class VidoesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category = \App\Models\Category::pluck('id');
        $videos = factory(\App\Models\Video::class,100)->make();
        foreach ($videos as $video) {
            $video = \App\Models\Video::create($video->toArray());
            $video->categories()->sync($category[0]);
        }
    }
}
