<?php

use Illuminate\Database\Seeder;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Arga\Auth\Admin::create([
            'name'     => 'Admin',
            'email'    => 'admin@admin.com',
            'password' => bcrypt('root'),
        ]);

        \Arga\Auth\Admin::create([
            'name'     => 'Sys Admin',
            'email'    => 'admin@root.com',
            'password' => bcrypt('password'),
        ]);
    }
}
