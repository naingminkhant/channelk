<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

/** @var \Faker\Factory $factory */
$factory->define(\App\Models\Country::class, function (Faker $faker) {
    return [
        'name' => $faker->country,
        'slug' => $faker->country,
    ];
});

$factory->define(\App\Models\Category::class, function (Faker $faker) {
    return [
        'slug'  => $faker->slug,
        'title' => $faker->title,
    ];
});

$factory->define(\App\Models\Company::class, function (Faker $faker) {
    return [
        'slug'        => $faker->company,
        'name'        => $faker->company,
        'web_url'     => $faker->url,
        'description' => $faker->text,
    ];
});

$factory->define(\App\Models\Director::class, function (Faker $faker) {
    return [
        'slug'        => $faker->slug,
        'name'        => $faker->name,
        'web_url'     => $faker->url,
        'description' => $faker->text,
        'gender'      => array_random(['male', 'female']),
    ];
});

$factory->define(\App\Models\Cast::class, function (Faker $faker) {
    return [
        'name'        => $faker->name,
        'slug'        => $faker->slug,
        'born_at'     => $faker->date(),
        'address'     => $faker->address,
        'web_url'     => $faker->url,
        'description' => $faker->text,
        'gender'      => array_random(['male', 'female']),
    ];
});

$factory->define(\Arga\Auth\User::class, function (Faker $faker) {
    return [
        'name'     => $faker->name,
        'email'    => $faker->safeEmail,
        'password' => 'password',
    ];
});

$factory->define(\App\Models\Video::class, function (Faker $faker) {
    return [
        'title'                 => $faker->title,
        'slug'                  => $faker->unique()->slug,
        'duration'              => '01:00:00',
        'country_id'            => \App\Models\Country::first()->id,
        'story_line'            => $faker->text,
        'released_date'         => '2019-01-08',
        'budget'                => 100,
        'director_id'           => \App\Models\Director::first()->id,
        'production_company_id' => \App\Models\Company::first()->id,
        'published_at'          => now(),
    ];
});
