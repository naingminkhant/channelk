<?php

namespace App\Exceptions;

use Exception;

class RoleDoesNotExist extends \InvalidArgumentException
{
    public static function named(string $roleName)
    {
        return new static("There is no role named `{$roleName}`.");
    }
}
