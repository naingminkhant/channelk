<?php

function msg_already_verify()
{
    return [
        'status'  => fals,
        'message' => 'This user is already verified',
    ];
}

function msg_success_verify()
{
    return [
        'status'  => true,
        'message' => 'Successfully Verification',
    ];
}

function msg_invalid_verify()
{
    return [
        'status'  => false,
        'message' => 'Your verification code is not valid',
    ];
}

function msg_success_register()
{
    return [
        'status'  => true,
        'message' => 'Successfully Registered!',
    ];
}

function msg_success_resend_verification()
{
    return [
        'status'  => true,
        'message' => 'Successfully send verification to your email',
    ];
}

function msg_unauthorised_login()
{
    return [
        'status'  => false,
        'message' => 'Unauthorised',
    ];
}

function msg_need_verify()
{
    return [
        'verify'  => false,
        'message' => 'Your account need verify. Check your email.',
    ];
}

function msg_success_logout()
{
    return [
        'status'  => true,
        'message' => 'Your account successfully logout',
    ];
}

function msg_unauthenticated()
{
    return [
        'status'  => false,
        'message' => 'Unauthenticated',
    ];
}

function msg_success_login($token, array $extra = [])
{
    $data = [
        'status' => true,
        'token'  => $token,
    ];

    return array_merge($data, $extra);
}

function msg_success_delete()
{
    return [
        'status'  => true,
        'message' => 'Successfully Deleted!',
    ];
}

function msg_error_delete()
{
    return [
        'status'  => false,
        'message' => 'Cannot Delete!',
    ];
}

function msg_data_not_found()
{
    return [
        'status'  => false,
        'message' => 'Data not found.',
    ];
}

function msg_success_save()
{
    return [
        'status'  => true,
        'message' => 'Successfully saved!',
    ];
}

function msg_success_update()
{
    return [
        'status'  => true,
        'message' => 'Successfully updated!',
    ];
}

function msg_success_destroy()
{
    return [
        'status'  => true,
        'message' => 'Successfully deleted!',
    ];
}

function msg_valid_token()
{
    return [
        'status'  => true,
        'message' => 'Authenticated',
    ];
}

function msg_invalid_email()
{
    return [
        'status'  => false,
        'message' => 'Invalid Email',
    ];
}

function msg_send_reset_password()
{
    return [
        'status'  => true,
        'message' => 'Successfully! Please check your email for reset.',
    ];
}

function msg_success_reset_password()
{
    return [
        'status'  => true,
        'message' => 'Successfully! Your password has been changed.',
    ];
}

function msg_custom_error($message)
{
    return [
        'status'  => false,
        'message' => $message,
    ];
}

function msg_invalid_phone()
{
    return [
        'status'  => false,
        'message' => 'Not allow this number',
    ];
}

function msg_invalid_activation_code()
{
    return [
        'status'  => false,
        'message' => 'Your activation code is Invalid. Please retry.',
    ];
}

function msg_already_rating()
{
    return [
        'status'  => false,
        'message' => 'You already rated.',
    ];
}
