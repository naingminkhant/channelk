<?php
/**
 * Created by PhpStorm.
 * User: tintnaingwin
 * Date: 12/13/18
 * Time: 1:02 AM
 */

use Illuminate\Support\Carbon;

function upcoming_id($sort) {
    if ($sort < 4) {
        return 0;
    }

    return $sort;
}

function carbon_parse($tz = null)
{
    return Carbon::parse($tz);
}
