<?php

namespace App\Service\Traits;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Pion\Laravel\ChunkUpload\Exceptions\UploadMissingFileException;
use Pion\Laravel\ChunkUpload\Receiver\FileReceiver;
use Pion\Laravel\ChunkUpload\Handler\AbstractHandler;

trait VideoUploadTrait
{
    /**
     * @param \Pion\Laravel\ChunkUpload\Receiver\FileReceiver $receiver
     * @return array
     * @throws \Pion\Laravel\ChunkUpload\Exceptions\UploadMissingFileException
     */
    protected function uploadFile(FileReceiver $receiver)
    {
        if ($receiver->isUploaded() === false) {
            throw new UploadMissingFileException();
        }
        $save = $receiver->receive();
        if ($save->isFinished()) {
            return $this->saveFile($save->getFile());
        }

        /** @var AbstractHandler $handler */
        $handler = $save->handler();

        return response()->json([
            "done" => $handler->getPercentageDone(),
        ]);
    }

    protected function saveFile(UploadedFile $file): ?array
    {
        $fileName = $this->createFilename($file);
        /**
         * Group files By Mine
         */
        //$mime = str_replace('/', '-', $file->getMimeType());
        /**
         * Group files By Date
         */
        $dateFolder = date("Y-m-d");

        /**
         * Build File Path (eg., storage/app/upload/date/)
         */
        $filePath = "upload/{$dateFolder}/";
        $finalPath = storage_path("app/public/".$filePath);

        /**
         * Move File Name
         *
         * @todo improved
         */
        $file->move($finalPath, $fileName);
        $path = $filePath.$fileName;

        return [
            'url' => $path,
        ];
    }

    protected function createFilename(UploadedFile $file)
    {
        $extension = $file->getClientOriginalExtension();
        $filename = str_replace(".".$extension, "", $file->getClientOriginalName()); // Filename without extension

        /**
         * Add timestamp hash to name of the file
         */
        $filename .= "_".md5(time()).".".$extension;

        return $filename;
    }
}
