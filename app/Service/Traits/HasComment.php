<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 12/17/18
 * Time: 9:48 PM
 */

namespace App\Service\Traits;

use App\Models\Comment;

/**
 * Trait HasComment
 *
 * @property \Illuminate\Database\Eloquent\Relations\MorphOne comment
 * @property \Illuminate\Database\Eloquent\Relations\MorphMany comments
 * @property \Illuminate\Database\Eloquent\Relations\HasMany user_comments
 */
trait HasComment
{
    public function comment()
    {
        return $this->morphOne(Comment::class, 'related_model');
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'related_model')->orderByDesc('created_at');
    }

    public function getCommentsCount()
    {
        return $this->comments->count();
    }

    public function getDetailComment()
    {
        $comment = $this->comment;
        if ($comment instanceof Comment) {
            return $comment->toAll();
        }

        return null;
    }

    public function getDetailComments()
    {
        $output = [];
        foreach ($this->comments as $comment) {
            if ($comment instanceof Comment) {
                array_push($output, $comment->toAll());
            }
        }

        return $output;
    }

    public function user_comments()
    {
        return $this->hasMany(Comment::class);
    }
}
