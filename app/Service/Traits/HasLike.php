<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 12/18/18
 * Time: 1:45 PM
 */

namespace App\Service\Traits;

use App\Models\Like;
use Arga\Auth\User;

/**
 * Trait HasLike
 *
 * @property \Illuminate\Database\Eloquent\Relations\MorphOne $likes
 */
trait HasLike
{
    public function likes()
    {
        return $this->morphMany(Like::class, 'related_model');
    }

    public function getLikes()
    {
        $likes = $this->likes;
        $output = [];
        foreach ($likes as $like) {
            if ($like instanceof Like) {
                array_push($output, $like->toAll());
            }
        }

        return $output;
    }

    public function getLikesCount()
    {
        return $this->likes->count();
    }

    public function like_by_user()
    {
        $user = auth()->user();
        if (!$user) {
            return null;
        }

        return $this->likes()->where('user_id', $user->id);
    }

    public function isAlreadyLike(): bool
    {
        $user = auth()->user() ?? request()->user('api');
        if ($user instanceof User) {
            $like = $this->likes()->where('user_id', $user->getId())->first();

            return $like ? true : false;
        }

        return false;
    }
}
