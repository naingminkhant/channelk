<?php
/**
 * Created by channelk.
 * Author: naingminkhant
 * Date: 2019-01-09
 * Time: 12:52
 */

namespace App\Service\Traits;

use App\Models\Role;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Trait HasRole
 *
 * @package App\Service\Traits
 * @property \Illuminate\Database\Eloquent\Relations\BelongsToMany $roles
 */
trait HasRole
{
    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(Role::class, 'admin_roles', 'admin_id', 'role_id');
    }

    public function getRoles(): array
    {
        $output = [];
        foreach ($this->roles as $role) {
            if ($role instanceof Role) {
                array_push($output, $role->toOriginal());
            }
        }

        return $output;
    }

    public function getRolesSlug(): array
    {
        return $this->roles()->pluck('slug')->toArray();
    }
}
