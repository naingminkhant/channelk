<?php
/**
 * Created by channelk.
 * Author: naingminkhant
 * Date: 2019-01-08
 * Time: 14:59
 */

namespace App\Service\Traits;

use App\Models\PlayedRecent;

/**
 * Trait HasPlayedRecent
 *
 * @property \Illuminate\Database\Eloquent\Relations\MorphOne $played_recent
 * @property \Illuminate\Database\Eloquent\Relations\HasMany $user_played_recent
 */
trait HasPlayedRecent
{
    /**
     * @return mixed
     */
    public function played_recent()
    {
        return $this->morphOne(PlayedRecent::class, 'related_model');
    }

    /**
     * @return null|mixed
     */
    public function played_recent_by_user()
    {
        if ($user = auth()->user()) {
            return $this->played_recent()->where('user_id', $user->id);
        }

        return null;
    }
}
