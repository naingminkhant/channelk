<?php
/**
 * Created by channelk.
 * Author: naingminkhant
 * Date: 2019-01-08
 * Time: 17:01
 */

namespace App\Service\Traits;

use App\Models\Favorite;

/**
 * @property \Illuminate\Database\Eloquent\Relations\MorphOne $favorite
 */
trait HasFavorite
{
    public function favorite()
    {
        return $this->morphOne(Favorite::class, 'related_model');
    }

    public function hasFavorite(): bool
    {
        $user = auth()->user() ?? request()->user('api');
        if (!$user) {
            return false;
        }

        return $this->favorite()->where('user_id', $user->id)->exists();
    }

    public function favorite_by_user()
    {
        $user = auth()->user();
        if (!$user) {
            return null;
        }

        return $this->favorite()->where('user_id', $user->id);
    }
}
