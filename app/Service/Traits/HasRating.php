<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 12/18/18
 * Time: 12:06 PM
 */

namespace App\Service\Traits;

use App\Models\Rating;
use Arga\Auth\User;

/**
 * Trait HasRating
 *
 * @property \Illuminate\Database\Eloquent\Relations\MorphOne $rating
 * @property \Illuminate\Database\Eloquent\Relations\MorphMany $ratings
 * @description rating system based 10 star
 */
trait HasRating
{
    public function rating()
    {
        return $this->morphOne(Rating::class, 'related_model');
    }

    public function ratings()
    {
        return $this->morphMany(Rating::class, 'related_model');
    }

    public function getAverageRating()
    {
        if(!$this->rating) {
            return 0;
        }
        $avg = $this->rating->avg('rating');

        return round($avg, 2);
    }

    public function getSumRating()
    {
        if(!$this->rating) {
            return 0;
        }
        return $this->rating->sum('rating');
    }

    public function getCountRating()
    {
        return $this->ratings->count();
    }

    public function getPercentRating()
    {
        $max = 10;
        $count = $this->getCountRating();
        $total = $this->getSumRating();

        $percent = ($count * $max) > 0 ?
            $total / (($count * $max) / 100) : 0;

        return round($percent, 2);
    }

    public function getRatings()
    {
        return [
            'average_rating' => $this->getAverageRating(),
            'count_rating'   => $this->getCountRating(),
            'percent_rating' => $this->getPercentRating(),
        ];
    }

    public function isAlredyUserRating(): bool
    {
        $user = auth()->user();
        if ($user instanceof User) {
            $check = $this->ratings()->where('user_id', $user->getId())->first();

            return $check ? true : false;
        }

        return false;
    }
}
