<?php
/**
 * Created by channelk.
 * Author: naingminkhant
 * Date: 2019-01-09
 * Time: 15:33
 */

namespace App\Service\Traits;

use App\Exceptions\PermissionDoesNotExist;
use App\Models\Permission;
use App\Models\Role;

trait HasPermission
{
    public function hasPermissionTo($permission): bool
    {
        if (!is_string($permission) && $permission instanceof Permission) {
            throw new PermissionDoesNotExist;
        }

        if ($this->roles()->where('slug', 'super-admin')->exists()) {
            return true;
        }

        foreach ($this->roles as $role) {
            if ($role instanceof Role) {
                return $role->permissions()->where('slug', $permission)->exists();
            }
        }

        return false;
    }
}
