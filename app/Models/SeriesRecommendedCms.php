<?php

namespace App\Models;

use Arga\Storage\Database\Contracts\SerializableModel;
use Illuminate\Database\Eloquent\Model;

/**
 * @property \Doctrine\DBAL\Schema\Column series_id
 * @property \Illuminate\Database\Eloquent\Relations\BelongsTo $series
 */
class SeriesRecommendedCms extends Model implements SerializableModel
{
    protected $table = 'series_recommended_cms';

    protected $fillable = [
        'series_id',
    ];

    public function series()
    {
        return $this->belongsTo(SeriesVideo::class);
    }

    public function getSeries(): array
    {
        if ($this->series instanceof SeriesVideo) {
            return $this->series->toOriginal();
        }

        return [];
    }

    public function toOriginal(): array
    {
        return $this->getSeries();
    }

    public function toAll(): array
    {
        return $this->getSeries();
    }
}
