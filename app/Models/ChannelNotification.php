<?php

namespace App\Models;

use Arga\Notification\Notification;
use Arga\Storage\Database\BaseModel;
use Arga\Storage\Database\Contracts\SerializableModel;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ChannelNotification
 *
 * @property \Doctrine\DBAL\Schema\Column recipient_type
 * @property \Doctrine\DBAL\Schema\Column title
 * @property \Doctrine\DBAL\Schema\Column body
 * @property \Doctrine\DBAL\Schema\Column published_at
 * @property \Illuminate\Database\Eloquent\Relations\HasMany user_notifications
 */
class ChannelNotification extends BaseModel implements SerializableModel
{
    use SoftDeletes;

    protected $table = 'channel_notifications';

    protected $fillable = [
        'recipient_type',
        'title',
        'body',
        'published_at',
    ];

    const ALL_USERS = 'all_users';
    const SPECIFIC_USERS = 'specific_users';

    public function user_notifications()
    {
        return $this->hasMany(Notification::class, 'channel_notification_id');
    }

    public function getUserNotifications()
    {
        $notifications = $this->user_notifications;
        $output = [];
        /** @var Notification $notification */
        foreach ($notifications as $notification) {
            array_push($output, $notification->toOriginal());
        }

        return $output;
    }

    public function toOriginal(): array
    {
        return [
            'id'             => $this->getId(),
            'recipient_type' => $this->recipient_type,
            'title'          => $this->title,
            'body'           => $this->body,
            'published_at'   => $this->published_at,
        ];
    }

    public function toAll(): array
    {
        return [
            'id'                => $this->getId(),
            'recipient_type'    => $this->recipient_type,
            'title'             => $this->title,
            'body'              => $this->body,
            'published_at'      => $this->published_at,
            'user_notification' => $this->getUserNotifications(),
        ];
    }
}
