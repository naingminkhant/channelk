<?php

namespace App\Models;

use Arga\Storage\Database\Contracts\SerializableModel;
use Illuminate\Database\Eloquent\Model;

/**
 * @property \Doctrine\DBAL\Schema\Column cast_id
 * @property \Illuminate\Database\Eloquent\Relations\BelongsTo $star
 */
class CastorCms extends Model implements SerializableModel
{
    protected $table = 'castor_cms';

    protected $fillable = [
        'cast_id',
    ];

    public function star()
    {
        return $this->belongsTo(Cast::class, 'cast_id');
    }

    public function getStar()
    {
        if ($this->star instanceof Cast) {
            return $this->star->toOriginal();
        }

        return null;
    }

    public function toOriginal(): array
    {
        return $this->getStar();
    }

    public function toAll(): array
    {
        return $this->getStar();
    }
}
