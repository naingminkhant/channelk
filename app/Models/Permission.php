<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Permission
 *
 * @package App\Models
 * @author Naing Min Khant <naingminkhant.cm@gmail.com>
 * @property \Doctrine\DBAL\Schema\Column name
 * @property \Doctrine\DBAL\Schema\Column slug
 * @property \Doctrine\DBAL\Schema\Column guard_name
 */
class Permission extends Model
{
    protected $table = 'permissions';

    protected $fillable = [
        'name',
        'slug',
        'guard_name',
    ];

    const USER_MANAGE = 'user-manage';

    const ROLE_MANAGE = 'role-manage';

    const
        STAFF_INDEX = 'staff-index',
        STAFF_CREATE = 'staff-create',
        STAFF_EDIT = 'staff-edit',
        STAFF_DELETE = 'staff-delete';

    const
        CATEGORY_INDEX = 'category-index',
        CATEGORY_CREATE = 'category-create',
        CATEGORY_EDIT = 'category-edit',
        CATEGORY_DELETE = 'category-delete';

    const
        COUNTRY_INDEX = 'country-index',
        COUNTRY_CREATE = 'country-create',
        COUNTRY_EDIT = 'country-edit',
        COUNTRY_DELETE = 'country-delete';

    const
        DIRECTOR_INDEX = 'director-index',
        DIRECTOR_CREATE = 'director-create',
        DIRECTOR_EDIT = 'director-edit',
        DIRECTOR_DELETE = 'director-delete';

    const
        CAST_INDEX = 'cast-index',
        CAST_CREATE = 'cast-create',
        CAST_EDIT = 'cast-edit',
        CAST_DELETE = 'cast-delete';

    const
        PRODUCTION_COMPANY_INDEX = 'production-company-index',
        PRODUCTION_COMPANY_CREATE = 'production-company-create',
        PRODUCTION_COMPANY_EDIT = 'production-company-edit',
        PRODUCTION_COMPANY_DELETE = 'production-company-delete';

    const
        VIDEO_INDEX = 'video-index',
        VIDEO_CREATE = 'video-create',
        VIDEO_EDIT = 'video-edit',
        VIDEO_DELETE = 'video-delete',
        VIDEO_SHOW = 'video-show';

    const
        SERIES_INDEX = 'series-index',
        SERIES_CREATE = 'series-create',
        SERIES_EDIT = 'series-edit',
        SERIES_DELETE = 'series-delete';

    const
        BANNER_INDEX = 'banner-index',
        BANNER_CREATE = 'banner-create',
        BANNER_EDIT = 'banner-edit',
        BANNER_DELETE = 'banner-delete',
        BANNER_SHOW = 'banner-show';

    const
        CHANNEL_AGENDA_INDEX = 'channel-agenda-index',
        CHANNEL_AGENDA_CREATE = 'channel-agenda-create',
        CHANNEL_AGENDA_EDIT = 'channel-agenda-edit',
        CHANNEL_AGENDA_DELETE = 'channel-agenda-delete';

    const
        NOTIFICATION_INDEX = 'notification-index',
        NOTIFICATION_CREATE = 'notification-create',
        NOTIFICATION_EDIT = 'notification-edit',
        NOTIFICATION_DELETE = 'notification-delete',
        NOTIFICATION_PUBLISH = 'notification-publish';

    const
        RTMPSTREAM_INDEX = 'rtmp-stream-index',
        RTMPSTREAM_CREATE = 'rtmp-stream-create',
        RTMPSTREAM_EDIT = 'rtmp-stream-edit',
        RTMPSTREAM_DELETE = 'rtmp-stream-delete';

    const INFO_MANAGE = 'info-manage';
}
