<?php

namespace App\Models;

use Arga\Storage\Database\BaseModel;
use Arga\Storage\Database\Contracts\SerializableModel;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Country
 *
 * @property \Doctrine\DBAL\Schema\Column slug
 * @property \Doctrine\DBAL\Schema\Column name
 */
class Country extends BaseModel implements SerializableModel
{
    use SoftDeletes;

    protected $table = 'countries';

    protected $fillable = [
        'slug',
        'name',
    ];

    public function toOriginal(): array
    {
        return [
            'id'   => $this->getId(),
            'slug' => $this->slug,
            'name' => $this->name,
        ];
    }

    public function toAll(): array
    {
        return [
            'id'   => $this->getId(),
            'slug' => $this->slug,
            'name' => $this->name,
        ];
    }
}
