<?php

namespace App\Models;

use Arga\Storage\Database\BaseModel;
use Arga\Storage\Database\Contracts\SerializableModel;

/**
 * Class SubCategory
 *
 * @package App\Models
 * @property \Doctrine\DBAL\Schema\Column category_id
 * @property \Doctrine\DBAL\Schema\Column slug
 * @property \Doctrine\DBAL\Schema\Column title
 * @property \Illuminate\Database\Eloquent\Relations\BelongsTo $category
 */
class SubCategory extends BaseModel implements SerializableModel
{
    protected $table = 'sub_categories';

    protected $fillable = [
        'category_id',
        'slug',
        'title',
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function getCategory()
    {
        $category = $this->category;
        if ($category instanceof Category) {
            return $category->toOriginal();
        }

        return null;
    }

    public function toOriginal(): array
    {
        return [
            'id'          => $this->getId(),
            'slug'        => $this->slug,
            'title'       => $this->title,
            'category_id' => $this->category_id,
        ];
    }

    public function toAll(): array
    {
        return [
            'id'          => $this->getId(),
            'slug'        => $this->slug,
            'title'       => $this->title,
            'category_id' => $this->category_id,
            'category'    => $this->getCategory(),
        ];
    }
}
