<?php

namespace App\Models;

use Arga\Storage\Database\BaseModel;

/**
 * @property \Doctrine\DBAL\Schema\Column body
 */
class ContactUs extends BaseModel
{
    protected $table = 'contact_us';

    protected $fillable = [
        'body'
    ];
}
