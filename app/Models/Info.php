<?php

namespace App\Models;

use Arga\Storage\Cloudinary\File;
use Arga\Storage\Database\BaseModel;
use Arga\Storage\Database\Contracts\SerializableModel;

/**
 * @property \Doctrine\DBAL\Schema\Column info_one
 * @property \Doctrine\DBAL\Schema\Column info_two
 * @property \Doctrine\DBAL\Schema\Column info_three
 * @property \Doctrine\DBAL\Schema\Column image_one
 * @property \Doctrine\DBAL\Schema\Column image_two
 * @property \Doctrine\DBAL\Schema\Column image_three
 * @property \Illuminate\Database\Eloquent\Relations\BelongsTo image_file_one
 * @property \Illuminate\Database\Eloquent\Relations\BelongsTo image_file_two
 * @property \Illuminate\Database\Eloquent\Relations\BelongsTo image_file_three
 */
class Info extends BaseModel implements SerializableModel
{
    protected $table = 'infos';

    protected $fillable = [
        'info_one',
        'info_two',
        'info_three',
        'image_one',
        'image_two',
        'image_three',
    ];

    public function image_file_one()
    {
        return $this->belongsTo(File::class, 'image_one', 'id');
    }

    public function image_file_two()
    {
        return $this->belongsTo(File::class, 'image_two', 'id');
    }

    public function image_file_three()
    {
        return $this->belongsTo(File::class, 'image_three', 'id');
    }

    public function getImageOneThumbnail()
    {
        if ($this->image_file_one instanceof File) {
            return $this->image_file_one->getThumbnailUrl();
        }

        return null;
    }

    public function getImageTwoThumbnail()
    {
        if ($this->image_file_two instanceof File) {
            return $this->image_file_two->getThumbnailUrl();
        }

        return null;
    }

    public function getImageThreeThumbnail()
    {
        if ($this->image_file_three instanceof File) {
            return $this->image_file_three->getThumbnailUrl();
        }

        return null;
    }

    public function getImageOne()
    {
        if ($this->image_file_one instanceof File) {
            return $this->image_file_one->toAll();
        }

        return null;
    }

    public function getImageTwo()
    {
        if ($this->image_file_two instanceof File) {
            return $this->image_file_two->toAll();
        }

        return null;
    }

    public function getImageThree()
    {
        if ($this->image_file_three instanceof File) {
            return $this->image_file_three->toAll();
        }

        return null;
    }

    public function getLimitInfoOne()
    {
        return str_limit($this->info_one, 300);
    }

    public function getLimitInfoTwo()
    {
        return str_limit($this->info_two, 300);
    }

    public function getLimitInfoThree()
    {
        return str_limit($this->info_three, 300);
    }

    /**
     * @return array
     * @prepare
     */
    public function toOriginal(): array
    {
        return [
            'id'              => $this->getId(),
            'info_one'        => $this->info_one,
            'info_two'        => $this->info_two,
            'limit_info_one'  => $this->getLimitInfoOne(),
            'limit_info_two'  => $this->getLimitInfoTwo(),
            'image_one_thumb' => $this->getImageOneThumbnail(),
            'image_two_thumb' => $this->getImageTwoThumbnail(),
        ];
    }

    public function toAll(): array
    {
        return [
            'id'                              => $this->getId(),
            'livestream'                      => $this->info_one,
            'privacy_policy'                  => $this->info_two,
            'terms_and_condition'             => $this->info_three,
            'preview_livestream'              => $this->getLimitInfoOne(),
            'preview_privacy_policy'          => $this->getLimitInfoTwo(),
            'preview_terms_and_condition'     => $this->getLimitInfoThree(),
            'image_livestream_thumb'          => $this->getImageOneThumbnail(),
            'image_privacy_policy_thumb'      => $this->getImageTwoThumbnail(),
            'image_terms_and_condition_thumb' => $this->getImageThreeThumbnail(),
            'livestream_image'                => $this->getImageOne(),
            'privacy_policy_image'            => $this->getImageTwo(),
            'terms_and_condition_image'       => $this->getImageThree(),
        ];
    }
}
