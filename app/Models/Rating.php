<?php

namespace App\Models;

use Arga\Auth\User;
use Arga\Storage\Database\Contracts\SerializableModel;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Rating
 *
 * @property \Doctrine\DBAL\Schema\Column rating
 * @property \Doctrine\DBAL\Schema\Column user_id
 * @property \Illuminate\Database\Eloquent\Relations\BelongsTo $user
 */
class Rating extends Model implements SerializableModel
{
    protected $table = 'ratings';

    protected $fillable = [
        'rating',
        'user_id',
    ];

    public function related_model()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getUser()
    {
        $user = $this->user;
        if ($user instanceof User) {
            return $user->toOriginal();
        }

        return null;
    }

    public function toOriginal(): array
    {
        return [
            'rating'  => $this->rating,
            'user_id' => $this->user_id,
        ];
    }

    public function toAll(): array
    {
        return [
            'rating' => $this->rating,
            'user'   => $this->getUser(),
        ];
    }
}
