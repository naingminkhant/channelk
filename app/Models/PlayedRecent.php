<?php

namespace App\Models;

use Arga\Storage\Database\BaseModel;
use Arga\Storage\Database\Contracts\SerializableModel;
use Session;
use Request;
use Auth;

/**
 * @property \Doctrine\DBAL\Schema\Column model_type
 * @property \Doctrine\DBAL\Schema\Column model_id
 * @property \Doctrine\DBAL\Schema\Column user_id
 * @property \Doctrine\DBAL\Schema\Column paused_time
 * @property \Illuminate\Database\Eloquent\Relations\MorphTo $related_model
 */
class PlayedRecent extends BaseModel implements SerializableModel
{
    protected $table = 'played_recents';

    protected $fillable = [
        'user_id',
        'paused_time',
    ];

    public function related_model()
    {
        return $this->morphTo();
    }

    public static function track(Video $video){
        if (!Session::has('played_recent_track') || Session::get('played_recent_track') !== Request::path()) {

            $video->played_recent()->create([
                'user_id' => Auth::id(),
            ]);

            Session::put('played_recent_track', Request::path());

            return ['message' => 'saved'];

        }else {
            return ['message' => Session::get('played_recent_track')];
        }

    }

    public function getRelatedVideo()
    {
        if ($this->related_model instanceof Video) {
            return $this->related_model->toOriginal();
        }

        return null;
    }

    public function getRelatedSeries()
    {
        if ($this->related_model instanceof SeriesVideo) {
            return $this->related_model->toOriginal();
        }

        return null;
    }

    public function toOriginal(): array
    {
        return [
            'id'          => $this->getId(),
            'user_id'     => $this->user_id,
            'paused_time' => $this->paused_time,
        ];
    }

    public function toAll(): array
    {
        return [
            'id'          => $this->getId(),
            'user_id'     => $this->user_id,
            'paused_time' => $this->paused_time,
        ];
    }
}
