<?php

namespace App\Models;

use Arga\Storage\Database\Contracts\SerializableModel;
use Illuminate\Database\Eloquent\Model;

/**
 * @property \Doctrine\DBAL\Schema\Column video_id
 * @property \Doctrine\DBAL\Schema\Column id
 * @property \Illuminate\Database\Eloquent\Relations\BelongsTo $video
 */
class VideoRecommendedCms extends Model implements SerializableModel
{
    protected $table = 'video_recommended_cms';

    protected $fillable = [
        'video_id',
    ];

    public function getId()
    {
        return $this->id;
    }

    public function video()
    {
        return $this->belongsTo(Video::class);
    }

    public function getVideo(): ?array
    {
        if ($this->video instanceof Video) {
            return $this->video->toOriginal();
        }

        return null;
    }

    public function toOriginal(): array
    {
        return $this->getVideo() ?? [];
    }

    public function toAll(): array
    {
        return $this->getVideo() ?? [];
    }

    public function toCustom(): array
    {
        if ($this->video instanceof Video) {
            return $this->video->toCustom();
        }
    }
}
