<?php

namespace App\Models;

use Arga\Storage\Cloudinary\FileableModel;
use Arga\Storage\Cloudinary\HasFile;
use Arga\Storage\Database\BaseModel;
use Arga\Storage\Database\Contracts\SerializableModel;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property \Doctrine\DBAL\Schema\Column slug
 * @property \Doctrine\DBAL\Schema\Column name
 * @property \Doctrine\DBAL\Schema\Column web_url
 * @property \Doctrine\DBAL\Schema\Column description
 * @property \Doctrine\DBAL\Schema\Column gender
 */
class Director extends BaseModel implements SerializableModel, FileableModel
{
    use SoftDeletes;
    use HasFile;

    protected $table = 'directors';

    protected $fillable = [
        'slug',
        'name',
        'web_url',
        'description',
        'gender',
    ];

    public function getSlug()
    {
        return $this->slug;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getWebUrl()
    {
        return $this->web_url;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getGender()
    {
        return $this->gender;
    }

    public function toOriginal(): array
    {
        $imageOptions = [
            'height'  => 100,
            'quality' => 50,
            'crop'    => 'thumb',
        ];

        return [
            'id'          => $this->getId(),
            'slug'        => $this->getSlug(),
            'name'        => $this->getName(),
            'web_url'     => $this->getWebUrl(),
            'description' => $this->getDescription(),
            'gender'      => $this->getGender(),
            'thumbnail'   => $this->getThumbnailUrl($imageOptions),
        ];
    }

    public function toAll(): array
    {
        return [
            'id'          => $this->getId(),
            'slug'        => $this->getSlug(),
            'name'        => $this->getName(),
            'web_url'     => $this->getWebUrl(),
            'description' => $this->getDescription(),
            'gender'      => $this->getGender(),
            'image'       => $this->getFiles(),
        ];
    }
}
