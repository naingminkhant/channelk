<?php

namespace App\Models;

use Arga\Auth\User;
use Arga\Storage\Database\Contracts\SerializableModel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Like
 *
 * @property \Doctrine\DBAL\Schema\Column id
 * @property \Doctrine\DBAL\Schema\Column like
 * @property \Doctrine\DBAL\Schema\Column user_id
 * @property \Illuminate\Database\Eloquent\Relations\BelongsTo $user
 */
class Like extends Model implements SerializableModel
{
    protected $table = 'likes';

    protected $fillable = [
        'like',
        'user_id',
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('like', function (Builder $builder) {
            $builder->where('like', true);
        });
    }

    public function related_model()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getUser()
    {
        $user = $this->user;
        if ($user instanceof User) {
            return $user->toOriginal();
        }

        return null;
    }

    public function toOriginal(): array
    {
        return [
            'id'      => $this->id,
            'like'    => $this->like,
            'user_id' => $this->user_id,
        ];
    }

    public function toAll(): array
    {
        return [
            'id'   => $this->id,
            'like' => $this->like,
            'user' => $this->getUser(),
        ];
    }
}
