<?php
/**
 * Created by channelk.
 * Author: naingminkhant
 * Date: 2019-06-13
 * Time: 19:22
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property \Doctrine\DBAL\Schema\Column id
 * @property \Doctrine\DBAL\Schema\Column keyword
 * @property \Doctrine\DBAL\Schema\Column user_id
 */
class SearchKeyword extends Model
{
    protected $table = 'search_keywords';

    protected $fillable = [
        'user_id',
        'keyword',
    ];
}
