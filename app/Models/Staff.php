<?php

namespace App\Models;

use App\Service\Traits\HasRole;
use Arga\Auth\Admin;
use Arga\Storage\Database\Contracts\SerializableModel;

class Staff extends Admin implements SerializableModel
{
    use HasRole;

    public function toOriginal(): array
    {
        return [
            'id'    => $this->id,
            'name'  => $this->name,
            'email' => $this->email,
        ];
    }

    public function toAll(): array
    {
        return [
            'id'         => $this->id,
            'name'       => $this->name,
            'email'      => $this->email,
            'roles_slug' => $this->getRolesSlug(),
            'roles'      => $this->getRoles(),
        ];
    }
}
