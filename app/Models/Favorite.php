<?php

namespace App\Models;

use Arga\Storage\Database\BaseModel;

class Favorite extends BaseModel
{
    protected $table = 'favorites';

    protected $fillable = [
        'user_id'
    ];

    public function related_model()
    {
        return $this->morphTo();
    }
}
