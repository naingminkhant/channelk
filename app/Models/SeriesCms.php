<?php

namespace App\Models;

use Arga\Storage\Database\Contracts\SerializableModel;
use Illuminate\Database\Eloquent\Model;

/**
 * @property \Doctrine\DBAL\Schema\Column series_id
 * @property \Illuminate\Database\Eloquent\Relations\BelongsTo $series
 */
class SeriesCms extends Model implements SerializableModel
{
    protected $table = 'series_cms';

    protected $fillable = [
        'series_id',
    ];

    public function series()
    {
        return $this->belongsTo(SeriesVideo::class);
    }

    public function getSeries()
    {
        if ($this->series instanceof SeriesVideo) {
            return $this->series->toOriginal();
        }

        return null;
    }

    public function toOriginal(): array
    {
        return $this->getSeries() ?? [];
    }

    public function toAll(): array
    {
        return $this->getSeries() ?? [];
    }
}
