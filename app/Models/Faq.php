<?php

namespace App\Models;

use Arga\Storage\Database\BaseModel;

/**
 * @property \Doctrine\DBAL\Schema\Column body
 */
class Faq extends BaseModel
{
    protected $table = 'faq';

    protected $fillable = [
        'body',
    ];
}
