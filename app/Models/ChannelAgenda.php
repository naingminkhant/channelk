<?php

namespace App\Models;

use Arga\Storage\Database\BaseModel;
use Arga\Storage\Database\Contracts\SerializableModel;
use Carbon\Carbon;

/**
 * Class ChannelAgenda
 *
 * @property \Doctrine\DBAL\Schema\Column date
 * @property \Doctrine\DBAL\Schema\Column time
 * @property \Doctrine\DBAL\Schema\Column title
 */
class ChannelAgenda extends BaseModel implements SerializableModel
{
    protected $table = 'channel_agendas';

    protected $fillable = [
        'date',
        'agendas',
    ];

    protected $casts = ['agendas' => 'array'];

    public function setDateAttribute($value)
    {
        $this->attributes['date'] = Carbon::parse($value)->format('Y-m-d');
    }

    public function toOriginal(): array
    {
        return [
            'id'      => $this->getId(),
            'date'    => $this->date,
            'agendas' => $this->agendas,
        ];
    }

    public function toAll(): array
    {
        // TODO: Implement toAll() method.
    }
}
