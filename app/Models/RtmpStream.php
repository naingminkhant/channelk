<?php

namespace App\Models;

use Arga\Storage\Database\BaseModel;
use Arga\Storage\Database\Contracts\SerializableModel;

/**
 * Class RtmpStream
 * @property \Doctrine\DBAL\Schema\Column name
 * @property \Doctrine\DBAL\Schema\Column url
 * @property \Doctrine\DBAL\Schema\Column streamer
 */
class RtmpStream extends BaseModel implements SerializableModel
{
    protected $table = 'rtmp_streams';

    protected $fillable = [
        'name',
        'url',
        'streamer',
    ];

    public function toOriginal(): array
    {
        return [
            'id'       => $this->getId(),
            'name'     => $this->name,
            'url'      => $this->url,
            'streamer' => $this->streamer,
        ];
    }

    public function toAll(): array
    {
        return [
            'id'       => $this->getId(),
            'name'     => $this->name,
            'url'      => $this->url,
            'streamer' => $this->streamer,
        ];
    }
}
