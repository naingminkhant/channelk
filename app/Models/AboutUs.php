<?php

namespace App\Models;

use Arga\Storage\Database\BaseModel;

/**
 * @property \Doctrine\DBAL\Schema\Column body
 */
class AboutUs extends BaseModel
{
    protected $table = 'about_us';

    protected $fillable = [
        'body',
    ];
}
