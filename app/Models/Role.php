<?php

namespace App\Models;

use App\Exceptions\RoleDoesNotExist;
use Arga\Storage\Database\Contracts\SerializableModel;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Role
 *
 * @package App\Models
 * @author Naing Min Khant <naingminkhant.cm@gmail.com>
 * @property \Doctrine\DBAL\Schema\Column id
 * @property \Doctrine\DBAL\Schema\Column slug
 * @property \Doctrine\DBAL\Schema\Column name
 * @property \Doctrine\DBAL\Schema\Column guard_name
 * @property \Illuminate\Database\Eloquent\Relations\BelongsToMany $permissions
 */
class Role extends Model implements SerializableModel
{
    protected $table = 'roles';

    protected $fillable = [
        'slug',
        'name',
        'gurad_name',
    ];

    public function permissions()
    {
        return $this->belongsToMany(Permission::class, 'role_permissions');
    }

    public static function findBySlug(string $slug)
    {
        $role = static::where('slug', $slug)->first();

        if (!$role) {
            throw RoleDoesNotExist::named($slug);
        }

        return $role;
    }

    public static function findPermissionBySlug(string $slug)
    {
        $permission = (new Permission())->where('slug', $slug)->first();
        if ($permission) {
            return $permission->id;
        }
        throw RoleDoesNotExist::named($slug);
    }

    public function syncPermissions($permissions)
    {
        $permissionIds = [];
        foreach ($permissions as $permission) {
            $permissionIds[] += static::findPermissionBySlug($permission);
        }

        return $this->permissions()->sync($permissionIds);
    }

    public function pluckPermissions()
    {
        return $this->permissions()->pluck('slug')->toArray();
    }

    public function toOriginal(): array
    {
        return [
            'id'         => $this->id,
            'slug'       => $this->slug,
            'name'       => $this->name,
            'gurad_name' => $this->guard_name,
        ];
    }

    public function toAll(): array
    {
        return [
            'id'         => $this->id,
            'slug'       => $this->slug,
            'name'       => $this->name,
            'gurad_name' => $this->guard_name,
        ];
    }
}
