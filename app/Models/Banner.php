<?php

namespace App\Models;

use Arga\Storage\Cloudinary\FileableModel;
use Arga\Storage\Cloudinary\HasFile;
use Arga\Storage\Database\BaseModel;
use Arga\Storage\Database\Contracts\SerializableModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

/**
 * Class Banner
 *
 * @property \Doctrine\DBAL\Schema\Column title
 * @property \Doctrine\DBAL\Schema\Column start_date
 * @property \Doctrine\DBAL\Schema\Column end_date
 * @property \Doctrine\DBAL\Schema\Column url
 * @property \Doctrine\DBAL\Schema\Column type
 * @property \Doctrine\DBAL\Schema\Column type_id
 * @property \Doctrine\DBAL\Schema\Column published_at
 */
class Banner extends BaseModel implements SerializableModel, FileableModel
{
    use SoftDeletes;
    use HasFile;

    protected $table = 'banners';

    protected $fillable = [
        'title',
        'start_date',
        'end_date',
        'url',
        'type',
        'type_id',
        'published_at',
    ];

    const VIDEO = 'video';
    const SERIES = 'series';

    public function setStartDateAttribute($value)
    {
        $this->attributes['start_date'] = Carbon::parse($value)->format('Y-m-d');
    }

    public function setEndDateAttribute($value)
    {
        $this->attributes['end_date'] = Carbon::parse($value)->format('Y-m-d');
    }

    public function setPublishedAtAttribute($value)
    {
        $this->attributes['published_at'] = $value ? Carbon::parse($value)->format('Y-m-d') : null;
    }

    public function getThumbnails()
    {
        $imageOptions = [
            'height'  => 100,
            'quality' => 50,
            'crop'    => 'thumb',
        ];

        return $this->getThumbnailUrls($imageOptions);
    }

    public function getThumbnail(): ?string
    {
        $imageOptions = [
            'height'  => 100,
            'quality' => 50,
            'crop'    => 'thumb',
        ];

        return $this->getThumbnailUrl($imageOptions);
    }

    public function getRelated()
    {
        if ($this->type === self::VIDEO) {
            $video = $this->belongsTo(Video::class, 'type_id')->first();
            if ($video instanceof Video) {
                return $video->toCustom();
            }
        } else {
            $series = $this->belongsTo(SeriesVideo::class, 'type_id')->first();
            if ($series instanceof SeriesVideo) {
                return $series->toAll();
            }
        }
    }

    public function toOriginal(): array
    {
        return [
            'id'           => $this->getId(),
            'title'        => $this->title,
            'start_date'   => $this->start_date,
            'end_date'     => $this->end_date,
            'images'       => [
                'thumbnail' => $this->getThumbnail(),
                'original'  => $this->getFileUrl(),
            ],
            'cover_image'  => [
                'thumbnail' => $this->getCoverThumbnailUrl(),
                'original'  => $this->getCoverImageUrl(),
            ],
            'url'          => $this->url,
            'type'         => $this->type,
            'type_id'      => $this->type_id,
            'published_at' => $this->published_at,
            'related_item' => $this->getRelated(),
        ];
    }

    public function toAll(): array
    {
        return [
            'id'           => $this->getId(),
            'title'        => $this->title,
            'start_date'   => $this->start_date,
            'end_date'     => $this->end_date,
            'images'       => $this->getFiles(),
            'cover_image'  => $this->getCoverImage(),
            'url'          => $this->url,
            'type'         => $this->type,
            'type_id'      => $this->type_id,
            'published_at' => $this->published_at,
            'related_item' => $this->getRelated(),
        ];
    }
}
