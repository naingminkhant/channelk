<?php

namespace App\Models;

use Arga\Auth\User;
use Arga\Storage\Database\BaseModel;
use Arga\Storage\Database\Contracts\SerializableModel;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Comment
 *
 * @property \Doctrine\DBAL\Schema\Column comment
 * @property \Doctrine\DBAL\Schema\Column user_id
 * @property \Doctrine\DBAL\Schema\Column created_at
 * @property \Illuminate\Database\Eloquent\Relations\BelongsTo $user
 */
class Comment extends BaseModel implements SerializableModel
{
    use SoftDeletes;

    protected $table = 'comments';

    protected $fillable = [
        'comment',
        'user_id',
    ];

    public function related_model()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getUser()
    {
        $user = $this->user;
        if ($user instanceof User) {
            return $user->toOriginal();
        }

        return null;
    }

    public function getCreatedAt()
    {
        return optional($this->created_at)->toDateTimeString();
    }

    public function toOriginal(): array
    {
        return [
            'id'         => $this->getId(),
            'comment'    => $this->comment,
            'user_id'    => $this->user_id,
            'created_at' => $this->getCreatedAt(),
        ];
    }

    public function toAll(): array
    {
        return [
            'id'         => $this->getId(),
            'comment'    => $this->comment,
            'user'       => $this->getUser(),
            'created_at' => $this->getCreatedAt(),
        ];
    }
}
