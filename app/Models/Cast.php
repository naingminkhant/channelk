<?php

namespace App\Models;

use Arga\Storage\Cloudinary\FileableModel;
use Arga\Storage\Cloudinary\HasFile;
use Arga\Storage\Database\BaseModel;
use Arga\Storage\Database\Contracts\SerializableModel;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Cast
 *
 * @property \Doctrine\DBAL\Schema\Column name
 * @property \Doctrine\DBAL\Schema\Column slug
 * @property \Doctrine\DBAL\Schema\Column born_at
 * @property \Doctrine\DBAL\Schema\Column address
 * @property \Doctrine\DBAL\Schema\Column web_url
 * @property \Doctrine\DBAL\Schema\Column description
 * @property \Doctrine\DBAL\Schema\Column gender
 * @property \Doctrine\DBAL\Schema\Column position
 * @property \Illuminate\Database\Eloquent\Relations\BelongsToMany videos
 */
class Cast extends BaseModel implements SerializableModel, FileableModel
{
    use SoftDeletes;
    use HasFile;

    protected $table = 'casts';

    protected $fillable = [
        'name',
        'slug',
        'born_at',
        'address',
        'web_url',
        'description',
        'gender',
        'position',
    ];

    public function setBornAtAttribute($value)
    {
        $this->attributes['born_at'] = Carbon::parse($value)->format('Y-m-d');
    }

    public function cms()
    {
        return $this->hasOne(CastorCms::class);
    }

    public function videos()
    {
        return $this->belongsToMany(Video::class, 'video_cast', 'cast_id', 'video_id');
    }

    public function getVideos()
    {
        $output = [];
        foreach ($this->videos as $video) {
            if ($video instanceof Video) {
                array_push($output, $video->toCustom());
            }
        }

        return $output;
    }

    public function toOriginal(): array
    {
        return [
            'id'          => $this->getId(),
            'name'        => $this->name,
            'slug'        => $this->slug,
            'born_at'     => $this->born_at,
            'address'     => $this->address,
            'web_url'     => $this->web_url,
            'description' => $this->description,
            'gender'      => $this->gender,
            'thumbnail'   => $this->getThumbnailUrl(),
            'position'    => $this->position,
        ];
    }

    public function toAll(): array
    {
        return [
            'id'          => $this->getId(),
            'name'        => $this->name,
            'slug'        => $this->slug,
            'born_at'     => $this->born_at,
            'address'     => $this->address,
            'web_url'     => $this->web_url,
            'description' => $this->description,
            'gender'      => $this->gender,
            'image'       => $this->getFiles(),
            'position'    => $this->position,
            'series'      => $this->getVideos(),
        ];
    }
}
