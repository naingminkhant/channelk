<?php

namespace App\Models;

use Arga\Storage\Cloudinary\FileableModel;
use Arga\Storage\Cloudinary\HasFile;
use Arga\Storage\Database\BaseModel;
use Arga\Storage\Database\Contracts\SerializableModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

/**
 * Class Episode
 *
 * @property \Doctrine\DBAL\Schema\Column title
 * @property \Doctrine\DBAL\Schema\Column slug
 * @property \Doctrine\DBAL\Schema\Column duration
 * @property \Doctrine\DBAL\Schema\Column trailer_url
 * @property \Doctrine\DBAL\Schema\Column full_url
 * @property \Doctrine\DBAL\Schema\Column story_line
 * @property \Doctrine\DBAL\Schema\Column released_date
 * @property \Doctrine\DBAL\Schema\Column series_id
 * @property \Doctrine\DBAL\Schema\Column sort
 * @property \Doctrine\DBAL\Schema\Column active
 */
class Episode extends BaseModel implements SerializableModel, FileableModel
{
    use SoftDeletes;
    use HasFile;

    protected $table = 'episodes';

    protected $fillable = [
        'title',
        'slug',
        'duration',
        'trailer_url',
        'full_url',
        'story_line',
        'released_date',
        'series_id',
        'sort',
        'active',
    ];

    public function setReleasedDateAttribute($value)
    {
        $this->attributes['released_date'] = Carbon::parse($value)->format('Y-m-d');
    }

    public function getTrailerUrl()
    {
        if (Storage::disk('public')->has($this->trailer_url)) {
            return $this->trailer_url;
        }

        return null;
    }

    public function getFullUrl()
    {
        if (Storage::disk('public')->has($this->full_url)) {
            return $this->full_url;
        }

        return null;
    }

    public function getPreviewTrailer()
    {
        if (Storage::disk('public')->has($this->trailer_url)) {
            return Storage::disk('public')->url($this->trailer_url);
        }

        return null;
    }

    public function getPreviewFull()
    {
        if (Storage::disk('public')->has($this->full_url)) {
            return Storage::disk('public')->url($this->full_url);
        }

        return null;
    }

    public function getThumbnails()
    {
        $imageOptions = [
            'height'  => 100,
            'quality' => 50,
            'crop'    => 'thumb',
        ];

        return $this->getThumbnailUrls($imageOptions);
    }

    public function toOriginal(): array
    {
        return [
            'id'              => $this->getId(),
            'title'           => $this->title,
            'slug'            => $this->slug,
            'duration'        => $this->duration,
            'trailer_url'     => $this->getTrailerUrl(),
            'preview_trailer' => $this->getPreviewTrailer(),
            'full_url'        => $this->getFullUrl(),
            'preview_full'    => $this->getPreviewFull(),
            'story_line'      => $this->story_line,
            'released_date'   => $this->released_date,
            'series_id'       => $this->series_id,
            'sort'            => $this->sort,
            'active'          => $this->active,
            'thumbnails'      => $this->getThumbnails(),
        ];
    }

    public function toAll(): array
    {
        return [
            'id'              => $this->getId(),
            'title'           => $this->title,
            'slug'            => $this->slug,
            'duration'        => $this->duration,
            'trailer_url'     => $this->getTrailerUrl(),
            'preview_trailer' => $this->getPreviewTrailer(),
            'full_url'        => $this->getFullUrl(),
            'preview_full'    => $this->getPreviewFull(),
            'story_line'      => $this->story_line,
            'released_date'   => $this->released_date,
            'series_id'       => $this->series_id,
            'sort'            => $this->sort,
            'active'          => $this->active,
            'images'          => $this->getFiles(),
        ];
    }
}
