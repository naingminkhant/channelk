<?php

namespace App\Models;

use App\Service\Traits\HasComment;
use App\Service\Traits\HasFavorite;
use App\Service\Traits\HasLike;
use App\Service\Traits\HasPlayedRecent;
use App\Service\Traits\HasRating;
use Arga\Storage\Cloudinary\FileableModel;
use Arga\Storage\Cloudinary\HasFile;
use Arga\Storage\Database\BaseModel;
use Arga\Storage\Database\Contracts\SerializableModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Session;
use Request;
use Auth;

/**
 * Class Video
 *
 * @package App\Models
 * @property \Doctrine\DBAL\Schema\Column title
 * @property \Doctrine\DBAL\Schema\Column title_mm
 * @property \Doctrine\DBAL\Schema\Column slug
 * @property \Doctrine\DBAL\Schema\Column duration
 * @property \Doctrine\DBAL\Schema\Column country_id
 * @property \Doctrine\DBAL\Schema\Column trailer_url
 * @property \Doctrine\DBAL\Schema\Column full_url
 * @property \Doctrine\DBAL\Schema\Column story_line
 * @property \Doctrine\DBAL\Schema\Column released_date
 * @property \Doctrine\DBAL\Schema\Column budget
 * @property \Doctrine\DBAL\Schema\Column director_id
 * @property \Doctrine\DBAL\Schema\Column production_company_id
 * @property \Doctrine\DBAL\Schema\Column published_at
 * @property \Doctrine\DBAL\Schema\Column viewed
 * @property \Doctrine\DBAL\Schema\Column price
 * @property \Doctrine\DBAL\Schema\Column notification
 * @property \Illuminate\Database\Eloquent\Relations\BelongsTo $country
 * @property \Illuminate\Database\Eloquent\Relations\BelongsTo $company
 * @property \Illuminate\Database\Eloquent\Relations\BelongsTo $director
 * @property \Illuminate\Database\Eloquent\Relations\BelongsToMany $categories
 * @property \Illuminate\Database\Eloquent\Relations\BelongsToMany $sub_categories
 * @property \Illuminate\Database\Eloquent\Relations\BelongsToMany $stars
 * @property \Illuminate\Database\Eloquent\Relations\HasOne $recommended_cms
 */
class Video extends BaseModel implements SerializableModel, FileableModel
{
    use SoftDeletes;

    use HasFile;
    use HasComment;
    use HasRating;
    use HasLike;
    use HasPlayedRecent;
    use HasFavorite;

    protected $table = 'videos';

    protected $fillable = [
        'title',
        'title_mm',
        'slug',
        'duration',
        'trailer_url',
        'full_url',
        'story_line',
        'released_date',
        'budget',
        'country_id',
        'director_id',
        'production_company_id',
        'published_at',
        'viewed',
        'price',
        'notification',
    ];

    const FULL_VIDEO = 'full_video';
    const TRAILER_VIDEO = 'trailer_video';

    const FULL_URL = 'full_url';
    const TRAILER_URL = 'trailer_url';

    public function setReleasedDateAttribute($value)
    {
        $this->attributes['released_date'] = Carbon::parse($value)->format('Y-m-d');
    }

    public function setPublishedAtAttribute($value)
    {
        $this->attributes['published_at'] = $value ? Carbon::parse($value)->format('Y-m-d') : null;
    }

    public function playlists()
    {
        return $this->belongsToMany(SeriesVideo::class, 'series_videos', 'video_id', 'series_id');
    }

    public function recommended_cms()
    {
        return $this->hasOne(VideoRecommendedCms::class);
    }

    public function new_released_cms()
    {
        return $this->hasOne(NewReleasedVideo::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'production_company_id');
    }

    public function director()
    {
        return $this->belongsTo(Director::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'video_category', 'video_id', 'category_id');
    }

    public function sub_categories()
    {
        return $this->belongsToMany(SubCategory::class, 'video_sub_categories', 'video_id', 'sub_category_id');
    }

    public function stars()
    {
        return $this->belongsToMany(Cast::class, 'video_cast', 'video_id', 'cast_id');
    }

    public static function track_view(Video $video)
    {
        if (!Session::has('view_track') || Session::get('view_track') !== Request::path()) {

            $video->increment('viewed');

            Session::put('view_track', Request::path());

            return ['message' => 'saved'];
        } else {
            return ['message' => Session::get('view_track')];
        }
    }

    public function getSingleImageUrl()
    {
        return $this->files->count() ? $this->files->first()->getFileUrl() : url('/frontend/images/r1.jpg');
    }

    public function getPublishedAt()
    {
        return $this->published_at;
    }

    public function getCategories(): array
    {
        $output = [];
        foreach ($this->categories as $category) {
            if ($category instanceof Category) {
                array_push($output, $category->toOriginal());
            }
        }

        return $output;
    }

    public function getSubCategories(): array
    {
        $output = [];
        foreach ($this->sub_categories as $subCategory) {
            if ($subCategory instanceof SubCategory) {
                array_push($output, $subCategory->toOriginal());
            }
        }

        return $output;
    }

    /**
     * @description stars mean casts.
     * @return array
     * @todo
     */
    public function getStars(): array
    {
        $output = [];
        foreach ($this->stars as $cast) {
            if ($cast instanceof Cast) {
                array_push($output, $cast->toOriginal());
            }
        }

        return $output;
    }

    public function getThumbnails()
    {
        $imageOptions = [
            'height'  => 100,
            'quality' => 50,
            'crop'    => 'thumb',
        ];

        $images = $this->getThumbnailUrl($imageOptions);
        if (empty($images)) {
            $images = url('/frontend/images/r1.jpg');
        }

        return $images;
    }

    /**
     * @desc provide only in local
     * @return \Doctrine\DBAL\Schema\Column|null
     * @todo
     */
    public function getTrailerUrl()
    {
        if (Storage::disk('public')->has($this->trailer_url)) {
            return $this->trailer_url;
        }

        return null;
    }

    public function getPreviewTrailer()
    {
        if (Storage::disk('public')->has($this->trailer_url)) {
            return Storage::disk('public')->url($this->trailer_url);
        }

        return null;
    }

    /**
     * @desc provide only in local
     * @return \Doctrine\DBAL\Schema\Column|null
     * @todo
     */
    public function getFullUrl()
    {
        if (Storage::disk('public')->has($this->full_url)) {
            return $this->full_url;
        }

        return null;
    }

    public function getPreviewFull()
    {
        if (Storage::disk('public')->has($this->full_url)) {
            return Storage::disk('public')->url($this->full_url);
        }

        return null;
    }

    public function getCountry()
    {
        $country = $this->country;
        if ($country instanceof Country) {
            return $country->toOriginal();
        }

        return null;
    }

    public function getCompany()
    {
        $company = $this->company;

        if ($company instanceof Company) {
            return $company->toOriginal();
        }

        return null;
    }

    public function getDirector()
    {
        $director = $this->director;
        if ($director instanceof Director) {
            return $director->toOriginal();
        }

        return null;
    }

    public function toCustom(): array
    {
        $series = $this->playlists()->first();
        if ($series instanceof SeriesVideo) {
            $data = $series->toAll();
            $data['video'] = $this->toOriginal();

            return $data;
        }

        return [];
    }

    public function getShareUrl()
    {
        $series = $this->playlists()->first();
        if ($series instanceof SeriesVideo) {
            $url = route('series.show', ['series_id' => $series->getId(), 'video_id' => $this->getId()]);

            return $url;
        }

        return null;
    }

    public function toOriginal(): array
    {
        return [
            'id'                    => $this->getId(),
            'title_en'              => $this->title,
            'title_mm'              => $this->title_mm,
            'slug'                  => $this->slug,
            'duration'              => $this->duration,
            'country_id'            => $this->country_id,
            'trailer_url'           => $this->trailer_url,
            'preview_trailer'       => $this->getPreviewTrailer(),
            'full_url'              => $this->full_url,
            'preview_full'          => $this->getPreviewFull(),
            'story_line'            => $this->story_line,
            'released_date'         => $this->released_date,
            'budget'                => $this->budget,
            'director_id'           => $this->director_id,
            'production_company_id' => $this->production_company_id,
            'published_at'          => $this->getPublishedAt(),
            'categories'            => $this->getCategories(),
            'sub_categories'        => $this->getSubCategories(),
            'casts'                 => $this->getStars(),
            //'images'                => [
            //    'thumbnail' => $this->getThumbnails(),
            //    'original'  => $this->getSingleImageUrl(),
            //],
            'images'                => $this->getFiles(),
            'viewed'                => $this->viewed,
            'comment_count'         => $this->getCommentsCount(),
            'ratings'               => $this->getRatings(),
            'like_count'            => $this->getLikesCount(),
            'price'                 => $this->price,
            'notification'          => $this->notification,
            'original_image'        => $this->getSingleImageUrl(),
            'share_url'             => $this->getShareUrl(),
        ];
    }

    public function toAll(): array
    {
        return [
            'id'                    => $this->getId(),
            'title_en'              => $this->title,
            'title_mm'              => $this->title_mm,
            'slug'                  => $this->slug,
            'duration'              => $this->duration,
            'country_id'            => $this->country_id,
            'country'               => $this->getCountry(),
            'preview_trailer'       => $this->getPreviewTrailer(),
            'trailer_url'           => $this->trailer_url,
            'full_url'              => $this->full_url,
            'preview_full'          => $this->getPreviewFull(),
            'story_line'            => $this->story_line,
            'released_date'         => $this->released_date,
            'budget'                => $this->budget,
            'director_id'           => $this->director_id,
            'director'              => $this->getDirector(),
            'production_company_id' => $this->production_company_id,
            'company'               => $this->getCompany(),
            'casts'                 => $this->getStars(),
            'published_at'          => $this->getPublishedAt(),
            'categories'            => $this->getCategories(),
            'sub_categories'        => $this->getSubCategories(),
            'viewed'                => $this->viewed,
            'comment_count'         => $this->getCommentsCount(),
            'comments'              => $this->getDetailComments(),
            'ratings'               => $this->getRatings(),
            'like_count'            => $this->getLikesCount(),
            'likes'                 => $this->getLikes(),
            'images'                => $this->getFiles(),
            'thumb_images'          => $this->getThumbnails(),
            'price'                 => $this->price,
            'notification'          => $this->notification,
            'original_image'        => $this->getSingleImageUrl(),
            'share_url'             => $this->getShareUrl(),
        ];
    }
}
