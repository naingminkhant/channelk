<?php

namespace App\Models;

use App\Transformers\VideoTransformer;
use Arga\Storage\Cloudinary\HasFile;
use Arga\Storage\Database\BaseModel;
use Arga\Storage\Database\Contracts\SerializableModel;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property \Doctrine\DBAL\Schema\Column slug
 * @property \Doctrine\DBAL\Schema\Column title
 * @property \Illuminate\Database\Eloquent\Relations\HasMany $sub_categories
 * @property \Illuminate\Database\Eloquent\Relations\BelongsToMany $videos
 */
class Category extends BaseModel implements SerializableModel
{
    use SoftDeletes;
    use HasFile;

    protected $table = 'categories';

    protected $fillable = [
        'slug',
        'title',
    ];

    public function sub_categories()
    {
        return $this->hasMany(SubCategory::class);
    }

    public function videos()
    {
        return $this->belongsToMany(Video::class, 'video_category', 'category_id', 'video_id');
    }

    public function series()
    {
        return $this->belongsToMany(SeriesVideo::class, 'series_category', 'category_id', 'series_id');
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getThumbnailUrl($options = []): ?string
    {
        if ($this->latest_file) {
            $options = [
                'height'  => 150,
                'width'   => 150,
                'quality' => 50,
                'crop'    => 'thumb',
            ];

            return $this->latest_file->getThumbnailUrl($options);
        }

        return null;
    }

    public function getFile(): ?array
    {
        return optional($this->latest_file)->toOriginal();
    }

    public function getSingleImageUrl()
    {
        return $this->files->count() ? $this->files->first()->getFileUrl() : url('/images/chanel-1.png');
    }

    public function getSubCategories(): array
    {
        $output = [];

        foreach ($this->sub_categories as $subCategory) {
            if ($subCategory instanceof SubCategory) {
                array_push($output, $subCategory->toOriginal());
            }
        }

        return $output;
    }

    public function getVideos()
    {
        $data = $this->videos()->whereHas('playlists')->paginate();

        return $this->collection($data, new VideoTransformer(false, null,true));
    }

    public function toOriginal(): array
    {
        return [
            'id'        => $this->getId(),
            'slug'      => $this->getSlug(),
            'title'     => $this->getTitle(),
            'thumbnail' => $this->getThumbnailUrl(),
        ];
    }

    public function withSubCategory(): array
    {
        return [
            'id'             => $this->getId(),
            'slug'           => $this->getSlug(),
            'title'          => $this->getTitle(),
            'sub_categories' => $this->getSubCategories(),
            'images'         => [
                'thumbnail' => $this->getThumbnailUrl(),
                'original'  => $this->getFileUrl(),
            ],
        ];
    }

    public function withVideo(): array
    {
        return [
            'id'     => $this->getId(),
            'slug'   => $this->getSlug(),
            'title'  => $this->getTitle(),
            'videos' => $this->getVideos(),
        ];
    }

    public function toAll(): array
    {
        return [
            'id'             => $this->getId(),
            'slug'           => $this->getSlug(),
            'title'          => $this->getTitle(),
            'sub_categories' => $this->getSubCategories(),
            'videos'         => $this->getVideos(),
            'thumbnail'      => $this->getThumbnailUrl(),
            'image'          => $this->getFile(),
        ];
    }
}
