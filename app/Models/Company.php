<?php

namespace App\Models;

use Arga\Storage\Database\BaseModel;
use Arga\Storage\Database\Contracts\SerializableModel;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Company
 *
 * @property \Doctrine\DBAL\Schema\Column slug
 * @property \Doctrine\DBAL\Schema\Column name
 * @property \Doctrine\DBAL\Schema\Column web_url
 * @property \Doctrine\DBAL\Schema\Column description
 */
class Company extends BaseModel implements SerializableModel
{
    use SoftDeletes;

    protected $table = 'production_companies';

    protected $fillable = [
        'slug',
        'name',
        'web_url',
        'description',
    ];

    public function toOriginal(): array
    {
        return [
            'id'          => $this->getId(),
            'slug'        => $this->slug,
            'name'        => $this->name,
            'web_url'     => $this->web_url,
            'description' => $this->description,
        ];
    }

    public function toAll(): array
    {
        return [
            'id'          => $this->getId(),
            'slug'        => $this->slug,
            'name'        => $this->name,
            'web_url'     => $this->web_url,
            'description' => $this->description,
        ];
    }
}
