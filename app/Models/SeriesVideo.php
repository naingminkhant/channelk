<?php

namespace App\Models;

use App\Service\Traits\HasComment;
use App\Service\Traits\HasFavorite;
use App\Service\Traits\HasLike;
use App\Service\Traits\HasPlayedRecent;
use App\Service\Traits\HasRating;
use Arga\Storage\Cloudinary\FileableModel;
use Arga\Storage\Cloudinary\HasFile;
use Arga\Storage\Database\BaseModel;
use Arga\Storage\Database\Contracts\SerializableModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

/**
 * Class SeriesVideo
 *
 * @property \Doctrine\DBAL\Schema\Column title
 * @property \Doctrine\DBAL\Schema\Column title_mm
 * @property \Doctrine\DBAL\Schema\Column slug
 * @property \Doctrine\DBAL\Schema\Column country_id
 * @property \Doctrine\DBAL\Schema\Column trailer_url
 * @property \Doctrine\DBAL\Schema\Column story_line
 * @property \Doctrine\DBAL\Schema\Column released_date
 * @property \Doctrine\DBAL\Schema\Column budget
 * @property \Doctrine\DBAL\Schema\Column director_id
 * @property \Doctrine\DBAL\Schema\Column production_company_id
 * @property \Doctrine\DBAL\Schema\Column published_at
 * @property \Doctrine\DBAL\Schema\Column viewed
 * @property \Illuminate\Database\Eloquent\Relations\BelongsTo $country
 * @property \Illuminate\Database\Eloquent\Relations\BelongsTo $director
 * @property \Illuminate\Database\Eloquent\Relations\BelongsTo $company
 * @property \Illuminate\Database\Eloquent\Relations\BelongsToMany $categories
 * @property \Illuminate\Database\Eloquent\Relations\BelongsToMany $stars
 * @property \Illuminate\Database\Eloquent\Relations\BelongsToMany $videos
 */
class SeriesVideo extends BaseModel implements SerializableModel, FileableModel
{
    use SoftDeletes;
    use HasFile;
    use HasComment;
    use HasRating;
    use HasLike;
    use HasPlayedRecent;
    use HasFavorite;

    protected $table = 'series';

    protected $fillable = [
        'title',
        'title_mm',
        'slug',
        'country_id',
        'trailer_url',
        'story_line',
        'released_date',
        'budget',
        'director_id',
        'production_company_id',
        'published_at',
        'viewed',
    ];

    public function setReleasedDateAttribute($value)
    {
        $this->attributes['released_date'] = Carbon::parse($value)->format('Y-m-d');
    }

    public function setPublishedAtAttribute($value)
    {
        $this->attributes['published_at'] = $value ? Carbon::parse($value)->format('Y-m-d') : null;
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function director()
    {
        return $this->belongsTo(Director::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'production_company_id');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'series_category', 'series_id', 'category_id');
    }

    public function stars()
    {
        return $this->belongsToMany(Cast::class, 'series_cast', 'series_id', 'cast_id');
    }

    public function videos()
    {
        return $this->belongsToMany(Video::class, 'series_videos', 'series_id', 'video_id');
    }

    public function recommended_cms()
    {
        return $this->hasOne(SeriesRecommendedCms::class, 'series_id');
    }

    public function cms()
    {
        return $this->hasOne(SeriesCms::class, 'series_id');
    }

    public function getSingleImageUrl()
    {
        return $this->files->count() ? $this->files->first()->getFileUrl() : url('/frontend/images/r1.jpg');
    }

    public function getVideos(): array
    {
        $output = [];
        foreach ($this->videos as $video) {
            if ($video instanceof Video) {
                array_push($output, $video->toOriginal());
            }
        }

        return $output;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * @deprecated
     */
    public function episodes()
    {
        return $this->hasMany(Episode::class, 'series_id');
    }

    /**
     * @return array
     * @deprecated
     */
    public function getEpisodes()
    {
        $output = [];
        foreach ($this->episodes as $episode) {
            if ($episode instanceof Episode) {
                array_push($output, $episode->toAll());
            }
        }

        return $output;
    }

    public function getCategories(): array
    {
        $output = [];
        foreach ($this->categories as $category) {
            if ($category instanceof Category) {
                array_push($output, $category->toOriginal());
            }
        }

        return $output;
    }

    /**
     * @description stars mean casts.
     * @return array
     * @todo
     */
    public function getStars(): array
    {
        $output = [];
        foreach ($this->stars as $cast) {
            if ($cast instanceof Cast) {
                array_push($output, $cast->toOriginal());
            }
        }

        return $output;
    }

    public function getThumbnails()
    {
        $imageOptions = [
            'height'  => 100,
            'quality' => 50,
            'crop'    => 'thumb',
        ];

        $images = $this->getThumbnailUrl($imageOptions);
        if (empty($images)) {
            $images = url('/frontend/images/r1.jpg');
        }

        return $images;
    }

    /**
     * @desc provide only local
     * @return \Doctrine\DBAL\Schema\Column|null
     * @todo
     */
    public function getTrailerUrl()
    {
        if (Storage::disk('public')->has($this->trailer_url)) {
            return $this->trailer_url;
        }

        return null;
    }

    public function getPreviewTrailer()
    {
        if (Storage::disk('public')->has($this->trailer_url)) {
            return Storage::disk('public')->url($this->trailer_url);
        }

        return null;
    }

    public function getCountry()
    {
        $country = $this->country;
        if ($country instanceof Country) {
            return $country->toOriginal();
        }

        return null;
    }

    public function getCompany()
    {
        $company = $this->company;

        if ($company instanceof Company) {
            return $company->toOriginal();
        }

        return null;
    }

    public function getDirector()
    {
        $director = $this->director;
        if ($director instanceof Director) {
            return $director->toOriginal();
        }

        return null;
    }

    public function toOriginal(): array
    {
        return [
            'id'                   => $this->getId(),
            'title_en'             => $this->title,
            'title_mm'             => $this->title_mm,
            'slug'                 => $this->slug,
            'country_id'           => $this->country_id,
            'trailer_url'          => $this->trailer_url,
            'preview_trailer'      => $this->getPreviewTrailer(),
            'story_line'           => $this->story_line,
            'release_date'         => $this->released_date,
            'budget'               => $this->budget,
            'director_id'          => $this->director_id,
            'prodution_company_id' => $this->production_company_id,
            'published_at'         => $this->published_at,
            'categories'           => $this->getCategories(),
            'casts'                => $this->getStars(),
            'director'             => $this->getDirector(),
            'original_image'       => $this->getSingleImageUrl(),
            'cover_image'          => $this->getCoverImage(),
            //'images'               => [
            //    'thumbnail' => $this->getThumbnails(),
            //    'original'  => $this->getSingleImageUrl(),
            //],
            'images'               => $this->getFiles(),
            'videos'               => $this->getVideos(),
            'viewed'               => $this->viewed,
            'comment_count'        => $this->getCommentsCount(),
            'ratings'              => $this->getRatings(),
            'like_count'           => $this->getLikesCount(),
        ];
    }

    public function toAll(): array
    {
        return [
            'id'                    => $this->getId(),
            'title_en'              => $this->title,
            'title_mm'              => $this->title_mm,
            'slug'                  => $this->slug,
            'country_id'            => $this->country_id,
            'country'               => $this->getCountry(),
            'trailer_url'           => $this->trailer_url,
            'story_line'            => $this->story_line,
            'released_date'         => $this->released_date,
            'budget'                => $this->budget,
            'director_id'           => $this->director_id,
            'director'              => $this->getDirector(),
            'production_company_id' => $this->production_company_id,
            'company'               => $this->getCompany(),
            'casts'                 => $this->getStars(),
            'published_at'          => $this->published_at,
            'images'                => $this->getFiles(),
            'cover_image'           => $this->getCoverImage(),
            'categories'            => $this->getCategories(),
            'videos'                => $this->getVideos(),
            'viewed'                => $this->viewed,
            'comment_count'         => $this->getCommentsCount(),
            'comments'              => $this->getDetailComments(),
            'ratings'               => $this->getRatings(),
            'like_count'            => $this->getLikesCount(),
            'likes'                 => $this->getLikes(),
            'original_image'        => $this->getSingleImageUrl(),
        ];
    }
}
