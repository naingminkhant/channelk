<?php

namespace App\Models;

use Arga\Storage\Database\BaseModel;

/**
 * @property \Doctrine\DBAL\Schema\Column body
 */
class Support extends BaseModel
{
    protected $table = 'support';

    protected $fillable = [
        'body',
    ];
}
