<?php

namespace App\Models;

use Arga\Storage\Database\Contracts\SerializableModel;
use Illuminate\Database\Eloquent\Model;

/**
 * @property \Doctrine\DBAL\Schema\Column video_id
 * @property \Illuminate\Database\Eloquent\Relations\BelongsTo $video
 */
class NewReleasedVideo extends Model implements SerializableModel
{
    protected $table = 'new_released_videos';

    protected $fillable = [
        'video_id',
    ];

    public function video()
    {
        return $this->belongsTo(Video::class);
    }

    public function getVideo()
    {
        if ($this->video instanceof Video) {
            return optional($this->video)->toOriginal();
        }

        return null;
    }

    public function toOriginal(): array
    {
        return $this->getVideo();
    }

    public function toAll(): array
    {
        return $this->getVideo() ?? [];
    }

    public function toCustom(): array
    {
        if ($this->video instanceof Video) {
            return $this->video->toCustom();
        }

        return [];
    }
}
