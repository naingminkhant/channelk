<?php

namespace App\QueryFilters;

use Arga\Storage\Database\Contracts\QueryFilter;
use Illuminate\Database\Eloquent\Builder;

class NotificationPublishedQueryFilter implements QueryFilter
{
    public function apply(Builder $query): Builder
    {
        return $query->whereNotNull('published_at');
    }
}
