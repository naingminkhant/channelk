<?php

namespace App\QueryFilters;

use Arga\Storage\Database\Contracts\QueryFilter;
use Illuminate\Database\Eloquent\Builder;

class UserQueryFilter implements QueryFilter
{
    private $filter;

    public function __construct($filter = [])
    {
        $this->filter = $filter;
    }

    public function apply(Builder $query): Builder
    {
        if ($q = array_get($this->filter, 'q')) {
            if (checkEmail($q)) {
                $query = $query->where('email', $q);
            } else {
                $query = $query
                    ->where('name', 'like', '%'.$q.'%')
                    ->orWhere('email', 'like', '%'.$q.'%');
            }
        }

        return $query;
    }
}
