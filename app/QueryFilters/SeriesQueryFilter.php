<?php

namespace App\QueryFilters;

use Arga\Storage\Database\Contracts\QueryFilter;
use Illuminate\Database\Eloquent\Builder;

class SeriesQueryFilter implements QueryFilter
{
    protected $q;

    public function __construct(string $q = null)
    {
        $this->q = $q;
    }

    public function apply(Builder $query): Builder
    {
        return $query
            ->where('slug', 'LIKE', '%'.$this->q.'%')
            ->orWhere("title", "LIKE", '%'.$this->q.'%');
    }
}
