<?php

namespace App\Providers;

use App\Models\Cast;
use App\Models\Category;
use App\Models\Director;
use App\Models\Info;
use App\Models\SeriesVideo;
use App\Models\Video;
use Arga\Auth\User;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Validator;
use Auth;
use Hash;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        $this->loadHelper();

        Relation::morphMap([
            'user'     => User::class,
            'director' => Director::class,
            'cast'     => Cast::class,
            'video'    => Video::class,
            'series'   => SeriesVideo::class,
            'info'     => Info::class,
            'category' => Category::class,
        ]);

        Validator::extend('password_hash_check', function($attribute, $value, $parameters, $validator) {
            return Hash::check($value, Auth::user()->password);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public function loadHelper()
    {
        require_once(__DIR__."/../Helpers/message.php");
    }
}
