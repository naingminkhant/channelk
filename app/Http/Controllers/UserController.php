<?php

namespace App\Http\Controllers;

use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\CommentCreateRequest;
use App\Http\Requests\UpdateAccountInfo;
use App\Http\Resources\CommentResource;
use App\Models\ChannelNotification;
use App\Models\Comment;
use App\Models\Favorite;
use App\Models\Like;
use App\Models\Video;
use Arga\Auth\User;
use Arga\Notification\Notification;
use Auth;

class UserController extends Controller
{

    /**
     * display profile page.
     * @return bool
     * @throws \Illuminate\Validation\ValidationException
     */
    public function profile()
    {
        $user = User::with([
            'favorite_videos',
            'recent_videos' => function($query) {
                $query->limit(6);
            },
        ])->find(Auth::id());

        $favorite_videos = $user->favorite_videos;
        $recent_videos = $user->recent_videos;

        return view('frontend.pages.profile', compact('favorite_videos', 'recent_videos'));
    }

    /**
     * display setting page.
     * @return bool
     * @throws \Illuminate\Validation\ValidationException
     */
    public function setting()
    {
        return view('frontend.pages.setting');
    }

    /**
     * Update the user's info
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function editProfile(UpdateAccountInfo $request)
    {
        Auth::user()->update($request->validated());

        return redirect('setting')->with('status', 'success')->with('message', 'Account Information updated!');
    }

    /**
     * Change the auth user's password.
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function passwordChange(ChangePasswordRequest $request)
    {
        $user = Auth::user();
        $user->password = bcrypt($request->get('password'));
        $user->save();

        return redirect('setting')->with('status', 'success')->with('message', 'Password updated!');
    }

    public function getComments($video_id)
    {
        $video = Video::with(['comments', 'comments.user'])->find($video_id);

        return CommentResource::collection($video->comments);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     * @throws \Illuminate\Validation\ValidationException
     */
    public function storeVideoComment(CommentCreateRequest $request, $video_id)
    {
        $video = Video::findOrFail($video_id);

        $data = $request->validated();

        $video->comment()->create([
            'comment' => $data['comment'],
            'user_id' => Auth::id(),
        ]);

        $video = Video::with(['comments', 'comments.user'])->find($video_id);

        return CommentResource::collection($video->comments);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroyComment($id)
    {
        $comment = Comment::findOrFail($id);

        if (Auth::id() === $comment->user_id) {
            $comment->delete();
            return response()->json(['status' => 'success']);
        }

        return response()->json(['status' => 'error', 'message' => 'unauthorized'], 401);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return bool
     * @throws \Illuminate\Validation\ValidationException
     */
    public function syncVideoFavorite($video_id)
    {
        $video = Video::findOrFail($video_id);

        if ($video->hasFavorite()) {
            $video->favorite()->where('user_id', Auth::id())->delete();
            return response()->json([ 'isFavorite' => false]);
        } else {
            $video->favorite()->where('user_id', Auth::id())->firstOrCreate([
                'user_id' => Auth::id(),
            ]);
            return response()->json([ 'isFavorite' => true]);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroyVideoFavorite($id)
    {
        $favorite = Favorite::findOrFail($id);

        if (Auth::id() === $favorite->user_id) {
            $favorite->delete();
            return response()->json(['status' => 'success']);
        }

        return response()->json(['status' => 'error', 'message' => 'unauthorized'], 401);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return bool
     * @throws \Illuminate\Validation\ValidationException
     */
    public function syncVideoLike($video_id)
    {
        $video = Video::findOrFail($video_id);

        if ($video->isAlreadyLike()) {
            $video->likes()->where('user_id', Auth::id())->delete();
            return response()->json([ 'isFavorite' => false]);
        } else {
            $video->likes()->where('user_id', Auth::id())->firstOrCreate([
                'user_id' => Auth::id(),
            ]);
            return response()->json([ 'isLike' => true]);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroyVideoLike($id)
    {
        $like = Like::findOrFail($id);

        if (Auth::id() === $like->user_id) {
            $like->delete();
            return response()->json(['status' => 'success']);
        }

        return response()->json(['status' => 'error', 'message' => 'unauthorized'], 401);
    }

    public function notification($id) {

        $notification = ChannelNotification::findOrFail($id);

        return view('frontend.pages.notification', compact('notification'));
    }

    public function readNotification() {

        Notification::whereNull('read_at')->where('model_id', Auth::id())->update(['read_at' => now()]);

        return response()->json(['message' => 'success']);
    }
}
