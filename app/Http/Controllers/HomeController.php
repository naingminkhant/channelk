<?php

namespace App\Http\Controllers;

use App\Models\AboutUs;
use App\Models\Banner;
use App\Models\Cast;
use App\Models\Category;
use App\Models\ContactUs;
use App\Models\Faq;
use App\Models\Info;
use App\Models\SeriesVideo;
use App\Models\Support;
use App\Models\Video;

class HomeController extends Controller
{
    /**
     * Display the home page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banners = Banner::all();

        $new_videos = Video::select('videos.*')
            ->join('new_released_videos', 'videos.id', 'new_released_videos.video_id')
            ->orderBy('new_released_videos.id')
            ->whereHas('playlists')
            ->with(['playlists'])
            ->limit(6)
            ->get();

        $series_recommended = SeriesVideo::select('series.*')
            ->join('series_recommended_cms', 'series.id', 'series_recommended_cms.series_id')
            ->orderBy('series_recommended_cms.id')
            ->limit(6)
            ->get();

        $tv_shows = SeriesVideo::select('series.*')
            ->join('series_cms', 'series.id', 'series_cms.series_id')
            ->orderBy('series_cms.id')
            ->limit(6)
            ->get();

        $hot_videos = Video::limit(6)->whereHas('playlists')->orderByDesc('viewed')->get();

        $hot_series = SeriesVideo::limit(6)->orderByDesc('viewed')->get();

        $recommended_videos = Video::select('videos.*')->join('video_recommended_cms', 'videos.id', 'video_recommended_cms.video_id')
            ->whereHas('playlists')
            ->orderBy('video_recommended_cms.id')
            ->limit(6)
            ->get();

        $castors = Cast::select('casts.*')
            ->join('castor_cms', 'casts.id', 'castor_cms.cast_id')
            ->orderBy('castor_cms.id')
            ->limit(6)
            ->get();

        $categories = Category::limit(16)->inRandomOrder()->get();

        return view('frontend.pages.home', compact('banners', 'new_videos', 'series_recommended', 'tv_shows', 'hot_videos', 'hot_series', 'recommended_videos', 'castors', 'categories'));
    }

    public function live()
    {
        $text = Info::select('info_one')->first();

        return view('frontend.pages.live', compact('text'));
    }

    public function privacy() {
        $info = Info::select('info_two')->first();

        return view('frontend.pages.privacy-policy', compact('info'));
    }

    public function termsAndConditions() {
        $info = Info::select('info_three')->first();

        return view('frontend.pages.terms-and-conditions', compact('info'));
    }

    public function aboutUsPage() {
        $data = AboutUs::first();

        return view('frontend.pages.about-us', compact('data'));
    }

    public function contactUsPage() {
        $data = ContactUs::first();

        return view('frontend.pages.contact-us', compact('data'));
    }

    public function faqPage() {
        $data = Faq::first();

        return view('frontend.pages.faq', compact('data'));
    }

    public function supportGuidePage() {
        $data = Support::first();

        return view('frontend.pages.support-guide', compact('data'));
    }
}
