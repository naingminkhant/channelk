<?php

namespace App\Http\Controllers;

use Storage;

class DownloadAppController extends Controller
{
    /**
     * Download the android apk.
     *
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function android()
    {
        return Storage::download('public/channelk-1.2.4.apk', null, [
            'Pragma' => 'public',
            'Expires' => 0,
            'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
            'Content-Description' => 'File Transfer',
            'Content-Type' => 'application/vnd.android.package-archive',
            'Content-Transfer-Encoding' => 'binary'
        ]);
    }

}
