<?php

namespace App\Http\Controllers;

use App\Models\Cast;
use App\Models\Category;
use App\Models\ChannelAgenda;
use App\Models\PlayedRecent;
use App\Models\SeriesVideo;
use App\Models\Video;
use Carbon\Carbon;
use DB;
use Auth;

class PagesController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function video($id)
    {
        $video = Video::findOrFail($id);

        $upcoming_videos = Video::whereExists(function ($query) {
            $query->select(DB::raw(1))
                ->from('series_videos')
                ->whereRaw('series_videos.video_id != videos.id');
        })->limit(10)->get();

        return view('frontend.pages.video.show', compact('video', 'upcoming_videos'));
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function series($id)
    {
        $series = SeriesVideo::with(['videos'])->findOrFail($id);

        $series->increment('viewed');

        $videos = Video::where('series_id', $id)
            ->join('series_videos', 'videos.id', '=', 'series_videos.video_id')
            ->paginate();

        return view('frontend.pages.series.index', compact('series', 'videos'));
    }


    public function seriesDetail($series_id, $video_id) {
        $series = SeriesVideo::findOrFail($series_id);

        $video = Video::join('series_videos', 'videos.id', '=', 'series_videos.video_id')->with(['comments', 'comments.user'])->findOrFail($video_id);

        Video::track_view($video);

        if (Auth::check()) {
            PlayedRecent::track($video);
        }

        $sort = upcoming_id($video->sort);

        $upcoming_videos = Video::where('series_id', $series_id)
            ->where('series_videos.sort', '>=', $sort)
            ->join('series_videos', 'videos.id', '=', 'series_videos.video_id')
            ->limit(10)
            ->get();

        return view('frontend.pages.series.show', compact('series', 'video', 'upcoming_videos'));
    }


    public function new_releases() {
        $new_videos = Video::select('videos.*', 'new_released_videos.created_at')->join('new_released_videos', 'videos.id', 'new_released_videos.video_id')
            ->orderByDesc('new_released_videos.created_at')
            ->whereHas('playlists')
            ->get();

        return view('frontend.pages.new-releases', compact('new_videos'));
    }


    public function series_recommended() {
        $series_recommended = SeriesVideo::select('series.*', 'series_recommended_cms.created_at')->join('series_recommended_cms', 'series.id', 'series_recommended_cms.series_id')
            ->orderByDesc('series_recommended_cms.created_at')
            ->get();

        return view('frontend.pages.series-recommended', compact('series_recommended'));
    }


    public function tv_show() {
        $tv_shows = SeriesVideo::select('series.*', 'series_cms.created_at')->join('series_cms', 'series.id', 'series_cms.series_id')
            ->orderByDesc('series_cms.created_at')
            ->get();

        return view('frontend.pages.tv-shows', compact('tv_shows'));
    }


    public function hot_videos() {
        $hot_videos = Video::limit(20)->whereHas('playlists')->orderByDesc('viewed')->get();

        return view('frontend.pages.hot-video', compact('hot_videos'));
    }

    public function hot_series() {
        $hot_series = SeriesVideo::limit(20)->orderByDesc('viewed')->get();

        return view('frontend.pages.hot-series', compact('hot_series'));
    }

    public function recommended_videos() {
        $recommended_videos = Video::select('videos.*', 'video_recommended_cms.created_at')->join('video_recommended_cms', 'videos.id', 'video_recommended_cms.video_id')
            ->whereHas('playlists')
            ->limit(6)
            ->orderByDesc('video_recommended_cms.created_at')
            ->get();

        return view('frontend.pages.recommended-videos', compact('recommended_videos'));
    }

    public function seriesByCategory($id) {
        $category = Category::with('series')->findOrFail($id);

        $series = $category->series;

        return view('frontend.pages.series.by-category', compact('category', 'series'));
    }


    public function recommended_castors() {
        $castors = Cast::select('casts.*')
            ->join('castor_cms', 'casts.id', 'castor_cms.cast_id')
            ->orderBy('castor_cms.id')
            ->paginate(8);

        return view('frontend.pages.recommended-castors', compact('castors'));
    }

    public function castors() {
        $castors = Cast::select('casts.*')
            ->orderByDesc('created_at')
            ->paginate(8);

        return view('frontend.pages.castors', compact('castors'));
    }

    public function castor($id) {
        $castor = Cast::findOrFail($id);

        $videos = Video::select('videos.*')
            ->join('video_cast', 'videos.id', 'video_cast.video_id')
            ->where('video_cast.cast_id', $id)
            ->with('playlists')
            ->paginate(8);

        return view('frontend.pages.castor', compact('castor', 'videos'));
    }


    public function recommended_category() {
        $categories = Category::limit(10)->inRandomOrder()->get();

        return view('frontend.pages.recommended-categories', compact('categories'));
    }

    public function categories() {
        $categories = Category::orderByDesc('created_at')->paginate(9);

        return view('frontend.pages.recommended-categories', compact('categories'));
    }

    public function agenda($date) {

        $date = new Carbon($date);

        $agenda = ChannelAgenda::whereDate('date', $date->toDateString())->first();

        if (is_null($agenda)) {
            $agenda = ChannelAgenda::create([
                'date' => $date->toDateString(),
                'agendas' => []
            ]);
        }

        return view('frontend.pages.agenda', compact('agenda'));
    }
}
