<?php

namespace App\Http\Controllers\Api;

use App\Repositories\DataRepo;
use App\Http\Controllers\Controller;
use App\Transformers\NewReleasedVideoTransformer;
use App\Transformers\VideoRecommendedTransformer;
use App\Transformers\VideoTransformer;
use Illuminate\Http\Request;

class CmsController extends Controller
{
    protected $recommended_video;

    protected $recommended_playlist;

    protected $new_released_video;

    protected $playlist_cms;

    protected $cast_cms;

    protected $video;

    public function __construct()
    {
        $this->recommended_video = DataRepo::video_recommended_cms();
        $this->recommended_playlist = DataRepo::series_recommended_cms();
        $this->new_released_video = DataRepo::new_released_video();
        $this->playlist_cms = DataRepo::series_cms();
        $this->cast_cms = DataRepo::castor_cms();
        $this->video = DataRepo::video();
    }

    public function recommended_video()
    {
        return $this->recommended_video
            ->setTransformer(new VideoRecommendedTransformer(true))
            ->forApi();
    }

    public function recommended_playlist()
    {
        return $this->recommended_playlist->paginate();
    }

    public function new_released_video(Request $request)
    {
        if ($limit = $request->get('limit')) {
            return $this->video
                ->setTransformer(new VideoTransformer(false, null, true))
                ->newReleaseList($limit);
        }

        return $this->new_released_video
            ->setTransformer(new NewReleasedVideoTransformer(true))
            ->forApi();
    }

    public function playlist()
    {
        return $this->playlist_cms->paginate();
    }

    public function cast()
    {
        return $this->cast_cms->paginate();
    }
}
