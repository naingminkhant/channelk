<?php

namespace App\Http\Controllers\Api;

use App\Repositories\ContactUsRepository;
use App\Http\Controllers\Controller;

class ContactUsController extends Controller
{
    protected $repo;

    public function __construct(ContactUsRepository $repo)
    {
        $this->repo = $repo;
    }

    public function index()
    {
        return $this->repo->first();
    }
}
