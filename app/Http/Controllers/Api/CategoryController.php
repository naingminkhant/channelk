<?php

namespace App\Http\Controllers\Api;

use App\Repositories\DataRepo;
use App\Transformers\CategoryTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    protected $repo;

    public function __construct()
    {
        $this->repo = DataRepo::category();
    }

    public function index(Request $request)
    {
        $sub_category = (bool) json_decode($request->get('sub_category'));
        $video = (bool) json_decode($request->get('video'));
        $detail = (bool) json_decode($request->get('detail'));

        if ((bool) json_decode($request->get('paginate'))) {
            $categories = $this->repo
                ->setTransformer(new CategoryTransformer($sub_category, $video, $detail))
                ->paginate();
        } else {
            $categories = $this->repo
                ->setTransformer(new CategoryTransformer($sub_category, $video, $detail))
                ->get();
        }

        return $categories;
    }

    public function show($id)
    {
        return $this->repo
            ->setTransformer(new CategoryTransformer(false, false, true))
            ->find($id);
    }
}
