<?php

namespace App\Http\Controllers\Api;

use App\Repositories\DataRepo;
use App\Transformers\NewReleasedVideoTransformer;
use App\Transformers\VideoTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VideoController extends Controller
{
    private $repo;

    private $new_released;

    public function __construct()
    {
        $this->repo = DataRepo::video();
        $this->new_released = DataRepo::new_released_video();
    }

    public function index(Request $request)
    {
        $detail = $request->get('detail');
        if ($paginate = $request->get('paginate')) {
            $videos = $this->repo
                ->setTransformer(new VideoTransformer($detail))
                ->paginate();
        } else {
            $videos = $this->repo
                ->setTransformer(new VideoTransformer($detail))
                ->get();
        }

        return $videos;
    }

    public function incrementViewed($id)
    {
        $this->repo->incrementViewed($id);

        return ok();
    }

    public function show($id)
    {
        return $this->repo
            ->setTransformer(new VideoTransformer(true))
            ->find($id);
    }

    public function like_comment($id)
    {
        $this->incrementViewed($id);

        return $this->repo->status($id);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function storeRating(Request $request, $id)
    {
        $data = $request->only([
            'rating',
        ]);

        return $this->repo->storeRating($data, $id);
    }

    public function syncLike($id)
    {
        return $this->repo->syncLike($id);
    }

    public function popularList(Request $request)
    {
        $per_page = (int) $request->get('per_page') ?? 6;

        return $this->repo
            ->setTransformer(new VideoTransformer(false, null, true))
            ->popularList($per_page);
    }

    public function newReleaseList(Request $request)
    {
        $per_page = (int) $request->get('per_page') ?? 6;

        return $this->new_released
            ->setTransformer(new NewReleasedVideoTransformer(true))
            ->paginateWithOrder($per_page);
    }

    public function syncPlayedRecent(Request $request, $id)
    {
        $data = $request->only([
            'paused_time',
        ]);

        $this->repo->syncPlayedRecent($data, $id);

        return ok();
    }

    public function playedRecent()
    {
        return $this->repo->getPlayedRecentByUser();
    }

    public function syncFavorite($id)
    {
        $this->repo->syncFavorite($id);

        return ok();
    }

    public function favorite()
    {
        return $this->repo->getFavoriteByUser();
    }

    /**
     * @param $id
     * @return array
     * @deprecated
     */
    public function favoriteByID($id)
    {
        return $this->repo->getFavoriteByUser($id);
    }

    public function likeVideoByID($id)
    {
        return $this->repo->getLikeByUser($id);
    }
}
