<?php

namespace App\Http\Controllers\Api;

use App\Repositories\DataRepo;
use App\Transformers\CastTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CastController extends Controller
{
    protected $repo;

    public function __construct()
    {
        $this->repo = DataRepo::cast();
    }

    public function show($id)
    {
        return $this->repo
            ->setTransformer(new CastTransformer(true))
            ->find($id);
    }
}
