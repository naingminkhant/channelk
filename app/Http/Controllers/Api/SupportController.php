<?php

namespace App\Http\Controllers\Api;

use App\Repositories\SupportRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SupportController extends Controller
{
    protected $repo;

    public function __construct(SupportRepository $repo)
    {
        $this->repo = $repo;
    }

    public function index()
    {
        return $this->repo->first();
    }
}
