<?php

namespace App\Http\Controllers\Api;

use App\Repositories\DataRepo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BannerController extends Controller
{
    protected $repo;

    public function __construct()
    {
        $this->repo = DataRepo::banner();
    }

    public function index()
    {
        return $this->repo->paginate();
    }
}
