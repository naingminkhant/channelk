<?php

namespace App\Http\Controllers\Api\Auth;

use App\Repositories\DataRepo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ForgetPasswordController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->user = DataRepo::user();
    }

    public function request(Request $request)
    {
        $request->validate([
            'email' => 'required|email|exists:users',
        ]);

        return $this->user->update_activation_code($request->get('email'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function reset(Request $request)
    {
        return $this->user->password_reset($request->only([
            'activation_code',
            'new_password',
            'password_confirmation',
        ]));
    }
}
