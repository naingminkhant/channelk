<?php

namespace App\Http\Controllers\Api\Auth;

use App\Repositories\DataRepo;
use Arga\Auth\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Laravel\Socialite\Facades\Socialite;

class SocialController extends Controller
{
    private $user;

    public function __construct()
    {
        $this->user = DataRepo::user();
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function facebook(Request $request)
    {
        $request->validate([
            'social_token' => 'required|string',
        ]);
        $social = Socialite::driver('facebook')->userFromToken($request->get('social_token'));

        $data = [
            'name'            => $social->getName(),
            'email'           => $social->getEmail(),
            'provider_id'     => $social->getId(),
            'avatar_original' => $social->avatar_original,
        ];

        $user = $this->user->saveFromSocial($data, 'facebook');
        $token = $user->createToken(User::PASSPORT_ACCESS_TOKEN_NAME)->accessToken;

        $auth = auth()->user();
        if ($auth instanceof User) {
            return msg_success_login($token, [
                'user' => $auth->toAll(),
            ]);
        }

        return msg_success_login($token);
    }
}
