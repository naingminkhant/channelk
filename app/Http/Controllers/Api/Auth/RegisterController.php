<?php

namespace App\Http\Controllers\Api\Auth;

use App\Repositories\DataRepo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RegisterController extends Controller
{
    private $repo;

    public function __construct()
    {
        $this->repo = DataRepo::user();
    }

    protected function setData(Request $request)
    {
        return $request->only([
            'name',
            'email',
            'password',
            'password_confirmation',
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function register(Request $request)
    {
        $data = $this->setData($request);
        $this->repo->save($data);

        return msg_success_register();
    }
}
