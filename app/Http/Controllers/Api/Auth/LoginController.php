<?php

namespace App\Http\Controllers\Api\Auth;

use Arga\Auth\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;
use Lcobucci\JWT\Parser;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Symfony\Component\HttpFoundation\Response|void
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            $this->sendLockoutResponse($request);

            return;
        }

        if ($this->attemptLogin($request)) {

            $response = $this->sendLoginResponse($request);

            /** @var array $response */
            return $response;
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        //$this->sendFailedLoginResponse($request);

        $error = [
            $this->username() => [trans('auth.failed')],
        ];

        return error($error);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    protected function sendLoginResponse(Request $request)
    {
        $user = auth()->user();

        if ($user instanceof User) {

            $this->clearLoginAttempts($request);

            $token = $user->createToken(User::PASSPORT_ACCESS_TOKEN_NAME)->accessToken;

            return msg_success_login($token, [
                'user' => $user->toAll(),
            ]);
        }

        return msg_unauthorised_login();
    }

    public function logout(Request $request)
    {
        $header = $request->bearerToken();
        $id = (new Parser())->parse($header)->getHeader('jti');
        $token = $request->user()->tokens->find($id);
        $token->revoke();

        return msg_success_logout();
    }
}
