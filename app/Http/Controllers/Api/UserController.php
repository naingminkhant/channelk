<?php

namespace App\Http\Controllers\Api;

use App\Repositories\DataRepo;
use App\Http\Controllers\Controller;
use Arga\Auth\User;
use Arga\Storage\Cloudinary\CloudinaryClient;
use Illuminate\Http\Request;

class UserController extends Controller
{
    private $repo;

    private $image;

    public function __construct()
    {
        $this->repo = DataRepo::user();
        $this->image = app(CloudinaryClient::class);
    }

    public function me()
    {
        return $this->repo->getUser();
    }

    protected function setData(Request $request)
    {
        return $request->only([
            'name',
            'email',
            'current_password',
            'password',
            'password_confirmation',
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request)
    {
        $data = $this->setData($request);
        $file = $request->file('image');
        $this->repo->updateFromApi($data, $file);

        return ok();
    }

    public function searchKeyword()
    {
        if ($this->authorizedUser() instanceof User) {
            return $this->authorizedUser()
                ->search_keywords()
                ->orderByDesc('created_at')
                ->limit(10)->get();
        }
    }

    public function destroySearchKeyword($id)
    {
        if ($this->authorizedUser() instanceof User) {
            $this->authorizedUser()
                ->search_keywords()
                ->where('search_keywords.id', $id)->delete();
            return ok();
        }
    }
}
