<?php

namespace App\Http\Controllers\Api;

use App\Repositories\DataRepo;
use App\Transformers\SeriesTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SeriesController extends Controller
{
    private $repo;

    public function __construct()
    {
        $this->repo = DataRepo::series_video();
    }

    public function index(Request $request)
    {
        $detail = $request->get('detail');
        $paginate = $request->get('paginate');

        if ($paginate) {
            $series = $this->repo
                ->setTransformer(new SeriesTransformer($detail))
                ->paginate();
        } else {
            $series = $this->repo
                ->setTransformer(new SeriesTransformer($detail))
                ->get();
        }

        return $series;
    }

    public function incrementViewed($id)
    {
        $this->repo->incrementViewed($id);

        return ok();
    }

    public function show($id)
    {
        return $this->repo
            ->setTransformer(new SeriesTransformer(true))
            ->find($id);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function storeRating(Request $request, $id)
    {
        return $this->repo->storeRating($request->only([
            'rating',
        ]), $id);
    }

    public function syncLike($id)
    {
        return $this->repo->syncLike($id);
    }

    public function popularList(Request $request)
    {
        $per_page = (int) $request->get('per_page') ?? 6;

        return $this->repo
            ->setTransformer(new SeriesTransformer(true))
            ->popularList($per_page);
    }

    public function newReleaseList(Request $request)
    {
        $per_page = (int) $request->get('per_page') ?? 6;

        return $this->repo
            ->setTransformer(new SeriesTransformer(true))
            ->newReleaseList($per_page);
    }

    public function syncPlayedRecent(Request $request, $id)
    {
        $data = $request->only([
            'paused_time',
        ]);

        $this->repo->syncPlayedRecent($data, $id);

        return ok();
    }

    public function playedRecent()
    {
        return $this->repo->getPlayedRecentByUser();
    }

    public function syncFavorite($id)
    {
        $this->repo->syncFavorite($id);

        return ok();
    }

    public function favorite()
    {
        return $this->repo->getFavoriteByUser();
    }
}
