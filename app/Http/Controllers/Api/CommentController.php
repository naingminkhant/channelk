<?php

namespace App\Http\Controllers\Api;

use App\Repositories\DataRepo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommentController extends Controller
{
    protected $repo;

    public function __construct()
    {
        $this->repo = DataRepo::comment();
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Database\Eloquent\Model
     * @throws \Illuminate\Validation\ValidationException
     */
    public function storeVideoComment(Request $request, $id)
    {
        $data = [
            'comment'  => $request->get('comment'),
            'video_id' => $id,
        ];

        return $this->repo->storeVideoComment($data);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function storeSeriesComment(Request $request, $id)
    {
        $data = [
            'comment'   => $request->get('comment'),
            'series_id' => $id,
        ];

        return $this->repo->storeSeriesComment($data);
    }

    public function updateComment(Request $request, $id)
    {
        return $this->repo->update($request->only('comment'), $id);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $this->repo->destroy($id);

        return ok();
    }

    public function findByVideo(Request $request, $video_id)
    {
        return $this->repo->findByVideo($video_id, $request->only([
            'per_page',
        ]));
    }

    public function findBySeries(Request $request, $series_id)
    {
        return $this->repo->findBySeries($series_id, $request->only([
            'per_page',
        ]));
    }
}
