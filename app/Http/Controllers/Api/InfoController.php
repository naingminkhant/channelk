<?php

namespace App\Http\Controllers\Api;

use App\Repositories\DataRepo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InfoController extends Controller
{
    protected $repo;

    public function __construct()
    {
        $this->repo = DataRepo::info();
    }

    public function index()
    {
        return $this->repo->paginate();
    }
}
