<?php

namespace App\Http\Controllers\Api;

use App\QueryFilters\CastQueryFilter;
use App\QueryFilters\SeriesQueryFilter;
use App\QueryFilters\VideoSearchQueryFilter;
use App\Repositories\DataRepo;
use App\Transformers\VideoTransformer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SearchController extends Controller
{
    protected $video;

    protected $cast;

    protected $series;

    protected $user;

    public function __construct()
    {
        $this->video = DataRepo::video();
        $this->cast = DataRepo::cast();
        $this->series = DataRepo::series_video();
        $this->user = DataRepo::user();
    }

    public function index(Request $request)
    {
        if (!$request->get('q')) {
            throw new ModelNotFoundException();
        }

        $this->user->storeSearchKeyword($request->get('q'));

        $videos = $this->video
            ->pushQueryFilter(new VideoSearchQueryFilter($request->get('q')))
            ->setTransformer(new VideoTransformer(false, null, true))
            ->get();

        $casts = $this->cast
            ->pushQueryFilter(new CastQueryFilter($request->get('q')))
            ->get();

        $series = $this->series
            ->pushQueryFilter(new SeriesQueryFilter($request->get('q')))
            ->get();

        return response()->json([
            'videos' => $videos,
            'sereis' => $series,
            'casts'  => $casts,
        ]);
    }

    public function autocomplete(Request $request)
    {
        $videos = $this->video
            ->pushQueryFilter(new VideoSearchQueryFilter($request->get('q')))
            ->forSelect();

        $casts = $this->cast
            ->pushQueryFilter(new CastQueryFilter($request->get('q')))
            ->forSelect();

        $series = $this->series
            ->pushQueryFilter(new SeriesQueryFilter($request->get('q')))
            ->forSelect();

        return response()->json([
            'videos' => $videos,
            'sereis' => $series,
            'casts'  => $casts,
        ]);
    }
}
