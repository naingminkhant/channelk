<?php

namespace App\Http\Controllers\Api;

use App\Repositories\FaqRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FaqController extends Controller
{
    protected $repo;

    public function __construct(FaqRepository $repo)
    {
        $this->repo = $repo;
    }

    public function index()
    {
        return $this->repo->first();
    }
}
