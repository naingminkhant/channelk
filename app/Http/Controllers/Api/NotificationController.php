<?php

namespace App\Http\Controllers\Api;

use App\QueryFilters\NotificationPublishedQueryFilter;
use App\Repositories\DataRepo;
use App\Http\Controllers\Controller;
use App\Transformers\ChannelNotificationTransformer;

class NotificationController extends Controller
{
    protected $repo;

    public function __construct()
    {
        $this->repo = DataRepo::channel_notification();
    }

    public function index()
    {
        return $this->repo
            ->pushQueryFilter(app(NotificationPublishedQueryFilter::class))
            ->setTransformer(new ChannelNotificationTransformer(false,true))
            ->paginate();
    }

    public function show($id)
    {
        return $this->repo->read_at($id);
    }
}
