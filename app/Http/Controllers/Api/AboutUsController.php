<?php

namespace App\Http\Controllers\Api;

use App\Repositories\AboutUsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AboutUsController extends Controller
{
    protected $repo;

    public function __construct(AboutUsRepository $repo)
    {
        $this->repo = $repo;
    }

    public function index()
    {
        return $this->repo->first();
    }
}
