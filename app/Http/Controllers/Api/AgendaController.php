<?php

namespace App\Http\Controllers\Api;

use App\Repositories\DataRepo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AgendaController extends Controller
{
    protected $repo;

    public function __construct()
    {
        $this->repo = DataRepo::channel_agenda();
    }

    public function index()
    {
        return $this->repo->getByCurrentDate();
    }
}
