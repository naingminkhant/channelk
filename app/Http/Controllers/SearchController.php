<?php

namespace App\Http\Controllers;

use App\Models\Cast;
use App\Models\SeriesVideo;
use App\Models\Video;
use MyanFont;

class SearchController extends Controller
{
    public function byVideo() {

        $query = MyanFont::zg2uni(request('q'));

        return Video::where('title', 'LIKE', '%'. $query .'%')
            ->whereHas('playlists')
            ->limit(5)
            ->get();
    }

    public function bySeries() {

        $query = MyanFont::zg2uni(request('q'));

        return SeriesVideo::where('title', 'like', '%'. $query .'%')
            ->limit(5)
            ->get();
    }

    public function byCast() {

        $query = MyanFont::zg2uni(request('q'));

        return Cast::where('name', 'LIKE', '%'. $query .'%')
            ->limit(5)
            ->get();
    }

    public function showAll() {
        $query = request('query');

        $query = MyanFont::zg2uni($query);

        $videos = Video::where('title', 'LIKE', '%'. $query .'%')
            ->whereHas('playlists')
            ->limit(4)
            ->get();

        $series_videos = SeriesVideo::where('title', 'like', '%'. $query .'%')
            ->limit(4)
            ->get();

        $casts = Cast::where('name', 'LIKE', '%'. $query .'%')
            ->limit(4)
            ->get();

//        dd($videos);

        return view('frontend.pages.search', compact('videos', 'series_videos', 'casts', 'query'));
    }

}
