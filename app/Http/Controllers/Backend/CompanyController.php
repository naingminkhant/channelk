<?php

namespace App\Http\Controllers\Backend;

use App\Repositories\DataRepo;
use Arga\Auth\PermissionMiddlewareTrait;
use Illuminate\Http\Request;
use App\Models\Permission;

class CompanyController extends BaseController
{
    use PermissionMiddlewareTrait;

    private $repo;

    public function __construct()
    {
        $this->actionMiddelware([
            'index'   => Permission::PRODUCTION_COMPANY_INDEX,
            'create'  => Permission::PRODUCTION_COMPANY_CREATE,
            'edit'    => Permission::PRODUCTION_COMPANY_EDIT,
            'store'   => Permission::PRODUCTION_COMPANY_CREATE,
            'update'  => Permission::PRODUCTION_COMPANY_EDIT,
            'destroy' => Permission::PRODUCTION_COMPANY_DELETE,
        ]);
        $this->repo = DataRepo::company();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function index()
    {
        $companies = $this->repo->paginate();
        $this->setupTitle(trans('company.company'));

        return $this->view('company.index', [
            'companies' => $companies,
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function create()
    {
        $this->setupTitle(trans('company.create_company'));

        return $this->view('company.create');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function edit($id)
    {
        $company = $this->repo->find($id);
        $this->setupTitle(trans('company.edit_company'));

        return $this->view('company.edit', [
            'company' => $company,
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function show($id)
    {
        $company = $this->repo->find($id);
        $this->setupTitle(trans('company.company_info'));

        return $this->view('company.info', [
            'company' => $company,
        ]);
    }

    protected function setData(Request $request)
    {
        return $request->only([
            'slug',
            'name',
            'web_url',
            'description',
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $data = $this->setData($request);
        $this->repo->store($data);

        return redirect()->route('backend.company.index')->with('success', 'Successfully!');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $data = $this->setData($request);
        $this->repo->update($data, $id);

        return redirect()->route('backend.company.index')->with('success', 'Successfully!');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $this->repo->destroy($id);

        return ok();
    }
}
