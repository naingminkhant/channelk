<?php

namespace App\Http\Controllers\Backend;

use App\Repositories\DataRepo;
use App\Transformers\BannerTransformer;
use Arga\Auth\PermissionMiddlewareTrait;
use Illuminate\Http\Request;
use App\Models\Permission;

class BannerController extends BaseController
{
    use PermissionMiddlewareTrait;

    private $repo;

    public function __construct()
    {
        $this->actionMiddelware([
            'index'   => Permission::BANNER_INDEX,
            'create'  => Permission::BANNER_CREATE,
            'edit'    => Permission::BANNER_EDIT,
            'store'   => Permission::BANNER_CREATE,
            'update'  => Permission::BANNER_EDIT,
            'destroy' => Permission::BANNER_DELETE,
            'show'    => Permission::BANNER_SHOW,
        ]);

        $this->repo = DataRepo::banner();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function index()
    {
        $banners = $this->repo->paginate();

        $this->setupTitle(trans('banner.banner'));

        return $this->view('banner.index', [
            'banners' => $banners,
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function create()
    {
        $this->setupTitle(trans('banner.create_banner'));

        return $this->view('banner.create');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function edit($id)
    {
        $banner = $this->repo
            ->setTransformer(new BannerTransformer(true))
            ->find($id);

        $this->setupTitle(trans('banner.edit_banner'));

        $this->shareJS([
            'uploaded_image' => $banner['images'],
            'cover_image'    => $banner['cover_image'],
        ]);

        return $this->view('banner.edit', [
            'banner' => $banner,
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function show($id)
    {
        $banner = $this->repo
            ->setTransformer(new BannerTransformer(true))
            ->find($id);
        $this->setupTitle(trans('banner.banner_info'));

        return $this->view('banner.info', [
            'banner' => $banner,
        ]);
    }

    protected function setData(Request $request)
    {
        return $request->only([
            'title',
            'start_date',
            'end_date',
            'images',
            'cover_images',
            'url',
            'type',
            'type_id',
            'published_at',
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $data = $this->setData($request);

        $this->repo->store($data);

        return redirect()->route('backend.banner.index')->with('success', 'Successfully!');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $data = $this->setData($request);
        $this->repo->update($data, $id);

        return redirect()->route('backend.banner.index')->with('success', 'Successfully!');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $this->repo->destroy($id);

        return ok();
    }
}
