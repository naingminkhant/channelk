<?php

namespace App\Http\Controllers\Backend;

use App\Repositories\DataRepo;
use App\Transformers\DirectorTransformer;
use Arga\Auth\PermissionMiddlewareTrait;
use Illuminate\Http\Request;
use App\Models\Permission;

class DirectorController extends BaseController
{
    use PermissionMiddlewareTrait;

    private $repo;

    public function __construct()
    {
        $this->actionMiddelware([
            'index'   => Permission::DIRECTOR_INDEX,
            'create'  => Permission::DIRECTOR_CREATE,
            'edit'    => Permission::DIRECTOR_EDIT,
            'store'   => Permission::DIRECTOR_CREATE,
            'update'  => Permission::DIRECTOR_EDIT,
            'destroy' => Permission::DIRECTOR_DELETE,
        ]);
        $this->repo = DataRepo::director();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function index()
    {
        $directors = $this->repo->paginate();

        $this->setupTitle(trans('director.director'));

        return $this->view('director.index', [
            'directors' => $directors,
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function create()
    {
        $this->setupTitle(trans('director.create_director'));

        return $this->view('director.create');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function edit($id)
    {
        $director = $this->repo
            ->setTransformer(new DirectorTransformer(true))
            ->find($id);

        $this->setupTitle(trans('director.edit_director'));

        $this->shareJS([
            'uploaded_image' => $director['image'],
        ]);

        return $this->view('director.edit', [
            'director' => $director,
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function show($id)
    {
        $director = $this->repo->find($id);

        $this->setupTitle(trans('director.director_info'));

        return $this->view('director.info', [
            'director' => $director,
        ]);
    }

    protected function setData(Request $request): array
    {
        $data = $request->only([
            'slug',
            'name',
            'web_url',
            'description',
            'gender',
            'description',
            'images',
        ]);

        return $data;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $data = $this->setData($request);
        $this->repo->store($data);

        return redirect()->route('backend.director.index')->with('success', 'Successfully');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $data = $this->setData($request);
        $this->repo->update($data, $id);

        return redirect()->route('backend.director.index')->with('success', 'Successfully');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $this->repo->destroy($id);

        return ok();
    }
}
