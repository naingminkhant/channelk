<?php

namespace App\Http\Controllers\Backend;

class DashboardController extends BaseController
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function index()
    {
        return $this->view('dashboard');
    }
}
