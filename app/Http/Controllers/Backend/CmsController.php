<?php

namespace App\Http\Controllers\Backend;

use App\Repositories\DataRepo;
use Illuminate\Http\Request;

class CmsController extends BaseController
{
    protected $video, $video_cms;

    protected $series, $series_recommended_cms;

    protected $new_released_video;

    protected $series_cms;

    protected $cast, $castor_cms;

    public function __construct()
    {
        $this->video = DataRepo::video();
        $this->video_cms = DataRepo::video_recommended_cms();
        $this->series = DataRepo::series_video();
        $this->series_recommended_cms = DataRepo::series_recommended_cms();
        $this->new_released_video = DataRepo::new_released_video();
        $this->series_cms = DataRepo::series_cms();
        $this->cast = DataRepo::cast();
        $this->castor_cms = DataRepo::castor_cms();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function individual_index()
    {
        $attached = $this->video_cms->get();
        $this->shareJS([
            'attached_lists' => $attached,
            'lists'          => $this->video->getVideoExceptRecommendedCms(),
        ]);

        $this->setupTitle('Individual Recommended CMS');

        return $this->view('cms.individual_index', [
            'attached_lists' => $attached,
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function individual_store(Request $request)
    {
        $data = $request->get('attached_list');
        $this->video_cms->store($data ?? []);

        return redirect()->route('backend.cms.video-recommended')->with('success', 'Successfully!');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function playlist_index()
    {
        $attached = $this->series_recommended_cms->get();
        $this->shareJS([
            'attached_lists' => $attached,
            'lists'          => $this->series->getSeriesExceptRecommendedCms(),
        ]);

        $this->setupTitle('Playlist Recommended CMS');

        return $this->view('cms.series_index', [
            'attached_lists' => $attached,
        ]);
    }

    public function playlist_store(Request $request)
    {
        $data = $request->get('attached_list');
        $this->series_recommended_cms->store($data ?? []);

        return redirect()->route('backend.cms.playlist-recommended')->with('success', 'Successfully!');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function new_released_video_index()
    {
        $attached = $this->new_released_video->get();
        $this->shareJS([
            'attached_lists' => $attached,
            'lists'          => $this->video->getVideoExceptNewReleasedCms(),
        ]);

        $this->setupTitle('New Released Video CMS');

        return $this->view('cms.new_released_video_index', [
            'attached_lists' => $attached,
        ]);
    }

    public function new_released_video_store(Request $request)
    {
        $data = $request->get('attached_list');
        $this->new_released_video->store($data ?? []);

        return redirect()->route('backend.cms.new-released.video')->with('success', 'Successfully!');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function series_index()
    {
        $attached = $this->series_cms->get();
        $this->shareJS([
            'attached_lists' => $attached,
            'lists'          => $this->series->getSeriesExceptCms(),
        ]);

        $this->setupTitle('Playlist CMS');

        return $this->view('cms.series_cms_index', [
            'attached_lists' => $attached,
        ]);
    }

    public function series_store(Request $request)
    {
        $data = $request->get('attached_list');
        $this->series_cms->store($data ?? []);

        return redirect()->route('backend.cms.playlist')->with('success', 'Successfully!');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function cast_index()
    {
        $attached = $this->castor_cms->get();
        $this->shareJS([
            'attached_lists' => $attached,
            'lists'          => $this->cast->getCastExceptCms(),
        ]);

        $this->setupTitle('Cast CMS');

        return $this->view('cms.cast_index', [
            'attached_lists' => $attached,
        ]);
    }

    public function cast_store(Request $request)
    {
        $data = $request->get('attached_list');
        $this->castor_cms->store($data ?? []);

        return redirect()->route('backend.cms.cast')->with('success', 'Successfully!');
    }
}
