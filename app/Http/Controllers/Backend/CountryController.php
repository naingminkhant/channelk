<?php

namespace App\Http\Controllers\Backend;

use App\Repositories\DataRepo;
use Arga\Auth\PermissionMiddlewareTrait;
use Illuminate\Http\Request;
use App\Models\Permission;

class CountryController extends BaseController
{
    use PermissionMiddlewareTrait;

    private $repo;

    public function __construct()
    {
        $this->actionMiddelware([
            'index'   => Permission::COUNTRY_INDEX,
            'create'  => Permission::COUNTRY_CREATE,
            'edit'    => Permission::COUNTRY_EDIT,
            'store'   => Permission::COUNTRY_CREATE,
            'update'  => Permission::COUNTRY_EDIT,
            'destroy' => Permission::COUNTRY_DELETE,
        ]);
        $this->repo = DataRepo::country();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function index()
    {
        $countries = $this->repo->paginate();

        $this->setupTitle(trans('country.country'));

        return $this->view('country.index', [
            'countries' => $countries,
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function create()
    {
        $this->setupTitle(trans('country.create_country'));

        return $this->view('country.create');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function edit($id)
    {
        $country = $this->repo->find($id);
        $this->setupTitle(trans('country.edit_country'));

        return $this->view('country.edit', [
            'country' => $country,
        ]);
    }

    protected function setData(Request $request)
    {
        return $request->only([
            'slug',
            'name',
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $data = $this->setData($request);
        $this->repo->store($data);

        return redirect()->route('backend.country.index')->with('success', 'Successfully!');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $data = $this->setData($request);
        $this->repo->update($data, $id);

        return redirect()->route('backend.country.index')->with('success', 'Successfully!');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $this->repo->destroy($id);

        return ok();
    }
}
