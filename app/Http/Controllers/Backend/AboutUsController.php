<?php

namespace App\Http\Controllers\Backend;

use App\Repositories\AboutUsRepository;
use Illuminate\Http\Request;

class AboutUsController extends BaseController
{
    protected $repo;

    public function __construct(AboutUsRepository $repo)
    {
        $this->repo = $repo;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function create()
    {
        $about = $this->repo->first();

        return $this->view('about_us.form', [
            'about' => $about,
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function save(Request $request)
    {
        $this->repo->save($request->only('body'));

        return redirect()->route('backend.about-us.create')->with('success', 'Successfully!');
    }
}
