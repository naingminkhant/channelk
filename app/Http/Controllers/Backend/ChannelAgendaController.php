<?php

namespace App\Http\Controllers\Backend;

use App\Repositories\DataRepo;
use Arga\Auth\PermissionMiddlewareTrait;
use Illuminate\Http\Request;
use App\Models\Permission;

class ChannelAgendaController extends BaseController
{
    use PermissionMiddlewareTrait;

    private $repo;

    public function __construct()
    {
        $this->actionMiddelware([
            'index'   => Permission::CHANNEL_AGENDA_INDEX,
            'create'  => Permission::CHANNEL_AGENDA_CREATE,
            'edit'    => Permission::CHANNEL_AGENDA_EDIT,
            'store'   => Permission::CHANNEL_AGENDA_CREATE,
            'update'  => Permission::CHANNEL_AGENDA_EDIT,
            'destroy' => Permission::CHANNEL_AGENDA_DELETE,
        ]);

        $this->repo = DataRepo::channel_agenda();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function index()
    {
        $agendas = $this->repo
            ->paginateForWeb();

        $this->setupTitle('Channel Agenda');

        return $this->view('agenda.index', [
            'agendas' => $agendas,
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function create()
    {
        return $this->view('agenda.create');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function edit($id)
    {
        $agenda = $this->repo->find($id);
        $this->shareJS([
            'agenda' => $agenda,
        ]);

        $this->setupTitle('Edit');

        return $this->view('agenda.edit', [
            'agenda' => $agenda,
        ]);
    }

    protected function setData(Request $request)
    {
        $request->validate([
            'date'    => 'required|date',
            'time'    => 'required|array',
            'time.*'  => 'required|string',
            'title'   => 'required|array',
            'title.*' => 'required|string',
        ]);
        $data = $request->only([
            'date',
            'time',
            'title',
        ]);

        $agendas = [];
        foreach ($data['time'] as $index => $time) {
            $agendas[] = array_merge([
                'time'  => $time,
                'title' => $data['title'][$index],
            ]);
        }

        return [
            'date'    => $data['date'],
            'agendas' => $agendas,
        ];
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $data = $this->setData($request);

        $this->repo->store($data);

        return redirect()->route('backend.channel-agenda.index')->with('success', 'Successfully!');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $data = $this->setData($request);
        $this->repo->update($data, $id);

        return redirect()->route('backend.channel-agenda.index')->with('success', 'Successfully!');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $this->repo->destroy($id);

        return ok();
    }
}
