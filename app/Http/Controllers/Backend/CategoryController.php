<?php

namespace App\Http\Controllers\Backend;

use App\Repositories\DataRepo;
use App\Transformers\CategoryTransformer;
use Arga\Auth\PermissionMiddlewareTrait;
use Illuminate\Http\Request;
use App\Models\Permission;

class CategoryController extends BaseController
{
    use PermissionMiddlewareTrait;

    private $repo;

    public function __construct()
    {
        $this->actionMiddelware([
            'index'   => Permission::CATEGORY_INDEX,
            'create'  => Permission::CATEGORY_CREATE,
            'edit'    => Permission::CATEGORY_EDIT,
            'store'   => Permission::CATEGORY_CREATE,
            'update'  => Permission::CATEGORY_EDIT,
            'destroy' => Permission::CATEGORY_DELETE,
        ]);
        $this->repo = DataRepo::category();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function index()
    {
        $categories = $this->repo
            ->setTransformer(new CategoryTransformer(true))
            ->paginate();

        $this->setupTitle(trans('category.category'));

        $this->shareJS([
            'categories' => $categories['data'],
            'edit_url'   => route('backend.category.edit', 1),
        ]);

        return $this->view('category.index', [
            'categories' => $categories,
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function create()
    {
        $this->setupTitle(trans('category.create_category'));

        return $this->view('category.create');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function edit($id)
    {
        $category = $this->repo
            ->setTransformer(new CategoryTransformer(false, false,true))
            ->find($id);

        $this->setupTitle(trans('category.edit_category'));

        $this->shareJS([
            'uploaded_image' => [$category['image']],
        ]);

        return $this->view('category.edit', [
            'category' => $category,
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    protected function setData(Request $request): array
    {
        $data = $request->only([
            'slug',
            'title',
            'images',
        ]);

        return $data;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $data = $this->setData($request);
        $this->repo->store($data);

        return redirect()->route('backend.category.index')->with('success', 'Successfully');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $data = $this->setData($request);
        $this->repo->update($data, $id);

        return redirect()->route('backend.category.index')->with('success', 'Successfully');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $this->repo->destroy($id);

        return ok();
    }
}
