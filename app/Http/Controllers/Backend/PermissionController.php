<?php

namespace App\Http\Controllers\Backend;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Http\Request;

class PermissionController extends BaseController
{
    /**
     * @param $role
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function index($role)
    {
        $permissions = config('permissions');

        foreach ($permissions as $title => $values) {
            foreach ($values as $key => $value) {
                $slug = $title.'-'.$key;
                $name = title_case($title).' '.$value;
                $this->refreshPermission($name, $slug);
            }
        }

        /** @var Role $role */
        $role = Role::findBySlug($role);
        $permissions = $role->pluckPermissions();

        return $this->view('permission.index', [
            'role'        => $role,
            'permissions' => $permissions,
        ]);
    }

    public function update($roleId, Request $request)
    {
        $request->validate([
            'permissions' => 'required|array',
        ]);

        /** @var Role $role */
        $role = Role::find($roleId);
        $role->syncPermissions($request->get('permissions'));

        return redirect()->route('backend.permission.index', $role->slug)->with('success', 'Successfully');
    }

    protected function refreshPermission(string $name, string $slug)
    {
        Permission::updateOrCreate([
            'name' => $name,
            'slug' => $slug,
        ]);
    }
}
