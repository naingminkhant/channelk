<?php

namespace App\Http\Controllers\Backend;

use App\Models\Permission;
use App\Repositories\DataRepo;
use Arga\Auth\PermissionMiddlewareTrait;
use Illuminate\Http\Request;

class RtmpStreamController extends BaseController
{
    use PermissionMiddlewareTrait;

    protected $repo;

    public function __construct()
    {
        $this->actionMiddelware([
            'index'   => Permission::RTMPSTREAM_INDEX,
            'create'  => Permission::RTMPSTREAM_CREATE,
            'edit'    => Permission::RTMPSTREAM_EDIT,
            'store'   => Permission::RTMPSTREAM_CREATE,
            'update'  => Permission::RTMPSTREAM_EDIT,
            'destroy' => Permission::RTMPSTREAM_DELETE,
        ]);
        
        $this->repo = DataRepo::rtmp_stream();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function index()
    {
        $streams = $this->repo->paginate();
        $this->setupTitle('Rtmp Stream');

        return $this->view('rtmp_stream.index', [
            'streams' => $streams,
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function create()
    {
        $this->setupTitle('Create');

        return $this->view('rtmp_stream.create');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function edit($id)
    {
        $stream = $this->repo->find($id);
        $this->setupTitle('Edit');

        return $this->view('rtmp_stream.edit', [
            'stream' => $stream,
        ]);
    }

    protected function setData(Request $request)
    {
        return $request->only([
            'name',
            'url',
            'streamer',
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $data = $this->setData($request);
        $this->repo->store($data);

        return redirect()->route('backend.rtmp-stream.index')->with('success', 'Successfully!');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $data = $this->setData($request);
        $this->repo->update($data, $id);

        return redirect()->route('backend.rtmp-stream.index')->with('success', 'Successfully!');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $this->repo->destroy($id);

        return ok();
    }
}
