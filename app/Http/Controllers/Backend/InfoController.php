<?php

namespace App\Http\Controllers\Backend;

use App\Repositories\DataRepo;
use Illuminate\Http\Request;

class InfoController extends BaseController
{
    protected $repo;

    public function __construct()
    {
        $this->repo = DataRepo::info();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function index()
    {
        $infos = $this->repo->paginate();
        $this->setupTitle('Info');

        return $this->view('info.index', [
            'infos' => $infos,
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function create()
    {
        $this->setupTitle('Create');

        return $this->view('info.create');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function edit($id)
    {
        $info = $this->repo->find($id);
        $this->setupTitle('Edit');

        return $this->view('info.edit', [
            'info' => $info,
        ]);
    }

    protected function setData(Request $request)
    {
        $data = $request->only([
            'livestream',
            'privacy_policy',
            'terms_and_condition',
            'livestream_image',
            'privacy_policy_image',
            'terms_and_condition_image',
        ]);

        return $data;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $data = $this->setData($request);
        $this->repo->store($data);

        return redirect()->route('backend.info.index')->with('success', 'Successfully!');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $data = $this->setData($request);
        $this->repo->update($data, $id);

        return redirect()->route('backend.info.index')->with('success', 'Successfully!');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $this->repo->destroy($id);

        return ok();
    }
}
