<?php

namespace App\Http\Controllers\Backend;

use App\QueryFilters\VideoSearchQueryFilter;
use App\Repositories\DataRepo;
use App\Service\Traits\VideoUploadTrait;
use App\Transformers\VideoTransformer;
use Arga\Auth\PermissionMiddlewareTrait;
use Illuminate\Http\Request;
use Pion\Laravel\ChunkUpload\Receiver\FileReceiver;
use Pion\Laravel\ChunkUpload\Handler\HandlerFactory;
use App\Models\Permission;

class VideoController extends BaseController
{
    use PermissionMiddlewareTrait;

    private $repo;

    private $country;

    private $company;

    private $director;

    private $cast;

    private $category;

    private $comment;

    use VideoUploadTrait;

    public function __construct()
    {
        $this->actionMiddelware([
            'index'   => Permission::VIDEO_INDEX,
            'create'  => Permission::VIDEO_CREATE,
            'edit'    => Permission::VIDEO_EDIT,
            'store'   => Permission::VIDEO_CREATE,
            'update'  => Permission::VIDEO_EDIT,
            'destroy' => Permission::VIDEO_DELETE,
            'show'    => Permission::VIDEO_SHOW,
        ]);
        $this->repo = DataRepo::video();
        $this->country = DataRepo::country();
        $this->company = DataRepo::company();
        $this->director = DataRepo::director();
        $this->cast = DataRepo::cast();
        $this->category = DataRepo::category();
        $this->comment = DataRepo::comment();
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function index(Request $request)
    {
        $videos = $this->repo
            ->pushQueryFilter(new VideoSearchQueryFilter($request->get('q')))
            ->paginate();

        $this->setupTitle(trans('video.video'));

        return $this->view('video.index', [
            'videos' => $videos,
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function create()
    {
        $countries = $this->country->forSelect();
        $directors = $this->director->forSelect();
        $companies = $this->company->forSelect();
        $categories = $this->category->forSelect();
        $sub_categories = $this->category->forSelectSubCategory();
        $casts = $this->cast->forSelect();

        $this->setupTitle(trans('video.create_video'));
        $this->shareJS([
            'video_upload_url' => route('backend.upload-video'),
            'delete_video_url' => route('backend.destroy-only-video'),
        ]);

        return $this->view('video.create', [
            'countries'      => $countries,
            'directors'      => $directors,
            'companies'      => $companies,
            'casts'          => $casts,
            'categories'     => $categories,
            'sub_categories' => $sub_categories,
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function edit($id)
    {
        $countries = $this->country->forSelect();
        $directors = $this->director->forSelect();
        $companies = $this->company->forSelect();
        $categories = $this->category->forSelect();
        $sub_categories = $this->category->forSelectSubCategory();
        $casts = $this->cast->forSelect();
        $video = $this->repo
            ->setTransformer(new VideoTransformer(true))
            ->find($id);

        $this->setupTitle(trans('video.create_video'));

        $this->shareJS([
            'video_upload_url' => route('backend.upload-video', $id),
            'delete_video_url' => route('backend.destroy-video', $id),
            'video'            => $video,
            'uploaded_image'   => $video['images'],
        ]);

        return $this->view('video.edit', [
            'countries'      => $countries,
            'directors'      => $directors,
            'companies'      => $companies,
            'casts'          => $casts,
            'categories'     => $categories,
            'video'          => $video,
            'sub_categories' => $sub_categories,
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function show($id)
    {
        $video = $this->repo
            ->setTransformer(new VideoTransformer(true))
            ->find($id);

        $comments = $this->comment->findByVideo($id, []);

        return $this->view('video.info', [
            'video'    => $video,
            'comments' => $comments,
        ]);
    }

    protected function setData(Request $request)
    {
        $data = $request->only([
            'title_en',
            'title_mm',
            'slug',
            'duration',
            'trailer_url',
            'full_url',
            'story_line',
            'released_date',
            'budget',
            'country_id',
            'director_id',
            'production_company_id',
            'category_ids',
            'sub_category_ids',
            'cast_ids',
            'images',
            'published_at',
            'price',
        ]);

        $data += [
            'notification' => $request->get('notification') ? true : false,
        ];

        return $data;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $data = $this->setData($request);
        $this->repo->store($data);

        return redirect()->route('backend.video.index')->with('success', 'Successfully!');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $data = $this->setData($request);
        $this->repo->update($data, $id);

        return redirect()->route('backend.video.index')->with('success', 'Successfully!');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $this->repo->destroy($id);

        return ok();
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return mixed
     * @throws \Pion\Laravel\ChunkUpload\Exceptions\UploadFailedException
     * @throws \Pion\Laravel\ChunkUpload\Exceptions\UploadMissingFileException
     */
    public function upload(Request $request)
    {
        $receiver = new FileReceiver('video', $request, HandlerFactory::classFromRequest($request));
        $response = $this->uploadFile($receiver);

        return $response;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function destroyVideo(Request $request, $id)
    {
        $data = [
            'filename' => $request->get('filename'),
        ];
        $this->repo->removeAttachVideo($data, $id);

        return ok();
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function destroyOnlyVideo(Request $request)
    {
        $this->repo->removeOnlyVideo($request->all());

        return ok();
    }
}
