<?php

namespace App\Http\Controllers\Backend;

use App\Repositories\FaqRepository;
use Illuminate\Http\Request;

class FaqController extends BaseController
{
    protected $repo;

    public function __construct(FaqRepository $repo)
    {
        $this->repo = $repo;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function create()
    {
        $faq = $this->repo->first();

        return $this->view('faq.form', [
            'faq' => $faq,
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function save(Request $request)
    {
        $this->repo->save($request->only('body'));

        return redirect()->route('backend.faq.create')->with('success', 'Successfully!');
    }
}
