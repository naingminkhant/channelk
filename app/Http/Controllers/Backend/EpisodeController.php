<?php

namespace App\Http\Controllers\Backend;

use App\Repositories\DataRepo;
use App\Transformers\EpisodeTransformer;
use Illuminate\Http\Request;

class EpisodeController extends BaseController
{
    private $repo;

    public function __construct()
    {
        $this->repo = DataRepo::episode();
    }

    /**
     * @param $seriesId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function create($seriesId)
    {
        $this->shareJS([
            'video_upload_url' => route('backend.upload-video'),
            'delete_video_url' => route('backend.destroy-only-video'),
        ]);

        $this->setupTitle(trans('series.create_episode'));

        return $this->view('series.episode.create', [
            'seriesId' => $seriesId,
        ]);
    }

    /**
     * @param $seriesId
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function edit($seriesId, $id)
    {
        $episode = $this->repo
            ->setTransformer(new EpisodeTransformer(true))
            ->find($id);

        $this->setupTitle(trans('series.edit_episode'));

        $this->shareJS([
            'video_upload_url' => route('backend.upload-video'),
            'delete_video_url' => route('backend.episode.destroy-video', $id),
            'video'            => $episode,
            'uploaded_image'   => $episode['images'],
        ]);

        return $this->view('series.episode.edit', [
            'seriesId' => $seriesId,
            'episode'  => $episode,
        ]);
    }

    /**
     * @param $seriesId
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function show($seriesId, $id)
    {
        $episode = $this->repo
            ->setTransformer(new EpisodeTransformer(true))
            ->find($id);

        $this->setupTitle(trans('series.episode_info'));

        return $this->view('series.episode.info', [
            'episode' => $episode,
        ]);
    }

    protected function setData(Request $request, $seriesId)
    {
        $data = $request->only([
            'title',
            'slug',
            'duration',
            'trailer_url',
            'full_url',
            'story_line',
            'released_date',
            'sort',
            'images',
        ]);

        $data['active'] = $request->only('active') ? true : false;
        $data['series_id'] = $seriesId;

        return $data;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $seriesId
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request, $seriesId)
    {
        $data = $this->setData($request, $seriesId);
        $this->repo->store($data);

        return redirect()->route('backend.series.index')->with('success', 'Successfully!');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $seriesId
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $seriesId, $id)
    {
        $data = $this->setData($request, $seriesId);
        $this->repo->update($data, $id);

        return redirect()->route('backend.series.index')->with('success', 'Successfully!');
    }

    /**
     * @param $seriesId
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($seriesId, $id)
    {
        $this->repo->destroy($id);

        return ok();
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function destroyVideo(Request $request, $id)
    {
        $data = [
            'filename' => $request->get('filename'),
        ];
        $this->repo->removeAttachVideo($data, $id);

        return ok();
    }
}
