<?php

namespace App\Http\Controllers\Backend;

use App\QueryFilters\SeriesQueryFilter;
use App\Repositories\DataRepo;
use App\Transformers\SeriesTransformer;
use Arga\Auth\PermissionMiddlewareTrait;
use Illuminate\Http\Request;
use App\Models\Permission;

class SeriesController extends BaseController
{
    use PermissionMiddlewareTrait;

    private $repo;

    private $director;

    private $video;

    private $company;

    private $country;

    private $category;

    private $cast;

    private $comment;

    public function __construct()
    {
        $this->actionMiddelware([
            'index'   => Permission::SERIES_INDEX,
            'create'  => Permission::SERIES_CREATE,
            'edit'    => Permission::SERIES_EDIT,
            'store'   => Permission::SERIES_CREATE,
            'update'  => Permission::SERIES_EDIT,
            'destroy' => Permission::SERIES_DELETE,
        ]);
        $this->repo = DataRepo::series_video();
        $this->director = DataRepo::director();
        $this->company = DataRepo::company();
        $this->country = DataRepo::country();
        $this->category = DataRepo::category();
        $this->cast = DataRepo::cast();
        $this->video = DataRepo::video();
        $this->comment = DataRepo::comment();
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function index(Request $request)
    {
        $series = $this->repo
            ->pushQueryFilter(new SeriesQueryFilter($request->get('q')))
            ->paginate();

        $this->setupTitle(trans('series.playlist'));

        return $this->view('series.index', [
            'series' => $series,
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function create()
    {
        $companies = $this->company->forSelect();
        $directors = $this->director->forSelect();
        $countries = $this->country->forSelect();
        $categories = $this->category->forSelect();
        $casts = $this->cast->forSelect();

        $this->setupTitle(trans('series.create_playlist'));

        $this->shareJS([
            'video_upload_url' => route('backend.upload-video'),
            'delete_video_url' => route('backend.destroy-only-video'),
            'lists'            => $this->video->getVideoExceptPlaylist(),
        ]);

        return $this->view('series.create', [
            'companies'  => $companies,
            'directors'  => $directors,
            'countries'  => $countries,
            'categories' => $categories,
            'casts'      => $casts,
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function edit($id)
    {
        $companies = $this->company->forSelect();
        $directors = $this->director->forSelect();
        $countries = $this->country->forSelect();
        $categories = $this->category->forSelect();
        $casts = $this->cast->forSelect();
        $series = $this->repo
            ->setTransformer(new SeriesTransformer(true))
            ->find($id);

        $this->setupTitle(trans('series.edit_playlist'));
        $this->shareJS([
            'video_upload_url' => route('backend.upload-video'),
            'delete_video_url' => route('backend.series.destroy-video', $id),
            'attached_lists'   => $series['videos'],
            'uploaded_image'   => $series['images'],
            'cover_image'      => $series['cover_image'],
            'lists'            => $this->video->getVideoExceptPlaylist($id),
        ]);

        return $this->view('series.edit', [
            'companies'  => $companies,
            'directors'  => $directors,
            'countries'  => $countries,
            'categories' => $categories,
            'casts'      => $casts,
            'series'     => $series,
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function show($id)
    {
        $series = $this->repo
            ->setTransformer(new SeriesTransformer(true))
            ->find($id);
        $comments = $this->comment->findBySeries($id, []);

        return $this->view('series.info', [
            'series'   => $series,
            'comments' => $comments,
        ]);
    }

    protected function setData(Request $request)
    {
        $data = $request->only([
            'title_en',
            'title_mm',
            'slug',
            'country_id',
            'trailer_url',
            'story_line',
            'released_date',
            'budget',
            'director_id',
            'production_company_id',
            'category_ids',
            'cast_ids',
            'images',
            'cover_images',
            'attached_list',
            'published_at',
        ]);

        return $data;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $data = $this->setData($request);
        $this->repo->store($data);

        return redirect()->route('backend.series.index')->with('success', 'Successfully!');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $data = $this->setData($request);
        $this->repo->update($data, $id);

        return redirect()->route('backend.series.index')->with('success', 'Successfully!');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $this->repo->destroy($id);

        return ok();
    }

    public function destroyVideo($id)
    {
        $this->repo->removeAttachVideo($id);

        return ok();
    }
}
