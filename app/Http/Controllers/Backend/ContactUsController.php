<?php

namespace App\Http\Controllers\Backend;

use App\Repositories\ContactUsRepository;
use Illuminate\Http\Request;

class ContactUsController extends BaseController
{
    protected $repo;

    public function __construct(ContactUsRepository $repo)
    {
        $this->repo = $repo;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function create()
    {
        $contact = $this->repo->first();

        return $this->view('contact_us.form', [
            'contact' => $contact,
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function save(Request $request)
    {
        $this->repo->save($request->only('body'));

        return redirect()->route('backend.contact-us.create')->with('success', 'Successfully!');
    }
}
