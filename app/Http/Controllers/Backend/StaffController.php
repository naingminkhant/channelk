<?php

namespace App\Http\Controllers\Backend;

use App\Repositories\DataRepo;
use Arga\Auth\PermissionMiddlewareTrait;
use Illuminate\Http\Request;
use App\Models\Permission;

class StaffController extends BaseController
{
    use PermissionMiddlewareTrait;

    protected $repo;

    protected $role;

    public function __construct()
    {
        $this->actionMiddelware([
            'index'   => Permission::STAFF_INDEX,
            'create'  => Permission::STAFF_CREATE,
            'edit'    => Permission::STAFF_EDIT,
            'store'   => Permission::STAFF_CREATE,
            'update'  => Permission::STAFF_EDIT,
            'destroy' => Permission::STAFF_DELETE,
        ]);
        $this->repo = DataRepo::staff();
        $this->role = DataRepo::role();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function index()
    {
        $staffs = $this->repo->paginate();
        $this->setupTitle('Staff');

        return $this->view('staff.index', [
            'staffs' => $staffs,
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function create()
    {
        $roles = $this->role->forSelect();
        $this->setupTitle('Create');

        return $this->view('staff.create', [
            'roles' => $roles,
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function edit($id)
    {
        $staff = $this->repo->find($id);
        $roles = $this->role->forSelect();
        $this->setupTitle('Edit');

        return $this->view('staff.edit', [
            'staff' => $staff,
            'roles' => $roles,
        ]);
    }

    protected function setData(Request $request)
    {
        return $request->only([
            'name',
            'email',
            'role_ids',
            'password',
            'password_confirmation',
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $data = $this->setData($request);
        $this->repo->store($data);

        return redirect()->route('backend.staff.index')->with('success', 'Successfully!');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $data = $this->setData($request);
        $this->repo->update($data, $id);

        return redirect()->route('backend.staff.index')->with('success', 'Successfully!');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $this->repo->destroy($id);

        return ok();
    }
}
