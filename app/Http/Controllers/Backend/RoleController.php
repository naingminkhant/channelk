<?php

namespace App\Http\Controllers\Backend;

use App\Models\Permission;
use App\Repositories\DataRepo;
use Arga\Auth\PermissionMiddlewareTrait;
use Illuminate\Http\Request;

class RoleController extends BaseController
{
    use PermissionMiddlewareTrait;

    protected $repo;

    public function __construct()
    {
        $this->actionMiddelware([
            'index'   => Permission::ROLE_MANAGE,
            'create'  => Permission::ROLE_MANAGE,
            'edit'    => Permission::ROLE_MANAGE,
            'store'   => Permission::ROLE_MANAGE,
            'update'  => Permission::ROLE_MANAGE,
            'destroy' => Permission::ROLE_MANAGE,
        ]);
        $this->repo = DataRepo::role();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function index()
    {
        $roles = $this->repo->get();

        $this->setupTitle('Role');

        return $this->view('role.index', [
            'roles' => $roles,
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function create()
    {
        $this->setupTitle('Create');

        return $this->view('role.create');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function edit($id)
    {
        $role = $this->repo->find($id);
        $this->setupTitle('Edit');

        return $this->view('role.edit', [
            'role' => $role,
        ]);
    }

    protected function setData(Request $request)
    {
        return $request->only([
            'slug',
            'name',
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $data = $this->setData($request);
        $this->repo->store($data);

        return redirect()->route('backend.role.index')->with('success', 'Successfully');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $data = $this->setData($request);
        $this->repo->update($data, $id);

        return redirect()->route('backend.role.index')->with('success', 'Successfully');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $this->repo->destroy($id);

        return ok();
    }
}
