<?php

namespace App\Http\Controllers\Backend;

use App\Models\ChannelNotification;
use App\Repositories\DataRepo;
use App\Transformers\ChannelNotificationTransformer;
use Arga\Auth\PermissionMiddlewareTrait;
use Illuminate\Http\Request;
use App\Models\Permission;

class NotificationController extends BaseController
{
    use PermissionMiddlewareTrait;

    private $user;

    private $repo;

    public function __construct()
    {
        $this->actionMiddelware([
            'index'     => Permission::NOTIFICATION_INDEX,
            'create'    => Permission::NOTIFICATION_CREATE,
            'edit'      => Permission::NOTIFICATION_EDIT,
            'store'     => Permission::NOTIFICATION_CREATE,
            'update'    => Permission::NOTIFICATION_EDIT,
            'destroy'   => Permission::NOTIFICATION_DELETE,
            'published' => Permission::NOTIFICATION_PUBLISH,
        ]);
        $this->user = DataRepo::user();
        $this->repo = DataRepo::channel_notification();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function index()
    {
        $notifications = $this->repo->paginate();

        $this->setupTitle('Notification');

        return $this->view('notification.index', [
            'notifications' => $notifications,
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function create()
    {
        $users = $this->user->forSelect();

        $recipient_types = [
            ChannelNotification::ALL_USERS      => 'All Users',
            ChannelNotification::SPECIFIC_USERS => 'Specific Users',
        ];

        $this->setupTitle('Create Notification');

        return $this->view('notification.create', [
            'users'           => $users,
            'recipient_types' => $recipient_types,
        ]);
    }

    protected function setData(Request $request)
    {
        return $request->only([
            'title',
            'recipient_type',
            'recipient_users',
            'body',
        ]);
    }

    public function store(Request $request)
    {
        $data = $this->setData($request);

        $this->repo->store($data);

        return redirect()->route('backend.notification.index')->with('success', 'Successfully');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function edit($id)
    {
        $notification = $this->repo
            ->setTransformer(new ChannelNotificationTransformer(true))
            ->find($id);

        $users = $this->user->forSelect();

        $recipient_types = [
            ChannelNotification::ALL_USERS      => 'All Users',
            ChannelNotification::SPECIFIC_USERS => 'Specific Users',
        ];

        $this->setupTitle('Edit Notification');

        return $this->view('notification.edit', [
            'notification'    => $notification,
            'users'           => $users,
            'recipient_types' => $recipient_types,
        ]);
    }

    public function update(Request $request, $id)
    {
        $data = $this->setData($request);
        $this->repo->update($data, $id);

        return redirect()->route('backend.notification.index')->with('success', 'Successfully');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $this->repo->destroy($id);

        return ok();
    }

    public function published($id)
    {
        $this->repo->published_at($id);

        return redirect()->route('backend.notification.index')->with('success', 'Published');
    }
}
