<?php

namespace App\Http\Controllers\Backend;

use Arga\Helpers\CloudinaryHelper;
use Arga\Storage\Cloudinary\LocalFileStorage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploadController extends Controller
{
    use CloudinaryHelper;

    /**
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Exception
     */
    public function upload(Request $request)
    {
        if (!$request->hasFile('images')) {
            abort(400, 'Image Files Not Include');
        }

        $this->validate($request, [
            'images' => 'required|array',
        ]);

        $maxByte = config('filesystems.image_upload_limit');

        try {
            $this->validate($request, [
                'images.*' => 'required|image|max:'.$maxByte, // 2Mb
            ]);
        } catch (ValidationException $e) {
            return response([
                'message' => 'Exceeds the maximum upload size('.strtoupper(human_memory($maxByte * 1024)).') for this site',
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $images = $request->file('images');
        $this->validateImageExtensions($images);
        $imageFilesInfo = [];
        $local = app(LocalFileStorage::class);
        foreach ($images as $image) {
            /** @var $image UploadedFile */
            /** @var $image */
            $imageFile = $local->upload($image);
            $imageFilesInfo[] = $imageFile->toOriginal();
        }

        return $imageFilesInfo;
    }

    /**
     * @param UploadedFile[] $images
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateImageExtensions($images): void
    {
        foreach ($images as $image) {
            $validator = Validator::make([
                'image_extension' => $image->getClientOriginalExtension(),
            ], [
                'image_extension' => 'required|in:jpg,png,jpeg',
            ]);
            if ($validator->fails()) {
                throw new ValidationException($validator);
            }
        }
    }

    public function destroy($id)
    {
        $this->destroyFile($id);

        return ok();
    }
}
