<?php

namespace App\Http\Controllers\Backend;

use App\Models\Permission;
use App\QueryFilters\UserQueryFilter;
use Arga\Auth\PermissionMiddlewareTrait;
use Illuminate\Http\Request;
use App\Repositories\DataRepo;

class UserController extends BaseController
{
    use PermissionMiddlewareTrait;

    private $repo;

    public function __construct()
    {
        $this->actionMiddelware([
            'index'  => Permission::USER_MANAGE,
            'create' => Permission::USER_MANAGE,
            'edit'   => Permission::USER_MANAGE,
            'store'  => Permission::USER_MANAGE,
            'update' => Permission::USER_MANAGE,
            'show'   => Permission::USER_MANAGE,
        ]);
        $this->repo = DataRepo::user();
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function index(Request $request)
    {
        $filter = $request->only(['q']);

        $users = $this->repo
            ->pushQueryFilter(new UserQueryFilter($filter))
            ->paginate();

        $this->setupTitle(trans('user.user'));

        return $this->view('user.index', [
            'users' => $users,
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function create()
    {
        $this->setupTitle(trans('user.create_user'));

        return $this->view('user.create');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function edit($id)
    {
        $user = $this->repo->findUuid($id);

        $this->setupTitle(trans('user.edit_user'));

        return $this->view('user.edit', [
            'user' => $user,
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    protected function setData(Request $request): array
    {
        $data = $request->only([
            'name',
            'email',
            'password',
            'password_confirmation',
        ]);

        $data += [
            'banned_at' => $request->get('ban') ? now() : null,
        ];

        return $data;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $data = $this->setData($request);

        $this->repo->save($data);

        return redirect()->route('backend.user.index')->with('success', 'Successfully');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $data = $this->setData($request);

        $this->repo->update($data, $id);

        return redirect()->route('backend.user.index')->with('success', 'Successfully');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function show($id)
    {
        $user = $this->repo->findUuid($id);

        $this->setupTitle('User Information');

        return $this->view('user.info', [
            'user' => $user,
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $this->repo->destroy($id);

        return ok();
    }
}
