<?php

namespace App\Http\Controllers\Backend;

use App\Repositories\DataRepo;
use App\Transformers\CastTransformer;
use Arga\Auth\PermissionMiddlewareTrait;
use Illuminate\Http\Request;
use App\Models\Permission;

class CastController extends BaseController
{
    use PermissionMiddlewareTrait;

    private $repo;

    public function __construct()
    {
        $this->actionMiddelware([
            'index'   => Permission::CAST_INDEX,
            'create'  => Permission::CAST_CREATE,
            'edit'    => Permission::CAST_EDIT,
            'store'   => Permission::CAST_CREATE,
            'update'  => Permission::CAST_EDIT,
            'destroy' => Permission::CAST_DELETE,
        ]);
        $this->repo = DataRepo::cast();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function index()
    {
        $casts = $this->repo->paginate();

        $this->setupTitle(trans('cast.cast'));

        return $this->view('cast.index', [
            'casts' => $casts,
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function create()
    {
        $this->setupTitle(trans('cast.create_cast'));

        return $this->view('cast.create');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function edit($id)
    {
        $cast = $this->repo
            ->setTransformer(new CastTransformer(true))
            ->find($id);

        $this->shareJS([
            'uploaded_image' => $cast['image'],
        ]);

        $this->setupTitle(trans('cast.edit_cast'));

        return $this->view('cast.edit', [
            'cast' => $cast,
        ]);
    }

    protected function setData(Request $request)
    {
        return $request->only([
            'name',
            'slug',
            'born_at',
            'address',
            'web_url',
            'description',
            'gender',
            'images',
            'position',
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $data = $this->setData($request);

        $this->repo->store($data);

        return redirect()->route('backend.cast.index')->with('success', 'Successfully!');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $data = $this->setData($request);
        $this->repo->update($data, $id);

        return redirect()->route('backend.cast.index')->with('success', 'Successfully!');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $this->repo->destroy($id);

        return ok();
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function show($id)
    {
        $cast = $this->repo->find($id);
        $this->setupTitle(trans('cast.cast_info'));

        return $this->view('cast.info', [
            'cast' => $cast,
        ]);
    }
}
