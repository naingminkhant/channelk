<?php

namespace App\Http\Controllers\Backend;

use App\Repositories\SupportRepository;
use Illuminate\Http\Request;

class SupportController extends BaseController
{
    protected $repo;

    public function __construct(SupportRepository $repo)
    {
        $this->repo = $repo;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function create()
    {
        $support = $this->repo->first();

        return $this->view('support.form', [
            'support' => $support,
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function save(Request $request)
    {
        $this->repo->save($request->only('body'));

        return redirect()->route('backend.support.create')->with('success', 'Successfully!');
    }
}
