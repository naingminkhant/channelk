<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller as Controller;
use Arga\Sidebar\Menu;
use Illuminate\Support\Facades\Route;

abstract class BaseController extends Controller
{
    protected function setupTitle($title)
    {
        view()->share('page_title', $title);

        return $this;
    }

    /**
     * @throws \Exception
     */
    protected function setupSidebar()
    {
        $confs = config('sidebar');
        Menu::$currentUrl = request()->fullUrl();

        foreach ($confs as $conf) {
            $sidebar[] = new Menu($conf, array_get($conf, 'parent'));
        }

        if (!isset($sidebar)) {
            throw new \Exception('Need Sidebar for template');
        }

        view()->share('sidebarMenus', $sidebar);
    }

    /**
     * @param array $data
     */
    protected function shareJS(array $data)
    {
        \JavaScript::put($data);
    }

    protected function setupBreadcrumb()
    {
        $breadcrumbs = $this->prepareBreadcrumbs();

        view()->share('breadcrumbs', $breadcrumbs);
    }

    /**
     * @return array
     */
    protected function prepareBreadcrumbs()
    {
        $current = [];
        $breadcrumbs = [];
        $confs = config('breadcrumbs');

        foreach ($confs as $k => $v) {
            if (isset($v['route']) && $v['route'] === Route::currentRouteName()) {
                $current = $v + [
                        'url' => 'javascript:void(0);',
                    ];
                $breadcrumbs[$k] = $current;
                break;
            }
        }

        do {
            $parentKey = array_get($current, 'parent');
            $parent = config('breadcrumbs.'.$parentKey);
            if ($parent) {
                $breadcrumbs[$parentKey] = $parent + [
                        'url' => route($parent['route']),
                    ];
                $current = $parent;
            }
        } while ($parent);

        $breadcrumbs = array_reverse($breadcrumbs);

        return $breadcrumbs;
    }

    /**
     * @param $view
     * @param array $data
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    protected function view($view, array $data = [])
    {
        view()->share('auth_user', auth('admin')->user());

        $this->setupSidebar();

        $this->setupBreadcrumb();

        if (!str_contains($view, 'backend')) {
            $view = 'backend.'.$view;
        }

        $this->shareJsData();

        $layout = view('backend.layouts.app')->nest('content', $view, $data);

        return $layout;
    }

    protected function shareJsData()
    {
        $this->shareJS([
            'file_upload_url' => route('backend.upload-file'),
            'file_delete_url' => url('backend/delete/files'),
        ]);
    }
}
