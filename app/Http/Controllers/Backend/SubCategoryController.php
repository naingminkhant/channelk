<?php

namespace App\Http\Controllers\Backend;

use App\Repositories\DataRepo;
use Illuminate\Http\Request;

class SubCategoryController extends BaseController
{
    protected $repo;

    public function __construct()
    {
        $this->repo = DataRepo::category();
    }

    /**
     * @param $categoryId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function create($categoryId)
    {
        $category = $this->repo->find($categoryId);

        $this->setupTitle('Create Sub-Category');

        return $this->view('sub_category.create', [
            'category' => $category,
        ]);
    }

    protected function setData(Request $request)
    {
        return $request->only([
            'slug',
            'title',
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $categoryId
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request, $categoryId)
    {
        $data = $this->setData($request);
        $this->repo->storeSubCategory($data, $categoryId);

        return redirect()->route('backend.category.index')->with('success', 'Successfully!');
    }

    /**
     * @param $categoryId
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function edit($categoryId, $id)
    {
        $sub_category = $this->repo->findSubCategory($id);

        return $this->view('sub_category.edit', [
            'sub_category' => $sub_category,
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $categoryId
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $categoryId, $id)
    {
        $data = $this->setData($request);
        $this->repo->updateSubCategory($data, $id);

        return redirect()->route('backend.category.index')->with('success', 'Successfully!');
    }

    /**
     * @param $categoryId
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($categoryId, $id)
    {
        $this->repo->destroySubCategory($id);

        return ok();
    }
}
