<?php
/**
 * Created by channelk.
 * Author: naingminkhant
 * Date: 2019-01-03
 * Time: 11:05
 */

namespace App\Http\Controllers;

use App\Repositories\DataRepo;

class BaseController extends Controller
{
    protected $category;

    public function __construct()
    {
        $this->category = DataRepo::category();
    }

    protected function shareView()
    {
        $categories = $this->category->paginate();

        view()->share('categories', $categories);
    }

    protected function view($view, array $data = [])
    {
        $this->shareView();

        if (!str_contains($view, 'frontend')) {
            $view = 'frontend.'.$view;
        }

        $layout = view('frontend.layouts.app')->nest('content', $view, $data);

        return $layout;
    }
}
