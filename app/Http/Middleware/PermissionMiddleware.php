<?php

namespace App\Http\Middleware;

use App\Exceptions\UnauthorizedException;
use Closure;

class PermissionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param $permission
     * @return mixed
     */
    public function handle($request, Closure $next, $permission)
    {
        /** @var \Arga\Auth\Admin $admin */
        $admin = auth('admin')->user();
        if (auth('admin')->guest()) {
            throw UnauthorizedException::notLoggedIn();
        }
        if ($admin->hasPermissionTo($permission)) {
            return $next($request);
        }

        throw UnauthorizedException::forPermissions([$permission]);
    }
}
