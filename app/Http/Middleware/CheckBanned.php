<?php

namespace App\Http\Middleware;

use Arga\Auth\User;
use Closure;
use Auth;

class CheckBanned
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        $user = auth()->user();

        if ($user instanceof User && $user->isBanned()) {
            Auth::guard()->logout();

            return redirect()->guest('login');
        }

        return $response;
    }
}
