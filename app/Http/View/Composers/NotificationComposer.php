<?php
/**
 * Created by PhpStorm.
 * User: tintnaingwin
 * Date: 2019-01-05
 * Time: 20:03
 */

namespace App\Http\View\Composers;

use App\Models\ChannelNotification;
use Arga\Notification\Notification;
use Illuminate\View\View;
use Auth;

class NotificationComposer
{

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     * @todo don't use this because of keyword
     */
    public function compose(View $view)
    {
        if (Auth::check()) {
            $notifications = ChannelNotification::whereHas('user_notifications', function ($query) {
                $query->where('model_id', Auth::id());
            })->orderByDesc('created_at')->get();

            $count = Notification::whereNull('read_at')->where('model_id', Auth::id())->count();

            $noti_count = $count <= 9? $count : '9+';

            $view->with('user_notifications', $notifications)->with('noti_count', $noti_count);
        }else {
            $view->with('user_notifications', null)->with('noti_count', null);
        }
    }
}
