<?php

namespace App\Transformers;

use App\Models\Company;
use League\Fractal\TransformerAbstract;

class CompanyTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param \App\Models\Company $company
     * @return array
     */
    public function transform(Company $company)
    {
        return $company->toOriginal();
    }
}
