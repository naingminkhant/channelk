<?php

namespace App\Transformers;

use App\Models\Support;
use Arga\Transformer\BaseTransformer;

class SupportTransformer extends BaseTransformer
{
    /**
     * A Fractal transformer.
     *
     * @param \App\Models\Support $support
     * @return array
     */
    public function transform(Support $support)
    {
        return [
            'id'   => $support->getId(),
            'body' => $support->body,
        ];
    }
}
