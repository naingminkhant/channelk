<?php

namespace App\Transformers;

use App\Models\RtmpStream;
use League\Fractal\TransformerAbstract;

class RtmpStreamTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param \App\Models\RtmpStream $stream
     * @return array
     */
    public function transform(RtmpStream $stream)
    {
        return $stream->toAll();
    }
}
