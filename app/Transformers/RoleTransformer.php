<?php

namespace App\Transformers;

use App\Models\Role;
use League\Fractal\TransformerAbstract;

class RoleTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param \App\Models\Role $role
     * @return array
     */
    public function transform(Role $role)
    {
        return $role->toOriginal();
    }
}
