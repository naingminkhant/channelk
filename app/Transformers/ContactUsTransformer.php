<?php

namespace App\Transformers;

use App\Models\ContactUs;
use Arga\Transformer\BaseTransformer;

class ContactUsTransformer extends BaseTransformer
{
    /**
     * A Fractal transformer.
     *
     * @param \App\Models\ContactUs $contactUs
     * @return array
     */
    public function transform(ContactUs $contactUs)
    {
        return [
            'id'   => $contactUs->getId(),
            'body' => $contactUs->body,
        ];
    }
}
