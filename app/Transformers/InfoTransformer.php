<?php

namespace App\Transformers;

use App\Models\Info;
use League\Fractal\TransformerAbstract;

class InfoTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param \App\Models\Info $info
     * @return array
     */
    public function transform(Info $info)
    {
        return $info->toAll();
    }
}
