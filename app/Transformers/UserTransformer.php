<?php

namespace App\Transformers;

use Arga\Auth\User;
use Arga\Transformer\BaseTransformer;

class UserTransformer extends BaseTransformer
{
    private $detail;

    public function __construct($detail = false, $filters = null)
    {
        $this->detail = $detail;
        parent::__construct($filters);
    }

    /**
     * A Fractal transformer.
     *
     * @param \Arga\Auth\User $user
     * @return array
     */
    public function transform(User $user)
    {
        $data = $this->detail ? $user->toAll() : $user->toOriginal();

        return $this->filterField($data);
    }
}
