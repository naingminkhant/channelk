<?php

namespace App\Transformers;

use App\Models\Cast;
use Arga\Transformer\BaseTransformer;

class CastTransformer extends BaseTransformer
{
    private $detail;

    public function __construct($detail = false, $filters = null)
    {
        parent::__construct($filters);
        $this->detail = $detail;
    }

    /**
     * A Fractal transformer.
     *
     * @param \App\Models\Cast $cast
     * @return array
     */
    public function transform(Cast $cast)
    {
        if ($this->detail) {
            return $cast->toAll();
        }

        return $cast->toOriginal();
    }
}
