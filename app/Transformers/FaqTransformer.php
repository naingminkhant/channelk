<?php

namespace App\Transformers;

use App\Models\Faq;
use Arga\Transformer\BaseTransformer;

class FaqTransformer extends BaseTransformer
{
    /**
     * A Fractal transformer.
     *
     * @param \App\Models\Faq $faq
     * @return array
     */
    public function transform(Faq $faq)
    {
        return [
            'id'   => $faq->getId(),
            'body' => $faq->body,
        ];
    }
}
