<?php

namespace App\Transformers;

use App\Models\SeriesVideo;
use Arga\Transformer\BaseTransformer;

class SeriesTransformer extends BaseTransformer
{
    private $detail;

    public function __construct($detail = false, $filters = null)
    {
        parent::__construct($filters);
        $this->detail = $detail;
    }

    /**
     * A Fractal transformer.
     *
     * @param \App\Models\SeriesVideo $video
     * @return array
     */
    public function transform(SeriesVideo $video)
    {
        if ($this->detail) {
            return $video->toAll();
        }

        return $video->toOriginal();
    }
}
