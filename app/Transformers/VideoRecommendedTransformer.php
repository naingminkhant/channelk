<?php

namespace App\Transformers;

use App\Models\VideoRecommendedCms;
use League\Fractal\TransformerAbstract;

class VideoRecommendedTransformer extends TransformerAbstract
{
    protected $custom;

    public function __construct($custom = false)
    {
        $this->custom = $custom;
    }

    /**
     * A Fractal transformer.
     *
     * @param \App\Models\VideoRecommendedCms $cms
     * @return array
     */
    public function transform(VideoRecommendedCms $cms)
    {
        if ($this->custom) {
            return $cms->toCustom();
        }

        return $cms->toAll();
    }
}
