<?php

namespace App\Transformers;

use App\Models\Staff;
use League\Fractal\TransformerAbstract;

class StaffTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param \App\Models\Staff $staff
     * @return array
     */
    public function transform(Staff $staff)
    {
        return $staff->toAll();
    }
}
