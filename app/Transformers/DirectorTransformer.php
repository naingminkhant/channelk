<?php

namespace App\Transformers;

use App\Models\Director;
use Arga\Transformer\BaseTransformer;

class DirectorTransformer extends BaseTransformer
{
    private $detail;

    public function __construct($detail = false, $filters = null)
    {
        parent::__construct($filters);
        $this->detail = $detail;
    }

    /**
     * A Fractal transformer.
     *
     * @param \App\Models\Director $director
     * @return array
     */
    public function transform(Director $director)
    {
        if ($this->detail) {
            return $director->toAll();
        }

        return $director->toOriginal();
    }
}
