<?php

namespace App\Transformers;

use App\Models\SeriesCms;
use League\Fractal\TransformerAbstract;

class SeriesCmsTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param \App\Models\SeriesCms $cms
     * @return array
     */
    public function transform(SeriesCms $cms)
    {
        return $cms->toAll();
    }
}
