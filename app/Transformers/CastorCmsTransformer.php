<?php

namespace App\Transformers;

use App\Models\CastorCms;
use League\Fractal\TransformerAbstract;

class CastorCmsTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param \App\Models\CastorCms $cms
     * @return array
     */
    public function transform(CastorCms $cms)
    {
        return $cms->toAll();
    }
}
