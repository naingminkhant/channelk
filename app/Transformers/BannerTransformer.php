<?php

namespace App\Transformers;

use App\Models\Banner;
use Arga\Transformer\BaseTransformer;

class BannerTransformer extends BaseTransformer
{
    private $detail;

    public function __construct($detail = false, $filters = null)
    {
        parent::__construct($filters);
        $this->detail = $detail;
    }

    /**
     * A Fractal transformer.
     *
     * @param \App\Models\Banner $banner
     * @return array
     */
    public function transform(Banner $banner)
    {
        if ($this->detail) {
            return $banner->toAll();
        }

        return $banner->toOriginal();
    }
}
