<?php

namespace App\Transformers;

use Arga\Notification\Notification;
use League\Fractal\TransformerAbstract;

class NotificationTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param \Arga\Notification\Notification $notification
     * @return array
     */
    public function transform(Notification $notification)
    {
        return $notification->toAll();
    }
}
