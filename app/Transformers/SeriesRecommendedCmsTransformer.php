<?php

namespace App\Transformers;

use App\Models\SeriesRecommendedCms;
use League\Fractal\TransformerAbstract;

class SeriesRecommendedCmsTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param \App\Models\SeriesRecommendedCms $cms
     * @return array
     */
    public function transform(SeriesRecommendedCms $cms)
    {
        return $cms->toAll();
    }
}
