<?php

namespace App\Transformers;

use App\Models\Comment;
use Arga\Transformer\BaseTransformer;

class CommentTransformer extends BaseTransformer
{
    /**
     * A Fractal transformer.
     *
     * @param \App\Models\Comment $comment
     * @return array
     */
    public function transform(Comment $comment)
    {
        return $comment->toAll();
    }
}
