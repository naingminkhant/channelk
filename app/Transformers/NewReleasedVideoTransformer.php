<?php

namespace App\Transformers;

use App\Models\NewReleasedVideo;
use League\Fractal\TransformerAbstract;

class NewReleasedVideoTransformer extends TransformerAbstract
{
    protected $custom;

    public function __construct($custom = false)
    {
        $this->custom = $custom;
    }

    /**
     * A Fractal transformer.
     *
     * @param \App\Models\NewReleasedVideo $video
     * @return array
     */
    public function transform(NewReleasedVideo $video)
    {
        if ($this->custom) {
            return $video->toCustom();
        }

        return $video->toAll();
    }
}
