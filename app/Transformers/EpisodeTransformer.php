<?php

namespace App\Transformers;

use App\Models\Episode;
use Arga\Transformer\BaseTransformer;

class EpisodeTransformer extends BaseTransformer
{
    private $detail;

    public function __construct($detail = false, $filters = null)
    {
        parent::__construct($filters);
        $this->detail = $detail;
    }

    /**
     * A Fractal transformer.
     *
     * @param \App\Models\Episode $episode
     * @return array
     */
    public function transform(Episode $episode)
    {
        if ($this->detail) {
            return $episode->toAll();
        }

        return $episode->toOriginal();
    }
}
