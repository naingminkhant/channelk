<?php

namespace App\Transformers;

use App\Models\ChannelAgenda;
use Arga\Transformer\BaseTransformer;

class ChannelAgendaTransformer extends BaseTransformer
{
    /**
     * A Fractal transformer.
     *
     * @param \App\Models\ChannelAgenda $agenda
     * @return array
     */
    public function transform(ChannelAgenda $agenda)
    {
        return $agenda->toOriginal();
    }
}
