<?php

namespace App\Transformers;

use App\Models\Country;
use Arga\Transformer\BaseTransformer;

class CountryTransformer extends BaseTransformer
{
    /**
     * A Fractal transformer.
     *
     * @param \App\Models\Country $country
     * @return array
     */
    public function transform(Country $country)
    {
        return $country->toOriginal();
    }
}
