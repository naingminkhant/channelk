<?php

namespace App\Transformers;

use App\Models\Video;
use Arga\Transformer\BaseTransformer;

class VideoTransformer extends BaseTransformer
{
    protected $detail;

    protected $custom;

    public function __construct($detail = false, $filters = null, $custom = false)
    {
        parent::__construct($filters);
        $this->detail = $detail;
        $this->custom = $custom;
    }

    /**
     * A Fractal transformer.
     *
     * @param \App\Models\Video $video
     * @return array
     */
    public function transform(Video $video)
    {
        if ($this->detail && !$this->custom) {
            return $video->toAll();
        }

        if ($this->custom) {
            return $video->toCustom();
        }

        return $video->toOriginal();
    }
}
