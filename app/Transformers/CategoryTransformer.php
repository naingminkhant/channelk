<?php

namespace App\Transformers;

use App\Models\Category;
use Arga\Transformer\BaseTransformer;

class CategoryTransformer extends BaseTransformer
{
    protected $detail;

    protected $subCategory;

    protected $video;

    public function __construct($subCategory = false, $video = false, $detail = false, $filters = null)
    {
        parent::__construct($filters);
        $this->detail = $detail;
        $this->video = $video;
        $this->subCategory = $subCategory;
    }

    /**
     * A Fractal transformer.
     *
     * @param \App\Models\Category $category
     * @return array
     */
    public function transform(Category $category)
    {
        if ($this->detail) {
            return $category->toAll();
        }

        if ($this->subCategory) {
            return $category->withSubCategory();
        }

        if($this->video) {
            return $category->withVideo();
        }

        return $category->toOriginal();
    }
}
