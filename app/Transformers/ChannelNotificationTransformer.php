<?php

namespace App\Transformers;

use App\Models\ChannelNotification;
use League\Fractal\TransformerAbstract;

class ChannelNotificationTransformer extends TransformerAbstract
{
    public $detail;

    protected $api;

    public function __construct($detail = false, $for_api = false)
    {
        $this->detail = $detail;
        $this->api = $for_api;
    }

    /**
     * A Fractal transformer.
     *
     * @param \App\Models\ChannelNotification $notification
     * @return array
     */
    public function transform(ChannelNotification $notification)
    {
        if ($this->detail) {
            return $notification->toAll();
        }

        if ($this->api && auth()->user()) {
            $data = $notification->toOriginal();
            $check = auth()->user()->related_model()->where('channel_notification_id', $notification->getId())->first();
            $data = array_merge($data, [
                'read_at' => optional($check)->read_at,
                'is_read' => optional($check)->read_at ? true : false,
            ]);

            return $data;
        }

        return $notification->toOriginal();
    }
}
