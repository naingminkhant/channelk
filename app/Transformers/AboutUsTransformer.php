<?php

namespace App\Transformers;

use App\Models\AboutUs;
use Arga\Transformer\BaseTransformer;

class AboutUsTransformer extends BaseTransformer
{
    /**
     * A Fractal transformer.
     *
     * @param \App\Models\AboutUs $aboutUs
     * @return array
     */
    public function transform(AboutUs $aboutUs)
    {
        return [
            'id'   => $aboutUs->getId(),
            'body' => $aboutUs->body,
        ];
    }
}
