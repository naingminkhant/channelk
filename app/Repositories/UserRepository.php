<?php

namespace App\Repositories;

use App\Notifications\ForgetPassword;
use App\Transformers\UserTransformer;
use Arga\Auth\User;
use Arga\Auth\ValidCurrentPassword;
use Arga\Storage\Cloudinary\LocalFileStorage;
use Arga\Storage\Database\BaseRepository;
use Arga\Storage\Database\QueryFilterTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\UploadedFile;
use League\Fractal\TransformerAbstract;

class UserRepository extends BaseRepository
{
    use QueryFilterTrait;

    private $image;

    private $phone;

    private static $activate = false;

    public function __construct(TransformerAbstract $transformer = null)
    {
        $this->transformer = $transformer ?? new UserTransformer();
        $this->phone = DataRepo::phone();
        $this->image = app(LocalFileStorage::class);
    }

    protected function model(): Builder
    {
        $query = User::query();

        return $this->applyQueryFilter($query);
    }

    /**
     * @param array $data
     * @param null $id
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateData(array $data, $id = null): array
    {
        $this->validate($data, [
            'name'                  => 'required|string|max:255',
            'email'                 => 'required|string|email|max:255|unique:users,email,'.$id,
            'password'              => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required|string|min:6',
            'banned_at'             => 'nullable|date',
        ]);

        return $data;
    }

    /**
     * @param array $attribute
     * @param \Illuminate\Http\UploadedFile|null $file
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Exception
     */
    public function save(array $attribute, UploadedFile $file = null): array
    {
        $data = $this->validateData($attribute);
        /** @var User $user */
        $user = $this->model()->create($data);
        //$user->sendEmailVerificationNotification();

        if ($file) {
            //$this->image->uploadAndAttach($user, $file);
            $this->image->uploadAndAttach($user, $file);
        }

        auth()->login($user);

        return $this->item($user);
    }

    /**
     * @param array $attribute
     * @param $provider
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function saveFromSocial(array $attribute, $provider)
    {
        $this->validate($attribute, [
            'name'        => 'required|string|max:255',
            'email'       => 'email',
            'provider_id' => 'required|string',
        ]);

        $data = array_only($attribute, [
            'name',
            'email',
            'provider_id',
        ]);
        $data['provider'] = $provider;

        /** @var User $user */
        $user = $this->model()->updateOrCreate(['email' => $data['email']], $data);
        $this->image->uploadAndAttachFromUrl($user, array_get($attribute, 'avatar_original'), true);

        return auth()->loginUsingId($user['id']);
    }

    /**
     * @param array $attribute
     * @param $id
     * @param \Illuminate\Http\UploadedFile|null $file
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Exception
     * @permit Access Internal|Api
     */
    public function update(array $attribute, $id, UploadedFile $file = null)
    {
        $data = array_only($attribute, [
            'name',
            'email',
            'banned_at',
        ]);

        $this->validate($data, [
            'name'  => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,'.$id,
        ]);

        $model = $this->model()->find($id);
        $model->update($data);

        if ($file) {
            $this->image->uploadAndAttach($model, $file);
        }

        if (array_get($attribute, 'password') && $model instanceof User) {
            $this->updatePassword($model, $attribute);
        }

        return $this->item($model);
    }

    /**
     * @param array $attribute
     * @param \Illuminate\Http\UploadedFile $file
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Exception
     */
    public function updateFromApi(array $attribute, UploadedFile $file = null)
    {
        $data = [
            'name' => array_get($attribute, 'name') ?? $this->user()->getName(),
        ];

        $this->validate($data, [
            'name' => 'required|string|max:255',
        ]);

        $this->user()->update($data);

        if (array_get($attribute, 'email') && $this->user() instanceof User) {
            $this->updateEmail($this->user(), $attribute);
        }

        if (array_get($attribute, 'password') && $this->user() instanceof User) {
            $this->updatePassword($this->user(), $attribute);
        }

        if ($file) {
            $this->validate(['image' => $file], [
                'image' => 'required|image',
            ]);
            $this->image->uploadAndAttach($this->user(), $file);
        }

        return $this->item($this->user());
    }

    /**
     * @param \Arga\Auth\User $user
     * @param array $data
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function updateEmail(User $user, array $data)
    {
        $data = array_only($data, [
            'email',
            'current_password',
        ]);

        $this->validate($data, [
            'email'            => 'required|string|email|max:255|unique:users,email,'.$this->getUserId(),
            'current_password' => [
                'required',
                new ValidCurrentPassword(),
            ],
        ]);

        $user->update([
            'email' => $data['email'],
        ]);

        return ok();
    }

    /**
     * @param \Arga\Auth\User $user
     * @param array $data
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function updatePassword(User $user, array $data)
    {
        $data = array_only($data, [
            'current_password',
            'password',
            'password_confirmation',
        ]);

        if (!$this->sysAdmin()) {
            $this->validate($data, [
                'current_password' => [
                    'required',
                    new ValidCurrentPassword(),
                ],
            ]);
        }

        $this->validate($data, [
            'password'              => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required|string|min:6',
        ]);

        $user->update([
            'password' => $data['password'],
        ]);

        return ok();
    }

    public function forSelect()
    {
        return $this->model()->pluck('name', 'id');
    }

    public function update_activation_code(string $email)
    {
        /** @var User $model */
        $model = $this->model()->where('email', $email)->first();
        if (!$model->exists()) {
            return msg_data_not_found();
        }

        $model->update([
            'activation_code' => $this->generate_activation_code(),
        ]);

        $model->notify(new ForgetPassword($model->activation_code));

        return msg_send_reset_password();
    }

    public function generate_activation_code()
    {
        do {
            $code = generateRandomString(6);
        } while ($this->model()->where('remember_token', $code)->first());

        return $code;
    }

    /**
     * @param array $attribute
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function password_reset(array $attribute)
    {
        $this->validate($attribute, [
            'activation_code'       => 'required',
            'new_password'          => 'required|string|min:6|required_with:password_confirmation|same:password_confirmation',
            'password_confirmation' => 'string|min:6',
        ]);

        /** @var User $model */
        $model = $this->model()->where('activation_code', array_get($attribute, 'activation_code'))->first();
        if (!$model->exists()) {
            return msg_invalid_activation_code();
        }

        $model->update([
            'password'        => array_get($attribute, 'password'),
            'activation_code' => null,
        ]);

        return msg_success_reset_password();
    }

    public function storeSearchKeyword(string $q = null)
    {
        if (!$q || !$this->user() instanceof User) {
            return;
        }

        $this->user()->search_keywords()->create([
            'keyword' => $q,
        ]);

        return ok();
    }
}
