<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 11/20/18
 * Time: 1:56 PM
 */

namespace App\Repositories;

use App\Models\Director;
use App\Transformers\DirectorTransformer;
use Arga\Storage\Cloudinary\CloudinaryClient;
use Arga\Storage\Database\BaseRepository;
use Illuminate\Database\Eloquent\Builder;
use League\Fractal\TransformerAbstract;

class DirectorRepository extends BaseRepository
{
    private $image;

    public function __construct(TransformerAbstract $abstract = null)
    {
        $this->transformer = $abstract ?? new DirectorTransformer();
        $this->image = app(CloudinaryClient::class);
    }

    /**
     * @return Builder
     */
    protected function model(): Builder
    {
        return Director::query();
    }

    /**
     * @param array $data
     * @param null $id
     * @return array|null
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateData(array $data, $id = null): ?array
    {
        $this->validate($data, [
            'slug'   => 'required|string|max:255|unique:directors,slug,'.$id,
            'name'   => 'required|string|max:255',
            'gender' => 'required|string',
        ]);

        return $data;
    }

    /**
     * @param array $attributes
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(array $attributes)
    {
        $data = $this->validateData($attributes);

        $director = $this->model()->create($data);

        /** @var Director $director */
        $this->onSaving($director, $attributes);

        return $this->item($director);
    }

    protected function onSaving(Director $director, array $attributes)
    {
        if ($ids = array_get($attributes, 'images')) {
            $this->image->update($ids, $director);

            return;
        }
    }

    /**
     * @param array $attributes
     * @param $id
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(array $attributes, $id)
    {
        $data = $this->validateData($attributes, $id);
        $model = $this->model()->find($id);

        $model->update($data);

        /** @var Director $model */
        $this->onSaving($model, $attributes);

        return $this->item($model);
    }

    public function forSelect()
    {
        return $this->model()->pluck('name', 'id');
    }
}
