<?php
/**
 * Created by channelk.
 * Author: naingminkhant
 * Date: 2019-01-10
 * Time: 12:08
 */

namespace App\Repositories;

use App\Models\RtmpStream;
use App\Transformers\RtmpStreamTransformer;
use Arga\Storage\Database\BaseRepository;
use Illuminate\Database\Eloquent\Builder;
use League\Fractal\TransformerAbstract;

class RtmpStreamRepository extends BaseRepository
{
    public function __construct(TransformerAbstract $abstract = null)
    {
        $this->transformer = $abstract ?? new RtmpStreamTransformer();
    }

    /**
     * @return Builder
     */
    protected function model(): Builder
    {
        return RtmpStream::query();
    }

    /**
     * @param array $data
     * @param null $id
     * @return array|null
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateData(array $data, $id = null): ?array
    {
        $this->validate($data, [
            'name'     => 'required|string|max:255',
            'url'      => 'required|string|max:255',
            'streamer' => 'required|string|max:255',
        ]);

        return $data;
    }

    /**
     * @param array $attribute
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(array $attribute)
    {
        $data = $this->validateData($attribute);
        $stream = $this->model()->create($data);

        return $this->item($stream);
    }

    /**
     * @param array $attribute
     * @param $id
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(array $attribute, $id)
    {
        $data = $this->validateData($attribute);
        $stream = $this->model()->findOrFail($id);
        $stream->update($data);

        return $this->item($stream);
    }
}
