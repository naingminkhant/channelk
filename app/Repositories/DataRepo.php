<?php

namespace App\Repositories;

use Arga\AddressBook\AddressBookRepository;
use Arga\PhoneBook\PhoneBookRepository;

class DataRepo
{
    public static function user(): UserRepository
    {
        return app(UserRepository::class);
    }

    public static function address(): AddressBookRepository
    {
        return app(AddressBookRepository::class);
    }

    public static function phone(): PhoneBookRepository
    {
        return app(PhoneBookRepository::class);
    }

    public static function category(): CategoryRepository
    {
        return app(CategoryRepository::class);
    }

    public static function director(): DirectorRepository
    {
        return app(DirectorRepository::class);
    }

    public static function cast(): CastRepository
    {
        return app(CastRepository::class);
    }

    public static function company(): CompanyRepository
    {
        return app(CompanyRepository::class);
    }

    public static function video(): VideoRepository
    {
        return app(VideoRepository::class);
    }

    public static function country(): CountryRepository
    {
        return app(CountryRepository::class);
    }

    public static function series_video(): SeriesRepository
    {
        return app(SeriesRepository::class);
    }

    public static function episode(): EpisodeRepository
    {
        return app(EpisodeRepository::class);
    }

    public static function banner(): BannerRepository
    {
        return app(BannerRepository::class);
    }

    public static function channel_agenda(): ChannelAgendaRepository
    {
        return app(ChannelAgendaRepository::class);
    }

    public static function channel_notification(): ChannelNotificationRepository
    {
        return app(ChannelNotificationRepository::class);
    }

    public static function notification(): NotificationRepository
    {
        return app(NotificationRepository::class);
    }

    public static function comment(): CommentRepository
    {
        return app(CommentRepository::class);
    }

    public static function role(): RoleRepository
    {
        return app(RoleRepository::class);
    }

    public static function staff(): StaffRepository
    {
        return app(StaffRepository::class);
    }

    public static function rtmp_stream(): RtmpStreamRepository
    {
        return app(RtmpStreamRepository::class);
    }

    public static function info(): InfoRepository
    {
        return app(InfoRepository::class);
    }

    public static function video_recommended_cms(): VideoRecommendedCmsRepository
    {
        return app(VideoRecommendedCmsRepository::class);
    }

    public static function series_recommended_cms(): SeriesRecommendedCmsRepository
    {
        return app(SeriesRecommendedCmsRepository::class);
    }

    public static function new_released_video(): NewReleasedVideoRepository
    {
        return app(NewReleasedVideoRepository::class);
    }

    public static function series_cms(): SeriesCmsRepository
    {
        return app(SeriesCmsRepository::class);
    }

    public static function castor_cms(): CastorCmsRepository
    {
        return app(CastorCmsRepository::class);
    }
}
