<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 11/28/18
 * Time: 8:49 PM
 */

namespace App\Repositories;

use App\Models\Episode;
use App\Models\Video;
use App\Transformers\EpisodeTransformer;
use Arga\Storage\Cloudinary\CloudinaryClient;
use Arga\Storage\Database\BaseRepository;
use Illuminate\Database\Eloquent\Builder;
use League\Fractal\TransformerAbstract;
use Illuminate\Support\Facades\Storage;

class EpisodeRepository extends BaseRepository
{
    private $image;

    public function __construct(TransformerAbstract $abstract = null)
    {
        $this->transformer = $abstract ?? new EpisodeTransformer();
        $this->image = app(CloudinaryClient::class);
    }

    /**
     * @return Builder
     */
    protected function model(): Builder
    {
        return Episode::query();
    }

    /**
     * @param array $data
     * @param null $id
     * @return array|null
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateData(array $data, $id = null): ?array
    {
        $this->validate($data, [
            'title'         => 'required|string|max:255',
            'slug'          => 'required|string|max:255|unique:episodes,slug,'.$id,
            'story_line'    => 'nullable|string|max:65535',
            'released_date' => 'nullable|date',
            'series_id'     => 'required|exists:series,id',
            'sort'          => 'required|integer',
            'active'        => 'nullable|boolean',
        ]);

        return $data;
    }

    /**
     * @param array $attributes
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(array $attributes)
    {
        $data = $this->validateData($attributes);

        $episode = $this->model()->create($data);

        /** @var Episode $episode */
        $this->onSaving($data, $episode);

        return $this->item($episode);
    }

    /**
     * @param array $attributes
     * @param $id
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(array $attributes, $id)
    {
        $data = $this->validateData($attributes, $id);
        $episode = $this->model()->find($id);
        $episode->update($data);

        /** @var Episode $episode */
        $this->onSaving($data, $episode);

        return $this->item($episode);
    }

    protected function onSaving(array $data, Episode $episode)
    {
        if ($ids = array_get($data, 'images')) {
            $this->image->update($ids, $episode);
        }
    }

    /**
     * @param array $attributes
     * @param $id
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function removeAttachVideo(array $attributes, $id)
    {
        $this->validate($attributes, [
            'filename' => [
                'required',
                'string',
                Rule::in([Video::TRAILER_VIDEO, Video::FULL_VIDEO]),
            ],
        ]);

        $filename = array_get($attributes, 'filename');
        $video = $this->model()->find($id);

        switch ($filename) {
            case Video::TRAILER_VIDEO:
                Storage::delete($video['trailer_url']);
                $video->update(['trailer_url' => null,]);
                break;

            case Video::FULL_VIDEO:
                Storage::delete($video['full_url']);
                $video->update(['full_url' => null,]);
                break;
        }

        return $this->item($video);
    }
}
