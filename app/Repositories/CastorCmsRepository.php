<?php
/**
 * Created by channelk.
 * Author: naingminkhant
 * Date: 2019-02-21
 * Time: 09:23
 */

namespace App\Repositories;

use App\Models\CastorCms;
use App\Transformers\CastorCmsTransformer;
use Arga\Storage\Database\BaseRepository;
use Illuminate\Database\Eloquent\Builder;
use League\Fractal\TransformerAbstract;

class CastorCmsRepository extends BaseRepository
{
    public function __construct(TransformerAbstract $abstract = null)
    {
        $this->transformer = $abstract ?? new CastorCmsTransformer();
    }

    /**
     * @return Builder
     */
    protected function model(): Builder
    {
        return CastorCms::query();
    }

    protected function validateData(array $data, $id = null): ?array
    {
        return $data;
    }

    public function store(array $attribute)
    {
        $data = $this->validateData($attribute);
        $this->model()->delete();
        foreach ($data as $item) {
            $this->model()->create([
                'cast_id' => $item,
            ]);
        }

        return ok();
    }

    public function get()
    {
        $castor = $this->model()->whereHas('star')->get();

        return $this->collection($castor);
    }

    public function paginate()
    {
        $castor = $this->model()->whereHas('star')->paginate(request()->get('per_page') ?? 15);

        return $this->collection($castor);
    }
}
