<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 11/28/18
 * Time: 10:49 PM
 */

namespace App\Repositories;

use App\Models\Banner;
use App\Transformers\BannerTransformer;
use Arga\Storage\Cloudinary\CloudinaryClient;
use Arga\Storage\Database\BaseRepository;
use Illuminate\Database\Eloquent\Builder;
use League\Fractal\TransformerAbstract;

class BannerRepository extends BaseRepository
{
    private $image;

    public function __construct(TransformerAbstract $abstract = null)
    {
        $this->transformer = $abstract ?? new BannerTransformer();
        $this->image = app(CloudinaryClient::class);
    }

    /**
     * @return Builder
     */
    protected function model(): Builder
    {
        return Banner::query();
    }

    /**
     * @param array $data
     * @param null $id
     * @return array|null
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateData(array $data, $id = null): ?array
    {
        $this->validate($data, [
            'title'        => 'required|string|max:255',
            'start_date'   => 'required|date|before:end_date',
            'end_date'     => 'required|date|after:start_date',
            'url'          => 'nullable|string|max:255',
            'published_at' => 'nullable|date',
        ]);

        return $data;
    }

    /**
     * @param array $attributes
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(array $attributes)
    {
        $data = $this->validateData($attributes);
        $banner = $this->model()->create($data);

        /** @var Banner $banner */
        $this->onSaving($banner, $data);

        return $this->item($banner);
    }

    protected function onSaving(Banner $banner, array $data)
    {
        if ($images = array_get($data, 'images')) {
            $this->image->update($images, $banner);
        }

        if ($cover_image = array_get($data, 'cover_images')) {
            $this->image->updateCoverImage($cover_image,$banner);
        }
    }

    /**
     * @param array $attributes
     * @param $id
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(array $attributes, $id)
    {
        $data = $this->validateData($attributes, $id);
        $banner = $this->model()->find($id);
        $banner->update($data);

        /** @var Banner $banner */
        $this->onSaving($banner, $data);

        return $this->item($banner);
    }
}
