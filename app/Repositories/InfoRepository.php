<?php
/**
 * Created by channelk.
 * Author: naingminkhant
 * Date: 2019-01-21
 * Time: 16:35
 */

namespace App\Repositories;

use App\Models\Info;
use App\Transformers\InfoTransformer;
use Arga\Storage\Database\BaseRepository;
use Illuminate\Database\Eloquent\Builder;
use League\Fractal\TransformerAbstract;

class InfoRepository extends BaseRepository
{
    public function __construct(TransformerAbstract $abstract = null)
    {
        $this->transformer = $abstract ?? new InfoTransformer();
    }

    /**
     * @return Builder
     */
    protected function model(): Builder
    {
        return Info::query();
    }

    /**
     * @param array $data
     * @param null $id
     * @return array|null
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateData(array $data, $id = null): ?array
    {
        $this->validate($data, [
            'livestream'                => 'required',
            'privacy_policy'            => 'required',
            'terms_and_condition'       => 'required',
            'livestream_image'          => 'required',
            'privacy_policy_image'      => 'required',
            'terms_and_condition_image' => 'required',
        ]);

        $data = [
            'info_one'    => array_get($data, 'livestream'),
            'info_two'    => array_get($data, 'privacy_policy'),
            'info_three'  => array_get($data, 'terms_and_condition'),
            'image_one'   => array_get($data, 'livestream_image'),
            'image_two'   => array_get($data, 'privacy_policy_image'),
            'image_three' => array_get($data, 'terms_and_condition_image'),
        ];

        return $data;
    }

    /**
     * @param array $attribute
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(array $attribute)
    {
        $data = $this->validateData($attribute);
        $model = $this->model()->create($data);

        return $this->item($model);
    }

    /**
     * @param array $attribute
     * @param $id
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(array $attribute, $id)
    {
        $data = $this->validateData($attribute);
        $model = $this->model()->findOrFail($id);
        $model->update($data);

        return $this->item($model);
    }
}
