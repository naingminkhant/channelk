<?php
/**
 * Created by channelk.
 * Author: naingminkhant
 * Date: 2019-02-21
 * Time: 09:23
 */

namespace App\Repositories;

use App\Models\NewReleasedVideo;
use App\Transformers\NewReleasedVideoTransformer;
use Arga\Storage\Database\BaseRepository;
use Illuminate\Database\Eloquent\Builder;
use League\Fractal\TransformerAbstract;

class NewReleasedVideoRepository extends BaseRepository
{
    public function __construct(TransformerAbstract $abstract = null)
    {
        $this->transformer = $abstract ?? new NewReleasedVideoTransformer();
    }

    /**
     * @return Builder
     */
    protected function model(): Builder
    {
        return NewReleasedVideo::query();
    }

    protected function validateData(array $data, $id = null): ?array
    {
        return $data;
    }

    public function store(array $attribute)
    {
        $data = $this->validateData($attribute);
        $this->model()->delete();
        foreach ($data as $item) {
            $this->model()->create([
                'video_id' => $item,
            ]);
        }

        return ok();
    }

    public function get()
    {
        $videos = $this->model()->whereHas('video')->get();

        return $this->collection($videos);
    }

    public function paginate()
    {
        $videos = $this->model()->whereHas('video')->paginate();

        return $this->collection($videos);
    }

    public function forApi()
    {
        $videos = $this->model()->whereHas('video', function ($sub) {
            $sub->whereHas('playlists');
        })->paginate(request()->get('per_page') ?? 15);

        return $this->collection($videos);
    }

    public function paginateWithOrder(int $per_page)
    {
        $videos = $this->model()
            ->whereHas('video', function ($sub) {
                $sub->whereHas('playlists');
            })
            ->orderByDesc('new_released_videos.created_at')
            ->paginate($per_page);

        return $this->collection($videos);
    }
}
