<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 12/1/18
 * Time: 5:49 PM
 */

namespace App\Repositories;

use App\Models\ChannelAgenda;
use App\Transformers\ChannelAgendaTransformer;
use Arga\Storage\Database\BaseRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use League\Fractal\TransformerAbstract;
use phpDocumentor\Reflection\Types\This;

class ChannelAgendaRepository extends BaseRepository
{

    public function __construct(TransformerAbstract $abstract = null)
    {
        $this->transformer = $abstract ?? new ChannelAgendaTransformer();
    }

    /**
     * @return Builder
     */
    protected function model(): Builder
    {
        return ChannelAgenda::query();
    }

    /**
     * @param array $data
     * @param null $id
     * @return array|null
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateData(array $data, $id = null): ?array
    {
        $this->validate($data, [
            'date'    => 'required',
            'agendas' => 'required',
        ]);

        return $data;
    }

    /**
     * @param array $attributes
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(array $attributes)
    {
        $data = $this->validateData($attributes);
        $agendas = $this->model()->create($data);

        return $this->item($agendas);
    }

    /**
     * @param array $attributes
     * @param $id
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(array $attributes, $id)
    {
        $data = $this->validateData($attributes, $id);
        $agenda = $this->model()->find($id);
        $agenda->update($data);

        return $this->item($agenda);
    }

    public function paginateForWeb()
    {
        $agendas = $this->model()
            ->orderBy('date', 'desc')
            ->paginate();

        return $this->collection($agendas);
    }

    public function getByCurrentDate()
    {
        $current = now()->toDateString();
        $tomorrow = now()->addDay(1)->toDateString();
        $model = $this->model()->whereBetween('date', [$current, $tomorrow])
            ->get()->sortBy('date');

        return $this->collection($model);
    }
}
