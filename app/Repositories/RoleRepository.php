<?php
/**
 * Created by channelk.
 * Author: naingminkhant
 * Date: 2019-01-09
 * Time: 10:48
 */

namespace App\Repositories;

use App\Models\Role;
use App\Transformers\RoleTransformer;
use Arga\Storage\Database\BaseRepository;
use Illuminate\Database\Eloquent\Builder;
use League\Fractal\TransformerAbstract;

class RoleRepository extends BaseRepository
{
    public function __construct(TransformerAbstract $abstract = null)
    {
        $this->transformer = $abstract ?? new RoleTransformer();
    }

    /**
     * @return Builder
     */
    protected function model(): Builder
    {
        return Role::query();
    }

    /**
     * @param array $data
     * @param null $id
     * @return array|null
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateData(array $data, $id = null): ?array
    {
        $this->validate($data, [
            'slug' => 'required|string|max:255|unique:roles,slug,'.$id,
            'name' => 'required|string|max:255|unique:roles,name,'.$id,
        ]);

        return $data;
    }

    /**
     * @param array $attribute
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(array $attribute)
    {
        $data = $this->validateData($attribute);
        $role = $this->model()->create($data);

        return $this->item($role);
    }

    /**
     * @param array $attribute
     * @param $id
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(array $attribute, $id)
    {
        $data = $this->validateData($attribute, $id);
        $role = $this->model()->findOrFail($id);
        $role->update($data);

        return $this->item($role);
    }

    public function forSelect()
    {
        return $this->model()->pluck('name','id');
    }
}
