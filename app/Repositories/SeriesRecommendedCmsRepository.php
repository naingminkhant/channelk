<?php
/**
 * Created by channelk.
 * Author: naingminkhant
 * Date: 2019-02-21
 * Time: 09:23
 */

namespace App\Repositories;

use App\Models\SeriesRecommendedCms;
use App\Transformers\SeriesRecommendedCmsTransformer;
use Arga\Storage\Database\BaseRepository;
use Illuminate\Database\Eloquent\Builder;
use League\Fractal\TransformerAbstract;

class SeriesRecommendedCmsRepository extends BaseRepository
{
    public function __construct(TransformerAbstract $abstract = null)
    {
        $this->transformer = $abstract ?? new SeriesRecommendedCmsTransformer();
    }

    /**
     * @return Builder
     */
    protected function model(): Builder
    {
        return SeriesRecommendedCms::query();
    }

    protected function validateData(array $data, $id = null): ?array
    {
        return $data;
    }

    public function store(array $attribute)
    {
        $data = $this->validateData($attribute);
        $this->model()->delete();
        foreach ($data as $item) {
            $this->model()->create([
                'series_id' => $item,
            ]);
        }

        return ok();
    }

    public function get()
    {
        $series = $this->model()->whereHas('series')->get();

        return $this->collection($series);
    }

    public function paginate()
    {
        $series = $this->model()->whereHas('series')->paginate(request()->get('per_page') ?? 15);

        return $this->collection($series);
    }
}
