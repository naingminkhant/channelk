<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 11/22/18
 * Time: 4:47 PM
 */

namespace App\Repositories;

use App\Models\Video;
use App\Transformers\VideoTransformer;
use Arga\Storage\Database\BaseRepository;
use Arga\Storage\Database\QueryFilterTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use League\Fractal\TransformerAbstract;
use Arga\Storage\Cloudinary\CloudinaryClient;

class VideoRepository extends BaseRepository
{
    use QueryFilterTrait;

    private $image;

    public function __construct(TransformerAbstract $abstract = null)
    {
        $this->transformer = $abstract ?? new VideoTransformer();
        $this->image = app(CloudinaryClient::class);
    }

    /**
     * @return Builder
     */
    protected function model(): Builder
    {
        $model = Video::query();
        if (!$this->isAdmin()) {
            $model = $model->whereDate('published_at', '<=', now());
        }

        return $this->applyQueryFilter($model);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        return Video::query();
    }

    /**
     * @param array $data
     * @param null $id
     * @return array|null
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateData(array $data, $id = null): ?array
    {
        $this->validate($data, [
            'title_en'              => 'required|string|max:255',
            'title_mm'              => 'required|string|max:255',
            'slug'                  => 'required|string|max:255|unique:videos,slug,'.$id,
            'duration'              => 'required',
            'country_id'            => 'required|exists:countries,id',
            'story_line'            => 'nullable|string|max:65535',
            'released_date'         => 'required|date',
            'budget'                => 'nullable|numeric',
            'director_id'           => 'required|exists:directors,id',
            'production_company_id' => 'required|exists:production_companies,id',
            'published_at'          => 'nullable|date',
            'price'                 => 'nullable|numeric',
            'notification'          => 'nullable|boolean',
            'trailer_url'           => 'nullable|string',
            'full_url'              => 'nullable|string',
        ]);
        $data['title'] = array_get($data, 'title_en');

        return $data;
    }

    /**
     * @param array $attributes
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(array $attributes)
    {
        $data = $this->validateData($attributes);
        $video = $this->model()->create($data);

        /** @var Video $video */
        $this->onSaving($video, $attributes);

        return $this->item($video);
    }

    protected function onSaving(Video $video, array $data)
    {
        if ($categories = array_get($data, 'category_ids')) {
            $video->categories()->sync($categories);
        }
        if ($casts = array_get($data, 'cast_ids')) {
            $video->stars()->sync($casts);
        }
        if ($subcategories = array_get($data, 'sub_category_ids')) {
            $video->sub_categories()->sync($subcategories);
        }

        if ($ids = array_get($data, 'images')) {
            $this->image->update($ids, $video);
        }
    }

    /**
     * @param array $attributes
     * @param $id
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(array $attributes, $id)
    {
        $data = $this->validateData($attributes, $id);
        $video = $this->model()->find($id);
        $video->update($data);

        /** @var Video $video */
        $this->onSaving($video, $attributes);

        return $this->item($video);
    }

    /**
     * @param $id
     * @param $url
     * @return mixed
     * @deprecated
     */
    public function attachTrailerVideo($id, $url)
    {
        $video = $this->model()->find($id);
        $video->update([
            'trailer_url' => $url,
        ]);

        return $this->item($video);
    }

    /**
     * @param $id
     * @param $url
     * @return mixed
     * @deprecated
     */
    public function attachFullVideo($id, $url)
    {
        $video = $this->model()->find($id);
        $video->update([
            'full_url' => $url,
        ]);

        return $this->item($video);
    }

    /**
     * @param array $attributes
     * @param $id
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function removeAttachVideo(array $attributes, $id)
    {
        $this->validate($attributes, [
            'filename' => [
                'required',
                'string',
                Rule::in([Video::TRAILER_VIDEO, Video::FULL_VIDEO]),
            ],
        ]);

        $filename = array_get($attributes, 'filename');
        $video = $this->model()->find($id);

        switch ($filename) {
            case Video::TRAILER_VIDEO:
                Storage::delete($video['trailer_url']);
                $video->update(['trailer_url' => null,]);
                break;

            case Video::FULL_VIDEO:
                Storage::delete($video['full_url']);
                $video->update(['full_url' => null,]);
                break;
        }

        return $this->item($video);
    }

    /**
     * @param array $attributes
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function removeOnlyVideo(array $attributes)
    {
        $this->validate($attributes, [
            'filename' => [
                'required',
                'string',
                Rule::in([Video::TRAILER_VIDEO, Video::FULL_VIDEO]),
            ],
        ]);

        $filename = array_get($attributes, 'filename');

        switch ($filename) {
            case Video::TRAILER_VIDEO:
                Storage::delete(array_get($attributes, 'trailer_url'));
                break;

            case Video::FULL_VIDEO:
                Storage::delete(array_get($attributes, 'full_url'));
                break;
        }

        return ok();
    }

    public function incrementViewed($id)
    {
        $video = $this->model()->findOrFail($id);
        $video->increment('viewed');

        if ($this->user()) {
            $this->syncPlayedRecent([
                'user_id' => $this->user()->getId(),
            ], $id);
        }

        return $this->item($video);
    }

    /**
     * @param array $attribute
     * @param $id
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function storeRating(array $attribute, $id)
    {
        $this->validate($attribute, [
            'rating' => 'required|integer|between:1,10',
        ]);

        $data = [
            'rating'  => $attribute['rating'],
            'user_id' => $this->getUserId(),
        ];

        /** @var Video $video */
        $video = $this->model()->findOrFail($id);

        $check = $video->ratings()->where('user_id', $this->getUserId())->first();
        if ($check) {
            return msg_already_rating();
        }
        $video->ratings()->create($data);

        return $this
            ->setTransformer(new VideoTransformer(true))
            ->item($video);
    }

    public function syncLike($id)
    {
        /** @var Video $video */
        $video = $this->model()->findOrFail($id);
        $check = $video->isAlreadyLike();

        $data = [
            'like'    => $check ? false : true,
            'user_id' => $this->getUserId(),
        ];

        if ($check) {
            $video->likes()->update($data);
        } else {
            $video->likes()->create($data);
        }

        return $this
            ->setTransformer(new VideoTransformer(true))
            ->item($video);
    }

    public function popularList(int $per_page)
    {
        $videos = $this->model()
            ->whereHas('playlists')
            ->orderBy('viewed', 'desc')->paginate($per_page);

        return $this->collection($videos);
    }

    public function newReleaseList(int $per_page)
    {
        $videos = $this->model()
            ->whereHas('playlists')
            ->orderBy('released_date', 'desc')->paginate($per_page);

        return $this->collection($videos);
    }

    public function syncPlayedRecent(array $attribute, $id)
    {
        /** @var Video $video */
        $video = $this->model()->findOrFail($id);
        $attribute += [
            'user_id' => $this->user()->getId(),
        ];
        $video->played_recent()->where('user_id', $this->user()->getId())->updateOrCreate($attribute);

        return $this->item($video);
    }

    public function getPlayedRecentByUser()
    {
        $video = $this->model()->whereHas('played_recent_by_user')->get();

        return $this
            ->setTransformer(new VideoTransformer(false, null, true))
            ->collection($video);
    }

    public function syncFavorite($id)
    {
        /** @var Video $video */
        $video = $this->model()->findOrFail($id);
        if ($video->hasFavorite()) {
            $video->favorite()->where('user_id', $this->user()->getId())->delete();
        } else {
            $video->favorite()->where('user_id', $this->user()->getId())->firstOrCreate([
                'user_id' => $this->user()->getId(),
            ]);
        }

        return $this->item($video);
    }

    public function getFavoriteByUser($id = null)
    {
        $video = $this->model()->whereHas('favorite_by_user')->get();

        if ($id) {
            $video = $this->model()->whereHas('favorite_by_user')->find($id);

            return $this->item($video);
        }

        return $this
            ->setTransformer(new VideoTransformer(false, null, true))
            ->collection($video);
    }

    /**
     * @param $id
     * @return mixed
     * @deprecated
     */
    public function getLikeByUser($id)
    {
        $video = $this->model()->whereHas('like_by_user')->find($id);

        return $this->item($video);
    }

    public function forSelect()
    {
        return $this->model()->pluck('title', 'id');
    }

    public function getVideoExceptPlaylist($seriesId = null)
    {
        return $this->model()
            ->whereDoesntHave('playlists', function ($sub) use ($seriesId) {
                $sub->where('series_id', '=', $seriesId);
            })->get();
    }

    public function getVideoExceptRecommendedCms()
    {
        return $this->model()->whereDoesntHave('recommended_cms')->get();
    }

    public function getVideoExceptNewReleasedCms()
    {
        return $this->model()->whereDoesntHave('new_released_cms')->get();
    }

    public function status($id)
    {
        /** @var Video $video */
        $video = $this->model()->findOrFail($id);

        return [
            'is_favorite' => $video->hasFavorite(),
            'like_count'  => $video->getLikesCount(),
            'is_liked'    => $video->isAlreadyLike(),
        ];
    }
}
