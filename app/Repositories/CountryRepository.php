<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 11/22/18
 * Time: 6:50 PM
 */

namespace App\Repositories;

use App\Models\Country;
use App\Transformers\CountryTransformer;
use Arga\Storage\Database\BaseRepository;
use Illuminate\Database\Eloquent\Builder;
use League\Fractal\TransformerAbstract;

class CountryRepository extends BaseRepository
{
    public function __construct(TransformerAbstract $abstract = null)
    {
        $this->transformer = $abstract ?? new CountryTransformer();
    }

    /**
     * @return Builder
     */
    protected function model(): Builder
    {
        return Country::query();
    }

    /**
     * @param array $data
     * @param null $id
     * @return array|null
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateData(array $data, $id = null): ?array
    {
        $this->validate($data, [
            'slug' => 'required|string|max:255|unique:countries,slug,'.$id,
            'name' => 'required|string|max:255',
        ]);

        return $data;
    }

    /**
     * @param array $attributes
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(array $attributes)
    {
        $data = $this->validateData($attributes);
        $country = $this->model()->create($data);

        return $this->item($country);
    }

    /**
     * @param array $attributes
     * @param $id
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(array $attributes, $id)
    {
        $data = $this->validateData($attributes, $id);
        $country = $this->model()->find($id);
        $country->update($data);

        return $this->item($country);
    }

    public function forSelect()
    {
        return $this->model()->pluck('name', 'id');
    }
}
