<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 11/21/18
 * Time: 3:12 PM
 */

namespace App\Repositories;

use App\Models\Cast;
use App\Transformers\CastTransformer;
use Arga\Storage\Cloudinary\CloudinaryClient;
use Arga\Storage\Database\BaseRepository;
use Arga\Storage\Database\QueryFilterTrait;
use Illuminate\Database\Eloquent\Builder;
use League\Fractal\TransformerAbstract;

class CastRepository extends BaseRepository
{
    private $image;

    use QueryFilterTrait;

    public function __construct(TransformerAbstract $abstract = null)
    {
        $this->transformer = $abstract ?? new CastTransformer();
        $this->image = app(CloudinaryClient::class);
    }

    /**
     * @return Builder
     */
    protected function model(): Builder
    {
        $model = Cast::query();

        return $this->applyQueryFilter($model);
    }

    /**
     * @param array $data
     * @param null $id
     * @return array|null
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateData(array $data, $id = null): ?array
    {
        $this->validate($data, [
            'name'     => 'required|string|max:255',
            'slug'     => 'required|string|max:255|unique:casts,slug,'.$id,
            'born_at'  => 'nullable|date',
            'gender'   => 'required|string',
            'position' => 'nullable|string',
        ]);

        return $data;
    }

    /**
     * @param array $attributes
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(array $attributes)
    {
        $data = $this->validateData($attributes);

        $cast = $this->model()->create($data);

        if (array_get($attributes, 'file_id')) {
            /** @var Cast $cast */
            $this->onSaving($cast, $attributes);
        }

        return $this->item($cast);
    }

    /**
     * @param array $attributes
     * @param $id
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(array $attributes, $id)
    {
        $data = $this->validateData($attributes, $id);

        $cast = $this->model()->find($id);
        $cast->update($data);

        /** @var Cast $cast */
        $this->onSaving($cast, $attributes);

        return $this->item($cast);
    }

    /**
     * @param \App\Models\Cast $cast
     * @param array $attributes
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function onSaving(Cast $cast, array $attributes)
    {
        if ($ids = array_get($attributes, 'images')) {
            $this->validate($attributes, [
                'images' => 'required|array',
            ]);

            return $this->image->update($ids, $cast);
        }
    }

    public function forSelect()
    {
        return $this->model()->pluck('name', 'id');
    }

    public function getCastExceptCms()
    {
        return $this->model()->whereDoesntHave('cms')->get();
    }
}
