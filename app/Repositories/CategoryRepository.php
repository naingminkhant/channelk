<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 11/20/18
 * Time: 12:54 PM
 */

namespace App\Repositories;

use App\Models\Category;
use App\Models\SubCategory;
use App\Transformers\CategoryTransformer;
use Arga\Storage\Database\BaseRepository;
use Illuminate\Database\Eloquent\Builder;
use League\Fractal\TransformerAbstract;
use Arga\Storage\Cloudinary\CloudinaryClient;

class CategoryRepository extends BaseRepository
{
    protected $image;
    public function __construct(TransformerAbstract $transformer = null)
    {
        $this->transformer = $transformer ?? new CategoryTransformer();
        $this->image = app(CloudinaryClient::class);
    }

    /**
     * @return Builder
     */
    protected function model(): Builder
    {
        return Category::query()->with([
            'sub_categories',
        ]);
    }

    /**
     * @param array $data
     * @param null $id
     * @return array|null
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateData(array $data, $id = null): ?array
    {
        $this->validate($data, [
            'slug'  => 'required|string|max:255|unique:categories,slug,'.$id,
            'title' => 'required|string|max:255',
        ]);

        return $data;
    }

    /**
     * @param array $attribute
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(array $attribute)
    {
        $data = $this->validateData($attribute);
        $category = $this->model()->create($data);

        /** @var Category $category */
        $this->onSaving($category, $attribute);

        return $this->item($category);
    }

    protected function onSaving(Category $category, array $attributes)
    {
        if ($ids = array_get($attributes, 'images')) {
            $this->image->update($ids, $category);

            return;
        }
    }

    /**
     * @param array $attribute
     * @param null $id
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(array $attribute, $id = null)
    {
        $data = $this->validateData($attribute, $id);
        $model = $this->model()->find($id);
        $model->update($data);

        /** @var Category $model */
        $this->onSaving($model, $attribute);

        return $this->item($model);
    }

    public function forSelect()
    {
        return $this->model()->pluck('title', 'id');
    }

    public function forSelectSubCategory()
    {
        return $this->model()->getRelation('sub_categories')->pluck('title','id');
    }

    /**
     * @param array $data
     * @param $id
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateSubCategory(array $data, $id = null): array
    {
        $this->validate($data, [
            'slug'  => 'required|string|max:255|unique:sub_categories,slug,'.$id,
            'title' => 'required|string|max:255',
        ]);

        return $data;
    }

    /**
     * @param array $attribute
     * @param $categoryId
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function storeSubCategory(array $attribute, $categoryId)
    {
        $data = $this->validateSubCategory($attribute);
        /** @var Category $model */
        $model = $this->model()->findOrFail($categoryId);
        $model->sub_categories()->create($data);

        return $this->item($model);
    }

    protected function subCategoryModel()
    {
        return SubCategory::query();
    }

    public function findSubCategory($id)
    {
        /** @var SubCategory $model */
        $model = $this->subCategoryModel()->findOrFail($id);

        return $model->toAll();
    }

    /**
     * @param array $attribute
     * @param $id
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updateSubCategory(array $attribute, $id)
    {
        $data = $this->validateSubCategory($attribute, $id);
        $model = $this->subCategoryModel()->findOrFail($id);
        $model->update($data);

        return $model;
    }

    /**
     * @param $id
     * @return bool|mixed|null
     * @throws \Exception
     */
    public function destroySubCategory($id)
    {
        $model = $this->subCategoryModel()->findOrFail($id);
        $status = $model->delete();

        return ['status' => $status];
    }
}
