<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 11/21/18
 * Time: 6:37 PM
 */

namespace App\Repositories;

use App\Models\Company;
use App\Transformers\CompanyTransformer;
use Arga\Storage\Database\BaseRepository;
use Illuminate\Database\Eloquent\Builder;
use League\Fractal\TransformerAbstract;

class CompanyRepository extends BaseRepository
{
    public function __construct(TransformerAbstract $abstract = null)
    {
        $this->transformer = $abstract ?? new CompanyTransformer();
    }

    /**
     * @return Builder
     */
    protected function model(): Builder
    {
        return Company::query();
    }

    /**
     * @param array $data
     * @param null $id
     * @return array|null
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateData(array $data, $id = null): ?array
    {
        $this->validate($data, [
            'slug' => 'required|string|max:255|unique:production_companies,slug,'.$id,
            'name' => 'required|string|max:255',
        ]);

        return $data;
    }

    /**
     * @param array $attributes
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(array $attributes)
    {
        $data = $this->validateData($attributes);

        $company = $this->model()->create($data);

        return $this->item($company);
    }

    /**
     * @param array $attributes
     * @param $id
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(array $attributes, $id)
    {
        $data = $this->validateData($attributes, $id);
        $company = $this->model()->find($id);
        $company->update($data);

        return $this->item($company);
    }

    public function forSelect()
    {
        return $this->model()->pluck('name', 'id');
    }
}
