<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 11/28/18
 * Time: 10:49 PM
 */

namespace App\Repositories;

use App\Models\ContactUs;
use App\Transformers\ContactUsTransformer;
use Arga\Storage\Database\BaseRepository;
use Illuminate\Database\Eloquent\Builder;
use League\Fractal\TransformerAbstract;

class ContactUsRepository extends BaseRepository
{
    public function __construct(TransformerAbstract $abstract = null)
    {
        $this->transformer = $abstract ?? new ContactUsTransformer();
    }

    /**
     * @return Builder
     */
    protected function model(): Builder
    {
        return ContactUs::query();
    }

    /**
     * @param array $data
     * @param null $id
     * @return array|null
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateData(array $data, $id = null): ?array
    {
        $this->validate($data, [
            'body' => 'required|string',
        ]);

        return $data;
    }

    /**
     * @param array $attributes
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function save(array $attributes)
    {
        $data = $this->validateData($attributes);
        if ($this->model()->exists()) {
            $about = $this->model()->first();
            $about->update($data);
        } else {
            $about = $this->model()->create($data);
        }

        return $this->item($about);
    }

    public function first()
    {
        $about = $this->model()->first();

        return $this->item($about);
    }
}
