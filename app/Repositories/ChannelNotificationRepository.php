<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 12/13/18
 * Time: 2:46 PM
 */

namespace App\Repositories;

use App\Models\ChannelNotification;
use App\Transformers\ChannelNotificationTransformer;
use Arga\Auth\User;
use Arga\Storage\Database\BaseRepository;
use Arga\Storage\Database\QueryFilterTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Notification;
use League\Fractal\TransformerAbstract;

class ChannelNotificationRepository extends BaseRepository
{
    use QueryFilterTrait;

    private $notification;

    public function __construct(TransformerAbstract $abstract = null)
    {
        $this->transformer = $abstract ?? new ChannelNotificationTransformer();
        $this->notification = DataRepo::notification();
    }

    /**
     * @return Builder
     */
    protected function model(): Builder
    {
        $model = ChannelNotification::query();
        $model = $this->applyQueryFilter($model);

        return $model;
    }

    protected function validateData(array $data, $id = null): ?array
    {
        return $data;
    }

    public function store(array $attributes)
    {
        $data = $this->validateData($attributes);
        $model = $this->model()->create($data);

        /** @var ChannelNotification $model */
        $this->onSaving($model, $data);

        return $this->item($model);
    }

    protected function onSaving(ChannelNotification $model, array $data)
    {
        $users = null;
        switch (array_get($data, 'recipient_type')) {
            case ChannelNotification::ALL_USERS:
                $users = User::all();
                break;
            case ChannelNotification::SPECIFIC_USERS:
                $users = User::whereIn('id', array_get($data, 'recipient_users'))->get();
                break;
            default:
                $users = null;
                break;
        }

        $model->user_notifications()->delete();

        foreach ($users as $user) {
            $user->related_model()->create([
                'channel_notification_id' => $model->getId(),
            ]);
        }
    }

    public function update(array $attributes, $id)
    {
        $data = $this->validateData($attributes);
        $model = $this->model()->find($id);
        $model->update($data);
        /** @var ChannelNotification $model */
        $this->onSaving($model, $data);

        return $this->item($model);
    }

    public function published_at($id)
    {
        $model = $this->model()->findOrFail($id);
        $model->update([
            'published_at' => now(),
        ]);

        return $this->item($model);
    }

    public function read_at($id)
    {
        $this->user()->related_model()->where('channel_notification_id', $id)->update([
            'read_at' => now(),
        ]);

        return $this
            ->setTransformer(new ChannelNotificationTransformer(false, true))
            ->find($id);
    }

    public function paginate()
    {
        $model = $this->model()->paginate();
        $count = 0;
        if ($this->user()) {
            $count = $this->user()->related_model()->whereNotNull('read_at')->count();
        }

        return $this
            ->setExtraData([
                'total'  => $model->total(),
                'unread' => $model->total() - $count,
            ])
            ->collection($model);
    }
}
