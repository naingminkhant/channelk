<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 12/17/18
 * Time: 8:54 PM
 */

namespace App\Repositories;

use App\Models\Comment;
use App\Models\SeriesVideo;
use App\Models\Video;
use App\Transformers\CommentTransformer;
use Arga\Storage\Database\BaseRepository;
use Illuminate\Database\Eloquent\Builder;

class CommentRepository extends BaseRepository
{
    protected $video;

    protected $series;

    public function __construct($abstract = null)
    {
        $this->transformer = $abstract ?? app(CommentTransformer::class);
        $this->video = DataRepo::video();
        $this->series = DataRepo::series_video();
    }

    /**
     * @return Builder
     */
    protected function model(): Builder
    {
        return Comment::query();
    }

    /**
     * @param array $data
     * @param null $id
     * @return array|null
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateData(array $data, $id = null): ?array
    {
        $this->validate($data, [
            'comment' => 'required|string',
        ]);

        return $data;
    }

    /**
     * @param array $attribute
     * @return \Illuminate\Database\Eloquent\Model
     * @throws \Illuminate\Validation\ValidationException
     */
    public function storeVideoComment(array $attribute)
    {
        $data = $this->validateData($attribute);
        /** @var Video $video */
        $video = Video::findOrFail($data['video_id']);

        return $video->comment()->create([
            'comment' => $data['comment'],
            'user_id' => $this->getUserId(),
        ]);
    }

    /**
     * @param array $attribute
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function storeSeriesComment(array $attribute)
    {
        $data = $this->validateData($attribute);
        /** @var SeriesVideo $series */
        $series = SeriesVideo::findOrFail($data['series_id']);

        return $series->comment()->create([
            'comment' => $data['comment'],
            'user_id' => $this->getUserId(),
        ]);
    }

    public function findByVideo($video_id, array $data)
    {
        /** @var Video $video */
        $video = $this->video->query()->find($video_id);
        $data = $video->comments()->paginate(array_get($data, 'per_page') ?? 6);

        return $this->collection($data);
    }

    public function findBySeries($series_id, array $data)
    {
        /** @var SeriesVideo $series */
        $series = $this->series->query()->find($series_id);
        $data = $series->comments()->paginate(array_get($data, 'per_page') ?? 6);

        return $this->collection($data);
    }

    /**
     * @param array $attribute
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(array $attribute, $id)
    {
        $data = $this->validateData($attribute);
        $comment = $this->user()->user_comments()->where('comments.id', $id)->first();
        $comment->update([
            'comment' => array_get($data, 'comment'),
        ]);

        return $this->item($comment);
    }
}
