<?php
/**
 * Created by channelk.
 * Author: naingminkhant
 * Date: 2019-01-09
 * Time: 12:55
 */

namespace App\Repositories;

use App\Models\Staff;
use App\Transformers\StaffTransformer;
use Arga\Storage\Database\BaseRepository;
use Illuminate\Database\Eloquent\Builder;
use League\Fractal\TransformerAbstract;

class StaffRepository extends BaseRepository
{
    public function __construct(TransformerAbstract $abstract = null)
    {
        $this->transformer = $abstract ?? new StaffTransformer();
    }

    /**
     * @return Builder
     */
    protected function model(): Builder
    {
        return Staff::query();
    }

    /**
     * @param array $data
     * @param null $id
     * @return array|null
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateData(array $data, $id = null): ?array
    {
        $this->validate($data, [
            'name'                  => 'required|string|max:255',
            'email'                 => 'required|string|max:255|unique:admins,email,'.$id,
            'role_ids'              => 'required|array',
            'password'              => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required|string|min:6',
        ]);

        $data['password'] = bcrypt($data['password']);

        return $data;
    }

    /**
     * @param array $attribute
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(array $attribute)
    {
        $data = $this->validateData($attribute);
        /** @var Staff $staff */
        $staff = $this->model()->create($data);
        $this->onSaving($attribute, $staff);

        return $this->item($staff);
    }

    protected function onSaving(array $attribute, Staff $staff)
    {
        if (!empty($attribute['role_ids'])) {
            return $staff->roles()->sync(array_get($attribute, 'role_ids'));
        }
    }

    /**
     * @param array $attribute
     * @param $id
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(array $attribute, $id)
    {
        $data = $this->validateData($attribute, $id);
        /** @var Staff $staff */
        $staff = $this->model()->findOrFail($id);
        $staff->update($data);
        $this->onSaving($attribute, $staff);

        return $this->item($staff);
    }
}
