<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 12/13/18
 * Time: 4:48 PM
 */

namespace App\Repositories;


use App\Transformers\NotificationTransformer;
use Arga\Notification\Notification;
use Arga\Storage\Database\BaseRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use League\Fractal\TransformerAbstract;

class NotificationRepository extends BaseRepository
{
    public function __construct(TransformerAbstract $abstract = null)
    {
        $this->transformer = $abstract ?? app(NotificationTransformer::class);
    }

    /**
     * @return Builder
     */
    protected function model(): Builder
    {
        return Notification::query();
    }

    protected function validateData(array $data, $id = null): ?array
    {
        return $data;
    }

    public function store(Model $model, $notiId)
    {
        $model->related_model()->create([
            'channel_notification_id' => $notiId
        ]);

        return $model;
    }
}
