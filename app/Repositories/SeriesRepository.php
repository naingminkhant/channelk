<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 11/28/18
 * Time: 1:53 PM
 */

namespace App\Repositories;

use App\Models\SeriesVideo;
use App\Transformers\SeriesTransformer;
use Arga\Storage\Cloudinary\CloudinaryClient;
use Arga\Storage\Database\BaseRepository;
use Arga\Storage\Database\QueryFilterTrait;
use Illuminate\Database\Eloquent\Builder;
use League\Fractal\TransformerAbstract;
use Illuminate\Support\Facades\Storage;

class SeriesRepository extends BaseRepository
{
    private $image;

    use QueryFilterTrait;

    public function __construct(TransformerAbstract $abstract = null)
    {
        $this->transformer = $abstract ?? new SeriesTransformer();
        $this->image = app(CloudinaryClient::class);
    }

    /**
     * @return Builder
     */
    protected function model(): Builder
    {
        $model = SeriesVideo::query();
        if (!$this->isAdmin()) {
            $model = $model->whereDate('published_at', '<=', now());
        }

        return $this->applyQueryFilter($model);
    }

    public function query(): Builder
    {
        return SeriesVideo::query();
    }

    /**
     * @param array $data
     * @param null $id
     * @return array|null
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateData(array $data, $id = null): ?array
    {
        $this->validate($data, [
            'title_en'              => 'required|string|max:255',
            'title_mm'              => 'required|string|max:255',
            'slug'                  => 'required|string|max:255|unique:series,slug,'.$id,
            'country_id'            => 'required|exists:countries,id',
            'story_line'            => 'nullable|string|max:65535',
            'released_date'         => 'nullable|date',
            'budget'                => 'nullable|numeric',
            'director_id'           => 'required|exists:directors,id',
            'production_company_id' => 'required|exists:production_companies,id',
            'published_at'          => 'nullable|date',
            'trailer_url'           => 'nullable|string',
        ]);

        $data['title'] = array_get($data, 'title_en');

        return $data;
    }

    /**
     * @param array $attributes
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(array $attributes)
    {
        $data = $this->validateData($attributes);
        $series = $this->model()->create($data);

        /** @var SeriesVideo $series */
        $this->onSaving($series, $data);

        return $this->item($series);
    }

    /**
     * @param \App\Models\SeriesVideo $video
     * @param array $data
     */
    protected function onSaving(SeriesVideo $video, array $data)
    {
        if ($categories = array_get($data, 'category_ids')) {
            $video->categories()->sync($categories);
        }
        if ($casts = array_get($data, 'cast_ids')) {
            $video->stars()->sync($casts);
        }
        if ($images = array_get($data, 'images')) {
            $this->image->update($images, $video);
        }
        if ($images = array_get($data, 'cover_images')) {
            $this->image->updateCoverImage($images, $video);
        }
        if ($attached = array_get($data, 'attached_list')) {
            $video->videos()->detach();
            foreach ($attached as $key => $value) {
                $video->videos()->sync([$value => ['sort' => $key]], false);
            }
        }
    }

    /**
     * @param array $attributes
     * @param $id
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(array $attributes, $id)
    {
        $data = $this->validateData($attributes, $id);
        $series = $this->model()->find($id);
        $series->update($data);

        /** @var SeriesVideo $series */
        $this->onSaving($series, $data);

        return $this->item($series);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function removeAttachVideo($id)
    {
        $video = $this->model()->find($id);

        Storage::delete($video['trailer_url']);

        $video->update(['trailer_url' => null]);

        return $this->item($video);
    }

    public function incrementViewed($id)
    {
        $video = $this->model()->find($id);
        $video->increment('viewed');

        return $this->item($video);
    }

    /**
     * @param array $attribute
     * @param $id
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function storeRating(array $attribute, $id)
    {
        $this->validate($attribute, [
            'rating' => 'required|integer|between:1,10',
        ]);

        $data = [
            'rating'  => $attribute['rating'],
            'user_id' => $this->getUserId(),
        ];

        /** @var SeriesVideo $series */
        $series = $this->model()->findOrFail($id);
        $check = $series->isAlredyUserRating();

        if ($check) {
            return msg_already_rating();
        }
        $series->rating()->create($data);

        return $this
            ->setTransformer(new SeriesTransformer(true))
            ->item($series);
    }

    public function syncLike($id)
    {
        /** @var SeriesVideo $series */
        $series = $this->model()->findOrFail($id);
        $check = $series->isAlreadyLike();
        $data = [
            'like'    => $check ? false : true,
            'user_id' => $this->getUserId(),
        ];

        if ($check) {
            $series->likes()->update($data);
        } else {
            $series->likes()->create($data);
        }

        return $this
            ->setTransformer(new SeriesTransformer(true))
            ->item($series);
    }

    public function popularList(int $per_page)
    {
        $series = $this->model()->orderBy('viewed', 'desc')->paginate($per_page);

        return $this->collection($series);
    }

    public function newReleaseList(int $per_page)
    {
        $series = $this->model()->orderBy('released_date', 'desc')->paginate($per_page);

        return $this->collection($series);
    }

    public function syncPlayedRecent(array $attribute, $id)
    {
        /** @var SeriesVideo $video */
        $series = $this->model()->findOrFail($id);
        $attribute += [
            'user_id' => $this->user()->getId(),
        ];
        $series->played_recent()->where('user_id', $this->user()->getId())->updateOrCreate($attribute);

        return $this->item($series);
    }

    public function getPlayedRecentByUser()
    {
        $series = $this->model()->whereHas('played_recent_by_user')->get();

        return $this->collection($series);
    }

    public function syncFavorite($id)
    {
        /** @var SeriesVideo $video */
        $series = $this->model()->findOrFail($id);
        if ($series->hasFavorite()) {
            $series->favorite()->where('user_id', $this->user()->getId())->delete();
        } else {
            $series->favorite()->where('user_id', $this->user()->getId())->firstOrCreate([
                'user_id' => $this->user()->getId(),
            ]);
        }

        return $this->item($series);
    }

    public function getFavoriteByUser()
    {
        $series = $this->model()->whereHas('favorite_by_user')->get();

        return $this->collection($series);
    }

    public function getSeriesExceptRecommendedCms()
    {
        return $this->model()->whereDoesntHave('recommended_cms')->get();
    }

    public function getSeriesExceptCms()
    {
        return $this->model()->whereDoesntHave('cms')->get();
    }

    public function forSelect()
    {
        return $this->model()->pluck('title', 'id');
    }
}
