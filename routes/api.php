<?php
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * Auth
 */
Route::namespace('Auth')->group(function () {
    Route::post('register', 'RegisterController@register');
    Route::post('login', 'LoginController@login');
    Route::post('auth/facebook', 'SocialController@facebook');
    Route::post('logout', 'LoginController@logout')->middleware('auth:api');
    Route::post('password/request', 'ForgetPasswordController@request');
    Route::post('password/reset', 'ForgetPasswordController@reset');
});

/**
 * Required Auth
 */
Route::group(['middleware' => 'auth:api'], function () {
    Route::get('me', 'UserController@me');
    Route::post('me', 'UserController@update');

    /**
     * Video Like, Rating, Comment
     */
    Route::post('comment/video/{video_id}', 'CommentController@storeVideoComment');
    Route::patch('comment/{comment_id}', 'CommentController@updateComment');
    Route::post('video/{id}/rating', 'VideoController@storeRating');
    Route::post('video/{id}/like', 'VideoController@syncLike');

    /**
     * Series Like, Rating, Comment
     */
    Route::post('comment/series/{series_id}', 'CommentController@storeSeriesComment');
    Route::post('series/{id}/rating', 'SeriesController@storeRating');
    Route::post('series/{id}/like', 'SeriesController@syncLike');

    Route::delete('comment/{id}', 'CommentController@destroy');

    /**
     * Played Recent
     */
    Route::post('video/{id}/played-recent', 'VideoController@syncPlayedRecent');
    Route::get('video/played-recent', 'VideoController@playedRecent');
    Route::post('series/{id}/played-recent', 'SeriesController@syncPlayedRecent');
    Route::get('series/played-recent', 'SeriesController@playedRecent');

    /**
     * Favorite Video, Series
     */
    Route::post('video/{id}/favorite', 'VideoController@syncFavorite');
    Route::get('video/favorite', 'VideoController@favorite');
    Route::post('series/{id}/favorite', 'SeriesController@syncFavorite');
    Route::get('series/favorite', 'SeriesController@favorite');

    /**
     * Notification
     */
    Route::get('notification', 'NotificationController@index');
    Route::get('notification/{id}', 'NotificationController@show');

    /**
     * Search Keyword
     */
    Route::get('user/search-keyword','UserController@searchKeyword');
    Route::delete('user/search-keyword/{id}','UserController@destroySearchKeyword');
});

@require_once 'public-api.php';
