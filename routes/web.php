<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * Backend Auth
 */
Route::prefix(config('system.backend_url'))
    ->namespace('Auth\Admin')
    ->group(function () {
        Route::get('/login', 'LoginController@showLoginForm')->name('admin.login');
        Route::post('/login', 'LoginController@login')->name('admin.login.submit');
        Route::get('/logout', 'LoginController@logout')->name('admin.logout');
    });

/**
 * Frontend
 */
Auth::routes(['verify' => true]);

Route::get('test/login', function() {
    $user = \Arga\Auth\User::find(12);
    Auth::login($user);
});

Route::get('auth/{provider}', 'Auth\RegisterController@redirectToProvider')->name('auth.social.redirect');
Route::get('auth/{provider}/callback', 'Auth\RegisterController@handleProviderCallback')->name('auth.social.callback');

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/series/{series_id}', 'PagesController@series')->name('series.index');
Route::get('/series/{series_id}/video/{video_id}', 'PagesController@seriesDetail')->name('series.show');

Route::get('/series-by-category/{category_id}', 'PagesController@seriesByCategory');

Route::get('search', 'SearchController@showAll');

Route::get('/search-by-video', 'SearchController@byVideo');
Route::get('/search-by-series', 'SearchController@bySeries');
Route::get('/search-by-cast', 'SearchController@byCast');

Route::get('/live', 'HomeController@live')->name('live');

Route::get('/privacy-policy', 'HomeController@privacy')->name('privacy');
Route::get('/terms-and-conditions', 'HomeController@termsAndConditions')->name('terms.and.conditions');
Route::get('/about-us', 'HomeController@aboutUsPage')->name('about.us');
Route::get('/contact-us', 'HomeController@contactUsPage')->name('contact.us');
Route::get('/faq', 'HomeController@faqPage')->name('faq');
Route::get('/support-guide', 'HomeController@supportGuidePage')->name('support.guide');

Route::get('new-releases', 'PagesController@new_releases');
Route::get('series-recommended', 'PagesController@series_recommended');
Route::get('tv-shows', 'PagesController@tv_show');
Route::get('hot-videos', 'PagesController@hot_videos');
Route::get('hot-series', 'PagesController@hot_series');
Route::get('recommended-videos', 'PagesController@recommended_videos');
Route::get('recommended-castors', 'PagesController@recommended_castors');
Route::get('recommended-categories', 'PagesController@recommended_category');

Route::get('castors', 'PagesController@castors');
Route::get('castor/{id}', 'PagesController@castor');
Route::get('categories', 'PagesController@categories');

Route::get('agenda/{date}', 'PagesController@agenda');

Route::get('/comment/video/{video_id}', 'UserController@getComments');

Route::group([ 'middleware' => 'auth'], function() {
    Route::post('/comment/video/{video_id}', 'UserController@storeVideoComment');
    Route::delete('/comment/video/{video_id}', 'UserController@destroy');

    Route::post('video/{id}/favorite', 'UserController@syncVideoFavorite');
    Route::post('video/{id}/like', 'UserController@syncVideoLike');

    Route::get('/profile', 'UserController@profile');
    Route::get('/setting', 'UserController@setting');
    Route::post('/password/change','UserController@passwordChange')->name('user.password.change');
    Route::post('/account/edit_account_info','UserController@editProfile');
    Route::get('/notification/{id}', 'UserController@notification');
    Route::post('/notification-read', 'UserController@readNotification');
});

Route::get('/download-android-apk','DownloadAppController@android');
