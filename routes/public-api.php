<?php
/**
 * Created by channelk.
 * Author: naingminkhant
 * Date: 2019-04-24
 * Time: 12:56
 */

/**
 * Individual Video
 */
Route::get('video', 'VideoController@index');
Route::get('video/{id}', 'VideoController@show');
Route::get('video/{id}/like_comment', 'VideoController@like_comment'); // Get Like, Favorite
Route::get('video/{id}/comment', 'CommentController@findByVideo');
Route::post('video/{id}/increment-viewed', 'VideoController@incrementViewed');
Route::get('video-popular-list', 'VideoController@popularList');
Route::get('video-new-list', 'VideoController@newReleaseList');

/**
 * Series
 */
Route::get('series', 'SeriesController@index');
Route::get('series/{id}', 'SeriesController@show');
Route::get('series/{id}/comment', 'CommentController@findBySeries');
Route::post('series/{id}/increment-viewed', 'SeriesController@incrementViewed');
Route::get('series-popular-list', 'SeriesController@popularList');
Route::get('series-new-list', 'SeriesController@newReleaseList');

Route::get('category', 'CategoryController@index');
Route::get('category/{id}', 'CategoryController@show');

/**
 * CMS
 */
Route::get('cms/recommended/video', 'CmsController@recommended_video');
Route::get('cms/recommended/playlist', 'CmsController@recommended_playlist');
Route::get('cms/new-released/video', 'CmsController@new_released_video');
Route::get('cms/playlist', 'CmsController@playlist');
Route::get('cms/cast', 'CmsController@cast');

Route::get('cast/{id}', 'CastController@show');

Route::get('banner', 'BannerController@index');
Route::get('search', 'SearchController@index');
Route::get('autocomplete', 'SearchController@autocomplete');
Route::get('agenda', 'AgendaController@index');

Route::get('info', 'InfoController@index');

Route::get('rtmp', 'RtmpController@index');

/**
 * Pages
 */
Route::get('about-us','AboutUsController@index');
Route::get('contact-us','ContactUsController@index');
Route::get('support','SupportController@index');
Route::get('faq','FaqController@index');
