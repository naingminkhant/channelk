<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 11/19/18
 * Time: 1:35 PM
 */

Route::get('/', 'DashboardController@index')->name('dashboard');

Route::group([
    'as' => 'backend.',
], function () {
    Route::resource('user', 'UserController');
    Route::resource('category', 'CategoryController');
    Route::resource('{category_id}/sub-category', 'SubCategoryController');

    Route::resource('director', 'DirectorController');
    Route::resource('cast', 'CastController');
    Route::resource('company', 'CompanyController');
    Route::resource('country', 'CountryController');

    /**
     * Individual Video
     */
    Route::post('video/{id}/delete', 'VideoController@destroyVideo')->name('destroy-video');
    Route::resource('video', 'VideoController');

    /**
     * Upload Video
     */
    Route::post('only-video/delete', 'VideoController@destroyOnlyVideo')->name('destroy-only-video');
    Route::post('video/upload', 'VideoController@upload')->name('upload-video');

    /**
     * Series Video
     */
    Route::post('series/{id}/delete', 'SeriesController@destroyVideo')->name('series.destroy-video');
    Route::resource('series', 'SeriesController');
    Route::group([
        'prefix' => 'episode',
        'as'     => 'episode.',
    ], function () {
        Route::get('{seriesId}/create', 'EpisodeController@create')->name('create');
        Route::post('{seriesId}/store', 'EpisodeController@store')->name('store');
        Route::get('{seriesId}/edit/{id}', 'EpisodeController@edit')->name('edit');
        Route::patch('{seriesId}/update/{id}', 'EpisodeController@update')->name('update');
        Route::delete('{seriesId}/destroy/{id}', 'EpisodeController@destroy')->name('destroy');
        Route::get('{seriesId}/{id}', 'EpisodeController@show')->name('show');
        Route::post('video/{id}/delete', 'EpisodeController@destroyVideo')->name('destroy-video');
    });

    Route::resource('banner', 'BannerController');
    Route::resource('channel-agenda', 'ChannelAgendaController');

    /**
     * File Manage (Image)
     *
     * @todo video
     */
    Route::post('upload/files', 'FileUploadController@upload')->name('upload-file');
    Route::post('delete/files/{id}', 'FileUploadController@destroy')->name('destroy-file');

    /**
     * Notification
     */
    Route::get('notification/{id}/published', 'NotificationController@published')->name('notification.published');
    Route::resource('notification', 'NotificationController');
    //Route::get('send-notice', 'NoticeController@sendNotice');

    /**
     * Role & Permission
     */
    Route::resource('role', 'RoleController');
    Route::get('permission/{role_slug}', 'PermissionController@index')->name('permission.index');
    Route::post('permission/{role_id}', 'PermissionController@update')->name('permission.update');

    Route::resource('staff', 'StaffController');
    Route::resource('rtmp-stream', 'RtmpStreamController');
    Route::resource('info', 'InfoController')
        ->middleware('permission:info-manage');

    /**
     * CMS
     */
    Route::group(['prefix' => 'cms'], function () {
        Route::get('recommended/video', 'CmsController@individual_index')->name('cms.video-recommended');
        Route::post('recommended/video', 'CmsController@individual_store')->name('cms.video-recommended.store');

        Route::get('recommended/playlist', 'CmsController@playlist_index')->name('cms.playlist-recommended');
        Route::post('recommended/playlist', 'CmsController@playlist_store')->name('cms.playlist-recommended.store');

        Route::get('new-released/video', 'CmsController@new_released_video_index')->name('cms.new-released.video');
        Route::post('new-released/video', 'CmsController@new_released_video_store')
            ->name('cms.new-released.video.store');

        Route::get('series', 'CmsController@series_index')->name('cms.playlist');
        Route::post('series', 'CmsController@series_store')->name('cms.playlist.store');

        Route::get('cast', 'CmsController@cast_index')->name('cms.cast');
        Route::post('cast', 'CmsController@cast_store')->name('cms.cast.store');
    });

    Route::delete('comment/{id}', 'CommentController@destroy')->name('comment.destroy');

    /**
     * Pages
     */
    Route::get('about-us', 'AboutUsController@create')->name('about-us.create');
    Route::post('about-us', 'AboutUsController@save')->name('about-us.save');

    Route::get('contact-us', 'ContactUsController@create')->name('contact-us.create');
    Route::post('contact-us', 'ContactUsController@save')->name('contact-us.save');

    Route::get('faq', 'FaqController@create')->name('faq.create');
    Route::post('faq', 'FaqController@save')->name('faq.save');

    Route::get('support', 'SupportController@create')->name('support.create');
    Route::post('support', 'SupportController@save')->name('support.save');
});

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

