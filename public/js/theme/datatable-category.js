var DatatableChildDataLocalDemo = function () {
    var e = function (e) {
        $("<div/>").attr("id", "child_data_local_" + e.data.id).appendTo(e.detailCell).mDatatable({
            data: {type: "local", source: e.data.sub_categories, pageSize: 5},
            layout: {
                theme: "default",
                scroll: !0,
                height: 200,
                minHeight: 200,
                footer: !1,
                spinner: {type: 1, theme: "default"}
            },
            sortable: !0,
            columns: [
                {field: "slug", title: "Slug"},
                {field: "title", title: "Title"},
                {field: "thumbnail", title: "Thumbnail"},
                {
                    field: "Actions",
                    width: 110,
                    title: "Actions",
                    sortable: !1,
                    overflow: "visible",
                    template: function (e, r, a) {
                        let editUrl = "/backend/" + e.category_id + '/sub-category/' + e.id + "/edit";
                        let deleteUrl = "/backend/" + e.category_id + '/sub-category/' + e.id;
                        return '<a href="' + editUrl + '" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit Sub-Category">' +
                            '<i class="la la-edit"></i>' +
                            '</a>' +
                            '<a href="#" id="deleteChild" class="delete_button m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" data-url="' + deleteUrl + '" title="Delete Sub-Category">' +
                            '<i class="la la-trash"></i>' +
                            '</a>'
                    }
                }]
        })
    };
    return {
        init: function () {
            var r = categories;
            $(".m_datatable").mDatatable({
                data: {type: "local", source: r, pageSize: 10},
                layout: {theme: "default", scroll: !1, height: null, footer: !1},
                sortable: !0,
                filterable: !1,
                pagination: !0,
                detail: {title: "Load sub table", content: e},
                search: {input: $("#generalSearch")},
                columns: [
                    {
                        field: "id",
                        title: "",
                        sortable: !1,
                        width: 20,
                        textAlign: "center"
                    },
                    {
                        field: "slug",
                        title: "Slug"
                    },
                    {
                        field: "title",
                        title: "Title"
                    },
                    {
                        field: "thumbnail",
                        title: "Thumbnail",
                        template:function (e, r, a) {
                            return '<img src="'+e.thumbnail+'" alt="">'
                        }
                    },
                    {
                        field: "Actions",
                        width: 110,
                        title: "Actions",
                        sortable: !1,
                        overflow: "visible",
                        template: function (e, r, a) {
                            let editUrl = "/backend/category/" + e.id + "/edit";
                            let deleteUrl = "/backend/category/" + e.id;
                            let addSubUrl = "/backend/" + e.id + "/sub-category/create";
                            return '<div class="dropdown ' + (a.getPageSize() - r <= 4 ? "dropup" : "") + '">' +
                                '<a href="#" class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown">' +
                                '<i class="la la-ellipsis-h"></i>' +
                                '</a>' +
                                '<div class="dropdown-menu dropdown-menu-right">' +
                                '<a class="dropdown-item" href=' + editUrl + '>' +
                                '<i class="la la-edit"></i> Edit' +
                                '</a>' +
                                '<a class="delete_button dropdown-item" href="#" data-url=' + deleteUrl + '>' +
                                '<i class="la la-trash"></i> Delete' +
                                '</a>' +
                                '</div>' +
                                '<a href="' + addSubUrl + '" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--pill" title="Create Sub-Category">' +
                                'Create Sub-Category' +
                                '</a>'
                        }
                    }]
            })
        }
    }
}();
jQuery(document).ready(function () {
    DatatableChildDataLocalDemo.init();
    $(document).on('click', '.delete_button', function (e) {
        confirmDelete($(this).data('url'));
    });

    function confirmDelete(url) {
        let onConfirm = function () {
            $.ajax({
                type: 'DELETE',
                url: url,
                dataType: 'json',
                success: function (data) {
                    swal("Deleted!", "Your record has been deleted.", "success").then(ans => {
                        ans.value ?
                            window.location.reload() :
                            toastr.error(data);
                    })
                },
                error: function (data) {
                    console.log(data);
                }
            })
        };

        swal({
            title: "Are you sure?",
            text: "You won't be able to revert this!",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            reverseButtons: !0
        }).then(function (ans) {
            ans.value ?
                onConfirm() :
                "cancel" === ans.dismiss && swal("Cancelled", "Your record is safe :)", "error")
        });
    }
});
