var SummernoteDemo = {
    init: function () {
        $('textarea').summernote('fontName', 'Ours-Unicode', {
            height: 300,
            placeholder: 'write here...',
        });
        $('.note-editor.note-frame .note-placeholder').css({
            'padding-top': '20px'
        });
        $('.note-editor.note-frame .note-editing-area .note-editable').css({
            'padding-top': '20px'
        })
    }
};
jQuery(document).ready(function () {
    SummernoteDemo.init()
});