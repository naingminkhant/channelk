webpackJsonp([3],{

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}],\"syntax-dynamic-import\"]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/backend/components/image/ImageWidget.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__UploadImageWidget__ = __webpack_require__("./resources/backend/components/image/UploadImageWidget.vue");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__UploadImageWidget___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__UploadImageWidget__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
    props: {
        preview: {
            default: false
        },
        thumbs: null,
        name: {
            default: 'images[]',
            type: String
        },
        only_one: false
    },
    components: {
        ImageUploadWidget: __WEBPACK_IMPORTED_MODULE_0__UploadImageWidget___default.a
    },
    data: function data() {
        return {
            openWidget: false,
            images: []
        };
    },
    mounted: function mounted() {
        if (!this.only_one) this.preview ? this.images = uploaded_image : null;else this.preview ? this.images.push(cover_image) : null;

        this.thumbs ? this.images.push(JSON.parse(this.thumbs)) : null;
    },

    methods: {
        removeImage: function removeImage(image) {
            var vm = this;
            swal({
                title: "Are you sure?",
                text: "You won't be able to revert this!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                reverseButtons: !0
            }).then(function (ans) {
                if (!ans) {
                    return;
                }
                axios.post(file_delete_url + '/' + image.id).then(function (res) {
                    vm.images.splice(vm.images.indexOf(image), 1);
                }).catch(function (err) {
                    console.log(err);
                });
            });
        },
        onClose: function onClose(files) {
            this.openWidget = false;
            if (!files) {
                return;
            }
            if (this.only_one && this.images.length) {
                return toastr.warning("Allow only one!");
            }
            for (var id in files) {
                this.images.push({
                    url: files[id].dataUrl,
                    id: files[id].id
                });
            }
        }
    },
    watch: {}
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}],\"syntax-dynamic-import\"]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/backend/components/image/UploadImageWidget.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue2_dropzone_dist_vue2Dropzone_css__ = __webpack_require__("./node_modules/vue2-dropzone/dist/vue2Dropzone.css");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue2_dropzone_dist_vue2Dropzone_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue2_dropzone_dist_vue2Dropzone_css__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
    name: 'app',
    components: {
        vueDropzone: function vueDropzone() {
            return __webpack_require__.e/* import() */(0/* duplicate */).then(__webpack_require__.bind(null, "./node_modules/vue2-dropzone/dist/vue2Dropzone.js"));
        }
    },
    props: {
        onClose: {
            type: Function,
            required: true
        },
        maxFiles: {
            default: 30
        }
    },
    data: function data() {
        return {
            files: {},
            dropzoneOptions: {
                url: file_upload_url,
                thumbnailWidth: 150,
                maxFilesize: 3,
                maxFiles: this.maxFiles,
                acceptedFiles: 'image/*',
                paramName: 'images[]',
                addRemoveLinks: true,
                dictDefaultMessage: "<i class='fa fa-cloud-upload fa-fw'></i> Drop files here to upload",
                parallelUploads: 10,
                headers: {
                    'X-CSRF-TOKEN': token.content
                }
            }
        };
    },
    methods: {
        onClickCross: function onClickCross() {
            this.onClose(false);
        },
        onAddImage: function onAddImage() {
            this.onClose(this.files);
        },
        onRemoveFile: function onRemoveFile(file) {
            delete this.files[file.upload.uuid];
        },
        onCanceled: function onCanceled(file) {},
        onUploadError: function onUploadError(file, response, xhr) {
            if (xhr.status === 422 && response.message) {
                toastr.error(response.message);
            }
            console.log(file, message, xhr);
        },
        onSuccessUpload: function onSuccessUpload(file, response) {
            this.files[file.upload.uuid] = {
                dataUrl: file.dataURL,
                id: response[0].id
            };
        }
    }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1f9cd804\",\"scoped\":false,\"hasInlineConfig\":true}!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/backend/components/image/UploadImageWidget.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.upload-wrapper {\n    position: fixed;\n    top: 0;\n    left: 0;\n    right: 0;\n    bottom: 0;\n    min-height: 100%;\n    width: 100%;\n    overflow: auto;\n    z-index: 10000;\n    background: rgba(0, 0, 0, 0.8);\n}\n.upload-widget {\n    border: 1px solid #333;\n    background: #fff;\n    width: 60%;\n    margin: 0 auto;\n    margin-top: 100px;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-5d376ac5\",\"scoped\":false,\"hasInlineConfig\":true}!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/backend/components/image/ImageWidget.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.prod-images {\n    background: #eee;\n    padding: 10px;\n}\n.prod-image {\n    background: #fff;\n    height: 150px;\n    margin: 0 auto 16px;\n    max-width: 100%;\n}\n.prod-image-col {\n    text-align: center;\n}\n.prod-image-col:hover .tools {\n    visibility: visible;\n}\n.tools {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    visibility: hidden;\n    position: absolute;\n    bottom: 15px;\n    left: 15px;\n    right: 15px;\n    padding: 4px;\n    background: rgba(0, 0, 0, 0.8);\n    -webkit-box-pack: justify;\n        -ms-flex-pack: justify;\n            justify-content: space-between;\n    color: #fff;\n}\n.tools a[disabled=disabled] {\n    opacity: 0.3;\n}\n.tools a {\n    color: #fff;\n    padding: 10px 15px;\n    border-radius: 3px;\n}\n.tools a:hover {\n    background: rgba(255, 255, 255, 0.4);\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-1f9cd804\",\"hasScoped\":false,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/backend/components/image/UploadImageWidget.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "upload-wrapper" }, [
    _c("div", { staticClass: "upload-widget card" }, [
      _c("div", { staticClass: "card-header clearfix" }, [
        _c("h4", { staticClass: "panel-title pull-left" }, [
          _vm._v("Upload Image(Max Size: 3Mb)")
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "btn-group pull-right" }, [
          _c(
            "a",
            {
              staticClass: "btn btn-default btn-sm",
              attrs: { href: "#" },
              on: {
                click: function($event) {
                  $event.preventDefault()
                  return _vm.onClickCross($event)
                }
              }
            },
            [_c("i", { staticClass: "fa fa-window-close" })]
          )
        ])
      ]),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "card-body" },
        [
          _c("vue-dropzone", {
            ref: "myVueDropzone",
            attrs: { id: "dropzone", options: _vm.dropzoneOptions },
            on: {
              "vdropzone-removed-file": _vm.onRemoveFile,
              "vdropzone-success": _vm.onSuccessUpload,
              "vdropzone-canceled": _vm.onCanceled,
              "vdropzone-error": _vm.onUploadError
            }
          }),
          _vm._v(" "),
          _c("hr"),
          _vm._v(" "),
          _vm._m(0),
          _vm._v(" "),
          _c("hr"),
          _vm._v(" "),
          _c(
            "button",
            {
              staticClass: "btn btn-success pull-right",
              on: {
                click: function($event) {
                  $event.preventDefault()
                  return _vm.onAddImage($event)
                }
              }
            },
            [_vm._v("Add")]
          )
        ],
        1
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", [
      _vm._v("Recommend size: "),
      _c("span", { staticClass: "text-danger" }, [_vm._v("1000px")]),
      _vm._v(" x "),
      _c("span", { staticClass: "text-danger" }, [_vm._v("1000px")]),
      _vm._v(" (Resloution 70pix)")
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-1f9cd804", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-5d376ac5\",\"hasScoped\":false,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/backend/components/image/ImageWidget.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "prod-images" },
    [
      _c(
        "div",
        { staticClass: "row" },
        _vm._l(_vm.images, function(img, index) {
          return img
            ? _c(
                "div",
                { key: index, staticClass: "col-sm-4 prod-image-col" },
                [
                  _c("div", { staticClass: "tools" }, [
                    _c("a", { attrs: { href: "#", disabled: true } }, [
                      _c("i", { staticClass: "fa fa-arrow-left" })
                    ]),
                    _vm._v(" "),
                    _c(
                      "a",
                      {
                        attrs: { href: "#" },
                        on: {
                          click: function($event) {
                            $event.preventDefault()
                            _vm.removeImage(img)
                          }
                        }
                      },
                      [_c("i", { staticClass: "fa fa-trash" })]
                    ),
                    _vm._v(" "),
                    _c("a", { attrs: { href: "#", disabled: true } }, [
                      _c("i", { staticClass: "fa fa-arrow-right" })
                    ])
                  ]),
                  _vm._v(" "),
                  img.thumbnail
                    ? _c("img", {
                        staticClass: "img-responsive prod-image",
                        attrs: { src: img.thumbnail, alt: "Cinque Terre" }
                      })
                    : _c("img", {
                        staticClass: "img-responsive prod-image",
                        attrs: { src: img.url, alt: "Cinque Terre" }
                      }),
                  _vm._v(" "),
                  _c("input", {
                    attrs: { type: "text", name: _vm.name, hidden: "" },
                    domProps: { value: img.id }
                  })
                ]
              )
            : _vm._e()
        })
      ),
      _vm._v(" "),
      _vm.openWidget
        ? [
            _c("image-upload-widget", {
              attrs: {
                "on-close": _vm.onClose,
                "max-files": _vm.only_one ? 1 : 30
              }
            })
          ]
        : _vm._e(),
      _vm._v(" "),
      _c(
        "button",
        {
          staticClass: "btn btn-sm",
          attrs: { type: "button" },
          on: {
            click: function($event) {
              _vm.openWidget = true
            }
          }
        },
        [_c("i", { staticClass: "fa fa-plus fa-fw" }), _vm._v("Upload\n    ")]
      )
    ],
    2
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-5d376ac5", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1f9cd804\",\"scoped\":false,\"hasInlineConfig\":true}!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/backend/components/image/UploadImageWidget.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1f9cd804\",\"scoped\":false,\"hasInlineConfig\":true}!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/backend/components/image/UploadImageWidget.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("6d1eeb8a", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1f9cd804\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./UploadImageWidget.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1f9cd804\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./UploadImageWidget.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-5d376ac5\",\"scoped\":false,\"hasInlineConfig\":true}!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/backend/components/image/ImageWidget.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-5d376ac5\",\"scoped\":false,\"hasInlineConfig\":true}!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/backend/components/image/ImageWidget.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("b4e8c818", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-5d376ac5\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./ImageWidget.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-5d376ac5\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./ImageWidget.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./resources/backend/components/image/ImageWidget.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-5d376ac5\",\"scoped\":false,\"hasInlineConfig\":true}!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/backend/components/image/ImageWidget.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}],\"syntax-dynamic-import\"]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/backend/components/image/ImageWidget.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-5d376ac5\",\"hasScoped\":false,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/backend/components/image/ImageWidget.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/backend/components/image/ImageWidget.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-5d376ac5", Component.options)
  } else {
    hotAPI.reload("data-v-5d376ac5", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./resources/backend/components/image/UploadImageWidget.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1f9cd804\",\"scoped\":false,\"hasInlineConfig\":true}!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/backend/components/image/UploadImageWidget.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}],\"syntax-dynamic-import\"]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/backend/components/image/UploadImageWidget.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-1f9cd804\",\"hasScoped\":false,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/backend/components/image/UploadImageWidget.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/backend/components/image/UploadImageWidget.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-1f9cd804", Component.options)
  } else {
    hotAPI.reload("data-v-1f9cd804", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});