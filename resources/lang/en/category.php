<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 11/20/18
 * Time: 1:29 PM
 */

return [
    'category'        => 'Category',
    'create_category' => 'Create Category',
    'edit_category'   => 'Edit Category',
];
