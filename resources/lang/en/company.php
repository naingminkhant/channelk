<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 11/21/18
 * Time: 4:34 PM
 */

return [
    'company'        => 'Company',
    'create_company' => 'Create Company',
    'edit_company'   => 'Edit Company',
    'company_info'   => 'Company Info',
];
