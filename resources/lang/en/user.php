<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 11/20/18
 * Time: 1:30 PM
 */

return [
    'user'        => 'User',
    'create_user' => 'Create User',
    'edit_user'   => 'Edit User',
];
