<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 11/20/18
 * Time: 2:09 PM
 */

return [
    'director'        => 'Director',
    'create_director' => 'Create Director',
    'edit_director'   => 'Edit Director',
    'director_info'   => 'Director Info',
];
