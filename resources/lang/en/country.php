<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 11/22/18
 * Time: 6:57 PM
 */

return [
    'country'        => 'Country',
    'create_country' => 'Create Country',
    'edit_country'   => 'Edit Country',
];
