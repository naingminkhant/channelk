<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 11/28/18
 * Time: 11:22 PM
 */

return [
    'series'          => 'Series Video',
    'create_series'   => 'Create Series',
    'create_playlist' => 'Create Playlist',
    'edit_playlist'   => 'Edit Playlist',
    'episode'         => 'Episode',
    'create_episode'  => 'Create Episode',
    'edit_episode'    => 'Edit Episode',
    'episode_info'    => 'Episode Info',
    'playlist'        => 'Playlist',
];
