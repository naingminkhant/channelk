<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 11/25/18
 * Time: 12:23 AM
 */

return [
    'video'        => 'Video',
    'create_video' => 'Create Video',
    'edit_video'   => 'Edit Video',
    'upload_video' => 'Upload Video',
];
