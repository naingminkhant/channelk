<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 11/21/18
 * Time: 4:34 PM
 */

return [
    'banner'        => 'Banner',
    'create_banner' => 'Create Banner',
    'edit_banner'   => 'Edit Banner',
    'banner_info'   => 'Banner Info',
];
