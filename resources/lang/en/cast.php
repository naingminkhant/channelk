<?php
/**
 * Created by PhpStorm.
 * User: naingminkhant
 * Date: 11/21/18
 * Time: 4:34 PM
 */

return [
    'cast'        => 'Cast',
    'create_cast' => 'Create Cast',
    'edit_cast'   => 'Edit Cast',
    'cast_info'   => 'Cast Info',
];
