/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

require('./delete-confirm');

window.Vue = require('vue');

Vue.component('video-widget', require('./components/video/VideoWidget.vue'));
Vue.component('agenda-form', require('./components/AgendaForm.vue'));
Vue.component('attach-list', require('./components/AttachList.vue'));
Vue.component('cast-attach-list', require('./components/CastAttachList.vue'));

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    components: {
        ImageWidget:() => import('./components/image/ImageWidget.vue')
    }
});
