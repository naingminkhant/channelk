function confirmDelete() {
    $('.delete_button').click(function (e) {
        e.preventDefault();

        const url = $(this).data('url');
        let onConfirm = function () {
            $.ajax({
                type: 'DELETE',
                url: url,
                dataType: 'json',
                success: function (data) {
                    swal("Deleted!", "Your record has been deleted.", "success").then(ans => {
                        ans.value ?
                            window.location.reload() :
                            toastr.error(data);
                    })
                },
                error: function (data) {
                    console.log(data);
                }
            })
        };

        swal({
            title: "Are you sure?",
            text: "You won't be able to revert this!",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            reverseButtons: !0
        }).then(function (ans) {
            ans.value ?
                onConfirm() :
                "cancel" === ans.dismiss && swal("Cancelled", "Your record is safe :)", "error")
        });
    });
}

$(document).ready(function () {
    confirmDelete();
});
