@extends('frontend.layouts.app')

@section('content')
    <div class="content-wrapper" id="BgSpecial">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="pad-10"></div>
                    <h3 class="main_title"><span>Reset Password</span></h3>

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf
                        <div class="form-group">
                            <div class="icon-addon">
                                <input type="text" name="email" value="{{ old('email') }}" placeholder="Email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" id="email">
                                <label for="email" class="fa fa-user" rel="tooltip" title="email"></label>
                                @if ($errors->has('email'))
                                    <span class="text-danger">{{ $errors->first('email') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-block btn-custom">{{ __('Send Password Reset Link') }}</button>
                        </div>
                    </form>
                    <div class="pad-30"></div>
                </div>
                <!-- Col 4 Ends -->
            </div>
            <!-- Row Ends -->
        </div>
        <!-- Container Ends -->
    </div>
@endsection
