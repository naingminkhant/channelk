@extends('frontend.layouts.app')

@section('content')
    <div class="content-wrapper" id="BgSpecial">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="pad-10"></div>
                    <h3 class="main_title"><span>Reset Password</span></h3>

                    <form method="POST" action="{{ route('password.update') }}">
                        @csrf
                        <div class="form-group">
                            <div class="icon-addon">
                                <input type="text" name="email" placeholder="Your Email" class="form-control" id="email">
                                <label for="email" class="fa fa-envelope" rel="tooltip" title="email"></label>
                                @if ($errors->has('email'))
                                    <span class="text-danger">{{ $errors->first('email') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="icon-addon">
                                <input type="password" name="password" placeholder="Enter Password" class="form-control" id="password">
                                <label for="password" class="fa fa-key" rel="tooltip" title="password"></label>
                                @if ($errors->has('password'))
                                    <span class="text-danger" role="alert">{{ $errors->first('password') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="icon-addon">
                                <input type="password" name="password_confirmation" placeholder="Confirm Password" class="form-control" id="password_confirmation">
                                <label for="password_confirmation" class="fa fa-check" rel="tooltip" title="password"></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-block btn-custom">{{ __('Reset Password') }}</button>
                        </div>
                    </form>

                    <div class="pad-30"></div>
                </div>
                <!-- Col 4 Ends -->
            </div>
            <!-- Row Ends -->
        </div>
        <!-- Container Ends -->
    </div>
@endsection
