@extends('frontend.layouts.app')

@section('content')
    <div class="content-wrapper" id="BgSpecial">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="pad-10"></div>
                    <h3 class="main_title"><span>{{ __('Verify Your Email Address') }}</span></h3>

                    <div class="l-form">
                        @csrf

                        @if (session('resent'))
                            <div class="alert alert-success" role="alert">
                                {{ __('A fresh verification link has been sent to your email address.') }}
                            </div>
                        @endif

                    </div>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            {{ __('Before proceeding, please check your email for a verification link.') }}
                            {{ __('If you did not receive the email') }}, <a
                                    href="{{ route('verification.resend') }}">{{ __('click here to request another') }}</a>
                        </div>
                    </div>


                    <div class="pad-30"></div>
                </div>
                <!-- Col 4 Ends -->
            </div>
            <!-- Row Ends -->
        </div>
        <!-- Container Ends -->
    </div>
@endsection
