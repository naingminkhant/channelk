<!DOCTYPE html>

<html lang="en">

<!-- begin::Head -->
<head>
    <meta charset="utf-8"/>
    <title>{{ env('APP_NAME') }}</title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">

    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {
                "families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
            },
            active: function () {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <!--end::Web font -->
    <!--begin::Base Styles -->
    <link href="{{ url('css/theme/vendors.bundle.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ url('css/theme/style.bundle.css') }}" rel="stylesheet" type="text/css"/>
</head>

<!-- end::Head -->

<!-- begin::Body -->
<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">

<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-1"
         id="m_login" style="background-image: url({{ url('img/theme/bg-1.jpg') }});">
        <div class="m-grid__item m-grid__item--fluid m-login__wrapper">
            <div class="m-login__container">
                <div class="m-login__logo">
                    <a href="#">
                        <img src="{{ url('img/theme/logo-1.png') }}">
                    </a>
                </div>
                <div class="m-login__signin">
                    <div class="m-login__head">
                        <h3 class="m-login__title">Sign In To Backend</h3>
                    </div>
                    <form class="m-login__form m-form" action="{{ route('admin.login') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group m-form__group {{ $errors->has('email') ? 'has-danger':'' }}">
                            <input class="form-control m-input" type="text" placeholder="Email" name="email"
                                   autocomplete="off" style="font-size: 18px;">
                            @if($errors->has('email'))
                                <div id="email-error" class="form-control-feedback" style="font-size: 15px; color: #ff1c0c;">{{ $errors->first('email') }}</div>
                            @endif
                        </div>
                        <div class="form-group m-form__group {{ $errors->has('email') ? 'has-danger':'' }}">
                            <input class="form-control m-input m-login__form-input--last" type="password"
                                   placeholder="Password" name="password" style="font-size: 18px;">
                            @if($errors->has('password'))
                                <div id="password-error" class="form-control-feedback" style="font-size: 15px; color: #ff1c0c;">{{ $errors->first('password') }}</div>
                            @endif
                        </div>
                        <div class="row m-login__form-sub">
                            <div class="col m--align-left m-login__form-left">
                                <label class="m-checkbox  m-checkbox--light">
                                    <input type="checkbox" name="remember"> Remember me
                                    <span></span>
                                </label>
                            </div>
                        </div>
                        <div class="m-login__form-action">
                            <button id="m_login_signin_submit" type="submit"
                                    class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn m-login__btn--primary">
                                Sign In
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- end:: Page -->

<!--begin::Base Scripts -->
<script src="{{ url('js/theme/vendors.bundle.js') }}" type="text/javascript"></script>
<script src="{{ url('js/theme/scripts.bundle.js') }}" type="text/javascript"></script>

<!--end::Base Scripts -->

<!--end::Page Snippets -->
</body>

<!-- end::Body -->
</html>
