@extends('frontend.layouts.app')

@section('content')
    <div class="content-wrapper" id="BgSpecial">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="pad-10"></div>
                    <h3 class="main_title"><span>Log In</span></h3>
                    <a href="{!! URL::to('auth/facebook') !!}" title="Facebook" class="btn btn-block btn-facebook">
                        <i class="fab fa-facebook-f btn-icon"></i>Log in with Facebook
                    </a>
                    <div class="m-tb-15 text-center">
                        <b>- OR -</b>
                    </div>
                    <form method="POST" action="/login">
                        @csrf
                        <div class="form-group">
                            <div class="icon-addon">
                                <input type="text" name="email" value="{{ old('email') }}" placeholder="Email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" id="email">
                                <label for="email" class="fa fa-user" rel="tooltip" title="email"></label>
                                @if ($errors->has('email'))
                                    <span class="text-danger">{{ $errors->first('email') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="icon-addon">
                                <input type="password" name="password" placeholder="Password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" id="password">
                                <label for="password" class="fa fa-key" rel="tooltip" title="password"></label>
                                @if ($errors->has('password'))
                                    <span class="text-danger" role="alert">{{ $errors->first('password') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-block btn-custom">Login Now</button>
                            <div class="m-t-15 text-center">
                                <small><a href="{{ route('password.request') }}">Forgot Password?</a></small>
                            </div>
                        </div>
                    </form>
                    <div class="m-t-b-15 text-center">
                        <small>Not a member yet? Please <a href="/register" class="custom-link">Sign Up </a>now.</small>
                    </div>
                    <div class="pad-30"></div>
                </div>
                <!-- Col 4 Ends -->
            </div>
            <!-- Row Ends -->
        </div>
        <!-- Container Ends -->
    </div>
@endsection
