@component('backend.components.full_box')
    {{ Form::model(null,['route' => 'backend.cms.playlist.store', 'method' => 'POST', 'class' => 'm-form m-form--fit m-form--label-align-right']) }}
    <div class="m-form__group form-group">
        <div class="col-lg-10 col-md-10 col-sm-12">
            <attach-list :edit="'{{ isset($attached_lists) ? true:false }}'"></attach-list>
        </div>
    </div>
    <div class="m-portlet__foot m-portlet__foot--fit">
        <div class="m-form__actions">
            <button type="submit" class="btn btn-primary">
                <i class="la la-save"></i> Submit
            </button>
            <a href="{{ route('backend.cms.playlist') }}" class="btn btn-secondary">
                <i class="la la-rotate-right"></i>
                Cancel
            </a>
        </div>
    </div>
    {{Form::close()}}
@endcomponent
