@php
    $paginator = createRangePaginator(
    $meta['pagination']['count'],
    $meta['pagination']['total'],
    $meta['pagination']['per_page']
    )
@endphp

{!!
$paginator
->appends(array_except(request()->all(), ['page']))
->render('pagination::bootstrap-4');
 !!}