<div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" id="local_data" style="">
    @if(empty($data['data']))
        <h1 class="text-center">Data Not Exists</h1>
    @else
        <table class="m-datatable__table" style="display: block; min-height: 300px; overflow-x: auto;">
            <thead class="m-datatable__head">
            <tr class="m-datatable__row">
                <th class="m-datatable__cell--center m-datatable__cell m-datatable__cell--check">
                <span style="width: 50px;">
                    #
                </span>
                </th>
                @foreach($configs['columns'] as $column)
                    <th class="m-datatable__cell m-datatable__cell--sort" data-sort="asc">
                    <span style="width: 110px;">{{ $column }}
                    </span>
                    </th>
                @endforeach
                <th class="m-datatable__cell m-datatable__cell--sort">
                    <span style="width: 110px;">Actions</span>
                </th>
            </tr>
            </thead>
            <tbody class="m-datatable__body" style="">
            @foreach($data['data'] as $index=>$item)
                <tr data-row="0" class="m-datatable__row" style="left: 0;">
                    <td class="m-datatable__cell--center m-datatable__cell m-datatable__cell--check">
                    <span style="width: 50px;">
                        {{ $data['meta']['pagination']['start_index'] + $index }}
                    </span>
                    </td>
                    @foreach($configs['viewable'] as $view)
                        @if(is_array($view))
                            <td class="m-datatable__cell--sorted m-datatable__cell">
                            <span style="width: 110px;">
                                @foreach($item[$view['index']] as $index)
                                    {{ $index[$view['viewable']] }}
                                    {{ $index !== end($item[$view['index']]) ? $view['delimiter']:'' }}
                                @endforeach
                            </span>
                            </td>
                        @else
                            @if(strpos($view,'image') !== false || $view === 'thumbnail' || $view === 'avatar')
                                <td class="m-datatable__cell--sorted m-datatable__cell">
                                <span style="width: 100px; height: 100px">
                                    <img src="{{ $item[$view] }}" width="64" height="64">
                                </span>
                                </td>
                            @else
                                <td class="m-datatable__cell--sorted m-datatable__cell">
                                    <span style="width: 110px;">{!! $item[$view] !!}</span>
                                </td>
                            @endif
                        @endif
                    @endforeach
                    <td data-field="Actions" class="m-datatable__cell">
                    <span style="overflow: visible; position: relative; width: 110px;">
                        @if(isset($configs["actions"]["edit"]) || isset($configs["actions"]["delete"]))
                            <div class="dropdown ">
                        <a href="#" class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                           data-toggle="dropdown">
                        <i class="la la-ellipsis-h"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            @if(isset($configs["actions"]["edit"]))
                                <a class="dropdown-item"
                                   href="{{ route($configs["actions"]["edit"]["route"], $item['id']) }}"><i
                                        class="la la-edit"></i> Edit</a>
                            @endif
                            @if(isset($configs["actions"]["delete"]))
                                <a class="delete_button dropdown-item"
                                   href="#" data-url="{{ route($configs["actions"]["delete"]["route"], $item['id']) }}">
                                    <i class="la la-trash"></i> Delete
                                </a>
                            @endif
                        </div>
                        </div>
                        @endif
                        @if(isset($configs["actions"]["info"]))
                            @if(isset($configs["actions"]["info"]["modal"]))
                                <a href="#" data-toggle="modal" data-target="#m_modal_basic_{{ $item['id'] }}"
                                   class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                                   title="View ">
                                    <i class="la la-search"></i>
                                </a>
                                @include($configs["actions"]["info"]["modal"], compact('item'))
                            @else
                                <a href="{{ route($configs["actions"]["info"]["route"], $item['id']) }}"
                                   class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                                   title="View ">
                                    <i class="la la-search"></i>
                                </a>
                            @endif
                        @endif
                        @if(isset($configs["actions"]["custom"]))
                            <a href="{{ route($configs["actions"]["custom"]["route"], $item[$configs['actions']['custom']['parameter']]) }}"
                               class="{{ $configs["actions"]["custom"]['class'] }}"
                               title="{{ $configs['actions']['custom']['tooltip'] }}"
                               target="{{ $configs['actions']['custom']['target'] }}">
                                @if(isset($configs["actions"]["custom"]['icon']))
                                    <i class="{{ $configs["actions"]["custom"]['icon'] }}"></i>
                                @elseif(isset($configs['actions']['custom']['lable']))
                                    {{ $configs['actions']['custom']['lable'] }}
                                @endif
                                </a>
                        @endif
                    </span>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
</div>
@component('backend.components.paginate',
    ['meta' => $data['meta']
])
@endcomponent
