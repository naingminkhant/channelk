<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
    @if(isset($breadcrumbs))
        @foreach($breadcrumbs as $k=>$v)
            @if($k === 'home')
                <li class="m-nav__item m-nav__item--home">
                    <a href="{{ $v['url'] }}" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
            @else
                <li class="m-nav__separator">
                    >
                </li>
                <li class="m-nav__item">
                    <a href="{{ $v['url'] }}" class="m-nav__link">
                        <span class="m-nav__link-text">{{ $v['label'] }}</span>
                    </a>
                </li>
            @endif
        @endforeach
    @endif
</ul>