<div class="m-portlet__body">
    <div class="m-form__group form-group row">
        <label class="col-form-label col-lg-2 col-sm-12">Image Of Livestream :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            <image-widget :name="'livestream_image'" :thumbs="'{{ isset($info['livestream_image']) ? json_encode($info['livestream_image'], true):null}}'"></image-widget>
        </div>
    </div>
    <div class="m-form__group form-group row">
        <label class="col-form-label col-lg-2 col-sm-12">Livestream :</label>
        <div class="col-lg-10 col-md-10 col-sm-12">
            <textarea name="livestream" cols="30" rows="10" class="form-control" title="body">
            {{ old('livestream') ?? (isset($info) ? $info['livestream']:null) }}
        </textarea>
        </div>
    </div>
    <div class="m-form__group form-group row">
        <label class="col-form-label col-lg-2 col-sm-12">Image Of Privacy Policy :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            <image-widget :name="'privacy_policy_image'" :thumbs="'{{ isset($info['privacy_policy_image']) ? json_encode($info['privacy_policy_image']):null}}'"></image-widget>
        </div>
    </div>
    <div class="m-form__group form-group row">
        <label class="col-form-label col-lg-2 col-sm-12">Privacy Policy :</label>
        <div class="col-lg-10 col-md-10 col-sm-12">
            <textarea name="privacy_policy" cols="30" rows="10" class="form-control" title="body">
            {{ old('privacy_policy') ?? (isset($info) ? $info['privacy_policy']:null) }}
        </textarea>
        </div>
    </div>
    <div class="m-form__group form-group row">
        <label class="col-form-label col-lg-2 col-sm-12">Image Of T & C :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            <image-widget :name="'terms_and_condition_image'" :thumbs="'{{ isset($info['terms_and_condition_image']) ? json_encode($info['terms_and_condition_image']):null}}'"></image-widget>
        </div>
    </div>
    <div class="m-form__group form-group row">
        <label class="col-form-label col-lg-2 col-sm-12">Terms & Condition :</label>
        <div class="col-lg-10 col-md-10 col-sm-12">
            <textarea name="terms_and_condition" cols="30" rows="10" class="form-control" title="body">
            {{ old('terms_and_condition') ?? (isset($info) ? $info['terms_and_condition']:null) }}
        </textarea>
        </div>
    </div>
</div>
<div class="m-portlet__foot m-portlet__foot--fit">
    <div class="m-form__actions">
        <button type="submit" class="btn btn-primary">
            <i class="la la-save"></i> Submit
        </button>
        <a href="{{ route('backend.info.index') }}" class="btn btn-secondary">
            <i class="la la-rotate-right"></i>
            Cancel
        </a>
    </div>
</div>

@section('extraJs')
    <script src="{{ url('js/theme/summernote.js') }}"></script>
@append
