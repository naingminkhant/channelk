<div class="m-portlet__body">
    <div class="form-group m-form__group row">
        <label class="col-form-label col-lg-2 col-sm-12">Slug :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            {{ Form::input('text','slug', null, ['class' => 'form-control m-input', 'placeholder' => 'Slug']) }}
        </div>
    </div>
    <div class="form-group m-form__group row">
        <label class="col-form-label col-lg-2 col-sm-12">Name :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            {{ Form::input('text','name', null, ['class' => 'form-control m-input', 'placeholder' => 'Name']) }}
        </div>
    </div>
</div>
<div class="m-portlet__foot m-portlet__foot--fit">
    <div class="m-form__actions">
        <button type="submit" class="btn btn-primary">
            <i class="la la-save"></i> Submit
        </button>
        <a href="{{ route('backend.country.index') }}" class="btn btn-secondary">
            <i class="la la-rotate-right"></i>
            Cancel
        </a>
    </div>
</div>

@section('extraJs')
    <script src="{{ url('js/theme/select2.js') }}"></script>
@append
