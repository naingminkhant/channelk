@component('backend.components.full_box')
    @slot('titleTool')
        <a href="{{ route('backend.channel-agenda.create') }}"
           class="btn btn-outline-primary m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air">
            <span>
                <i class="la la-plus-circle"></i>
                <span>ADD</span>
            </span>
        </a>
    @endslot
    @foreach($agendas['data'] as $index=>$agenda)
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            {{ $agenda['date']  }}
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="{{ route('backend.channel-agenda.edit', $agenda['id']) }}"
                               class="btn btn-outline-primary m-btn m-btn--icon">
                                <i class="la la-edit"></i> Edit
                            </a>
                            &nbsp;
                            <a href="#"
                               data-url="{{ route('backend.channel-agenda.destroy', $agenda['id']) }}"
                               class="delete_button btn btn-outline-danger m-btn m-btn--icon">
                                <i class="la la-trash"></i> Delete
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                <!--begin::Section-->
                <div class="m-section">
                    <div class="m-section-"></div>
                    <div class="m-section__content">
                        <table class="table table-bordered m-table m-table--border-brand m-table--head-bg-brand">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Time</th>
                                <th>Title</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($agenda['agendas'] as $index=>$data)
                                <tr>
                                    <th scope="row">{{ $index + 1 }}</th>
                                    <td>{{ $data['time'] }}</td>
                                    <td>{{ $data['title'] }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!--end::Section-->
            </div>
            <!--end::Form-->
        </div>
    @endforeach
@endcomponent
