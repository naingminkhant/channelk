<div class="m-portlet__body" style="padding-top: 0;">
    <agenda-form :preview="'{{ isset($agenda) ? true:false  }}'"></agenda-form>
</div>
<div class="m-portlet__foot m-portlet__foot--fit">
    <div class="m-form__actions">
        <button type="submit" class="btn btn-primary">
            <i class="la la-save"></i> Submit
        </button>
        <a href="{{ route('backend.channel-agenda.index') }}" class="btn btn-secondary">
            <i class="la la-rotate-right"></i>
            Cancel
        </a>
    </div>
</div>

@section('extraJs')
    <script src="{{ url('js/theme/bootstrap-datepicker.js') }}"></script>
    <script src="{{ url('js/theme/bootstrap-timepicker.js') }}"></script>
@append
