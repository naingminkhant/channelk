<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
    <div class="m-portlet__body">
        <div class="form-group m-form__group row" style="background-color: #fbfafa;">
            <div class="col-lg-4">
                <input type="text" name="q" class="form-control m-input" placeholder="enter keyword (eg., name)" value="{{ request()->q }}">
            </div>
            <div class="col-lg-4">
                <button class="btn btn-primary">Search</button>
                <a href="{{route('backend.user.index')}}" class="btn btn-danger">Reset</a>
            </div>
        </div>
    </div>
</form>
