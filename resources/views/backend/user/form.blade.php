<div class="m-portlet__body">
    <div class="form-group m-form__group row">
        <label class="col-form-label col-lg-2 col-sm-12">Name :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            {{ Form::input('text','name', null, ['class' => 'form-control m-input', 'placeholder' => 'Full Name']) }}
        </div>
    </div>
    <div class="form-group m-form__group row">
        <label class="col-form-label col-lg-2 col-sm-12">Email :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            {{ Form::input('email','email', null, ['class' => 'form-control m-input', 'placeholder' => 'Email']) }}
        </div>
    </div>
    <div class="form-group m-form__group row">
        <label class="col-form-label col-lg-2 col-sm-12">Password :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            {{ Form::input('password','password', null, ['class' => 'form-control m-input', 'placeholder' => 'Password']) }}
        </div>
    </div>
    <div class="form-group m-form__group row">
        <label class="col-form-label col-lg-2 col-sm-12">Confirm Password :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            {{ Form::input('password','password_confirmation', null, ['class' => 'form-control m-input', 'placeholder' => 'Confirm Password']) }}
        </div>
    </div>
    <div class="form-group m-form__group row">
        <label class="col-form-label col-lg-2 col-sm-12"></label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            <div class="m-checkbox-inline">
                <label class="m-checkbox">
                    {!! Form::checkbox('ban', null, isset($user['banned_at']) ? $user['banned_at']:false) !!}
                    <label>Ban</label>
                    <span></span>
                </label>
            </div>
        </div>
    </div>
</div>
<div class="m-portlet__foot m-portlet__foot--fit">
    <div class="m-form__actions">
        <button type="submit" class="btn btn-primary">
            <i class="la la-save"></i> Submit
        </button>
        <a href="{{ route('backend.user.index') }}" class="btn btn-secondary">
            <i class="la la-rotate-right"></i>
            Cancel
        </a>
    </div>
</div>

@section('extraJs')
    <script src="{{ url('js/theme/select2.js') }}"></script>
@append
