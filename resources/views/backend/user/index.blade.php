@component('backend.components.full_box')
    @slot('titleTool')
        <a href="{{ route('backend.user.create') }}"
           class="btn btn-outline-primary m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air">
            <span>
                <i class="la la-plus-circle"></i>
                <span>ADD</span>
            </span>
        </a>
    @endslot
    <div class="form-group">
        @include('backend.user.search')
    </div>
    @component('backend.components.table',[
        'data'      => $users,
        'configs'   => config('tables.user')
    ])
    @endcomponent
@endcomponent