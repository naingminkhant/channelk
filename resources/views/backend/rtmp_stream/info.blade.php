@component('backend.components.full_box')
    @slot('title')

    @endslot
    <div class="col-xl-8 offset-xl-2">
        <div class="m-form__section m-form__section--first">
            <div class="m-form__heading">
                <h3 class="m-form__heading-title">Basic Details</h3>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-xl-3 col-lg-3 col-form-label">Name:</label>
                <div class="col-xl-9 col-lg-9">
                    <input type="text" name="name" class="form-control m-input" placeholder=""
                           value="{{ $user['name'] }}" readonly>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-xl-3 col-lg-3 col-form-label">Email:</label>
                <div class="col-xl-9 col-lg-9">
                    <input type="email" name="email" class="form-control m-input" placeholder=""
                           value="{{ $user['email'] }}" readonly>
                </div>
            </div>
        </div>
        <div class="m-separator m-separator--dashed m-separator--lg"></div>
        <div class="m-form__section m-form__section--first">
            <div class="m-form__heading">
                <h3 class="m-form__heading-title">Security Details</h3>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-xl-3 col-lg-3 col-form-label">Banned At:</label>
                <div class="col-xl-9 col-lg-9">
                    <input type="text" name="name" class="form-control m-input" placeholder=""
                           value="{{ $user['banned_at'] }}" readonly>
                </div>
            </div>
        </div>
    </div>
@endcomponent
