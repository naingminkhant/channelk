<div class="m-portlet__body">
    <div class="form-group m-form__group row">
        <label class="col-form-label col-lg-2 col-sm-12">Slug :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            {{ Form::input('text','slug', null, ['class' => 'form-control m-input', 'placeholder' => 'Slug']) }}
        </div>
    </div>
    <div class="form-group m-form__group row">
        <label class="col-form-label col-lg-2 col-sm-12">Title :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            {{ Form::input('text','title', null, ['class' => 'form-control m-input', 'placeholder' => 'Title']) }}
        </div>
    </div>
    <div class="form-group m-form__group row">
        <label class="col-form-label col-lg-2 col-sm-12">Sort :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            {{ Form::input('number','sort', null, ['class' => 'form-control m-input', 'placeholder' => 'Sort Number']) }}
        </div>
    </div>
    <div class="form-group m-form__group row">
        <label class="col-form-label col-lg-2 col-sm-12">Duration :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            <div class='input-group timepicker' id=''>
                {{ Form::input('text','duration', null, ['class' => 'form-control m-input', 'placeholder' => 'Select Duration', 'readonly', 'id' => 'm_timepicker_2']) }}
                <div class="input-group-append">
                    <span class="input-group-text">
                        <i class="la la-clock-o"></i>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group m-form__group row">
        <label class="col-form-label col-lg-2 col-sm-12">Released Date :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            <div class="input-group date">
                {{ Form::input('text','released_date', null, ['class' => 'form-control m-input', 'placeholder' => 'Released Date', 'readonly', 'id'=>"m_datepicker_3"]) }}
                <div class="input-group-append">
                    <span class="input-group-text">
                        <i class="la la-calendar"></i>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="m-form__group form-group row">
        <label class="col-form-label col-lg-2 col-sm-12">Image :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            <image-widget :preview="'{{ isset($episode) ? true:false}}'"></image-widget>
        </div>
    </div>
    <div class="form-group m-form__group row">
        <label class="col-form-label col-lg-2 col-sm-12">Trailer Video:</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            <video-widget :file-name="'trailer_video'"
                          :preview="'{{ isset($episode["trailer_url"]) ? true:false}}'"></video-widget>
        </div>
    </div>
    <div class="form-group m-form__group row">
        <label class="col-form-label col-lg-2 col-sm-12">Full Video:</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            <video-widget :file-name="'full_video'"
                          :preview="'{{ isset($episode["full_url"]) ? true:false}}'"></video-widget>
        </div>
    </div>
    <div class="m-form__group form-group row">
        <label class="col-form-label col-lg-2 col-sm-12">Story Line :</label>
        <textarea name="story_line" cols="30" rows="10" class="form-control" title="body">
            {{ old('story_line') ?? (isset($video) ? $video['story_line']:null) }}
        </textarea>
    </div>
    <div class="form-group m-form__group row">
        <label class="col-form-label col-lg-2 col-sm-12"></label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            <div class="m-checkbox-inline">
                <label class="m-checkbox">
                    {!! Form::checkbox('active', null) !!}
                    <label>Active</label>
                    <span></span>
                </label>
            </div>
        </div>
    </div>

</div>
<div class="m-portlet__foot m-portlet__foot--fit">
    <div class="m-form__actions">
        <button type="submit" class="btn btn-primary">
            <i class="la la-save"></i> Submit
        </button>
        <a href="{{ route('backend.series.index') }}" class="btn btn-secondary">
            <i class="la la-rotate-right"></i>
            Cancel
        </a>
    </div>
</div>

@section('extraJs')
    <script src="{{ url('js/theme/select2.js') }}"></script>
    <script src="{{ url('js/theme/summernote.js') }}"></script>
    <script src="{{ url('js/theme/bootstrap-timepicker.js') }}"></script>
    <script src="{{ url('js/theme/bootstrap-datepicker.js') }}"></script>
@append
