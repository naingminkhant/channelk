@component('backend.components.full_box')
    @slot('title')

    @endslot
    <div class="col-xl-8 offset-xl-2">
        <div class="m-form__section m-form__section--first">
            <div class="m-form__heading">
                <h3 class="m-form__heading-title">Record Information</h3>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-xl-3 col-lg-3 col-form-label">Title:</label>
                <div class="col-xl-9 col-lg-9">
                    <input type="text" name="name" class="form-control m-input" placeholder=""
                           value="{{ $episode['title'] }}" readonly>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-xl-3 col-lg-3 col-form-label">Slug:</label>
                <div class="col-xl-9 col-lg-9">
                    <input type="email" name="email" class="form-control m-input" placeholder=""
                           value="{{ $episode['slug'] }}" readonly>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-xl-3 col-lg-3 col-form-label">Sort:</label>
                <div class="col-xl-9 col-lg-9">
                    <input type="email" name="email" class="form-control m-input" placeholder=""
                           value="{{ $episode['sort'] }}" readonly>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-xl-3 col-lg-3 col-form-label">Duration:</label>
                <div class="col-xl-9 col-lg-9">
                    <input class="form-control m-input" placeholder=""
                           value="{{ $episode['duration'] }}" readonly>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-xl-3 col-lg-3 col-form-label">Story Line:</label>
                <div class="col-xl-9 col-lg-9">
                    {!! $episode['story_line'] !!}
                </div>
            </div>
        </div>
        <div class="m-separator m-separator--dashed m-separator--lg"></div>
        <div class="m-form__section m-form__section--first">
            <div class="m-form__heading">
                <h3 class="m-form__heading-title">Video</h3>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-xl-3 col-lg-3 col-form-label">Trailer:</label>
                <div class="col-xl-9 col-lg-9">
                    @if($episode['trailer_url'])
                        <video id="my-video" class="video-js" controls preload="auto" width="150"
                               height="100"
                               data-setup="{}">
                            <source src="{{ $episode['preview_trailer'] }}" type='video/mp4'>
                        </video>
                    @else
                        <img src="{{ url('img/theme/no_video_available.jpg') }}" alt="" width="150" height="100">
                    @endif
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-xl-3 col-lg-3 col-form-label">Full:</label>
                <div class="col-xl-9 col-lg-9">
                    @if($episode['full_url'])
                        <video id="my-video" class="video-js" controls preload="auto" width="150"
                               height="100"
                               data-setup="{}">
                            <source src="{{ $episode['preview_full'] }}" type='video/mp4'>
                        </video>
                    @else
                        <img src="{{ url('img/theme/no_video_available.jpg') }}" alt="" width="150" height="100">
                    @endif
                </div>
            </div>
        </div>
    </div>
@endcomponent

@section('extraJs')
    <link href="https://vjs.zencdn.net/7.3.0/video-js.css" rel="stylesheet">
    <script src="https://vjs.zencdn.net/7.3.0/video.js"></script>
@endsection
