<div class="m-portlet__body">
    <div class="form-group m-form__group row">
        <label class="col-form-label col-lg-2 col-sm-12">Slug :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            {{ Form::input('text','slug', null, ['class' => 'form-control m-input', 'placeholder' => 'Slug']) }}
        </div>
    </div>
    <div class="form-group m-form__group row">
        <label class="col-form-label col-lg-2 col-sm-12">Title (En) :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            {{ Form::input('text','title_en', null, ['class' => 'form-control m-input', 'placeholder' => 'Title En']) }}
        </div>
    </div>
    <div class="form-group m-form__group row">
        <label class="col-form-label col-lg-2 col-sm-12">Title (MM) :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            {{ Form::input('text','title_mm', null, ['class' => 'form-control m-input', 'placeholder' => 'Title MM']) }}
        </div>
    </div>
    <div class="form-group m-form__group row">
        <label class="col-form-label col-lg-2 col-sm-12">Released Date :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            <div class="input-group date">
                {{ Form::input('text','released_date', null, ['class' => 'form-control m-input', 'placeholder' => 'Released Date', 'readonly', 'id'=>"m_datepicker_3"]) }}
                <div class="input-group-append">
                    <span class="input-group-text">
                        <i class="la la-calendar"></i>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group m-form__group row">
        <label class="col-form-label col-lg-2 col-sm-12">Budget :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            {{ Form::input('number','budget', null, ['class' => 'form-control m-input', 'placeholder' => 'Budget']) }}
        </div>
    </div>
    <div class="form-group m-form__group row">
        <label class="col-form-label col-lg-2 col-sm-12">Country :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            {{ Form::select('country_id', $countries, null, ['class' => 'form-control m-select2 m-select2-general']) }}
        </div>
    </div>
    <div class="form-group m-form__group row">
        <label class="col-form-label col-lg-2 col-sm-12">Director :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            {{ Form::select('director_id', $directors, null, ['class' => 'form-control m-select2 m-select2-general']) }}
        </div>
    </div>
    <div class="form-group m-form__group row">
        <label class="col-form-label col-lg-2 col-sm-12">Company :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            {{ Form::select('production_company_id', $companies, null, ['class' => 'form-control m-select2 m-select2-general']) }}
        </div>
    </div>
    <div class="form-group m-form__group row">
        <label class="col-form-label col-lg-2 col-sm-12">Casts :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            {!! Form::select('cast_ids[]', $casts, isset($series['casts']) ? array_column($series['casts'], 'id'):null, ['class' => 'form-control m-select2', 'id' => 'm_select2_11', 'multiple']) !!}
        </div>
    </div>
    <div class="form-group m-form__group row">
        <label class="col-form-label col-lg-2 col-sm-12">Category :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            {!! Form::select('category_ids[]', $categories, isset($series['categories']) ? array_column($series['categories'], 'id'):null, ['class' => 'form-control m-select2', 'id' => 'm_select2_11_1', 'multiple']) !!}
        </div>
    </div>
    <div class="m-form__group form-group row">
        <label class="col-form-label col-lg-2 col-sm-12">Cover Image :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            <image-widget :preview="'{{ isset($series) ? true:false}}'" :name="'cover_images[]'" :only_one="true"></image-widget>
        </div>
    </div>
    <div class="m-form__group form-group row">
        <label class="col-form-label col-lg-2 col-sm-12">Image :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            <image-widget :preview="'{{ isset($series) ? true:false}}'"></image-widget>
        </div>
    </div>
    <div class="form-group m-form__group row">
        <label class="col-form-label col-lg-2 col-sm-12">Trailer Video:</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            {{ Form::input('text','trailer_url', null, ['class' => 'form-control m-input', 'placeholder' => 'Trailer Video Url']) }}
        </div>
    </div>
    <div class="m-form__group form-group row">
        <label class="col-form-label col-lg-2 col-sm-12">Attaching Video :</label>
        <div class="col-lg-10 col-md-10 col-sm-12">
            <attach-list :edit="'{{ isset($series['videos']) ? true:false }}'"></attach-list>
        </div>
    </div>
    <div class="m-form__group form-group row">
        <label class="col-form-label col-lg-2 col-sm-12">Story Line :</label>
        <div class="col-lg-10 col-sm-12">
            <textarea name="story_line" cols="30" rows="10" class="form-control" title="body">
            {{ old('story_line') ?? (isset($series) ? $series['story_line']:null) }}
        </textarea>
        </div>
    </div>
    <div class="form-group m-form__group row">
        <label class="col-form-label col-lg-2 col-sm-12">Published Date :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            <div class="input-group date">
                <input type="text" name="published_at" class="form-control m-input" placeholder="Published Date"
                       value="{{ isset($series) ? $series['published_at']:now()->format('m/d/Y') }}"
                       id="m_datepicker_3_modal"/>
                <div class="input-group-append">
                    <span class="input-group-text">
                        <i class="la la-calendar"></i>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="m-portlet__foot m-portlet__foot--fit">
    <div class="m-form__actions">
        <button type="submit" class="btn btn-primary">
            <i class="la la-save"></i> Submit
        </button>
        <a href="{{ route('backend.series.index') }}" class="btn btn-secondary">
            <i class="la la-rotate-right"></i>
            Cancel
        </a>
    </div>
</div>

@section('extraJs')
    <script src="{{ url('js/theme/select2.js') }}"></script>
    <script src="{{ url('js/theme/summernote.js') }}"></script>
    <script src="{{ url('js/theme/bootstrap-timepicker.js') }}"></script>
    <script src="{{ url('js/theme/bootstrap-datepicker.js') }}"></script>
@append
