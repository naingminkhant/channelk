@component('backend.components.full_box')
    @slot('title')

    @endslot
    <div class="row">
        <div class="col-xl-6">
            <div class="m-form__section m-form__section--first">
                <div class="m-form__heading">
                    <h3 class="m-form__heading-title">Record Information</h3>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-xl-3 col-lg-3 col-form-label">Title:</label>
                    <div class="col-xl-9 col-lg-9">
                        <input type="text" name="name" class="form-control m-input" placeholder=""
                               value="{{ $series['title_en'] }}" readonly>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-xl-3 col-lg-3 col-form-label">Slug:</label>
                    <div class="col-xl-9 col-lg-9">
                        <input type="email" name="email" class="form-control m-input" placeholder=""
                               value="{{ $series['slug'] }}" readonly>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-xl-3 col-lg-3 col-form-label">Category:</label>
                    <div class="col-xl-9 col-lg-9">
                        @foreach($series['categories'] as $category)
                            <span class="m-badge m-badge--success m-badge--wide"
                                  style="font-size: 14px; padding: 10px;">
                            {{ $category['title'] }}
                        </span>
                        @endforeach
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-xl-3 col-lg-3 col-form-label">Country:</label>
                    <div class="col-xl-9 col-lg-9">
                        <input class="form-control m-input" placeholder=""
                               value="{{ $series['country']['name'] }}" readonly>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-xl-3 col-lg-3 col-form-label">Director:</label>
                    <div class="col-xl-9 col-lg-9">
                        <input class="form-control m-input" placeholder=""
                               value="{{ $series['director']['name'] }}" readonly>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-xl-3 col-lg-3 col-form-label">Company:</label>
                    <div class="col-xl-9 col-lg-9">
                        <input class="form-control m-input" placeholder=""
                               value="{{ $series['company']['name'] }}" readonly>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-xl-3 col-lg-3 col-form-label">Casts:</label>
                    <div class="col-xl-9 col-lg-9">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>URL</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($series['casts'] as $cast)
                                <tr>
                                    <td>{{ $cast['name'] }}</td>
                                    <td>{{ $cast['web_url'] }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-xl-3 col-lg-3 col-form-label">Story Line:</label>
                    <div class="col-xl-9 col-lg-9">
                        {!! $series['story_line'] !!}
                    </div>
                </div>
            </div>
            <div class="m-separator m-separator--dashed m-separator--lg"></div>
            <div class="m-form__section m-form__section--first">
                <div class="m-form__heading">
                    <h3 class="m-form__heading-title">Video</h3>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-xl-3 col-lg-3 col-form-label">Trailer:</label>
                    <div class="col-xl-9 col-lg-9">
                        @if($series['trailer_url'])
                            <video id="my-video" class="video-js" controls preload="auto" width="150"
                                   height="100"
                                   data-setup="{}">
                                <source src="{{ $series['trailer_url'] }}" type='video/mp4'>
                            </video>
                        @else
                            <img src="{{ url('img/theme/no_video_available.jpg') }}" alt="" width="150" height="100">
                        @endif
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-xl-3 col-lg-3 col-form-label">Full:</label>
                    <div class="col-xl-9 col-lg-9">
                        @if($series['trailer_url'])
                            <video id="my-video" class="video-js" controls preload="auto" width="150"
                                   height="100"
                                   data-setup="{}">
                                <source src="{{ $series['full_url'] }}" type='video/mp4'>
                            </video>
                        @else
                            <img src="{{ url('img/theme/no_video_available.jpg') }}" alt="" width="150" height="100">
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-6">
            <div class="m-portlet m-portlet--full-height ">
                <div class="m-portlet__body">
                    <div class="m-widget3">
                        @foreach($comments['data'] as $comment)
                            <div class="m-widget3__item">
                                <div class="m-widget3__header">
                                    <div class="m-widget3__user-img">
                                        <img class="m-widget3__img" src="{{ $comment['user']['avatar'] }}" alt="">
                                    </div>
                                    <div class="m-widget3__info">
                                        <span class="m-widget3__username">
                                            {{ $comment['user']['name'] }}
                                        </span><br>
                                        <span class="m-widget3__time">
                                            {{ $comment['created_at'] }}
                                        </span>
                                    </div>
                                    <span class="m-widget3__status m--font-info">
                                        <a href="#"
                                           data-url="{{ route('backend.comment.destroy', $comment['id']) }}"
                                           class="delete_button btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--outline-2x">
                                            <i class="la la-trash"></i>
                                        </a>
                                    </span>
                                </div>
                                <div class="m-widget3__body">
                                    <p class="m-widget3__text">
                                        {{ $comment['comment'] }}
                                    </p>
                                </div>
                            </div>
                        @endforeach
                        @component('backend.components.paginate', ['meta' => $comments['meta']])
                        @endcomponent
                    </div>
                </div>
            </div>
        </div>
    </div>
@endcomponent

@section('extraJs')
    <link href="https://vjs.zencdn.net/7.3.0/video-js.css" rel="stylesheet">
    <script src="https://vjs.zencdn.net/7.3.0/video.js"></script>
@endsection
