@component('backend.components.full_box')
    @slot('titleTool')
        <a href="{{ route('backend.series.create') }}"
           class="btn btn-outline-primary m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air">
            <span>
                <i class="la la-plus-circle"></i>
                <span>ADD</span>
            </span>
        </a>
    @endslot

    <div class="form-group">
        @include('backend.series.search')
    </div>

    <div class="m-widget3">
        <table class="table m-table m-table--head-bg-brand">
            <thead>
            <tr>
                <th>#</th>
                <th>Title</th>
                <th>Slug</th>
                <th>Trailer Video</th>
                <th>Published Date</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($series['data'] as $index => $serie)
                <tr>
                    <td>
                        {{ $series['meta']['pagination']['start_index'] + $index }}
                    </td>
                    <td>{{ $serie['title_en'] }}</td>
                    <td>{{ $serie['slug'] }}</td>
                    <td>
                        @if($serie['trailer_url'])
                            <video id="my-video" class="video-js" controls preload="auto" width="150"
                                   height="100"
                                   data-setup="{}">
                                <source src="{{ $serie['preview_trailer'] }}" type='video/mp4'>
                            </video>
                        @else
                            <img src="{{ url('img/theme/no_video_available.jpg') }}" alt="" width="150"
                                 height="100">
                        @endif
                    </td>
                    <td>{{ $serie['published_at'] }}</td>
                    <td>
                        <a href="{{ route('backend.series.show', [$serie['id']]) }}"
                           class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                           title="View ">
                            <i class="la la-search"></i>
                        </a>
                        <a href="{{ route('backend.series.edit',[$serie['id']]) }}"
                           class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                           title="View ">
                            <i class="la la-edit"></i>
                        </a>
                        <a href="#"
                           class="delete_button m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                           data-url="{{ route('backend.series.destroy',[$serie['id']]) }}"
                           title="View ">
                            <i class="la la-trash"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    @component('backend.components.paginate',
        ['meta' => $series['meta']
    ])
    @endcomponent
@endcomponent
