@component('backend.components.full_box')
    @slot('titleTool')
        <a href="#" class="btn btn-outline-info m-btn m-btn--icon btn-lg m-btn--icon-only  m-btn--pill">
            <i class="la la-question"></i>
        </a>
    @endslot
    {{ Form::model(null,['route' => 'backend.series.store', 'method' => 'POST', 'class' => 'm-form m-form--fit m-form--label-align-right']) }}
    @include('backend.series.form')
    {{ Form::close() }}
@endcomponent
