@component('backend.components.full_box')
    @slot('title')
        <h4>Create Sub-Category For {{ $category['title'] }}</h4>
    @endslot
    @slot('titleTool')
        <a href="#" class="btn btn-outline-info m-btn m-btn--icon btn-lg m-btn--icon-only  m-btn--pill">
            <i class="la la-question"></i>
        </a>
    @endslot
    {{ Form::model(null,['route' => ['backend.sub-category.store', $category['id']], 'method' => 'POST', 'class' => 'm-form m-form--fit m-form--label-align-right']) }}
    @include('backend.sub_category.form')
    {{ Form::close() }}
@endcomponent
