@component('backend.components.full_box')
    @slot('titleTool')
        <a href="#" class="btn btn-outline-info m-btn m-btn--icon btn-lg m-btn--icon-only  m-btn--pill">
            <i class="la la-question"></i>
        </a>
    @endslot
    {{ Form::model($category,[
    'route' => ['backend.category.update', $category['id']],
    'method' => 'PATCH',
    'class' => 'm-form m-form--fit m-form--label-align-right']) }}
    @include('backend.category.form')
    {{ Form::close() }}
@endcomponent
