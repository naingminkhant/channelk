@component('backend.components.full_box')
    @slot('titleTool')
        <a href="{{ route('backend.role.create') }}"
           class="btn btn-outline-primary m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air">
            <span>
                <i class="la la-plus-circle"></i>
                <span>ADD</span>
            </span>
        </a>
    @endslot
    <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" id="local_data">
        <table class="m-datatable__table" style="display: block; min-height: 300px; overflow-x: auto;">
            <thead class="m-datatable__head">
            <tr class="m-datatable__row">
                <th class="m-datatable__cell m-datatable__cell--sort" data-sort="asc">
                <span style="width: 110px;">
                    Name
                </span>
                </th>
                <th class="m-datatable__cell m-datatable__cell--sort" data-sort="asc">
                <span style="width: 110px;">
                    Action
                </span>
                </th>
                <th class="m-datatable__cell m-datatable__cell--sort" data-sort="asc">
                <span style="width: 110px;">
                    Permission
                </span>
                </th>
            </tr>
            </thead>
            <tbody class="m-datatable__body">
            @foreach($roles as $role)
                <tr data-row="0" class="m-datatable__row" style="left: 0;">
                    <td class="m-datatable__cell--sorted m-datatable__cell">
                        <span style="width: 110px;">
                            {{ $role['name'] }}
                        </span>
                    </td>
                    <td class="m-datatable__cell--sorted m-datatable__cell">
                        <span style="width: 110px;">
                             <a href="{{ route('backend.role.edit', $role['id']) }}" class="btn btn-sm btn-primary">
                                 <i class="la la-edit"></i>
                             </a>
                            <a href="#" data-url="{{ route('backend.role.destroy', $role['id']) }}" class="delete_button btn btn-sm btn-danger">
                                <i class="la la-trash"></i>
                            </a>
                        </span>
                    </td>
                    <td class="m-datatable__cell--sorted m-datatable__cell">
                        <span style="width: 110px;">
                             <a href="{{ route('backend.permission.index',$role['slug']) }}" class="btn btn-sm btn-outline-accent">
                                 <i class="fa fa-1x fa-universal-access"></i>
                             </a>
                        </span>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endcomponent
