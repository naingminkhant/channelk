<!--begin::Base Styles -->

<!--begin::Page Vendors -->
<link href="{{ url('css/theme/fullcalendar.bundle.css') }}" rel="stylesheet" type="text/css"/>

<link rel="stylesheet" href="{{ url('css/theme/flaticon.css') }}">
<!--end::Page Vendors -->
<link href="{{ url('css/theme/vendors.bundle.css') }}" rel="stylesheet" type="text/css"/>

<link href="{{ url('css/theme/style.bundle.css') }}" rel="stylesheet" type="text/css"/>

<link rel="stylesheet" href="{{ url('css/custom.css') }}">
<link rel="icon" href="https://piay.iflix.com/app/favicon.ico" type="image/x-icon"/>
<link rel="shortcut icon" href="https://piay.iflix.com/app/favicon.ico" type="image/x-icon"/>
