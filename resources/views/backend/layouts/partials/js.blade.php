<!--begin::Base Scripts -->
<script src="{{ url('js/theme/vendors.bundle.js') }}" type="text/javascript"></script>
<script src="{{ url('js/theme/scripts.bundle.js') }}" type="text/javascript"></script>
<!--end::Base Scripts -->

<!--begin::Page Vendors -->
<script src="{{ url('js/theme/fullcalendar.bundle.js') }}" type="text/javascript"></script>

<!--end::Page Vendors -->
<script src="{{ asset('js/backend.js') }}" defer></script>
<!--begin::Page Snippets -->
<script src="{{ url('js/theme/dashboard.js') }}" type="text/javascript"></script>
<!--end::Page Snippets -->
