<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">
    <i class="la la-close"></i>
</button>
<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">

    <!-- BEGIN: Aside Menu -->
    <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark "
         m-menu-vertical="1" m-menu-scrollable="1" m-menu-dropdown-timeout="500" style="position: relative;">
        <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
            @if(isset($sidebarMenus))
                @foreach($sidebarMenus as $sidebar)
                    @if ($sidebar->hasPermission())
                        @if($sidebar->hasChildren())
                            @if($sidebar->getVisibility())
                                <li class="m-menu__item  m-menu__item--submenu {{ $sidebar->isActive('m-menu__item--open m-menu__item--active') }}"
                                    aria-haspopup="true"
                                    m-menu-submenu-toggle="hover">
                                    <a href="javascript:;" class="m-menu__link m-menu__toggle">
                                        <i class="m-menu__link-icon {{ $sidebar->getIcon() }}"></i>
                                        <span class="m-menu__link-text">
                                    {{ $sidebar->getLabel() }}
                                </span>
                                        <i class="m-menu__ver-arrow la la-angle-right"></i>
                                    </a>
                                    @foreach($sidebar->getChildren() as $child)
                                        @if($child->hasPermission())
                                            @if($child->getVisibility())
                                                <div class="m-menu__submenu ">
                                                    <span class="m-menu__arrow"></span>
                                                    <ul class="m-menu__subnav">
                                                        <li class="m-menu__item {{ $child->isActive() }}"
                                                            aria-haspopup="true">
                                                            <a href="{{ $child->getUrl() }}" class="m-menu__link ">
                                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                                    <span></span>
                                                                </i>
                                                                <span
                                                                    class="m-menu__link-text">{{ $child->getLabel() }}</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            @endif
                                        @endif
                                    @endforeach
                                </li>
                            @endif
                        @else
                            @if($sidebar->getVisibility())
                                <li class="m-menu__item {{ $sidebar->isActive() }}" aria-haspopup="true">
                                    <a href="{{ $sidebar->getUrl() }}" class="m-menu__link ">
                                        <i class="m-menu__link-icon {{ $sidebar->getIcon() }}"></i>
                                        <span class="m-menu__link-title">
                                    <span class="m-menu__link-wrap">
                                        <span class="m-menu__link-text">
                                            {{ $sidebar->getLabel() }}
                                        </span>
                                    </span>
                                </span>
                                    </a>
                                </li>
                            @endif
                        @endif
                    @endif
                @endforeach
            @endif
        </ul>
    </div>

    <!-- END: Aside Menu -->
</div>
