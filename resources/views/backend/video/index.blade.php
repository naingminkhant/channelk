@component('backend.components.full_box')
    <div class="form-group">
        @include('backend.video.search')
    </div>
    @slot('titleTool')
        <a href="{{ route('backend.video.create') }}"
           class="btn btn-outline-primary m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air">
            <span>
                <i class="la la-plus-circle"></i>
                <span>ADD</span>
            </span>
        </a>
    @endslot

    <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" id="local_data" style="">
        @if(empty($videos['data']))
            <h1 class="text-center">Data Not Exists</h1>
        @else
            <table class="m-datatable__table" style="display: block; min-height: 300px; overflow-x: auto;">
                <thead class="m-datatable__head">
                <tr class="m-datatable__row">
                    <th class="m-datatable__cell--center m-datatable__cell m-datatable__cell--check">
                        <span style="width: 50px;">#</span>
                    </th>
                    <th class="m-datatable__cell m-datatable__cell--sort" data-sort="asc">
                        <span style="width: 110px;">Title</span>
                    </th>
                    <th class="m-datatable__cell m-datatable__cell--sort" data-sort="asc">
                        <span style="width: 110px;">Price</span>
                    </th>
                    <th class="m-datatable__cell m-datatable__cell--sort" data-sort="asc">
                        <span style="width: 110px;">Duration</span>
                    </th>
                    <th class="m-datatable__cell m-datatable__cell--sort" data-sort="asc">
                        <span style="width: 110px;">Published Date</span>
                    </th>
                    <th class="m-datatable__cell m-datatable__cell--sort" data-sort="asc">
                        <span style="width: 150px;">Trailer Video</span>
                    </th>
                    <th class="m-datatable__cell m-datatable__cell--sort" data-sort="asc">
                        <span style="width: 150px;">Full Video</span>
                    </th>
                    <th class="m-datatable__cell m-datatable__cell--sort">
                        <span style="width: 110px;">Actions</span>
                    </th>
                </tr>
                </thead>
                <tbody class="m-datatable__body" style="">
                @foreach($videos['data'] as $index=>$video)
                    <tr data-row="0" class="m-datatable__row" style="left: 0;">
                        <td class="m-datatable__cell--center m-datatable__cell m-datatable__cell--check">
                            <span style="width: 50px;">
                                {{ $videos['meta']['pagination']['start_index'] + $index }}
                            </span>
                        </td>
                        <td class="m-datatable__cell--sorted m-datatable__cell">
                            <span style="width: 110px;">
                                {{ $video['title_en'] }}
                            </span>
                        </td>
                        <td class="m-datatable__cell--sorted m-datatable__cell">
                            <span style="width: 110px;">
                                {{ number_format($video['price']) }}
                            </span>
                        </td>
                        <td class="m-datatable__cell--sorted m-datatable__cell">
                            <span style="width: 110px;">
                                {{ $video['duration'] }}
                            </span>
                        </td>
                        <td class="m-datatable__cell--sorted m-datatable__cell">
                            <span style="width: 110px;">
                                {{ $video['published_at'] }}
                            </span>
                        </td>
                        <td class="m-datatable__cell--sorted m-datatable__cell">
                            <span style="width: 150px;">
                                @if($video['trailer_url'])
                                    <img src="{{ url('img/theme/no_video_available.jpg') }}" alt="" width="150"
                                         height="100">
                                    {{--<video id="my-video" class="video-js" controls preload="auto" width="150"--}}
                                    {{--height="100"--}}
                                    {{--data-setup="{}">--}}
                                    {{--<source src="{{ $video['preview_trailer'] }}" type='video/mp4'>--}}
                                    {{--</video>--}}
                                @else
                                    <img src="{{ url('img/theme/no_video_available.jpg') }}" alt="" width="150"
                                         height="100">
                                @endif
                            </span>
                        </td>
                        <td class="m-datatable__cell--sorted m-datatable__cell">
                            <span style="width: 150px;">
                                @if($video['full_url'])
                                    <img src="{{ url('img/theme/no_video_available.jpg') }}" alt="" width="150"
                                         height="100">
                                    {{--<video id="my-video" class="video-js" controls preload="auto" width="150"--}}
                                    {{--height="100"--}}
                                    {{--data-setup="{}">--}}
                                    {{--<source src="{{ $video['preview_full'] }}" type='video/mp4'>--}}
                                    {{--</video>--}}
                                @else
                                    <img src="{{ url('img/theme/no_video_available.jpg') }}" alt="" width="150"
                                         height="100">
                                @endif
                            </span>
                        </td>
                        <td data-field="Actions" class="m-datatable__cell">
                            <span style="overflow: visible; position: relative; width: 110px;">
                                <div class="dropdown ">
                                    <a href="#"
                                       class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                                       data-toggle="dropdown">
                                        <i class="la la-ellipsis-h"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item"
                                           href="{{ route('backend.video.edit', $video['id']) }}"><i
                                                class="la la-edit"></i> Edit</a>
                                        <a class="delete_button dropdown-item"
                                           href="#"
                                           data-url="{{ route('backend.video.destroy', $video['id']) }}">
                                            <i class="la la-trash"></i> Delete
                                        </a>
                                    </div>
                                </div>
                                <a href="{{ route('backend.video.show', $video['id']) }}"
                                   class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"
                                   title="View ">
                                    <i class="la la-search"></i>
                                </a>
                            </span>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @endif
    </div>
    @component('backend.components.paginate',
        ['meta' => $videos['meta']
    ])
    @endcomponent

@endcomponent

@section('extraJs')
    <link href="https://vjs.zencdn.net/7.3.0/video-js.css" rel="stylesheet">
    <script src="https://vjs.zencdn.net/7.3.0/video.js"></script>
@endsection
