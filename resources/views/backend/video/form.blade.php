<div class="m-portlet__body">
    <div class="form-group m-form__group row required">
        <label class="col-form-label col-lg-2 col-sm-12">Slug :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            {{ Form::input('text','slug', null, ['class' => 'form-control m-input', 'placeholder' => 'Slug']) }}
        </div>
    </div>
    <div class="form-group m-form__group row required">
        <label class="col-form-label col-lg-2 col-sm-12">Title (En) :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            {{ Form::input('text','title_en', null, ['class' => 'form-control m-input', 'placeholder' => 'Title En']) }}
        </div>
    </div>
    <div class="form-group m-form__group row required">
        <label class="col-form-label col-lg-2 col-sm-12">Title (MM) :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            {{ Form::input('text','title_mm', null, ['class' => 'form-control m-input', 'placeholder' => 'Title MM']) }}
        </div>
    </div>
    <div class="form-group m-form__group row required">
        <label class="col-form-label col-lg-2 col-sm-12">Price :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            {{ Form::input('number','price', null, ['class' => 'form-control m-input', 'placeholder' => 'Price']) }}
        </div>
    </div>
    <div class="form-group m-form__group row required">
        <label class="col-form-label col-lg-2 col-sm-12">Duration :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            <div class='input-group timepicker' id=''>
                {{ Form::input('text','duration', null, ['class' => 'form-control m-input', 'placeholder' => 'Select Duration', 'readonly', 'id' => 'm_timepicker_2']) }}
                <div class="input-group-append">
                    <span class="input-group-text">
                        <i class="la la-clock-o"></i>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group m-form__group row required">
        <label class="col-form-label col-lg-2 col-sm-12">Released Date :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            <div class="input-group date">
                {{ Form::input('text','released_date', null, ['class' => 'form-control m-input', 'placeholder' => 'Released Date', 'readonly', 'id'=>"m_datepicker_3"]) }}
                <div class="input-group-append">
                    <span class="input-group-text">
                        <i class="la la-calendar"></i>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group m-form__group row">
        <label class="col-form-label col-lg-2 col-sm-12">Budget :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            {{ Form::input('number','budget', null, ['class' => 'form-control m-input', 'placeholder' => 'Budget']) }}
        </div>
    </div>
    <div class="form-group m-form__group row required">
        <label class="col-form-label col-lg-2 col-sm-12">Country :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            {{ Form::select('country_id', $countries, null, ['class' => 'form-control m-select2 m-select2-general']) }}
        </div>
    </div>
    <div class="form-group m-form__group row required">
        <label class="col-form-label col-lg-2 col-sm-12">Director :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            {{ Form::select('director_id', $directors, null, ['class' => 'form-control m-select2 m-select2-general']) }}
        </div>
    </div>
    <div class="form-group m-form__group row required">
        <label class="col-form-label col-lg-2 col-sm-12">Company :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            {{ Form::select('production_company_id', $companies, null, ['class' => 'form-control m-select2 m-select2-general']) }}
        </div>
    </div>
    <div class="form-group m-form__group row required">
        <label class="col-form-label col-lg-2 col-sm-12">Casts :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            {!! Form::select('cast_ids[]', $casts, isset($video['casts']) ? array_column($video['casts'], 'id'):null, ['class' => 'form-control m-select2', 'id' => 'm_select2_11', 'multiple']) !!}
        </div>
    </div>
    <div class="form-group m-form__group row required">
        <label class="col-form-label col-lg-2 col-sm-12">Category :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            {!! Form::select('category_ids[]', $categories, isset($video['categories']) ? array_column($video['categories'], 'id'):null, ['class' => 'form-control m-select2', 'id' => 'm_select2_11_1', 'multiple']) !!}
        </div>
    </div>
    <div class="form-group m-form__group row required">
        <label class="col-form-label col-lg-2 col-sm-12">Sub Category :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            {!! Form::select('sub_category_ids[]', $sub_categories, isset($video['sub_categories']) ? array_column($video['sub_categories'], 'id'):null, ['class' => 'form-control m-select2', 'id' => 'm_select2_11_2', 'multiple']) !!}
        </div>
    </div>
    <div class="m-form__group form-group row">
        <label class="col-form-label col-lg-2 col-sm-12">Image :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            <image-widget :preview="'{{ isset($video) ? true:false}}'"></image-widget>
        </div>
    </div>
    <div class="form-group m-form__group row">
        <label class="col-form-label col-lg-2 col-sm-12">Trailer Video:</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            {{ Form::input('text','trailer_url', null, ['class' => 'form-control m-input', 'placeholder' => 'Trailer Video Url']) }}
        </div>
    </div>
    <div class="form-group m-form__group row">
        <label class="col-form-label col-lg-2 col-sm-12">Full Video:</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            {{ Form::input('text','full_url', null, ['class' => 'form-control m-input', 'placeholder' => 'Full Video Url']) }}
        </div>
    </div>
    <div class="form-group m-form__group row">
        <label class="col-form-label col-lg-2 col-sm-12">Published Date :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            <div class="input-group date">
                <input type="text" name="published_at" class="form-control m-input" placeholder="Published Date"
                       value="{{ isset($video) ? $video['published_at']:now()->format('m/d/Y') }}"
                       id="m_datepicker_3_modal"/>
                <div class="input-group-append">
                    <span class="input-group-text">
                        <i class="la la-calendar"></i>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group m-form__group row">
        <label class="col-form-label col-lg-2 col-sm-12"></label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            <div class="m-checkbox-inline">
                <label class="m-checkbox">
                    {!! Form::checkbox('notification', null, isset($video['notification']) ? $video['notification']:false) !!}
                    <label>Notification</label>
                    <span></span>
                </label>
            </div>
        </div>
    </div>
    <div class="m-form__group form-group row">
        <label class="col-form-label col-lg-2 col-sm-12">Story Line :</label>
        <div class="col-lg-10 col-md-10 col-sm-12">
            <textarea name="story_line" cols="30" rows="10" class="form-control" title="body">
                {{ old('story_line') ?? (isset($video) ? $video['story_line']:null) }}
            </textarea>
        </div>
    </div>
</div>
<div class="m-portlet__foot m-portlet__foot--fit">
    <div class="m-form__actions">
        <button type="submit" class="btn btn-primary">
            <i class="la la-save"></i> Submit
        </button>
        <a href="{{ route('backend.video.index') }}" class="btn btn-secondary">
            <i class="la la-rotate-right"></i>
            Cancel
        </a>
    </div>
</div>

@section('extraJs')
    <script src="{{ url('js/theme/select2.js') }}"></script>
    <script src="{{ url('js/theme/summernote.js') }}"></script>
    <script src="{{ url('js/theme/bootstrap-timepicker.js') }}"></script>
    <script src="{{ url('js/theme/bootstrap-datepicker.js') }}"></script>
@append
