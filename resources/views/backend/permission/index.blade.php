@component('backend.components.full_box', [
    'title' => $role->name.' Permissions'
])
    {!! Form::open(['url' => route('backend.permission.update', $role)]) !!}
    <div class="m-portlet__body">
        <div class="form-group m-form__group">
            @foreach(config('permissions') as $title => $values)
                <h5>{{ title_case(str_replace('-',' ',$title)) }}</h5>
                <br>
                <ul>
                    @foreach($values as $key => $value)
                        <li>
                            {!! Form::checkbox('permissions[]', $title.'-'.$key, array_has(array_flip($permissions), $title.'-'.$key) ? true : false); !!}
                            {{ $value }}
                        </li>
                    @endforeach
                </ul>
            @endforeach
        </div>
    </div>
    <div class="m-portlet__foot m-portlet__foot--fit form-group">
        <div class="m-form__actions">
            <button type="submit" class="btn btn-primary">
                <i class="la la-save"></i> Submit
            </button>
            <a href="{{ route('backend.role.index') }}" class="btn btn-secondary">
                <i class="la la-rotate-right"></i>
                Cancel
            </a>
        </div>
    </div>
    {!! Form::close() !!}
@endcomponent
