@component('backend.components.full_box')
    @slot('titleTool')
        <a href="#" class="btn btn-outline-info m-btn m-btn--icon btn-lg m-btn--icon-only  m-btn--pill">
            <i class="la la-question"></i>
        </a>
    @endslot
    {{ Form::model(null,['route' => 'backend.support.save', 'method' => 'POST', 'class' => 'm-form m-form--fit m-form--label-align-right']) }}

    <div class="m-portlet__body">
        <div class="m-form__group form-group row">
            <label class="col-form-label col-lg-2 col-sm-12">Body :</label>
            <div class="col-lg-10 col-md-10 col-sm-12">
                <textarea name="body" class="summernote" id="m_summernote_1" title="body">
                    {{ old('body') ?? (isset($support['body']) ? $support['body']:null) }}
                </textarea>
            </div>
        </div>
    </div>
    <div class="m-portlet__foot m-portlet__foot--fit">
        <div class="m-form__actions">
            <button type="submit" class="btn btn-primary">
                <i class="la la-save"></i> Submit
            </button>
            <a href="{{ route('backend.support.create') }}" class="btn btn-secondary">
                <i class="la la-rotate-right"></i>
                Cancel
            </a>
        </div>
    </div>
    {{ Form::close() }}

@section('extraJs')
    <script>
        var SummernoteDemo = {
            init: function () {
                $(".summernote").summernote({height: 150})
            }
        };
        jQuery(document).ready(function () {
            SummernoteDemo.init()
        });
    </script>
@append
@endcomponent
