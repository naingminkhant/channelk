@component('backend.components.full_box')
    @slot('titleTool')
        <a href="{{ route('backend.notification.create') }}"
           class="btn btn-outline-primary m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air">
            <span>
                <i class="la la-plus-circle"></i>
                <span>ADD</span>
            </span>
        </a>
    @endslot
    @component('backend.components.table',[
            'data'      => $notifications,
            'configs'   => config('tables.notification')
        ])
    @endcomponent
@endcomponent
