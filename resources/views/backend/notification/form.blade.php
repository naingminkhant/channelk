<div class="m-portlet__body">
    <div class="form-group m-form__group row">
        <label class="col-form-label col-lg-2 col-sm-12">Title :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            {{ Form::input('text','title', null, ['class' => 'form-control m-input', 'placeholder' => 'Title']) }}
        </div>
    </div>
    <div class="form-group m-form__group row">
        <label class="col-form-label col-lg-2 col-sm-12">Recipient Type :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            {{ Form::select('recipient_type',$recipient_types, null, ['placeholder' => 'choose ...','class' => 'form-control m-input','id'=>'recipient_type']) }}
        </div>
    </div>
    <div class="form-group m-form__group row" id="recipient_user">
        <label class="col-form-label col-lg-2 col-sm-12">Recipient Users :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            {!! Form::select('recipient_users[]', $users, isset($notification['user_notification']) ? array_column($notification['user_notification'], 'model_id'):null, ['class' => 'form-control m-select2', 'id' => 'm_select2_11', 'multiple']) !!}
        </div>
    </div>
    <div class="m-form__group form-group row">
        <label class="col-form-label col-lg-2 col-sm-12">Body :</label>
        <div class="col-lg-10 col-md-10 col-sm-12">
            <textarea name="body" cols="30" rows="10" class="form-control" title="body">
            {{ old('body') ?? (isset($notification) ? $notification['body']:null) }}
        </textarea>
        </div>
    </div>
</div>
<div class="m-portlet__foot m-portlet__foot--fit">
    <div class="m-form__actions">
        <button type="submit" class="btn btn-primary">
            <i class="la la-save"></i> Submit
        </button>
        <a href="{{ route('backend.notification.index') }}" class="btn btn-secondary">
            <i class="la la-rotate-right"></i>
            Cancel
        </a>
    </div>
</div>

@section('extraJs')
    <script src="{{ url('js/theme/select2.js') }}"></script>
    <script src="{{ url('js/theme/summernote.js') }}"></script>

    <script>
        const SPECIFIC_USERS = 'specific_users';
        let recipientType = null;
        $(document).ready(function () {
            recipientType = $('#recipient_type');

            if (recipientType.val() === SPECIFIC_USERS) {
                $('#recipient_user').show();
            } else {
                $('#recipient_user').hide();
            }

            recipientType.on('change', function () {
                if (this.value === SPECIFIC_USERS) {
                    $('#recipient_user').show();
                } else {
                    $('#recipient_user').hide();
                }
            });

        })
    </script>
@append
