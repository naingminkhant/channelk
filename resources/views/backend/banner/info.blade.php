@component('backend.components.full_box')
    @slot('title')

    @endslot
    <div class="col-xl-8 offset-xl-2">
        <div class="m-form__section m-form__section--first">
            <div class="m-form__heading">
                <h3 class="m-form__heading-title">Information</h3>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-xl-3 col-lg-3 col-form-label">Title:</label>
                <div class="col-xl-9 col-lg-9">
                    <input type="text" name="name" class="form-control m-input" placeholder=""
                           value="{{ $banner['title'] }}" readonly>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-xl-3 col-lg-3 col-form-label">Start Date:</label>
                <div class="col-xl-9 col-lg-9">
                    <input class="form-control m-input" placeholder=""
                           value="{{ $banner['start_date'] }}" readonly>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-xl-3 col-lg-3 col-form-label">End Date:</label>
                <div class="col-xl-9 col-lg-9">
                    <input class="form-control m-input" placeholder=""
                           value="{{ $banner['end_date'] }}" readonly>
                </div>
            </div>
            <div class="m-separator m-separator--dashed m-separator--lg"></div>
            <div class="form-group">
                <div class="row">
                    @foreach($banner['images'] as $image)
                        <div class="col-xl-4 col-lg-4 col-md-4">
                            <img src="{{ $image['thumbnail'] }}" alt="" width="200">
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endcomponent
