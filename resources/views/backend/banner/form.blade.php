<div class="m-portlet__body">
    <div class="form-group m-form__group row">
        <label class="col-form-label col-lg-2 col-sm-12">Title :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            {{ Form::input('text','title', null, ['class' => 'form-control m-input', 'placeholder' => 'Title']) }}
        </div>
    </div>
    <div class="m-form__group form-group row">
        <label class="col-form-label col-lg-2 col-sm-12">Cover Image :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            <image-widget :preview="'{{ isset($banner) ? true:false}}'" :name="'cover_images[]'" only_one="true"></image-widget>
        </div>
    </div>
    <div class="m-form__group form-group row">
        <label class="col-form-label col-lg-2 col-sm-12">Image :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            <image-widget :preview="'{{ isset($banner) ? true:false}}'"></image-widget>
        </div>
    </div>
    <div class="m-form__group form-group row">
        <label class="col-form-label col-lg-2 col-sm-12">Url :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            {{ Form::input('text','url', null, ['class' => 'form-control m-input', 'placeholder' => 'Banner Url']) }}
        </div>
    </div>
    <div class="m-form__group form-group row">
        <label class="col-form-label col-lg-2 col-sm-12">Type :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            {{ Form::input('text','type', null, ['class' => 'form-control m-input', 'placeholder' => 'Banner Type']) }}
        </div>
    </div>
    <div class="m-form__group form-group row">
        <label class="col-form-label col-lg-2 col-sm-12">Type ID :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            {{ Form::input('text','type_id', null, ['class' => 'form-control m-input', 'placeholder' => 'Banner Type ID']) }}
        </div>
    </div>
    <div class="form-group m-form__group row">
        <label class="col-form-label col-lg-2 col-sm-12">Start Date :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            <div class="input-group date">
                {{ Form::input('text','start_date', null, ['class' => 'form-control m-input', 'placeholder' => 'Start Date', 'readonly', 'id'=>"m_datepicker_3"]) }}
                <div class="input-group-append">
                    <span class="input-group-text">
                        <i class="la la-calendar"></i>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group m-form__group row">
        <label class="col-form-label col-lg-2 col-sm-12">End Date :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            <div class="input-group date">
                {{ Form::input('text','end_date', null, ['class' => 'form-control m-input', 'placeholder' => 'End Date', 'readonly', 'id'=>"m_datepicker_3"]) }}
                <div class="input-group-append">
                    <span class="input-group-text">
                        <i class="la la-calendar"></i>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group m-form__group row">
        <label class="col-form-label col-lg-2 col-sm-12">Published Date :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            <div class="input-group date">
                {{ Form::input('text','published_at', null, ['class' => 'form-control m-input', 'placeholder' => 'Published Date', 'readonly', 'id'=>"m_datepicker_3"]) }}
                <div class="input-group-append">
                    <span class="input-group-text">
                        <i class="la la-calendar"></i>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="m-portlet__foot m-portlet__foot--fit">
    <div class="m-form__actions">
        <button type="submit" class="btn btn-primary">
            <i class="la la-save"></i> Submit
        </button>
        <a href="{{ route('backend.user.index') }}" class="btn btn-secondary">
            <i class="la la-rotate-right"></i>
            Cancel
        </a>
    </div>
</div>

@section('extraJs')
    <script src="{{ url('js/theme/bootstrap-datepicker.js') }}"></script>
@append
