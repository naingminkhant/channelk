@component('backend.components.full_box')
    @slot('title')

    @endslot
    <div class="col-xl-8 offset-xl-2">
        <div class="m-form__section m-form__section--first">
            <div class="form-group m-form__group row">
                <label class="col-xl-3 col-lg-3 col-form-label">Name:</label>
                <div class="col-xl-9 col-lg-9">
                    <input type="text" name="name" class="form-control m-input" placeholder=""
                           value="{{ $cast['name'] }}" readonly>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-xl-3 col-lg-3 col-form-label">Slug:</label>
                <div class="col-xl-9 col-lg-9">
                    <input type="text" class="form-control m-input" placeholder=""
                           value="{{ $cast['slug'] }}" readonly>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-xl-3 col-lg-3 col-form-label">Birthday:</label>
                <div class="col-xl-9 col-lg-9">
                    <input type="text" class="form-control m-input" placeholder=""
                           value="{{ $cast['born_at'] }}" readonly>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-xl-3 col-lg-3 col-form-label">Address:</label>
                <div class="col-xl-9 col-lg-9">
                    <input type="text" class="form-control m-input" placeholder=""
                           value="{{ $cast['address'] }}" readonly>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-xl-3 col-lg-3 col-form-label">Gender:</label>
                <div class="col-xl-9 col-lg-9">
                    <div class="m-checkbox-list">
                        <label class="m-checkbox m-checkbox--check-bold m-checkbox--state-success m-checkbox--disabled">
                            <input type="checkbox"
                                   disabled="disabled" {{ $cast['gender']=='male' ? 'checked':'' }}> Male
                            <span></span>
                        </label>
                        <label class="m-checkbox m-checkbox--check-bold m-checkbox--state-success m-checkbox--disabled">
                            <input type="checkbox"
                                   disabled="disabled" {{ $cast['gender']=='female' ? 'checked':'' }}> Female
                            <span></span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-xl-3 col-lg-3 col-form-label">Website:</label>
                <div class="col-xl-9 col-lg-9 col-form-label">
                    <a href="{{ $cast['web_url']  }}" target="_blank">
                        <strong>{{ $cast['web_url'] }}</strong>
                    </a>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-xl-3 col-lg-3 col-form-label">Description:</label>
                <div class="col-xl-9 col-lg-9 col-form-label">
                    {!! $cast['description'] !!}
                </div>
            </div>
        </div>
        <div class="m-separator m-separator--dashed m-separator--lg"></div>
    </div>
@endcomponent
