<div class="m-portlet__body">
    <div class="form-group m-form__group row">
        <label class="col-form-label col-lg-2 col-sm-12">Slug :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            {{ Form::input('text','slug', null, ['class' => 'form-control m-input', 'placeholder' => 'Slug']) }}
        </div>
    </div>
    <div class="form-group m-form__group row">
        <label class="col-form-label col-lg-2 col-sm-12">Name :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            {{ Form::input('text','name', null, ['class' => 'form-control m-input', 'placeholder' => 'Title']) }}
        </div>
    </div>
    <div class="form-group m-form__group row">
        <label class="col-form-label col-lg-2 col-sm-12">Position :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            {{ Form::input('text','position', null, ['class' => 'form-control m-input', 'placeholder' => 'Position']) }}
        </div>
    </div>
    <div class="form-group m-form__group row">
        <label class="col-form-label col-lg-2 col-sm-12">Birthday :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            <div class="input-group date">
                {{ Form::input('text','born_at', null, ['class' => 'form-control m-input', 'placeholder' => 'Birth Date', 'readonly', 'id'=>"m_datepicker_3"]) }}
                <div class="input-group-append">
                    <span class="input-group-text">
                        <i class="la la-calendar"></i>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group m-form__group row">
        <label class="col-form-label col-lg-2 col-sm-12">Address :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            {{ Form::input('text','address', null, ['class' => 'form-control m-input', 'placeholder' => 'Address']) }}
        </div>
    </div>
    <div class="form-group m-form__group row">
        <label class="col-form-label col-lg-2 col-sm-12">WebUrl :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            {{ Form::input('text','web_url', null, ['class' => 'form-control m-input', 'placeholder' => 'Title']) }}
        </div>
    </div>
    <div class="m-form__group form-group row">
        <label class="col-form-label col-lg-2 col-sm-12">Gender :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            <div class="m-radio-inline">
                <label class="m-radio">
                    {{ Form::radio('gender', 'male', true) }}
                    Male
                    <span></span>
                </label>
                <label class="m-radio">
                    {{ Form::radio('gender', 'female', true) }} Female
                    <span></span>
                </label>
            </div>
        </div>
    </div>
    <div class="m-form__group form-group row">
        <label class="col-form-label col-lg-2 col-sm-12">Image :</label>
        <div class="col-lg-6 col-md-10 col-sm-12">
            <image-widget :preview="'{{ isset($cast) ? true:false}}'"></image-widget>
        </div>
    </div>
    <div class="m-form__group form-group row">
        <label class="col-form-label col-lg-2 col-sm-12">Description :</label>
        <div class="col-lg-10 col-sm-12 col-md-10">
            <textarea name="description" cols="30" rows="10" class="form-control" title="body">
            {{ old('description') ?? (isset($cast) ? $cast['description']:null) }}
        </textarea>
        </div>
    </div>
</div>
<div class="m-portlet__foot m-portlet__foot--fit">
    <div class="m-form__actions">
        <button type="submit" class="btn btn-primary">
            <i class="la la-save"></i> Submit
        </button>
        <a href="{{ route('backend.cast.index') }}" class="btn btn-secondary">
            <i class="la la-rotate-right"></i>
            Cancel
        </a>
    </div>
</div>

@section('extraJs')
    <script src="{{ url('js/theme/summernote.js') }}"></script>
    <script src="{{ url('js/theme/bootstrap-datepicker.js') }}"></script>
@append
