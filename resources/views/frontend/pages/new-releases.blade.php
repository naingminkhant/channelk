@extends('frontend.layouts.app')

@section('body-class', 'single-video')

@section('content')

    <div class="content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <h3 class="main_title"><span>New Releases</span></h3>
                </div>
            </div>

            <div class="row">
                @foreach($new_videos as $new_video)
                    @if($new_video->playlists->isNotEmpty())
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="b-video">
                                <div class="v-img">
                                    <a href="{{ url('series/' . $new_video->playlists->first()->id . '/video/' . $new_video->id) }}"><img
                                                src="{{ $new_video->getSingleImageUrl() }}" alt=""
                                                width="100%" height="100%"></a>
                                    <div class="time">{{ $new_video->title }}</div>
                                </div>
                                <div class="v-desc">
                                    <a href="{{ url('series/' . $new_video->playlists->first()->id . '/video/' . $new_video->id) }}">{{ $new_video->title }}</a>
                                </div>
                                <div class="v-views">
                                    {{ $new_video->viewed }} {{ str_plural('view', $new_video->viewed) }}.
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>

        </div>
    </div>

@endsection
