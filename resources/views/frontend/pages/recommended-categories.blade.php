@extends('frontend.layouts.app')

@section('body-class', 'single-video')

@section('content')

    <div class="content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <h3 class="main_title"><span>Categories</span></h3>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">

                    <!-- Featured Videos -->
                    <div class="content-block">
                        <div class="cb-header">
                            <div class="row">
                                <div class="col-lg-8 col-xs-6">

                                    <div class="clearfix"></div>
                                </div>

                            </div>
                        </div>
                        <div class="cb-content videolist">
                            <div class="row">
                                @foreach($categories as $category)
                                    <div class="col-lg-2 col-sm-4 col-xs-6">
                                        <div class="b-chanel mb-8">
                                            <a href="/series-by-category/{{ $category->id }}">
                                                <img src="{{ $category->getSingleImageUrl() }}" alt="" width="100%">
                                            </a>
                                        </div>
                                        <a href="/series-by-category/{{ $category->id }}">{{ $category->title }}</a>
                                    </div>
                                @endforeach

                            </div>
                        </div>
                    </div>
                    <!-- /Featured Videos -->


                </div>
            </div>
        </div>
    </div>

@endsection
