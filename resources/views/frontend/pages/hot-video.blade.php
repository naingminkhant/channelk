@extends('frontend.layouts.app')

@section('body-class', 'single-video')

@section('content')

    <div class="content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <h3 class="main_title"><span>Hot Videos</span></h3>
                </div>
            </div>

            <div class="row">
                @foreach($hot_videos as $hot_video)
                    @if($hot_video->playlists->isNotEmpty())
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="b-video">
                                <div class="v-img">
                                    <a href="{{ url('series/' . $hot_video->playlists->first()->id . '/video/' . $hot_video->id) }}"><img
                                                src="{{ $hot_video->getSingleImageUrl() }}" alt=""
                                                width="100%" height="100%"></a>
                                    <div class="time">{{ $hot_video->duration }}</div>
                                </div>
                                <div class="v-desc">
                                    <a href="{{ url('series/' . $hot_video->playlists->first()->id . '/video/' . $hot_video->id) }}">{{ $hot_video->title }}</a>
                                </div>
                                <div class="v-views">
                                    {{ $hot_video->viewed }} {{ str_plural('view', $hot_video->viewed) }}
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach

            </div>
        </div>
    </div>

@endsection
