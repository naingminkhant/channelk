@extends('frontend.layouts.app')

@section('content')

    <div class="content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 v-history">
                    <div class="content-block">
                        <div class="col-md-12 text-center">
                            <h3 class="main_title"><span>Privacy Policy</span></h3>
                        </div>
                        {!! $info->info_two !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
