@extends('frontend.layouts.app')

@section('content')

    <div class="content-wrapper">
        <div class="container">
            <div class="row">
                @include('frontend.layouts.profile-card')

                <div class="col-md-9">
                    <h4 class="m-t-15 f-bold"><span>Recently Played Videos</span></h4>
                    <div class="row video-grid">
                        @forelse($recent_videos as $recent_video)
                            <div class=" col-lg-4 col-sm-6 videoitem">
                                <div class="b-video">
                                    <div class="v-img">
                                        <a href="{{ url('series/' . $recent_video->playlists->first()->id . '/video/' . $recent_video->id) }}"><img
                                                    src="{{ $recent_video->getSingleImageUrl() }}" alt=""
                                                    width="100%" height="100%"></a>
                                        <div class="time">{{ $recent_video->duration }}</div>
                                    </div>
                                    <div class="v-desc">
                                        <a href="{{ url('series/' . $recent_video->playlists->first()->id . '/video/' . $recent_video->id) }}">{{ $recent_video->title }}</a>
                                    </div>
                                    <div class="v-views">
                                        {{ $recent_video->viewed }} {{ str_plural('view', $recent_video->viewed) }}.
                                    </div>
                                </div>
                            </div>
                            <!-- Video item ends -->
                        @empty
                            <div class="col-lg-12 col-sm-12">
                                <h4 class="text-center">No recently played videos</h4>
                            </div>
                        @endforelse
                    </div>
                    <!-- Row Ends -->
                    <hr>
                    <h4 class="m-t-15 f-bold"><span>Favourite Videos</span></h4>
                    <div class="row video-grid">
                        @forelse($favorite_videos as $video)
                        <div class=" col-lg-4 col-sm-6 videoitem">
                            <div class="b-video">
                                <div class="v-img">
                                    <a href="{{ url('series/' . $video->playlists->first()->id . '/video/' . $video->id) }}"><img
                                                src="{{ $video->getSingleImageUrl() }}" alt=""
                                                width="100%" height="100%"></a>
                                    <div class="time">{{ $video->duration }}</div>
                                </div>
                                <div class="v-desc">
                                    <a href="{{ url('series/' . $video->playlists->first()->id . '/video/' . $video->id) }}">{{ $video->title }}</a>
                                </div>
                                <div class="v-views">
                                    {{ $video->viewed }} {{ str_plural('view', $video->viewed) }}.
                                </div>
                            </div>
                        </div>
                        <!-- Video item ends -->
                        @empty
                            <div class="col-lg-12 col-sm-12">
                                <h4 class="text-center">No favorite videos</h4>
                            </div>
                        @endforelse

                    </div>
                    <!-- Row Ends -->
                </div>
                <!-- Col 9 ends -->
            </div>
            <!-- Row Ends -->
        </div>
        <!-- Container Ends -->
    </div>

@endsection
