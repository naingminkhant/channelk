@extends('frontend.layouts.app')

@section('body-class', 'single-video')

@section('content')

    <div class="content-wrapper" style="">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">

                    <div class="channel-details">
                        <div class="row">
                            <div class="col-lg-9 col-xs-12">
                                <div class="c-details">
                                    <div class="c-name">
                                        <h2>TV Shows</h2>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Featured Videos -->
                    <div class="content-block">
                        <div class="cb-header">
                            <div class="row">
                                <div class="col-lg-8 col-xs-6">

                                    <div class="clearfix"></div>
                                </div>

                            </div>
                        </div>
                        <div class="cb-content videolist">
                            <div class="row">
                                @foreach($tv_shows as $tv_show)
                                    <div class="col-lg-2 col-sm-6 videoitem">
                                        <div class="b-video">
                                            <div class="v-img">
                                                <a href="{{ url('series/' . $tv_show->id) }}"><img src="{{ $tv_show->getSingleImageUrl() }}" alt="" width="100%" height="100%"></a>
                                                
                                            </div>
                                            <div class="v-desc">
                                                <a href="{{ url('series/' . $tv_show->id) }}">{{ $tv_show->title }}</a>
                                            </div>
                                            <div class="v-views">
                                                {{ $tv_show->viewed }} {{ str_plural('view', $tv_show->viewed) }}.
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <!-- /Featured Videos -->


                </div>
            </div>
        </div>
    </div>

@endsection
