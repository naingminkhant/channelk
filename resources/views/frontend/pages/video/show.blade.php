@extends('frontend.layouts.app')

@section('body-class', 'single-video')

@section('content')
    <div class="content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-xs-12 col-sm-12">
                    <div class="sv-video">
                        <div class="video-responsive">
                            <iframe src="https://player.vimeo.com/video/{{ $video->full_url }}?autoplay=1" width="860" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen="" data-ready="true"></iframe>
                        </div>
                    </div>
                    <h1><a href="#">{{ $video->title }}</a></h1>

                </div>

                <!-- right column -->
                <div class="col-lg-4 col-xs-12 col-sm-12 visible-lg">

                    <!-- up next -->
                    <div class="caption">
                        <div class="left">
                            <a href="#">Upcoming Videos</a>
                        </div>
                        <div class="right">
                            <a href="#">View all</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="list" style="height: 370px; overflow: scroll">
                        @foreach($upcoming_videos as $upcoming_video)
                            <div class="h-video row">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="v-img">
                                        <a href="{{ url('video/' . $upcoming_video->id) }}"><img src="{{ $upcoming_video->getSingleImageUrl() }}" alt="{{ $upcoming_video->title }}" width="100%" height="100%"></a>
                                        <div class="time">{{ $upcoming_video->duration }}</div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <div class="v-desc">
                                        <a href="{{ url('video/' . $upcoming_video->id) }}">{{ $upcoming_video->title }}</a>
                                    </div>
                                    <div class="v-views">
                                        {{ $upcoming_video->viewed }} {{ str_plural('view', $upcoming_video->viewed) }}.
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        @endforeach

                    </div>
                    <!-- END up next -->
                </div>

                <div class="col-lg-12">
                    <div class="info">
                        <div class="info-content">
                            <h4>Cast:</h4>
                            <p>{{ $video->stars->implode('name', ', ') }}</p>

                            <h4>Category :</h4>
                            <p>
                                @foreach($video->sub_categories as $sub_category)
                                    {{ $sub_category->title }}
                                    @if(!$loop->last) , @endif
                                @endforeach
                            </p>

                            <h4>About :</h4>
                            {!! $video->story_line !!}

                            <div class="row date-lic">
                                <div class="col-xs-6">
                                    <h4>Release Date:</h4>
                                    <p>{{ $video->released_date }}</p>
                                </div>
                            </div>
                        </div>

                        <div class="showless hidden-xs"></div>

                        <div class="content-block head-div head-arrow head-arrow-top visible-xs">
                            <div class="head-arrow-icon">
                                <i class="cv cvicon-cv-next"></i>
                            </div>
                        </div>

                        <div class="adblock2">
                            <div class="img">
                            <span class="hidden-xs">
                                Google AdSense<br>728 x 90
                            </span>
                            </div>
                        </div>

                        <!-- similar videos -->
                        <div class="caption visible-md">
                            <div class="left">
                                <a href="#">Upcoming Videos</a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="single-v-footer">
                            <div class="single-v-footer-switch">
                                <a href="#" class="active" data-toggle=".similar-v">
                                    <i class="cv cvicon-cv-play-circle"></i>
                                    <span>Upcoming Videos</span>
                                </a>
                                <a href="#" data-toggle=".comments">
                                    <i class="cv cvicon-cv-comment"></i>
                                    <span>1 Comments</span>
                                </a>
                            </div>
                            <div class="similar-v single-video video-mobile-02 hidden-lg">
                                <div class="row">
                                    @foreach($upcoming_videos as $upcoming_video)

                                        <div class="col-lg-3 col-sm-6 col-xs-12">
                                            <div class="h-video row">
                                                <div class="col-sm-12 col-xs-6">
                                                    <div class="v-img">
                                                        <a href="{{ url('video/' . $upcoming_video->id) }}"><img src="{{ $upcoming_video->getSingleImageUrl() }}" alt="{{ $upcoming_video->title }}" width="100%" height="100%"></a>
                                                        <div class="time">{{ $upcoming_video->duration }}</div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-xs-6">
                                                    <div class="v-desc">
                                                        <a href="{{ url('video/' . $upcoming_video->id) }}">{{ $upcoming_video->title }}</a>
                                                    </div>
                                                    <div class="v-views">
                                                        {{ $upcoming_video->viewed }} {{ str_plural('view', $upcoming_video->viewed) }}.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    @endforeach
                                </div>
                                <div>
                                    <div class="col-lg-12">
                                        <div class="loadmore-comments text-center">
                                            <button class="btn btn-default h-btn">View All</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END similar videos -->

                            <!-- comments -->
                            <div class="comments">
                                <div class="reply-comment">
                                    <div class="rc-header"><i class="cv cvicon-cv-comment"></i> <span class="semibold">1</span> Comments</div>
                                    <div class="rc-ava"><a href="#"><img src="/images/ava5.png" alt=""></a></div>
                                    <div class="rc-comment">
                                        <form action="#" method="post">
                                            <textarea rows="3">Share what you think?</textarea >
                                            <button type="submit">
                                                <i class="cv cvicon-cv-add-comment"></i>
                                            </button>
                                        </form>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="comments-list">

                                    <div class="cl-header hidden-xs">
                                        <div class="c-nav">
                                            <ul class="list-inline">
                                                <li><a href="#" class="active">Comments </a></li>
                                            </ul>
                                        </div>
                                    </div>

                                    <!-- comment -->
                                    <div class="cl-comment">
                                        <div class="cl-avatar"><a href="#"><img src="/images/ava8.png" alt=""></a></div>
                                        <div class="cl-comment-text">
                                            <div class="cl-name-date"><a href="#">BOWTZ pros</a> . 1 week ago</div>
                                            <div class="cl-text">Really great story. You're doing a great job. Keep it up pal.</div>
                                            <div class="cl-meta"></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <!-- END comment -->
                                </div>
                            </div>
                            <!-- END comments -->
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection


@section('js')
    <script  src="/js/vendor/player/johndyer-mediaelement-89793bc/build/mediaelement-and-player.min.js"></script>
@endsection
