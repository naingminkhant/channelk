@extends('frontend.layouts.app')

@section('body-class', 'single-video')

@section('content')

    <div class="content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <h3 class="main_title"><span>Popular TV Shows</span></h3>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <!-- Featured Videos -->
                    <div class="content-block">

                        <div class="cb-content videolist">
                            <div class="row">
                                @foreach($hot_series as $hot_series_video)
                                    <div class="col-lg-2 col-sm-6 videoitem">
                                        <div class="b-video">
                                            <div class="v-img">
                                                <a href="{{ url('series/' . $hot_series_video->id) }}"><img src="{{ $hot_series_video->getSingleImageUrl() }}" alt="" width="100%" height="100%"></a>
                                            </div>
                                            <div class="v-desc">
                                                <a href="{{ url('series/' . $hot_series_video->id) }}">{{ $hot_series_video->title }}</a>
                                            </div>
                                            <div class="v-views">
                                                {{ $hot_series_video->viewed }} {{ str_plural('view', $hot_series_video->viewed) }}.
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <!-- /Featured Videos -->


                </div>
            </div>
        </div>
    </div>

@endsection
