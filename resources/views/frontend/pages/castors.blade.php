@extends('frontend.layouts.app')

@section('body-class', 'single-video')

@section('content')

    <div class="content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <h3 class="main_title"><span>Castors</span></h3>
                </div>
            </div>

            <div class="row">
                @foreach($castors as $castor)
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <a href="{{ url('/castor/' . $castor->id) }}" class="card-link">
                            <div class="card-bordered">
                                <img src="{{ $castor->getFileUrl() ? $castor->getFileUrl() : '/images/ava2.png' }}" alt="Presenter" class="img-responsive" />
                                <p class="castor-name">{{ $castor->name }}</p>
                            </div>
                        </a>
                    </div>
                @endforeach

            </div>

            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    {{ $castors->links() }}
                </div>
            </div>

        </div>
    </div>

@endsection
