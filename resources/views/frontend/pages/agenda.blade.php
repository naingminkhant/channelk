@extends('frontend.layouts.app')

@section('content')

    <div class="content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 v-history">
                    <!-- History -->
                    <div class="content-block">
                        <div class="cb-header">
                            <div class="row">
                                <div class="col-lg-12 col-xs-12 col-sm-12">
                                    <ul class="list-inline">
                                        <li><a href="{{ url('agenda/today') }}" class="{{ Request::is('agenda/today') ? 'active' : '' }}">Today Program &nbsp;&nbsp;</a> <span class="hidden-xs">|</span></li>
                                        <li><a href="{{ url('agenda/tomorrow') }}" class="{{ Request::is('agenda/tomorrow') ? 'active' : '' }}">Tomorrow Program</a> <span class="hidden-xs">|</span></li>
                                        <li><a href="{{ url('agenda/' . today()->addDays(2)->format('Y-m-d')) }}" class="{{ Request::is('agenda/' . today()->addDays(2)->format('Y-m-d')) ? 'active' : '' }}">{{ today()->addDays(2)->format('F d') }} Program</a> <span class="hidden-xs">|</span></li>
                                        <li><a href="{{ url('agenda/' . today()->addDays(3)->format('Y-m-d')) }}" class="{{ Request::is('agenda/' . today()->addDays(3)->format('Y-m-d')) ? 'active' : '' }}">{{ today()->addDays(3)->format('F d') }} Program</a> <span class="hidden-xs">|</span></li>
                                        <li><a href="{{ url('agenda/' . today()->addDays(4)->format('Y-m-d')) }}" class="{{ Request::is('agenda/' . today()->addDays(4)->format('Y-m-d')) ? 'active' : '' }}">{{ today()->addDays(4)->format('F d') }} Program</a> <span class="hidden-xs">|</span></li>
                                        <li><a href="{{ url('agenda/' . today()->addDays(5)->format('Y-m-d')) }}" class="{{ Request::is('agenda/' . today()->addDays(5)->format('Y-m-d')) ? 'active' : '' }}">{{ today()->addDays(5)->format('F d') }} Program</a> <span class="hidden-xs">|</span></li>
                                        <li><a href="{{ url('agenda/' . today()->addDays(6)->format('Y-m-d')) }}" class="{{ Request::is('agenda/' . today()->addDays(6)->format('Y-m-d')) ? 'active' : '' }}">{{ today()->addDays(6)->format('F d') }} Program</a> <span class="hidden-xs">|</span></li>
                                        <li><a href="{{ url('agenda/' . today()->addDays(7)->format('Y-m-d')) }}" class="{{ Request::is('agenda/' . today()->addDays(7)->format('Y-m-d')) ? 'active' : '' }}">{{ today()->addDays(7)->format('F d') }} Program</a></li>
                                    </ul>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="cb-content">
                            <!-- history video row -->
                            <div class="row">
                                <div class="h-video">
                                    <div class="col-lg-12 col-sm-12 col-xs-12">
                                        <div class="m-section__content">
                                            <table class="table table-bordered m-table m-table--border-brand m-table--head-bg-brand">
                                                <thead style="background: #FCD40D;">
                                                <tr>
                                                    <th>#</th>
                                                    <th>Time</th>
                                                    <th>Title</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($agenda->agendas as $key => $item)
                                                    <tr >
                                                        <th scope="row"> {{ $key+1 }}</th>
                                                        <td>{{ $item['time'] }}</td>
                                                        <td>{{ $item['title'] }}</td>
                                                    </tr>
                                                 @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="h-divider"></div>
                                </div>
                            </div>
                            <!-- ///history video row -->
                        </div>
                    </div>
                    <!-- /History -->
                    <div class="content-block head-div head-arrow mb-40 mt-40 visible-xs">
                        <div class="head-arrow-icon">
                            <i class="cv cvicon-cv-next"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
