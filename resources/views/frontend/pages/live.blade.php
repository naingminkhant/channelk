@extends('frontend.layouts.app')

@section('css')
    <!-- unpkg : use a specific version of Video.js (change the version numbers as necessary) -->
    <link href="https://vjs.zencdn.net/7.3.0/video-js.min.css" rel="stylesheet">
    <script src="https://vjs.zencdn.net/7.3.0/video.min.js"></script>

@endsection

@section('body-class', 'single-video')

@section('content')

    <div class="content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-xs-12 col-sm-12">
                    <div class="sv-video">
                        <div class="video-responsive">
                            <video id="livestream" class="video-js vjs-default-skin vjs-16-9" >
                                <source src="https://www.channelkmyanmar.tk/live/livestream.m3u8" type="application/x-mpegURL">
                            </video>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="info">
                        <div class="info-content">
                            {!! $text->info_one !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('js')
    <script>
        videojs('livestream', {
            controls: true,
            autoplay: true,
        });
    </script>
@endsection
