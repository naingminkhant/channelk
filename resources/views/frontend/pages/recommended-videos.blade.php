@extends('frontend.layouts.app')

@section('body-class', 'single-video')

@section('content')

    <div class="content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <h3 class="main_title"><span>Recommended Videos</span></h3>
                </div>
            </div>

            @foreach($recommended_videos as $recommended_video)
                @if($recommended_video->playlists->isNotEmpty())
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="b-video">
                            <div class="v-img">
                                <a href="{{ url('series/' . $recommended_video->playlists->first()->id . '/video/' . $recommended_video->id) }}"><img
                                            src="{{ $recommended_video->getSingleImageUrl() }}"
                                            alt="{{ $recommended_video->title }}" width="100%"
                                            height="100%"></a>
                                <div class="time">{{ $recommended_video->duration }}</div>
                            </div>
                            <div class="v-desc">
                                <a href="{{ url('series/' . $recommended_video->playlists->first()->id . '/video/' . $recommended_video->id) }}">{{ $recommended_video->title }}</a>
                            </div>
                            <div class="v-views">
                                {{ $recommended_video->viewed }} {{ str_plural('view', $recommended_video->viewed) }}.
                            </div>
                        </div>
                    </div>
                @endif
            @endforeach
        </div>
    </div>

@endsection
