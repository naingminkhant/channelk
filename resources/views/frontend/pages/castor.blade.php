@extends('frontend.layouts.app')

@section('body-class', 'single-video')

@section('content')

    <div class="content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="card-bordered m-t-15">
                        <img src="{{ $castor->getFileUrl() ? $castor->getFileUrl() : '/images/ava2.png' }}" alt="Presenter" class="img-responsive" />
                        <p class="castor-name">{{ $castor->name }}</p>
                        <p>{{ $castor->position }}</p>
                    </div>
                </div>
                <div class="col-md-8">
                    <h4 class="m-t-15 f-bold"><span>{{ $castor->name }}'s Videos</span></h4>
                    <div class="row">

                        @foreach($videos as $video)
                            <div class="col-lg-4 col-sm-6 videoitem">
                                <div class="b-video">
                                    <div class="v-img">
                                        <a href="{{ url('series/' . $video->playlists->first()->id . '/video/' . $video->id) }}">
                                            <img src="{{ $video->getSingleImageUrl() }}"
                                                 alt="{{ $video->title }}" width="100%" height="100%">
                                        </a>
                                        <div class="time">{{ $video->duration }}</div>
                                    </div>
                                    <div class="v-desc">
                                        <a href="{{ url('series/' . $video->playlists->first()->id . '/video/' . $video->id) }}">{{ $video->title }}</a>
                                    </div>
                                    <div class="v-views">
                                        {{ $video->viewed }} {{ str_plural('view', $video->viewed) }}.
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            {{ $videos->links() }}
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection
