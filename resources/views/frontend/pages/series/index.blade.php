@extends('frontend.layouts.app')

@section('body-class', 'channel')

@section('content')
    <!-- channel -->
    <div class="container-fluid">
        <div class="row">
            <div class="img">
                <div class="img-image">
                    <img src="{{ $series->getCoverImageUrl() ?: '/images/s-detail-bg.png'}}" alt="" class="c-banner">
                    <div class="col-lg-5"></div>
                    <div class="col-lg-2" style="position: relative; top: -200px;margin-bottom: -200px;">
                        <center><img src="{{ $series->getSingleImageUrl() }}" width="100%"></center><br>
                        <div style="font-weight: bold;font-size: 30px;text-align: center;">
                            {{ $series->title }}
                        </div>
                    </div>
                    <div class="col-lg-5"></div>
                </div>
                <a href="#" class="add"><i class="cv cvicon-cv-plus"></i></a>
                <div class="c-social hidden-xs">
                    Social
                    <a href="#" class="fb"><i class="fa fa-facebook"></i></a>
                    <a href="#" class="tw"><i class="fa fa-twitter"></i></a>
                    <a href="#" class="gp"><i class="fa fa-google-plus"></i></a>
                </div>
            </div>
        </div>
    </div>
    <!-- ///channel -->

    <div class="content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">

                    <div class="channel-details">
                        <div class="row">
                            <div class="col-lg-12 col-xs-12">
                                <div class="c-details">
                                    <div class="c-nav">
                                        <ul class="list-inline">
                                            <li role="presentation" class="active"><a href="#playlist" aria-controls="playlist" role="tab" data-toggle="tab">Playlist</a></li>
                                            <li role="presentation"><a href="#about" aria-controls="about" role="tab" data-toggle="tab">About</a></li>
                                        </ul>
                                    </div>

                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="playlist">
                            <!-- Featured Videos -->
                            <div class="content-block">
                                <div class="cb-header"></div>
                                <div class="cb-content videolist">
                                    <div class="row">
                                        @foreach($series->videos as $video)
                                            <div class=" col-lg-3 col-sm-6 videoitem">
                                                <div class="b-video">
                                                    <div class="v-img">
                                                        <a href="{{ url('series/' . $series->id . '/video/' . $video->id) }}"><img src="{{ $video->getSingleImageUrl() }}" alt="{{ $video->title }}" width="100%" height="100%"></a>
                                                        <div class="time">{{ $video->duration }}</div>
                                                    </div>
                                                    <div class="v-desc">
                                                        <a href="{{ url('series/' . $series->id . '/video/' . $video->id) }}">{{ $video->title }}</a>
                                                    </div>
                                                    <div class="v-views">
                                                        {{ $video->viewed }} {{ str_plural('view', $video->viewed) }}.
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach

                                    </div>
                                </div>
                            </div>
                            <!-- /Featured Videos -->
                        </div>
                        <div role="tabpanel" class="tab-pane" id="about">
                            <!-- Featured Videos -->
                            <div class="content-block">
                                <div class="cb-header"></div>
                                <div class="cb-content videolist">
                                    <div class="row">
                                        <div class="info">
                                            <div class="info-content">
                                                <h4>Cast:</h4>
                                                <p>{{ $series->stars->implode('name', ', ') }}</p>

                                                <h4>About :</h4>
                                                {!! $series->story_line !!}

                                                <div class="row date-lic">
                                                    <div class="col-xs-6">
                                                        <h4>Release Date:</h4>
                                                        <p>{{ $series->released_date }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!-- /Featured Videos -->
                        </div>
                    </div>




                </div>
            </div>
        </div>
    </div>
@endsection
