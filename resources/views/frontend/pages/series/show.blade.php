@extends('frontend.layouts.app')
@section('css')
    <script src="https://cdn.jsdelivr.net/npm/vue"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
@endsection

@section('body-class', 'single-video')
@section('content')
    <div class="content-wrapper" id="app">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-xs-12 col-sm-12">
                    <div class="sv-video">
                        <div class="video-responsive">
                            <iframe src="https://player.vimeo.com/video/{{ $video->full_url }}?autoplay=1" width="860" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen="" data-ready="true"></iframe>
                        </div>
                    </div>
                    <h1><a href="javascript:;">{{ $video->title }}</a></h1>

                </div>

                <!-- right column -->
                <div class="col-lg-4 col-xs-12 col-sm-12 visible-lg">

                    <!-- up next -->
                    <div class="caption">
                        <div class="left">
                            <a href="#">Upcoming Videos</a>
                        </div>
                        <div class="right">
                            <a href="{{ url('series/' . $series->id) }}">View all</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="list" style="height: 370px; overflow: scroll">
                        @foreach($upcoming_videos as $upcoming_video)
                            <div class="h-video row">
                                <div class="col-lg-6 col-sm-6">
                                    <div class="v-img">
                                        <a href="{{ url('series/' . $series->id .'/video/' . $upcoming_video->id) }}"><img src="{{ $upcoming_video->getSingleImageUrl() }}" alt="{{ $upcoming_video->title }}" width="100%" height="100%"></a>
                                        <div class="time">{{ $upcoming_video->duration }}</div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <div class="v-desc">
                                        <a href="{{ url('series/' . $series->id .'/video/' . $upcoming_video->id) }}">{{ $upcoming_video->title }}</a>
                                    </div>
                                    <div class="v-views">
                                        {{ $upcoming_video->viewed }} {{ str_plural('view', $upcoming_video->viewed) }}.
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        @endforeach

                    </div>
                    <!-- END up next -->
                </div>

                <div class="col-lg-8 col-md-8">
                    <div class="info">
                        <div class="custom-tabs">
                            <div class="tabs-panel">
                                <a href="#" class="active" data-tab="tab-1">
                                    <i class="cv cvicon-cv-about" data-toggle="tooltip" data-placement="top" title="About"></i>
                                    <span>About</span>
                                </a>

                                <a href="#" data-tab="tab-6">
                                    <i class="cv cvicon-cv-comment" data-toggle="tooltip" data-placement="top" title="Add to"></i>
                                    <span>Comments</span>
                                </a>

                                <a href="#" data-tab="tab-2">
                                    <i class="cv cvicon-cv-share" data-toggle="tooltip" data-placement="top" title="Share"></i>
                                    <span>Share</span>
                                </a>

                                <div class="acide-panel hidden-xs">
                                    @auth
                                        <a href="#" @click.prevent="submitFavorite"><i :class="isFavorite ? 'fa fa-heart' : 'far fa-heart'" data-toggle="tooltip" data-placement="top" title="Favorite"></i></a>
                                        <a href="#" @click.prevent="submitLike"><i :class="isLike ? 'fa fa-thumbs-up' : 'far fa-thumbs-up'" data-toggle="tooltip" data-placement="top" title="Like"></i></a>
                                    @else
                                        <a href="javascript:;"><i class="far fa-heart" data-html="true"  data-toggle="popover" data-placement="top" title="Favorite" data-content="Please <a href='/login'>sign in</a> or <a href='/register'>create an account</a> to favorite this video."></i></a>
                                        <a href="javascript:;"><i class="far fa-thumbs-up" data-html="true"  data-toggle="popover" data-placement="top" title="Like" data-content="Please <a href='/login'>sign in</a> or <a href='/register'>create an account</a> to like this video."></i></a>
                                    @endauth
                                </div>

                            </div>
                            <div class="clearfix"></div>

                            <!-- BEGIN tabs-content -->
                            <div class="tabs-content">
                                <!-- BEGIN tab-1 -->
                                <div class="tab-1">
                                    <div>
                                        @if($video->stars->isNotEmpty())
                                            <h4>Cast:</h4>
                                            <p class="sv-tags">
                                                @foreach($video->stars as $star)
                                                    <span><a href="{{ url('castor/' . $star->id) }}">{{ $star->name }}</a></span>
                                                @endforeach
                                            </p>
                                        @endif

                                        @if($video->categories->isNotEmpty())
                                                <h4>Category :</h4>
                                                <p class="sv-tags">
                                                    @foreach($video->categories as $category)
                                                        <span><a href="#">{{ $category->title }}</a></span>
                                                    @endforeach
                                                </p>
                                        @endif



                                        <h4>About :</h4>
                                        <p>{!! $video->story_line !!}</p>

                                        <div class="row date-lic">
                                            <div class="col-xs-6">
                                                <h4>Release Date:</h4>
                                                <p>{{ \Carbon\Carbon::parse($video->released_date)->diffForHumans() }}</p>
                                            </div>
                                            <div class="col-xs-6 ta-r">
                                                <h4>{{ $video->viewed }} {{ str_plural('view', $video->viewed) }}</h4>
                                            </div>
                                        </div>

                                        <div class="row date-lic">
                                            <div class="col-xs-6">
                                                <h4>Publish Date:</h4>
                                                <p>{{ $video->published_at }}</p>
                                            </div>
                                            {{--<div class="col-xs-6 ta-r">--}}
                                                {{--<h4>License:</h4>--}}
                                                {{--<p>Standard</p>--}}
                                            {{--</div>--}}
                                        </div>
                                    </div>

                                </div>
                                <!-- END tab-1 -->

                                <!-- BEGIN tab-2 -->
                                <div class="tab-2">
                                    <h4>Share:</h4>
                                    <div class="social">
                                        <a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode( request()->url() ) }}" class="facebook" target="_blank"><i class="fab fa-facebook-f" aria-hidden="true"></i></a>
                                        <a href="http://twitter.com/share?url={{ urlencode( request()->url() ) }}" class="twitter" target="_blank"><i class="fab fa-twitter" aria-hidden="true"></i></a>
                                        <a href="http://www.linkedin.com/shareArticle?url={{ urlencode( request()->url() ) }}" class="linkedin" target="_blank"><i class="fab fa-linkedin-in" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-9">
                                            <h4>Link:</h4>
                                            <label class="clipboard">
                                                <input type="text" name="#" class="share-link" value="{{ request()->url() }}" readonly>
                                                <div class="btn-copy" data-clipboard-target=".share-link">Copy</div>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <!-- END tab-2 -->

                                <div class="tab-6">
                                    <!-- comments -->
                                    <div class="comments">
                                        <div class="reply-comment">
                                            @auth
                                                <div class="rc-header"><i class="cv cvicon-cv-comment"></i> <span class="semibold">@{{ comments.length }}</span> Comments</div>
                                                <div class="rc-ava"><a href="#"><img src="{{ Auth::user()->getAvatar() }}" alt="" class="img-circle"></a></div>
                                                <div class="rc-comment">
                                                    <form action="/video/{{ $video->id }}/comment" @submit.prevent="submitCommentForm" method="post">
                                                        @csrf
                                                        <textarea name="comment" v-model="comment" rows="3" required></textarea >
                                                        <button type="submit">
                                                            <i class="cv cvicon-cv-add-comment"></i>
                                                        </button>
                                                    </form>
                                                </div>
                                                <div class="clearfix"></div>
                                             @else
                                                <div class="rc-header"><i class="cv cvicon-cv-comment"></i> <span class="semibold">{{ $video->comments->count() }}</span> Comments</div>
                                                <p class="">
                                                    Please <a href="/login">sign in</a> or <a href="/register">create an account</a> to participate in this conversation.
                                                </p>
                                                <div class="clearfix"></div>
                                             @endauth
                                        </div>
                                        <div class="comments-list" v-if="comments.length">

                                            <div class="cl-header hidden-xs">
                                                <div class="c-nav">
                                                    <ul class="list-inline">
                                                        <li><a href="#" class="active">Comments</a></li>
                                                    </ul>
                                                </div>
                                            </div>

                                            <!-- comment -->
                                            <div v-for="comment in comments" class="cl-comment">
                                                <div class="cl-avatar"><a href="javascript:;"><img :src="comment.avatar" alt="" width="65%" class="img-circle"></a></div>
                                                <div class="cl-comment-text">
                                                    <div class="cl-name-date"><a href="javascript:;">@{{ comment.name }}</a> . @{{ comment.time }}</div>
                                                    <div class="cl-text">@{{ comment.text }}</div>
                                                    <div class="cl-meta"></div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <!-- END comment -->

                                        </div>
                                    </div>
                                    <!-- END comments -->
                                </div>
                            </div>
                            <!-- END tabs-content -->
                        </div>

                        <div class="showless hidden-xs"></div>

                        <div class="content-block head-div head-arrow head-arrow-top visible-xs">
                            <div class="head-arrow-icon">
                                <i class="cv cvicon-cv-next"></i>
                            </div>
                        </div>

                        <div class="adblock2">
                            <div class="img">
                            <span class="hidden-xs">
                                Google AdSense<br>728 x 90
                            </span>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection


@section('js')
    <script  src="/js/vendor/player/johndyer-mediaelement-89793bc/build/mediaelement-and-player.min.js"></script>

    <script>

        axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

        /**
         * Next we will register the CSRF Token as a common header with Axios so that
         * all outgoing HTTP requests automatically have it attached. This is just
         * a simple convenience so we don't have to attach every token manually.
         */

        token = document.head.querySelector('meta[name="csrf-token"]');

        if (token) {
            axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': token.content
                }
            });
        } else {
            console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
        }

        var app = new Vue({
            el: '#app',
            data: {
                video_id: "{{ $video->id }}",
                comments: {},
                comment: null,
                loading: true,
                isFavorite : "{{ $video->hasFavorite() }}",
                isLike : "{{ $video->isAlreadyLike() }}",
            },

            mounted() {
                this.getComments();
            },

            methods: {
                submitCommentForm() {
                    let data = {
                        comment : this.comment
                    };

                    axios.post('/comment/video/' + this.video_id, data ).then((response) => {
                        this.comment = null;
                        this.comments = response.data.data;
                    }).catch((error) => {
                        this.loading = false;
                    });
                },

                getComments() {
                    axios.get('/comment/video/' + this.video_id).then((response) => {
                        this.comments = response.data.data;
                        this.loading = false;
                    }).catch((error) => {
                        this.loading = false;
                    });
                },

                submitFavorite() {
                    axios.post('/video/' + this.video_id + '/favorite').then((response) => {
                        this.isFavorite = response.data.isFavorite;
                    }).catch((error) => {
                        this.loading = false;
                    });
                },

                submitLike() {
                    axios.post('/video/' + this.video_id + '/like').then((response) => {
                        this.isLike = response.data.isLike;
                    }).catch((error) => {
                        this.loading = false;
                    });
                },
            }
        })
    </script>

@endsection
