@extends('frontend.layouts.app')

@section('content')

    <div class="content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">

                    <!-- Featured Videos -->
                    <div class="content-block">
                        <div class="cb-header">
                            <div class="row">
                                <div class="col-lg-10 col-xs-10">
                                    <ul class="list-inline">
                                        <li>Search Results <a href="#">"{{ $query }}"</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        @if($videos->count())
                            <ul class="list-inline">
                                <li><a href="#" class="color-active active-border">Videos</a></li>
                            </ul>

                            <br>

                            <div class="single-video video-mobile-02">
                                <div class="row">
                                    @foreach($videos as $video)
                                        @if($video->playlists->isNotEmpty())
                                        <div class="col-lg-3 col-sm-6 col-xs-12">
                                            <div class="h-video row">
                                                <div class="col-sm-12 col-xs-6">
                                                    <div class="v-img">
                                                        <a href="{{ url('series/' . $video->playlists->first()->id . '/video/' . $video->id) }}"><img
                                                                    src="{{ $video->getSingleImageUrl() }}" alt=""
                                                                    width="100%" height="100%"></a>
                                                        <div class="time">{{ $video->duration }}</div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-xs-6">
                                                    <div class="v-desc">
                                                        <a href="{{ url('series/' . $video->playlists->first()->id . '/video/' . $video->id) }}">{{ $video->title }}</a>
                                                    </div>
                                                    <div class="v-views">
                                                        {{ $video->viewed }} {{ str_plural('view', $video->viewed) }}.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        @endif


                        @if($series_videos->count())
                            <ul class="list-inline">
                                <li><a href="#" class="color-active active-border">Series</a></li>
                            </ul>

                            <br>

                            <div class="single-video video-mobile-02">
                                <div class="row">
                                    @foreach($series_videos as $series_video)
                                        <div class="col-lg-3 col-sm-6 col-xs-12">
                                            <div class="h-video row">
                                                <div class="col-sm-12 col-xs-6">
                                                    <div class="v-img">
                                                        <a href="{{ url('series/' . $series_video->id) }}"><img src="{{ $series_video->getSingleImageUrl() }}" alt="" width="100%" height="100%"></a>
                                                        <div class="time">{{ url('') }}</div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-xs-6">
                                                    <div class="v-desc">
                                                        <a href="{{ url('series/'. $series_video->id) }}">{{ $series_video->title }}</a>
                                                    </div>
                                                    <div class="v-views">
                                                        {{ $series_video->viewed }} {{ str_plural('view', $series_video->viewed) }}.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endif

                        @if($casts->count())
                            <ul class="list-inline">
                                <li><a href="#" class="color-active active-border">Cast</a></li>
                            </ul>

                            <br>

                            <div class="single-video video-mobile-02">
                                <div class="row">
                                    @foreach($casts as $cast)
                                        <div class="col-lg-1 col-sm-2 col-xs-3">
                                            <a href="{{ url('/castor/' . $cast->id) }}">
                                                <img src="{{ $cast->getFileUrl() ? $cast->getFileUrl() : '/images/ava2.png' }}" alt="" width="100%">
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endif


                    </div>
                    <!-- /Featured Videos -->

                </div>
            </div>
        </div>
    </div>


@endsection


@section('js')
    <script>

    </script>
@endsection
