@extends('frontend.layouts.app')

@section('content')

    <div class="content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 v-history">
                    <div class="content-block">
                        <div class="content-block">
                            <div class="col-md-12 text-center">
                                <h3 class="main_title"><span>Contact Us</span></h3>
                            </div>
                            {!! optional($data)->body !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
