@extends('frontend.layouts.app')

@section('content')
    <div class="content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">


                    @if($banners->isNotEmpty())
                        <div class="content-block head-div">
                            <br>
                            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                <!-- Indicators -->
                                <ol class="carousel-indicators">
                                    @foreach($banners as $key => $banner)
                                        <li data-target="#myCarousel" data-slide-to="{{ $key }}"
                                            class="{{ $loop->first ? 'active' : '' }}"></li>
                                    @endforeach
                                </ol>

                                <!-- Wrapper for slides -->
                                <div class="carousel-inner">
                                    @foreach($banners as $banner)
                                        <div class="item{{ $loop->first ? ' active' : '' }}">
                                            <a href="{{ $banner->url }}">
                                                <img src="{{ $banner->getFileUrl() }}" alt="{{ $banner->title }}"
                                                     style="width: 100%;">
                                            </a>
                                        </div>
                                    @endforeach
                                </div>

                                <!-- Left and right controls -->
                                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                            <br>
                        </div>
                @endif

                <!-- New Releases Videos -->
                    <div class="content-block head-div head-arrow">

                        <div class="cb-header">
                            <div class="row">
                                <div class="col-lg-10 col-sm-10 col-xs-8">

                                            <a href="{{ url('new-releases') }}" class="color-active">
                                                <span class="active-border">NEW RELEASE</span>
                                            </a>

                                </div>
                                <div class="col-lg-2 col-sm-2 col-xs-4">
                                    <div class="btn-group pull-right bg-clean">
                                        <a href="{{ url('new-releases') }}" class="viewmore"> View More</a>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="cb-content videolist">
                            <div class="row">
                                @foreach($new_videos as $new_video)
                                    <div class="col-lg-2 col-sm-3 col-xs-3 videoitem">
                                        <div class="b-video">
                                            <div class="v-img">
                                                <a href="{{ url('series/' . $new_video->playlists->first()->id . '/video/' . $new_video->id) }}"><img
                                                            src="{{ $new_video->getSingleImageUrl() }}" alt=""
                                                            width="100%" height="100%" style="box-shadow: 1px;">

                                                        </a>
                                                <div class="time">{{ $new_video->duration }}</div>
                                            </div>
                                            <div class="v-desc h-desc">
                                                <a href="{{ url('series/' . $new_video->playlists->first()->id . '/video/' . $new_video->id) }}">{{ $new_video->title }}</a>
                                                <div class="v-views">

                                                <div>{!! str_limit(strip_tags($new_video->story_line), $limit = 50, $end = '...') !!}</div><br>
                                                <div class="flex-container">
                                                <div>
                                                <span class="glyphicon glyphicon-eye-open"></span>  {{ $new_video->viewed }} {{ str_plural('view', $new_video->viewed) }}.
                                                </div>
                                                <div>{{ \Carbon\Carbon::parse($new_video->released_date)->diffForHumans() }}</div>
                                                </div>

                                            </div>
                                            </div>

                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <!-- /New Releases Videos -->

                    <!-- TV Show -->
                    <div class="content-block head-div head-arrow">
                        <div class="cb-header">
                            <div class="row">
                                <div class="col-lg-10 col-sm-10 col-xs-8">
                                    <ul class="list-inline">
                                        <li><a href="{{ url('tv-shows') }}" class="color-active active-border">TV
                                                Shows</a></li>
                                    </ul>
                                </div>
                                <div class="col-lg-2 col-sm-2 col-xs-4">
                                    <div class="btn-group pull-right bg-clean">
                                        <a href="{{ url('tv-shows') }}" class="viewmore">VIEW MORE<a>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="cb-content">
                            <div class="row">
                                @foreach($tv_shows as $tv_show)
                                    <div class="col-lg-2 col-sm-3 col-xs-3 videoitem">
                                        <div class="b-video">
                                            <div class="v-img">
                                                <a href="{{ url('series/' . $tv_show->id) }}"><img
                                                            src="{{ $tv_show->getSingleImageUrl() }}" alt=""
                                                            width="100%" height="100%"></a>

                                            </div>
                                            <div class="v-desc">
                                                <a href="{{ url('series/' . $tv_show->id) }}">{{ $tv_show->title }}</a>
                                            </div>
                                            <div class="v-views">

                                                <div>{!! str_limit(strip_tags($tv_show->story_line), $limit = 50, $end = '...') !!}</div><br>
                                                <div class="flex-container">
                                                <div>
                                                <span class="glyphicon glyphicon-eye-open"></span>  {{ $tv_show->viewed }} {{ str_plural('view', $tv_show->viewed) }}.
                                                </div>
                                                <div>{{ \Carbon\Carbon::parse($new_video->released_date)->diffForHumans() }}</div>
                                                </div>

                                               
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <!-- /TV Show -->


                     <!-- Castor -->
                    <div class="content-block head-div head-arrow">

                        <div class="cb-header">
                            <div class="row">
                                <div class="col-lg-10 col-sm-10 col-xs-10">
                                    <ul class="list-inline">
                                        <li><a href="{{ url('recommended-castors') }}"
                                               class="color-active active-border">Castor</a></li>
                                    </ul>
                                </div>
                                <div class="col-lg-2 col-sm-2 col-xs-4">
                                    <div class="btn-group pull-right bg-clean">
                                        <a href="{{ url('recommended-castors') }}" class="viewmore"> View
                                            More</a>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="cb-content avatars">
                            <div class="row">
                                @foreach($castors as $castor)
                                    <div class="col-lg-2 col-sm-2 col-xs-3">
                                        <a href="{{ url('/castor/' . $castor->id) }}">
                                            <img src="{{ $castor->getFileUrl() ? $castor->getFileUrl() : '/images/ava2.png' }}"
                                                 alt="Presenter" width="200px;">
                                        </a><br>
                                        <div><center><b>{{$castor->name}}</b></center></div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <!-- /Castor -->

  <!-- Hot Series -->
                    <div class="content-block head-div head-arrow">

                        <div class="cb-header">
                            <div class="row">
                                <div class="col-lg-10 col-sm-10 col-xs-4">
                                    <ul class="list-inline">
                                        <li><a href="{{ url('hot-series') }}" class="color-active active-border">Popular TV Shows</a></li>
                                    </ul>
                                </div>
                                <div class="col-lg-2 col-sm-2 col-xs-4">
                                    <div class="btn-group pull-right bg-clean">
                                        <a href="{{ url('hot-series') }}"  class="viewmore"> View More</a>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="cb-content">
                            <div class="row">
                                @foreach($hot_series as $hot_series_video)
                                    <div class="col-lg-2 col-sm-3 col-xs-3 videoitem">
                                        <div class="b-video">
                                            <div class="v-img">
                                                <a href="{{ url('series/' . $hot_series_video->id) }}"><img
                                                            src="{{ $hot_series_video->getSingleImageUrl() }}" alt=""
                                                            width="100%" height="100%"></a>

                                            </div>
                                            <div class="v-desc">
                                                <a href="{{ url('series/' . $hot_series_video->id) }}">{{ $hot_series_video->title }}</a>
                                            </div>
                                            <div class="v-views">
                                                <div>{!! str_limit(strip_tags($hot_series_video->story_line), $limit = 50, $end = '...') !!}</div><br>
                                                
                                                <div class="flex-container">
                                                <div>
                                                <span class="glyphicon glyphicon-eye-open"></span>   {{ $hot_series_video->viewed }} {{ str_plural('view', $hot_series_video->viewed) }}
                                                </div>
                                                <div>{{ \Carbon\Carbon::parse($new_video->released_date)->diffForHumans() }}</div>
                                                </div>

                                               
                                                .
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <!-- /Hot Series -->

                    <!-- Hot Video -->
                    <div class="content-block head-div head-arrow">

                        <div class="cb-header">
                            <div class="row">
                                <div class="col-lg-10 col-sm-10 col-xs-8">
                                    <ul class="list-inline">
                                        <li>
                                            <a href="{{ url('hot-videos') }}" class="color-active active-border">
                                                <span>Hot Videos</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-2 col-sm-2 col-xs-4">
                                    <div class="btn-group pull-right bg-clean">
                                        <a href="{{ url('hot-videos') }}" class="viewmore"> View More</a>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="cb-content videolist">

                            <div class="row">
                                @foreach($hot_videos as $hot_video)
                                    <div class="col-lg-6 col-sm-6 col-xs-6 videoitem">
                                        <div class="b-video">
                                            <div class="row">
                                            <div class="col-lg-5 col-sm-5 col-xs-5">
                                                <div class="v-img">
                                                <a href="{{ url('series/' . $hot_video->playlists->first()->id . '/video/' . $hot_video->id) }}"><img
                                                            src="{{ $hot_video->getSingleImageUrl() }}" alt=""
                                                            width="100%" height="100%"></a>
                                                <div class="time">{{ $hot_video->duration }}</div>
                                                </div>
                                            </div>

                                            <div class="col-lg-7 col-sm-7 col-xs-7">
                                                <div class="v-desc" style="padding-top: 0px;">
                                                    <a href="{{ url('series/' . $hot_video->playlists->first()->id . '/video/' . $hot_video->id) }}">{{ $hot_video->title }}</a>
                                                </div>
                                                <div class="v-views">
                                                    <div>{!! str_limit(strip_tags($hot_video->story_line), $limit = 50, $end = '...') !!}</div><br>
                                                    <div class="flex-container">
                                                    <div>
                                                    <span class="glyphicon glyphicon-eye-open"></span>   {{ $hot_video->viewed }} {{ str_plural('view', $hot_video->viewed) }}.
                                                    </div>
                                                    <div>{{ \Carbon\Carbon::parse($new_video->released_date)->diffForHumans() }}</div>
                                                    </div>
                                                   
                                                </div>
                                            </div>
                                           </div><br>
                                        </div>
                                    </div>
                                @endforeach
                            </div>

                        </div>
                    </div>
                    <!-- /Hot Video -->



                    <!-- Recommended video -->
                    <div class="content-block head-div head-arrow">

                        <div class="cb-header">
                            <div class="row">
                                <div class="col-lg-10 col-sm-10 col-xs-8">
                                    <ul class="list-inline">
                                        <li><a href="{{ url('recommended-videos') }}"
                                               class="color-active active-border">Recommended Videos</a></li>
                                    </ul>
                                </div>
                                <div class="col-lg-2 col-sm-2 col-xs-4">
                                    <div class="btn-group pull-right bg-clean">
                                        <a href="{{ url('recommended-videos') }}" class="viewmore"> View More</a>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="cb-content videolist">

                            <div class="row">
                                @foreach($recommended_videos as $recommended_video)
                                    <div class="col-lg-2 col-sm-3 col-xs-3 videoitem">
                                        <div class="b-video">
                                            <div class="v-img">
                                                <a href="{{ url('series/' . $recommended_video->playlists->first()->id . '/video/' . $recommended_video->id) }}"><img
                                                            src="{{ $recommended_video->getSingleImageUrl() }}"
                                                            alt="{{ $recommended_video->title }}" width="100%"
                                                            height="100%"></a>
                                                <div class="time">{{ $recommended_video->duration }}</div>
                                            </div>
                                            <div class="v-desc">
                                                <a href="{{ url('series/' . $recommended_video->playlists->first()->id . '/video/' . $recommended_video->id) }}">{{ $recommended_video->title }}</a>
                                            </div>
                                            <div class="v-views">
                                                <div>{!! str_limit(strip_tags($recommended_video->story_line), $limit = 50, $end = '...') !!}</div><br>
                                                <div class="flex-container">
                                                    <div>
                                                    <span class="glyphicon glyphicon-eye-open"></span> {{ $recommended_video->viewed }} {{ str_plural('view', $recommended_video->viewed) }}.
                                                    </div>
                                                    <div>{{ \Carbon\Carbon::parse($new_video->released_date)->diffForHumans() }}</div>
                                                    </div>
                                                
                                                
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>

                        </div>
                    </div>
                    <!-- /Recommended video -->




                    <!-- Categories -->
                    <div class="content-block head-div head-arrow">

                        <div class="cb-header">
                            <div class="row">
                                <div class="col-lg-10 col-sm-10 col-xs-6">
                                    <ul class="list-inline">
                                        <li><a href="{{ url('recommended-categories') }}"
                                               class="color-active active-border">Categories</a></li>
                                    </ul>
                                </div>
                                <div class="col-lg-2 col-sm-2 col-xs-6">
                                    <div class="btn-group pull-right bg-clean">
                                        <a href="{{ url('recommended-categories') }}" class="viewmore"> View
                                            More</a>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="cb-content chanels-row">
                            <div class="row">
                                @foreach($categories as $category)

                                    <div class="col-lg-1 col-sm-1 col-xs-1 videoitem">
                                        <div class="b-video">
                                            <div class="v-img cate-img">
                                                <a href="/series-by-category/{{ $category->id }}"><img
                                                            src="{{ $category->getSingleImageUrl() }}"
                                                            alt="{{ $category->title }}" width="100%"></a>
                                            </div>
                                            <div class="v-desc text-center">
                                                <a href="/series-by-category/{{ $category->id }}">{{ $category->title }}</a>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <!-- /Categories -->

                </div>
            </div>
        </div>
    </div>


@endsection


@section('js')

@endsection
