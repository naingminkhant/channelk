@extends('frontend.layouts.app')

@section('content')

    <div class="content-wrapper">
        <div class="container">
            <div class="row">
                @include('frontend.layouts.profile-card')

                <div class="col-md-9">
                    <h4 class="m-t-b-15 f-bold"><span>Settings</span></h4>
                    <hr>
                    <div class="settings">
                        <div class="row">
                            <div class="col-md-12">
                                {!! Form::open(['method' => 'POST', 'url' => '/account/edit_account_info', 'class' => 'form-horizontal' ]) !!}
                                    <div class="form-group{{$errors->has('name') ? ' has-error' : '' }}">
                                        <label class="control-label col-sm-3" for="name">Change Name</label>
                                        <div class="col-sm-6">
                                            <input type="text" name="name" class="form-control" id="name" value="{{ auth()->user()->name }}">
                                            @if ($errors->has('name'))
                                                <span class="help-block">{{ $errors->first('name') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-6">
                                            <button type="submit" class="btn btn-success">Save Changes</button>
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                                    <hr>
                                <br>

                                {!! Form::open(['method' => 'POST', 'route' => 'user.password.change', 'class' => 'form-horizontal' ]) !!}
                                    <div class="form-group{{$errors->has('old_password') ? ' has-error' : '' }}">
                                        <label class="control-label col-sm-3" for="pwd">Old Password:</label>
                                        <div class="col-sm-6">
                                            <input type="password" name="old_password" class="form-control" id="pwd" placeholder="Enter password">
                                            @if ($errors->has('old_password'))
                                                <span class="help-block">{{ $errors->first('old_password') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group{{$errors->has('password') ? ' has-error' : '' }}">
                                        <label class="control-label col-sm-3" for="pwd">New Password:</label>
                                        <div class="col-sm-6">
                                            <input type="password" name="password" class="form-control" id="pwd" placeholder="Enter password">
                                            @if ($errors->has('password'))
                                                <span class="help-block">{{ $errors->first('password') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-3" for="pwd">Confirm Password:</label>
                                        <div class="col-sm-6">
                                            <input type="password" name="password_confirmation" class="form-control" id="pwd" placeholder="Enter password">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-6">
                                            <button type="submit" class="btn btn-success">Save Changes</button>
                                        </div>
                                    </div>
                            {!! Form::close() !!}
                                <!-- form ends -->
                            </div>
                            <!-- col 10 ends -->
                        </div>
                        <!-- row ends -->
                    </div>
                </div>
                <!-- Col 9 ends -->
            </div>
            <!-- Row Ends -->
        </div>
        <!-- Container Ends -->
    </div>

@endsection
