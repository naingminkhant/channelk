@extends('frontend.layouts.app')

@section('content')

    <div class="content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 v-history">
                    <!-- History -->
                    <div class="content-block">
                        <h4 class="m-t-15 f-bold"><span>{{ $notification->title }}</span></h4>
                        {!! $notification->body !!}
                    </div>
                    <!-- /History -->
                    <div class="content-block head-div head-arrow mb-40 mt-40 visible-xs">
                        <div class="head-arrow-icon">
                            <i class="cv cvicon-cv-next"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
