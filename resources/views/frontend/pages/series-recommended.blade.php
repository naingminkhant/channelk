@extends('frontend.layouts.app')

@section('body-class', 'single-video')

@section('content')

    <div class="content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <h3 class="main_title"><span>Recommended Series</span></h3>
                </div>
            </div>

            <div class="row">
                @foreach($series_recommended as $series)
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="b-video">
                            <div class="v-img">
                                <a href="{{ url('series/' . $series->id) }}"><img src="{{ $series->getSingleImageUrl() }}" alt="" width="100%" height="100%"></a>
                                <div class="time">{{ $series->duration }}</div>
                            </div>
                            <div class="v-desc">
                                <a href="{{ url('series/' . $series->id) }}">{{ $series->title }}</a>
                            </div>
                            <div class="v-views">
                                {{ $series->viewed }} {{ str_plural('view', $series->viewed) }}.
                            </div>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
    </div>

@endsection
