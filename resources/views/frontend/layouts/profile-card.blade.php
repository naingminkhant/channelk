<div class="col-md-3 sticky">
    <div class="card-bordered m-t-15">
        <img src="{{ auth()->user()->getAvatar() }}" alt="Presenter" class="user-image" />
        <p class="f-bold m-tb-15">{{ auth()->user()->name }}</p>
            <ul class="custom-list">
            <li>
                <a href="#" class="link-button">Subscribe</a>
            </li>
            <li>
                <a href="#" class="link-button">Top Up</a>
            </li>
        </ul>
    </div>
</div>
<!-- Col 3 Ends -->
