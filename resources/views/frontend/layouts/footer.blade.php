<!-- footer -->

<footer>
    <div class="container">
        <div class="padding-def">
            <div class="row">
                <div class="col-lg-2 col-sm-6 col-xs-12 text-center">
                    <div style="padding-top: 18px">
                        <img src="/images/new_logo.png" alt="Channel K" class="logo"/>
                    </div>
                </div>
                <div class="col-lg-8 col-sm-6 col-xs-12">
                    <div class="f-links">
                        <ul class="list-inline">
                            <li><a href="{{ url('support-guide') }}">Support Guide</a></li>
                            <li><a href="{{ url('faq') }}">FAQ</a></li>
                            <li><a href="https://play.google.com/store/apps/details?id=com.channelkmyanmar.channelK" id="play-store-link" target="_blank" class="foo_app_btn"><img src="/images/google_play.jpg" alt="" class="img-fluid img-rounded"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-2 col-sm-6 col-xs-12">
                    <div class="f-last-line">
                        <div class="f-icon pull-left">
                            <a href="https://www.facebook.com/channelkmyanmar/"><i class="fab fa-facebook-square"></i></a>
                            <a href="#"><i class="fab fa-linkedin-in"></i></a>
                            <a href="#"><i class="fab fa-twitter"></i></a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

            <div class="delimiter" style="margin: 23px 0 20px 0;border: solid 1px #424242;"></div>

            <div class="row">
                <div class="col-lg-4 col-sm-4 col-xs-12">
                    <div class="f-links">
                        <ul class="list-inline">
                            <li><a href="/">&copy; 2018 - {{ date("Y") }}  <span>ChannelK Myanmar</span> </a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <div class="f-links">
                        <ul class="list-inline pull-right">
                            <li><a href="{{ url('about-us') }}">About Us</a></li>
                            <li><a href="{{ url('contact-us') }}">Contact Us</a></li>
                            <li><a href="{{ url('terms-and-conditions') }}">Terms & Conditions</a></li>
                            <li><a href="{{ url('privacy-policy') }}">Privacy Policy</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- /footer -->
