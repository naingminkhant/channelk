<!-- logo, menu, search, avatar -->
<style type="text/css">
    .popover{
    min-width: 350px; /* Max Width of the popover (depending on the container!) */
    }
    .popover-title{
        font-size: 18px;
        font-weight: bold;
        text-align: center;
        letter-spacing: 1px;
        background: #fff;
        padding: 15px 14px;
    }
    .popover-content{
        background: #E9EFF2;
        border-bottom: 1px solid #fff;
    }
</style>

<div class="container-fluid">
    <div class="row">
        <div class="navbar-container single">
            <div class="container">
                <div class="row">
                    <div class="col-xs-3 visible-xs">
                        <a href="#" class="btn-menu-toggle"><i class="cv cvicon-cv-menu"></i></a>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-6">
                        <a class="navbar-brand" href="/">
                            <img src="/images/new_logo.png" alt="Channel K" class="logo" />
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-2 col-sm-6 hidden-xs">
                        <ul class="list-inline menu">
                            <li><a href="/live"><img src="/img/icon/tv-retro.png" style="width: 21px; padding-bottom: 8px"> Live</a></li>
                            <li><a href="{{ url('agenda/today') }}"> <i class="fa fa-calendar"></i> Agenda</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-8 col-xs-3">
                        <form action="/search" method="post">
                            <div class="topsearch">
                                <i class="cv cvicon-cv-cancel topsearch-close"></i>
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon2"><i class="fa fa-search"></i></span>
                                    <input type="text" class="form-control typeahead" data-provide="typeahead" id="search" placeholder="Search" aria-describedby="sizing-addon2" autocomplete="off">
                                    <div class="input-group-btn">
                                    </div><!-- /btn-group -->
                                </div>
                            </div>
                        </form>

                        <div class="col-md-12 hidden-lg hidden-md hidden-xs"><br></div>
                    </div>
                    <div class="col-lg-2 col-sm-4 hidden-xs">
                        @auth
                            <div class="m-t-20 pull-right">
                                <a  href="javascript:void(0)" data-html="true" title="<b>Notifications</b>"  data-toggle="popover" data-placement="bottom"
                                    data-content="@include('frontend.layouts.notification-content')"  style="font-size: 20px;"><i class="fa fa-bell"></i><span class="noti-badge">&nbsp;<span class="noti_count">{{ $noti_count }}</span>&nbsp;</span></a>&nbsp;&nbsp;

                                <a href="javascript:void(0)" type="button" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span>{{ auth()->user()->name }}</span><span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="{{ url('profile') }}"><i class="fa fa-user"></i> Profile</a></li>
                                    <li><a href="{{ url('setting') }}"><i class="fa fa-cog"></i> Settings</a></li>
                                    <li></li>
                                    <li>
                                        <a href="#"
                                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                            <i class="fa fa-door-open"></i> Logout
                                        </a>
                                        <form action="{{ route('logout') }}" id="logout-form" method="POST"
                                              style="display: none;">
                                            @csrf
                                        </form>
                                    </li>
                                </ul>
                            </div>
                        @else
                            <ul class="list-inline menu pull-right">
                                <li><a href="/login"> <i class="fa fa-door-open"></i> Login</a></li>
                                <li><a href="/register"><i class="fa fa-user-plus"></i> Sign up</a></li>
                            </ul>
                        @endauth

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /logo -->

<div class="mobile-menu">
    <div class="mobile-menu-head">
        <a href="#" class="mobile-menu-close"></a>
        <a class="navbar-brand" href="/">
            <img src="/images/new_logo.png" alt="Project name" class="logo" />
        </a>
        <div class="mobile-menu-btn-color"></div>
    </div>
    <div class="mobile-menu-content">
        @auth
            <div class="mobile-menu-user">
                <div class="mobile-menu-user-img">
                    <img src="{{ auth()->user()->getAvatar() }}" alt="" width="100%">
                </div>
                <p>{{ Auth::user()->name }} </p>
                <span class="caret"></span>
            </div>
        @endauth

            <a href="/live" class="btn mobile-menu-upload">
                <i class="cv cvicon-cv-play-circle"></i><span>Live</span>
            </a>

        <div class="mobile-menu-list">
            <ul>
                <li>
                    <a href="{{ url('agenda/today') }}">
                        <i class="fa fa-calendar" style="font-size: 20px; margin-left: 2px"></i> <p>Agenda</p>
                    </a>
                </li>
                @guest
                    <li>
                        <a href="/login"><i class="fa fa-door-open"></i> <p>Login</p></a>
                    </li>
                    <li>
                        <a href="/register"><i class="fa fa-user-plus"></i> <p>Sign up</p></a>
                    </li>
                @else
                    <li>

                        <a  href="javascript:void(0)" data-html="true" title="<b>Notifications</b></a>"  data-toggle="popover" data-placement="bottom"
                            data-content="@include('frontend.layouts.notification-content')"><i class="fa fa-bell"></i> <p>Notifications</p><span class="badge">&nbsp;<span class='noti_count'>{{ $noti_count }}</span></span></a>
                    </li>
                    <li>
                        <a href="{{ url('setting') }}">
                            <i class="fa fa-cog" style="font-size: 20px; margin-left: 2px"></i> <p>Setting</p>
                        </a>
                    </li>
                @endguest
            </ul>
        </div>
        @auth
            <a href="{{ route('logout') }}" class="btn mobile-menu-logout"
               onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                {{ __('Logout') }}
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        @endauth

    </div>
</div>
