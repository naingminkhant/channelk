@auth
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog vertical-align-center">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span
                                    aria-hidden="true">&times;</span><span class="sr-only">Close</span>

                        </button>
                        <h4 class="modal-title" id="myModalLabel">Notifications</h4>

                    </div>
                    <div class="modal-body noti_body">
                        <ul class="noti_list">
                            @foreach($user_notifications as $notification)
                                <li>
                                    <a href="javascript:;">
                                        <div class="noti_item">
                                            <span class="title">{{ $notification->title }}</span>
                                            {!! $notification->body !!}
                                            <div class='date-time'>
                                                <span class="fa fa-clock-o time_icon"></span>
                                                <span class="date-time_detail">{{ $notification->created_at->format('H:m A Y M d') }} </span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <!-- modal content ends -->
            </div>
            <!-- modal dialog ends -->
        </div>
        <!-- vertical-align-helper ends -->
    </div>
    <!-- Modal Ends -->
@endauth
