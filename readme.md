# Channel K

#### Installation

```
git clone https://naingminkhant@bitbucket.org/naingminkhant/channelk.git
cp .env.example .env
composer install
chmod -R 777 storage bootstrap/cache
php artisan key:generate
php artisan migrate
yarn
npm run [dev | production]
php artisan storage:link
php artisan vendor:publish --provider="Arga\ApiKeyManager\Providers\ApiManagerServiceProvider"
```

#### Commands
- php artisan make:admin
- php artisan update:admin
- php artisan apikey:make {key_name}
- php artisan apikey:activate {key_name}
- php artisan apikey:deactivate {key_name}

#### Docs
- https://github.com/pionl/laravel-chunk-upload

#### Usage
- apikey => middleware

#### Configuration
- Change APP_NAME (.env)
- Change APP_ENV (.env)
- Change API_ENV [test | ](.env)
- Change APP_DEBUG [true | false] (.env)
- Add CLOUDINARY_API_KEY (.env)
- Add CLOUDINARY_API_SECRET (.env)
- Add CLOUDINARY_CLOUD_NAME (.env)
- Add FACEBOOK_ID (.env)
- Add FACEBOOK_SECRET (.env)
- Add FACEBOOK_URL (.env)


